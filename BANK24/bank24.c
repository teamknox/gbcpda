/****************************************************************************
 *  bank24.c                                                                *
 *                                                                          *
 *    GB-DRAW  V2.03    2002/05/19                                          *
 *                                                                          *
 *    Copyright(C)  2002   TeamKNOx                                         *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank24\i_draw.h"
#include "bank24\c_draw.h"

extern void gb_draw();


/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_24[FUNC_NUM] = {
  { "GB-DRAW ", "2.03", gb_draw, i_draw, i_drawCGBPal0c1, i_drawCGBPal0c2 }
};
struct description desc_24 = { FUNC_NUM, (struct function *)func_24 };

/*==========================================================================*
 |  definitions                                                             |
 *==========================================================================*/
#define DRW_MODE_SELECT  0
#define DRW_MODE_DRAWING 1

#define DRW_TYPE_FROM    10
#define DRW_TYPE_TO      11

/* command */
#define DRW_C_DOT        0
#define DRW_C_POLYLINE   1
#define DRW_C_RECTANGLE  2
#define DRW_C_CIRCLE     3
#define DRW_C_PAINT      4

/* attribute bits */
#define DRW_A_COLOR_MASK 0x0F
#define DRW_A_BOLD       0x10
#define DRW_A_DOT        0x00  /* 0 */
#define DRW_A_POLYLINE   0x20  /* 1 */
#define DRW_A_RECTANGLE  0x40  /* 2 */
#define DRW_A_CIRCLE     0x60  /* 3 */
#define DRW_A_PAINT      0x80  /* 4 */

/* screen */
#define DRW_MAX_X        143
#define DRW_MAX_Y        99

/* drawing data */
#define DRW_MAX_LINE     256  /* depend as nvm_drw_data[] */
typedef struct line_t {
  UBYTE attr;
  struct cursor_pos pos;
} _line_t;

typedef struct p_t {
  struct line_t line[1];
} _p_t;

typedef struct drw_t {
  UBYTE cmd;
  UBYTE type;
  struct cursor_pos pos;
  UBYTE colorA;
  UBYTE colorB;
  struct p_t *p;
} _drw_t;


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank24\i_draw.c"
#include "bank24\c_draw.c"
#include "bank24\s_draw.c"
#include "bank24\b_draw.c"

unsigned char drw_head[] = {
    0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03
};
unsigned char drw_line[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07
};
unsigned char drw_tail[] = {
    0x05,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,
    0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x06
};
unsigned char drw_colorbar[] = { 9,9,9,9,9,9,9,9,9 };

unsigned char drw_btn[] = {
  0x80,0x81,0x82,0x89,0x8A,0x8B,0x92,0x93,0x94,0x9B,0x9C,0x9D,0xA4,0xA5,0xA6,
  0x83,0x84,0x85,0x8C,0x8D,0x8E,0x95,0x96,0x97,0x9E,0x9F,0xA0,0xA7,0xA8,0xA9,
  0x86,0x87,0x88,0x8F,0x90,0x91,0x98,0x99,0x9A,0xA1,0xA2,0xA3,0xAA,0xAB,0xAC
};
unsigned char drw_btn_a[]  = {
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2
};
unsigned char drw_yes_no[] = {
    0x01,0x02,0x02,0x44,0x65,0x6C,0x65,0x74,0x65,0x3F,0x02,0x02,0x02,0x03,
    0x04,0x00,0x10,0x11,0x11,0x12,0x00,0x00,0x10,0x11,0x11,0x12,0x00,0x07,
    0x04,0x00,0x15,0x16,0x16,0x17,0x00,0x00,0x15,0x16,0x16,0x17,0x00,0x07,
    0x05,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x06
};
unsigned char drw_yes_no_a[] = {
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x02,0x02,0x02,0x02,0x00,0x00,0x02,0x02,0x02,0x02,0x00,0x00,
    0x00,0x00,0x02,0x02,0x02,0x02,0x00,0x00,0x02,0x02,0x02,0x02,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};
unsigned char drw_yn_press[] = { 0x18,0x19,0x19,0x1A,0x1D,0x1E,0x1E,0x1F };

UWORD draw_palette[] = {
    c_drawCGBPal1c1, c_drawCGBPal1c2, c_drawCGBPal1c3,
    c_drawCGBPal2c1, c_drawCGBPal2c2, c_drawCGBPal2c3,
    c_drawCGBPal3c1, c_drawCGBPal3c2, c_drawCGBPal3c3
};

UBYTE drw_bitshift[] = { 0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01 };

struct line_t drw_logo[] = {
  { 0x03, { 25, 39} },  /* G */
  { 0x33, { 10, 39} },
  { 0x33, { 10, 54} },
  { 0x33, { 25, 54} },
  { 0x33, { 25, 46} },
  { 0x33, { 17, 46} },
  { 0x04, { 30, 46} },  /* B */
  { 0x34, { 40, 46} },
  { 0x34, { 45, 43} },
  { 0x34, { 40, 39} },
  { 0x34, { 30, 39} },
  { 0x34, { 30, 54} },
  { 0x34, { 40, 54} },
  { 0x34, { 45, 49} },
  { 0x34, { 40, 46} },
  { 0x05, { 50, 46} },  /* - */
  { 0x35, { 54, 46} },
  { 0x01, { 69, 39} },  /* D */
  { 0x31, { 59, 39} },
  { 0x31, { 59, 54} },
  { 0x31, { 69, 54} },
  { 0x31, { 74, 49} },
  { 0x31, { 74, 44} },
  { 0x31, { 69, 39} },
  { 0x02, { 79, 54} },  /* R */
  { 0x32, { 79, 39} },
  { 0x32, { 89, 39} },
  { 0x32, { 94, 43} },
  { 0x32, { 89, 46} },
  { 0x32, { 79, 46} },
  { 0x02, { 89, 46} },
  { 0x32, { 94, 49} },
  { 0x32, { 94, 54} },
  { 0x06, { 99, 54} },  /* A */
  { 0x36, { 99, 39} },
  { 0x36, {114, 39} },
  { 0x36, {114, 54} },
  { 0x06, { 99, 46} },
  { 0x36, {114, 46} },
  { 0x07, {119, 39} },  /* W */
  { 0x37, {119, 49} },
  { 0x37, {124, 54} },
  { 0x37, {126, 49} },
  { 0x37, {129, 54} },
  { 0x37, {134, 49} },
  { 0x37, {134, 39} }
};


/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
/*--------------------------------------------------------------------------*
 |  engine                                                                  |
 *--------------------------------------------------------------------------*/
void drw_dot_engine( UWORD x, UWORD y, UBYTE c )
{
  UWORD  dx, dy, bit;
  UBYTE  *p;
  UWORD  n, addr;
  unsigned char d[2];

  /* calc char code */
  n = (y/8)*18 + x/8 + 22;
  if( n < 128 )  p = (UBYTE *)0x9000;
  else           p = (UBYTE *)0x8000;

  /* calc address */
  dx = x % 8;
  dy = y % 8;
  addr = n*16 + dy*2;
  p += addr;

  /* check attribute for over 4 colors... */

  /* put bitmap */
  disable_interrupts();
  VBK_REG = 1;
  bit = drw_bitshift[dx];
  c &= DRW_A_COLOR_MASK; 
  get_data( d, (unsigned char *)p, 2 );
  if( c == 8 ) {
    d[0] &= ~bit;  /* plane #1 */
    d[1] &= ~bit;  /* plane #2 */
    set_data( (unsigned char *)p, d, 2 );
  } else {
    switch( c%3 ) {
      case 0:
        d[0] |= bit;   /* plane #1 */
        d[1] &= ~bit;  /* plane #2 */
        break;
      case 1:
        d[0] &= ~bit;  /* plane #1 */
        d[1] |= bit;   /* plane #2 */
        break;
      case 2:
        d[0] |= bit;   /* plane #1 */
        d[1] |= bit;   /* plane #2 */
        break;
    }
    set_data( (unsigned char *)p, d, 2 );
    d[0] = c/3 + 0x0B;
    set_bkg_tiles( x/8+1, y/8+1, 1, 1, d );
  }
  VBK_REG = 0;
  enable_interrupts();
}

UBYTE drw_get_pos( UWORD x, UWORD y )
{
  UWORD  dx, dy, bit;
  UBYTE  a, a1, a2, *p;
  UWORD  n, addr;
  unsigned char d[2];

  /* calc char code */
  n = (y/8)*18 + x/8 + 22;
  if( n < 128 )  p = (UBYTE *)0x9000;
  else           p = (UBYTE *)0x8000;

  /* calc address */
  dx = x % 8;
  dy = y % 8;
  addr = n*16 + dy*2;
  p += addr;

  /* check attribute */
  disable_interrupts();
  VBK_REG = 1;
  bit = drw_bitshift[dx];
  get_data( d, (unsigned char *)p, 2 );
  if( d[0] & bit )  a  = 1;  /* plane #1 */
  else              a  = 0;
  if( d[1] & bit )  a += 2;  /* plane #2 */
  VBK_REG = 0;
  enable_interrupts();

  return( a );
}

void drw_draw_dot( UWORD x, UWORD y, UBYTE a )
{
  drw_dot_engine( x, y, a );
  if( a & DRW_A_BOLD ) {
    if( x >   0 )  drw_dot_engine( x-1, y, a );
    if( x < 143 )  drw_dot_engine( x+1, y, a );
    if( y >   0 )  drw_dot_engine( x, y-1, a );
    if( y <  99 )  drw_dot_engine( x, y+1, a );
  }
}

void drw_line_engine( struct cursor_pos *p1, struct cursor_pos *p2, UBYTE a )
{ // Bresenham algorithm
  UWORD x, y, dx, dy, xs, ys, xe, ye, tmp;
  WORD  e, con1, con2, reverse;

  /* check 90< RAD <270 */
  if( (p1->x) > (p2->x) ) {
    xs = (UWORD)p2->x;  xe = (UWORD)p1->x;
    ys = (UWORD)p2->y;  ye = (UWORD)p1->y;
  } else {
    xs = (UWORD)p1->x;  xe = (UWORD)p2->x;
    ys = (UWORD)p1->y;  ye = (UWORD)p2->y;
  }

  /* check 270< RAD <360 */
  if( ys > ye ) {
    reverse = -1;
    ye = 2 * ys - ye;
    dx = xe - xs;
    dy = ye - ys;
  } else {
    reverse = 1;
    dx = xe - xs;
    dy = ye - ys;
  }

  /* check 45< RAD <=90 */
  if( dy > dx ) {
    if( (p1->y) > (p2->y) ) {
      xs = (UWORD)p2->x;  xe = (UWORD)p1->x;
      ys = (UWORD)p2->y;  ye = (UWORD)p1->y;
    } else {
      xs = (UWORD)p1->x;  xe = (UWORD)p2->x;
      ys = (UWORD)p1->y;  ye = (UWORD)p2->y;
    }
    if( xs > xe ) {
      reverse = -1;
      xe = 2 * xs - xe;
      dx = xe - xs;
      dy = ye - ys;
    } else {
      reverse = 1;
      dx = xe - xs;
      dy = ye - ys;
    }
    /* calc */
    e = 2 * dx - dy;
    con1 = 2 * (dx - dy);
    con2 = 2 * dx;
    x = xs;

    /* draw */
    drw_draw_dot( xs, ys, a );

    for( y=ys+1; y<=ye; y++ ) {
      if( e > 0 ) {
        x += reverse;
        e += con1;
      } else {
        e += con2;
      }
      drw_draw_dot( x, y, a );
    }
  } else {  /* 0<= RAD <=45 */
    /* calc */
    if( reverse == -1 ) {
      ye = 2 * ys - ye;
    }
    e = 2 * dy -dx;
    con1 = 2 * (dy - dx);
    con2 = 2 * dy;
    y = ys;

    /* draw */
    drw_draw_dot( xs, ys, a );
    for( x=xs+1; x<=xe; x++ ) {
      if( e > 0 ) {
        y += reverse;
        e += con1;
      } else {
        e += con2;
      }
      drw_draw_dot( x, y, a );
    }
  }
}

void drw_raster_line( struct cursor_pos *p1, struct cursor_pos *p2, UBYTE a )
{ // not supported bold
  UWORD xs, xe, x1, x2, x, y, n, addr, dx, dy;
  UBYTE xl, xr, c;
  UBYTE *p;
  unsigned char d[2], buff[1];

  if( (p1->x) > (p2->x) ) {
    xs = (UWORD)p2->x;  xe = (UWORD)p1->x;
  } else {
    xs = (UWORD)p1->x;  xe = (UWORD)p2->x;
  }
  y   = (UWORD)p1->y;
  x1  = (xs+7)/8;
  x2  = (xe+1)/8;
  dy  = (y % 8)*2;
  a  &= DRW_A_COLOR_MASK; 
  buff[0] = a/3 + 0x0B;
  if( a == 8 ) {
    d[0] = 0x00;  /* plane #1 */
    d[1] = 0x00;  /* plane #2 */
  } else {
    switch( a%3 ) {
      case 0:
        d[0] = 0xFF;  /* plane #1 */
        d[1] = 0x00;  /* plane #2 */
        break;
      case 1:
        d[0] = 0x00;  /* plane #1 */
        d[1] = 0xFF;  /* plane #2 */
        break;
      case 2:
        d[0] = 0xFF;  /* plane #1 */
        d[1] = 0xFF;  /* plane #2 */
        break;
    }
  }
  if( x2 <= x1 ) {
    for( x=xs; x<=xe; x++ ) {
      drw_draw_dot( x, y, a );
    }
    return;
  }
  for( x=xs; x<(x1*8); x++ ) {
    drw_draw_dot( x, y, a );
  }
  disable_interrupts();
  VBK_REG = 1;
  for( x=x1; x<x2; x++ ) {

    /* calc char code */
    n = (y/8)*18 + x + 22;
    if( n < 128 )  p = (UBYTE *)0x9000;
    else           p = (UBYTE *)0x8000;
    addr = n*16 + dy;
    p += addr;

    /* put bitmap */
    set_data( (unsigned char *)p, d, 2 );
    if( a != 8 )  set_bkg_tiles( x+1, y/8+1, 1, 1, buff );
  }
  VBK_REG = 0;
  enable_interrupts();
  for( x=(x2*8); x<=xe; x++ ) {
    drw_draw_dot( x, y, a );
  }
}

void drw_rectangle( struct cursor_pos *p1, struct cursor_pos *p2, UBYTE a )
{
  struct cursor_pos p;

  p.x = p1->x;  p.y = p2->y;
  drw_line_engine( p1, &p, a );
  drw_line_engine( &p, p2, a );
  p.x = p2->x;  p.y = p1->y;
  drw_line_engine( p1, &p, a );
  drw_line_engine( &p, p2, a );
}

#if 1
/* I wish to change from circle to ellipse,
   but ellipse could not draw big circle... */
void drw_circle( struct cursor_pos *p1, struct cursor_pos *p2, UBYTE a )
{ // Michener algorithm
  UWORD x0, y0, r, rx, ry, x, y;
  WORD  d;

  x0 = ((UWORD)p1->x + (UWORD)p2->x)/2;
  y0 = ((UWORD)p1->y + (UWORD)p2->y)/2;
  if( (p1->x) > (p2->x) ) {
    rx = (UWORD)p1->x - (UWORD)p2->x;
  } else {
    rx = (UWORD)p2->x - (UWORD)p1->x;
  }
  if( (p1->y) > (p2->y) ) {
    ry = (UWORD)p1->y - (UWORD)p2->y;
  } else {
    ry = (UWORD)p2->y - (UWORD)p1->y;
  }
  if( rx > ry )  r = ry/2;
  else           r = rx/2;

  /* draw */
  x = r;
  y = 0;
  d = 3 - (WORD)2*r;
  while( x >= y ) {
    drw_draw_dot( x0+x, y0+y, a );
    drw_draw_dot( x0-x, y0+y, a );
    drw_draw_dot( x0+x, y0-y, a );
    drw_draw_dot( x0-x, y0-y, a );
    drw_draw_dot( x0+y, y0+x, a );
    drw_draw_dot( x0-y, y0+x, a );
    drw_draw_dot( x0+y, y0-x, a );
    drw_draw_dot( x0-y, y0-x, a );
    if( d >= 0 ) {
      x--;
      d -= (WORD)4*x;
    }
    y++;
    d += 4*y + 2;
  }
}
#else
void drw_circle( struct cursor_pos *p1, struct cursor_pos *p2, UBYTE c )
{ // Horn algorithm (modify to ellipse)
  UWORD x0, y0, a, b, rx, ry, x, y, a2, b2, c1, c2;
  WORD  d;

  x0 = ((UWORD)p1->x + (UWORD)p2->x)/2;
  y0 = ((UWORD)p1->y + (UWORD)p2->y)/2;
  if( (p1->x) > (p2->x) ) {
    rx = (UWORD)p1->x - (UWORD)p2->x;
  } else {
    rx = (UWORD)p2->x - (UWORD)p1->x;
  }
  if( (p1->y) > (p2->y) ) {
    ry = (UWORD)p1->y - (UWORD)p2->y;
  } else {
    ry = (UWORD)p2->y - (UWORD)p1->y;
  }
  a = rx / 2;
  b = ry / 2;

  /* draw */
  x = 0;  y = b;
  a2 = a * a;   b2 = b * b;
  c1 = 4 * b2;  c2 = 8 * a2; 
  d = a2 * (1-4*b);
  while( (b2*x) <= (a2*y) ) {
    drw_draw_dot( x0+x, y0+y, c );
    drw_draw_dot( x0-x, y0+y, c );
    drw_draw_dot( x0+x, y0-y, c );
    drw_draw_dot( x0-x, y0-y, c );
    d += c1*(2*x+1);
    x++;
    if( d > 0 ) {
      y--;
      d -= c2*y;
    }
  }
  x = a;  y = 0;
  c1 = 4 * a2;  c2 = 8 * b2;
  d = b2 * (1-4*a);
  while( (a2*y) < (b2*x) ) {
    drw_draw_dot( x0+x, y0+y, c );
    drw_draw_dot( x0-x, y0+y, c );
    drw_draw_dot( x0+x, y0-y, c );
    drw_draw_dot( x0-x, y0-y, c );
    d += c1*(2*y+1);
    y++;
    if( d > 0 ) {
      x--;
      d -= c2*x;
    }
  }
}
#endif


#if 0 // another algorithm
void drw_ellipse( struct cursor_pos *p1, struct cursor_pos *p2, UBYTE c )
{
  WORD x0, y0, r, a, b, rx, ry;
  WORD x, y, f, h;

  x0 = ((WORD)p1->x + (WORD)p2->x)/2;
  y0 = ((WORD)p1->y + (WORD)p2->y)/2;
  if( (p1->x) > (p2->x) ) {
    rx = (WORD)p1->x - (WORD)p2->x;
  } else {
    rx = (WORD)p2->x - (WORD)p1->x;
  }
  if( (p1->y) > (p2->y) ) {
    ry = (WORD)p1->y - (WORD)p2->y;
  } else {
    ry = (WORD)p2->y - (WORD)p1->y;
  }
  r = 5*rx;
  a = (WORD)100;
  b = (100*rx)/ry;

  /* draw */
  x = r / 10;
  y = 0;
  f = -10*r + a + 2*b;
  h = -20*r + 2*a + b;

  while( x >= 0 ) {
    drw_draw_dot( x0+x, y0+y, c );
    drw_draw_dot( x0-x, y0+y, c );
    drw_draw_dot( x0+x, y0-y, c );
    drw_draw_dot( x0-x, y0-y, c );
    if( f < 0 ) {
      y++;
      f += 4*b*y + 2*b;
      h += 4*b*y;
    } else if( h >=0 ) {
      x--;
      f -= 4*a*x;
      h -= 4*a*x - 2*a;
    } else {
      x--;
      y++;
      f += 4*b*y - 4*a*x + 2*b;
      h += 4*b*y - 4*a*x + 2*a;
    }
  }
}
#endif


WORD drw_check_right( WORD x, WORD y, UBYTE c )
{
  while( x < DRW_MAX_X ) {
    x++;
    if( drw_get_pos((UWORD)x,(UWORD)y) != c ) {
      x--;
      return( x );
    }
  }
  return( x );
}

WORD drw_check_left( WORD x, WORD y, UBYTE c )
{
  while( x > 0 ) {
    x--;
    if( drw_get_pos((UWORD)x,(UWORD)y) != c ) {
      x++;
      return( x );
    }
  }
  return( x );
}

void drw_paint( struct cursor_pos *p, UBYTE a )
{ /* seed fill algorithm */
  UBYTE  b, c, d, f, s, e;
  WORD   i, lx, rx, uy, dy;
  struct cursor_pos p1, p2;
  struct cursor_pos buff[120];

  s = 0;
  e = 1;
  buff[s].x = p->x;
  buff[s].y = p->y;
  b = (a&DRW_A_COLOR_MASK);
  if( b == 8 )  b = 0;
  else          b = b%3 + 1;
  c = drw_get_pos( (UWORD)p->x, (UWORD)p->y );

  while( s != e ) {
    lx = (WORD)buff[s].x;
    rx = (WORD)buff[s].x;
    uy = (WORD)buff[s].y;
    dy = (WORD)buff[s].y;
    s++;  if( s == 120 )  s = 0;
    if( drw_get_pos((UWORD)lx,(UWORD)uy) != b ) {
      rx = drw_check_right( rx, uy, c );
      lx = drw_check_left( lx, uy, c );
      p1.x = (UBYTE)lx;  p1.y = (UBYTE)uy;
      p2.x = (UBYTE)rx;  p2.y = (UBYTE)uy;
      drw_raster_line( &p1, &p2, a );
      if( (joypad()&(J_START|J_SELECT)) == (J_START|J_SELECT) ) {
        keycode.brk |= J_EXIT;  /* abort */
        return;
      }

      if( uy > 0 ) {
        uy--;
        f = 0;
        for( i=lx; i<=rx; i++ ) {
          d = drw_get_pos( (UWORD)i, (UWORD)uy );
          if( f ) {
            if( d != c ) {
              f = 0;
              buff[e].x = (UBYTE)(i - 1);
              buff[e].y = (UBYTE)uy;
              e++;  if( e == 120 )  e = 0;
            }
          } else if( d == c ) {
            f = 1;
          }
        }
        if( f == 1 ) {
          buff[e].x = (UBYTE)rx;
          buff[e].y = (UBYTE)uy;
          e++;  if( e == 120 )  e = 0;
        }
      }

      if( dy < DRW_MAX_Y ) {
        dy++;
        f = 0;
        for( i=lx; i<=rx; i++ ) {
          d = drw_get_pos( (UWORD)i, (UWORD)dy );
          if( f ) {
            if( d != c ) {
              f = 0;
              buff[e].x = (UBYTE)(i - 1);
              buff[e].y = (UBYTE)dy;
              e++;  if( e == 120 )  e = 0;
            }
          } else if( d == c ) {
            f = 1;
          }
        }
        if( f == 1 ) {
          buff[e].x = (UBYTE)rx;
          buff[e].y = (UBYTE)dy;
          e++;  if( e == 120 )  e = 0;
        }
      }
    }
  }
}

void drw_clear_screen()
{
  UBYTE  i;
  unsigned char buff[16];

  /* clear screen */
  for( i=0; i<16; i++ )  buff[i] = 0x00;
  VBK_REG = 1;
  for( i=0; i<234; i++ ) {
    set_bkg_data( i+22, 1, buff );
  }
  VBK_REG = 0;
}

void drw_redraw( struct drw_t *drw )
{
  UBYTE  a;
  UWORD  i, n;
  struct cursor_pos *f, *t;

  drw_clear_screen();

  n = nvm->draw_num;
  if( n == 0 )  return;

  for( i=0; i<n; i++ ) {
    f = t;
    t = &drw->p->line[i].pos;
    a = drw->p->line[i].attr;
    switch( a & 0xE0 ) {
      case DRW_A_DOT:
        if( a & DRW_A_BOLD ) {
          drw_draw_dot( t->x, t->y, a );
        }
        break;
      case DRW_A_POLYLINE:
        drw_line_engine( f, t, a );
        break;
      case DRW_A_RECTANGLE:
        drw_rectangle( f, t, a );
        break;
      case DRW_A_CIRCLE:
        drw_circle( f, t, a );
        break;
      case DRW_A_PAINT:
        drw_paint( t, a );
        break;
    }
    if( keycode.brk & J_EXIT )  return;
  }
}

/*--------------------------------------------------------------------------*
 |  demo                                                                    |
 *--------------------------------------------------------------------------*/
void drw_demo( struct drw_t *drw )
{
  UBYTE  a, i, j;
  struct cursor_pos p1, p2, *f, *t;

  a = 0;
  for( i=1; i<=8; i++ ) {
    p1.x = 16-i*2;
    p1.y = 16-i*2;
    p2.x = 16+i*4;
    p2.y = 16+i*4;
    drw_rectangle( &p1, &p2, a+3 );
    a++; if( a > 2 )  a = 0;
  }
  for( i=1; i<=8; i++ ) {
    p1.x = 111-i*2;  // 143-32=111
    p1.y = 67-i*2;   //  99-32= 67
    p2.x = 111+i*4;
    p2.y = 67+i*4;
    drw_circle( &p1, &p2, a+6 );
    a++; if( a > 1 )  a = 0;
  }
#if 0
  for( i=1; i<13; i++ ) {
    p1.x =  89 - i*3;
    p1.y =  25 + i*2;
    p2.x = 104 + i*3;
    p2.y = 114 - i*2;
    drw_ellipse( &p1, &p2, a );
    a++; if( a > 2 )  a = 0;
  }
#endif
  for( i=1; i<=8; i++ ) {
    p1.x = 0;
    p1.y = 99;
    p2.x = 102 - i*10;
    p2.y = 56;
    drw_line_engine( &p1, &p2, a );
    a++; if( a > 2 )  a = 0;
  }

  for( j=0; j<2; j++ ) {
    for( i=0; i<46; i++ ) {
      f = t;
      t = &drw_logo[i].pos;
      a  = drw_logo[i].attr;
      if( j==1 ) {
        a &= 0xE0;
        a |= 8;
      }
      switch( a & 0xE0 ) {
        case DRW_A_POLYLINE:
          drw_line_engine( f, t, a );
          break;
      }
    }
  }
}

/*--------------------------------------------------------------------------*
 |  delete                                                                  |
 *--------------------------------------------------------------------------*/
struct cursor_info drw_yn_cursor = {  2,  1, 60, 80, 48, 0 };
UBYTE drw_check_yes_no()
{
    struct cursor_pos pos;
    UBYTE i, x, y;
    unsigned char *p, buf_f[56], buf_a[56];

    /* save attibutes */
    VBK_REG = 1;
    p = (unsigned char *)0x98C3;
    for( y=0, i=0; y<4; y++ ) {
        for( x=0; x<14; x++, i++ ) {
            buf_f[i] = 22+(x+2)+(y+5)*18;
            buf_a[i] = *p++;
        }
        p += 18;
    }
    VBK_REG = 0;

    pos.x = 1;  pos.y = 0;
    set_bkg_tiles( 3, 6, 14, 4, drw_yes_no );
    set_bkg_attribute( 3, 6, 14, 4, drw_yes_no_a );
    for( i=0; i<5; i++ ) {
        set_sprite_tile( i+20, i+65 );
        if( i < 3 ) { /* yes */
            move_sprite( i+20, i*8+52, 76 );
        } else {
            move_sprite( i+20, i*9+78, 76 );
        }
    }
    pda_syscall( PDA_SET_CURSOR_MAP, &drw_yn_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( cursor.x*6+5, 7, 4, 2, drw_yn_press );
            while( keycode.make & J_A ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( 0 );
            }
            for( i=0; i<5; i++ ) {
                move_sprite( i+20, 0, 0 );
            }
            /* restore attibutes */
            set_bkg_tiles( 3, 6, 14, 4, buf_f );
            set_bkg_attribute( 3, 6, 14, 4, buf_a );
            return( 1 - cursor.x );
        }
    }
    return( 0 );
}

/*--------------------------------------------------------------------------*
 |  drawing                                                                 |
 *--------------------------------------------------------------------------*/
void drw_draw_info( struct drw_t *drw )
{
  UBYTE x1, x10, y1, y10;

  x1  = drw->pos.x % 10;
  x10 = drw->pos.x / 10;
  y1  = drw->pos.y % 10;
  y10 = drw->pos.y / 10;

  if( x10 ) {
    if( x10 > 9 )  move_sprite(  7, 144, 2*8 );
    else           move_sprite(  7, 145, 2*8 );
    set_sprite_tile(  7, x10+39 );
    set_sprite_tile(  8, x1+54 );
  } else {
    set_sprite_tile(  7, 38 );
    set_sprite_tile(  8, x1+54 );
  }
  if( y10 ) {
    set_sprite_tile(  9, y10+39 );
    set_sprite_tile( 10, y1+39 );
  } else {
    set_sprite_tile(  9, 64 );
    set_sprite_tile( 10, y1+39 );
  }
}

struct cursor_info drw_draw_cursor = { 144, 100, 11, 25, 1, 1 };
UBYTE drw_fromto( struct drw_t *drw )
{
  UBYTE  a, flag, count;
  UWORD  n;
  unsigned char buff[4];
  struct cursor_pos p, *b;

  pda_syscall( PDA_SET_CURSOR_MAP, &drw_draw_cursor );
  pda_syscall( PDA_INIT_CURSOR, &drw->pos );
  flag = count = 0;

  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_SELECT )  return( DRW_MODE_SELECT );
    p.x = drw->pos.x = cursor.x;
    p.y = drw->pos.y = cursor.y;
    drw_draw_info( drw );
    n = nvm->draw_num;
    if( n )  b = &drw->p->line[n-1].pos;

    if( cursor.sp > 24 )  cursor.sp = 24;
    if( (keycode.flag==1) && !(keycode.code&J_START) ) {
      if( count > 0 ) {
        count--;
      } else {
        keycode.flag = 0;
        count = 4;
      }
    }
    if( drw->type == DRW_TYPE_TO ) {
      move_sprite( 6, (b->x)+12, (b->y)+21 );
      flag = 1 - flag;
      if( flag )  set_sprite_tile( 6, 33 );
      else        set_sprite_tile( 6, 34 );
    } else {
      move_sprite( 6, 0, 0 );
    }
#if 0   /* for debug */
    drw_draw_dot( p.x, p.y, drw->colorB );
#endif  /* for debug */

    if( keycode.brk & J_A ) {
      keycode.brk &= ~J_A;
      drw->p->line[n].pos.x = p.x;
      drw->p->line[n].pos.y = p.y;
      drw->p->line[n].attr  = DRW_A_DOT | drw->colorA;
//    drw->p->line[n].attr |= DRW_A_BOLD;
      if( drw->type == DRW_TYPE_FROM ) {
        if( (drw->cmd)*DRW_A_POLYLINE == DRW_A_PAINT ) {
          drw->p->line[n].attr |= DRW_A_PAINT;
          drw_paint( &p, drw->p->line[n].attr );
          if( keycode.brk & J_EXIT ) {
            keycode.brk &= ~J_EXIT;
            return( DRW_MODE_SELECT );
          }
          if( nvm->draw_num < DRW_MAX_LINE-1 )  nvm->draw_num++;
        } else {
          if( (!n) || ((b->x)!=p.x) || ((b->y)!=p.y) ) {
            if( nvm->draw_num < DRW_MAX_LINE-1 )  nvm->draw_num++;
          }
          drw->type = DRW_TYPE_TO;
        }
      } else {
        a = drw->p->line[n].attr |= (drw->cmd)*DRW_A_POLYLINE;
        switch( (drw->cmd)*DRW_A_POLYLINE ) {
          case DRW_A_POLYLINE:
            drw_line_engine( b, &p, a );
            break;
          case DRW_A_RECTANGLE:
            drw_rectangle( b, &p, a );
            break;
          case DRW_A_CIRCLE:
            drw_circle( b, &p, a );
            break;
        }
        if( nvm->draw_num < DRW_MAX_LINE-1 )  nvm->draw_num++;
        drw->type = DRW_TYPE_FROM;
      }
    }

    if( keycode.brk & J_B ) {
      keycode.brk &= ~J_B;
      if( drw->type == DRW_TYPE_FROM ) {
        if( n > 0 ) {
          nvm->draw_num--;
          drw_redraw( drw );
          drw->pos.x = b->x;
          drw->pos.y = b->y;
          if( ((drw->p->line[n-1].attr)&0xE0) != DRW_A_PAINT ) {
            drw->type = DRW_TYPE_TO;
          }
        }
      } else {
        drw->type = DRW_TYPE_FROM;
        drw->pos.x = b->x;
        drw->pos.y = b->y;
        if( ((drw->p->line[n-1].attr)&0xE0) == DRW_A_DOT ) {
          nvm->draw_num--;
        }
      }
      return( DRW_MODE_DRAWING );
    }
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  select color                                                            |
 *--------------------------------------------------------------------------*/
void drw_draw_color( struct drw_t *drw )
{
  unsigned char buff[1];
  UWORD palette[4];

  buff[0] = 10;
  set_bkg_tiles( 1, 15, 3, 3, drw_colorbar );
  set_bkg_tiles( (drw->colorA)%3+1,(drw->colorA)/3+15, 1, 1, buff );

  palette[1] = ~draw_palette[drw->colorA];
  palette[2] = draw_palette[drw->colorB];
  palette[3] = draw_palette[drw->colorA];
  set_sprite_palette( 1, 1, palette );
}

struct cursor_info drw_color_cursor = { 4, 3, 14, 140, 8, 8 };
struct cursor_info drw_cmd_cursor = { 6, 1, 30, 148, 24, 0 };
UBYTE drw_select_color( struct drw_t *drw )
{
  UBYTE  i;
  struct cursor_pos pos;
  struct button btn;

  while( !(keycode.brk & J_EXIT) ) {
    pos.x = drw->colorA % 3;
    pos.y = drw->colorA / 3;
    pda_syscall( PDA_SET_CURSOR_MAP, &drw_color_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( cursor.x < 3 ) {
      pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
      if( keycode.brk & J_EXIT )  return( 0 );
      if( keycode.brk & J_A ) {
        keycode.brk &= ~J_A;
        drw->colorA = cursor.x + cursor.y * 3;
        drw_draw_color( drw );
      }
      if( keycode.brk & J_B ) {
        keycode.brk &= ~J_B;
        drw->colorB = cursor.x + cursor.y * 3;
        drw_draw_color( drw );
      }
      if( keycode.brk & J_SELECT ) {
        keycode.brk &= ~J_SELECT;
        i = drw->colorA;
        drw->colorA = drw->colorB;
        drw->colorB = i;
        drw_draw_color( drw );
      }
      if( keycode.brk & J_START ) {
        keycode.brk &= ~J_START;
        return( DRW_MODE_DRAWING );
      }
    }

    pos.x = drw->cmd;
    pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &drw_cmd_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( cursor.x > 0 ) {
      pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
      if( keycode.brk & J_EXIT )  return( 0 );
      if( keycode.make & J_A ) {
        if( (drw->cmd) != cursor.x ) {
          btn.no = drw->cmd - 1;
          btn.on = 0; // off
          btn.data = b_draw+btn.no*128;
          pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
          btn.no = cursor.x - 1;
          btn.on = 1; // on
          btn.data = b_draw+btn.no*128+64;
          pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
        }
        if( cursor.x <  5 ) {
          while( keycode.make & J_A ) {
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return( 0 );
          }
          drw->cmd = cursor.x;
          return( DRW_MODE_DRAWING );
        } else {
          while( keycode.make & J_A ) {
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return( 0 );
          }
          if( drw_check_yes_no() ) {
            drw_clear_screen();
            nvm->draw_num = 0;
            drw->type = DRW_TYPE_FROM;
          }
          pos.x = 5;  pos.y = 0;
          pda_syscall( PDA_SET_CURSOR_MAP, &drw_cmd_cursor );
          pda_syscall( PDA_INIT_CURSOR, &pos );
          btn.on = 0; // off
          btn.data = b_draw+btn.no*128;
          pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
          btn.no = drw->cmd - 1;
          btn.on = 1; // on
          btn.data = b_draw+btn.no*128+64;
          pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
        }
      }
      if( keycode.brk & J_B ) {
        keycode.brk &= ~J_B;
        if( cursor.x == 5 ) {
          drw_clear_screen();
          drw_demo( drw );
        }
      }
      if( keycode.brk & J_START ) {
        keycode.brk &= ~J_START;
        return( DRW_MODE_DRAWING );
      }
    }
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void drw_init( struct drw_t *drw )
{
  UBYTE  a, i, n, x, y;
  UWORD  j, palette[4];
  unsigned char buff[16];
  struct button btn;

  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

  /* setup bkg data */
  set_bkg_data( 1, 10, c_draw );
  set_sprite_data( 33, 37, s_draw );
  set_sprite_tile(  6, 33 );
  set_sprite_prop(  6, 1 );

  move_sprite(  8, 152, 2*8 );
  move_sprite(  9, 156, 2*8 );
  move_sprite( 10, 160, 2*8 );
  set_sprite_prop(  7, 1 );
  set_sprite_prop(  8, 1 );
  set_sprite_prop(  9, 1 );
  set_sprite_prop( 10, 1 );

  palette[0] = ~nvm->back_color;
  for( i=0; i<3 ; i++ ) {
    palette[1] = draw_palette[i*3];
    palette[2] = draw_palette[i*3+1];
    palette[3] = draw_palette[i*3+2];
    set_bkg_palette( i+3, 1, palette );
    set_sprite_palette( i+3, 1, palette );
  }

  /* draw sheet */
  set_bkg_tiles( 0, 0, 20, 1, drw_head );
  for( i=0; i<13; i++ ) {
    set_bkg_tiles( 0, i+1, 20, 1, drw_line );
  }
  set_bkg_tiles( 0, 14, 20, 1, drw_tail );

  /* button */
  for( i=0, j=0; i<5; i++ ) {
    btn.no = i;
    if( i==0 ) {
      btn.on = 1;  /* on  */
      btn.data = b_draw + j + 64;
    } else {
      btn.on = 0;  /* off */
      btn.data = b_draw + j;
    }
    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
    j += 128;
  }
  set_bkg_tiles( 5, 15, 15, 3, drw_btn );
  set_bkg_attribute( 5, 15, 15, 3, drw_btn_a );

  /* colorbar */
  for( y=0; y<3; y++ ) {
    for( x=0; x<3; x++ ) {
      i = y*3 + x;
      if( i < 8 ) {
        i += 11;
        set_sprite_tile( i, x+35 );
        set_sprite_prop( i, y+3 );
        move_sprite( i, (x+2)*8, (y+17)*8 );
      }
    }
  }

  /* params */
  drw->colorA = 0;
  drw->colorB = 1;
  drw_draw_color( drw );
  drw->pos.x = DRW_MAX_X / 2;
  drw->pos.y = DRW_MAX_Y / 2;
  drw_draw_info( drw );
  drw->type = DRW_TYPE_FROM;
  drw->cmd = DRW_C_POLYLINE;
  drw->p = (struct p_t *)nvm_draw_data;

  /* replace real map, dot(144x104)=char(18x13)=234char */
  drw_clear_screen();
  i = 22;  /* first tile number (to 255) */
  for( y=1; y<14; y++ ) {
    for( x=1; x<19; x++ ) {
      buff[0] = 0x0B;
      set_bkg_attribute( x, y, 1, 1, buff );
      buff[0] = i++;
      set_bkg_tiles( x, y, 1, 1, buff );
    }
  }

  drw_redraw( drw );
}


/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_draw()
{
  UBYTE  mode;
  struct drw_t drw;

  drw_init( &drw );
  if( keycode.brk & J_EXIT ) {
    keycode.brk &= ~J_EXIT;
    mode = DRW_MODE_SELECT;
  } else {
    mode = DRW_MODE_DRAWING;
  }

  while( !(keycode.brk & J_EXIT) ) {

    switch( mode ) {

      case DRW_MODE_SELECT:
        mode = drw_select_color( &drw );
        break;

      case DRW_MODE_DRAWING:
        mode = drw_fromto( &drw );
        break;
    }
  }
}

/* EOF */
