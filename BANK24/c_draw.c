/*

 C_DRAW.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 9

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char c_draw[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x1F,0x00,0x1F,0x00,0x18,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0xFF,0x00,0xFF,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0xF0,0x00,0xF0,0x00,0x3E,0x0E,
  0x18,0x00,0x18,0x00,0x18,0x00,0x18,0x00,
  0x18,0x00,0x18,0x00,0x18,0x00,0x18,0x00,
  0x18,0x00,0x18,0x00,0x1F,0x00,0x1F,0x00,
  0x04,0x04,0x04,0x04,0x07,0x07,0x00,0x00,
  0x32,0x02,0x32,0x02,0xF2,0x02,0xF2,0x02,
  0x02,0x02,0x02,0x02,0xFE,0xFE,0x00,0x00,
  0x32,0x02,0x32,0x02,0x32,0x02,0x32,0x02,
  0x32,0x02,0x32,0x02,0x32,0x02,0x32,0x02,
  0x00,0x00,0x00,0x00,0xFF,0x00,0xFF,0x00,
  0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,
  0xFC,0x02,0x82,0x02,0x82,0x02,0x82,0x02,
  0x82,0x02,0x82,0x02,0x7E,0xFE,0x00,0x00,
  0xFC,0xFE,0x82,0x80,0x82,0x80,0x82,0x80,
  0x82,0x80,0x82,0x80,0x7E,0x80,0x00,0x00
};

/* End of C_DRAW.C */
