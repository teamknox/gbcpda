/****************************************************************************
 *  bank08.c                                                                *
 *                                                                          *
 *    GB-CLOCK  V2.12    2002/05/19                                         *
 *                                                                          *
 *    Copyright(C)  1999 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank08\i_clock.h"
#include "bank08\c_clock.h"
#include "bank08\s_clock.h"

extern void gb_clock( void *arg );

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank08\i_clock.c"
#include "bank08\c_clock.c"
#include "bank08\m_clock.c"
#include "bank08\s_clock.c"
#include "bank08\lcd_clock.c"
#include "bank08\map_clock.c"

unsigned char clock_attribute[] = {
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,7,7,7,7,7,7,7,7,1,1,1,1,1,1,
    1,1,1,1,1,1,7,7,7,7,7,7,7,7,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
    2,2,2,2,2,2,2,2,2,2,4,4,4,4,4,4,4,4,4,4,
    2,2,3,3,3,3,3,3,2,2,4,4,5,5,5,5,5,5,4,4,
    2,2,2,2,2,2,2,2,2,2,4,4,4,4,4,4,4,4,4,4
};

UWORD clock_palette[] = {
    c_clockCGBPal0c0, c_clockCGBPal0c1, c_clockCGBPal0c2, c_clockCGBPal0c3, 
    c_clockCGBPal1c0, c_clockCGBPal1c1, c_clockCGBPal1c2, c_clockCGBPal1c3,
    c_clockCGBPal2c0, c_clockCGBPal2c1, c_clockCGBPal2c2, c_clockCGBPal2c3,
    c_clockCGBPal3c0, c_clockCGBPal3c1, c_clockCGBPal3c2, c_clockCGBPal3c3,
    c_clockCGBPal4c0, c_clockCGBPal4c1, c_clockCGBPal4c2, c_clockCGBPal4c3,
    c_clockCGBPal5c0, c_clockCGBPal5c1, c_clockCGBPal5c2, c_clockCGBPal5c3,
    c_clockCGBPal6c0, c_clockCGBPal6c1, c_clockCGBPal6c2, c_clockCGBPal6c3,
    c_clockCGBPal7c0, c_clockCGBPal7c1, c_clockCGBPal7c2, c_clockCGBPal7c3
};
UWORD clock_s_palette[] = {
    s_clockCGBPal1c0, s_clockCGBPal1c1, s_clockCGBPal1c2, s_clockCGBPal1c3, 
    s_clockCGBPal2c0, s_clockCGBPal2c1, s_clockCGBPal2c2, s_clockCGBPal2c3,
    s_clockCGBPal3c0, s_clockCGBPal3c1, s_clockCGBPal3c2, s_clockCGBPal3c3
};
unsigned char clock_on[]  = { 0x76, 0x77 };
unsigned char clock_off[] = { 0x78, 0x79 };

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_08[FUNC_NUM] = {
    { "GB-CLOCK", "2.12", gb_clock, i_clock, i_clockCGBPal0c1, i_clockCGBPal0c2 }
};
struct description desc_08 = { FUNC_NUM, (struct function *)func_08 };

/*==========================================================================*
 |  map data                                                                |
 *==========================================================================*/
unsigned char map00[] = { 0x80,0x8D,0x9E,0x80,0xB7,0x80,0x80,0x80,0x80 };
unsigned char map01[] = { 0x80,0x8E,0x9F,0x80,0x80,0x80,0x80,0x80,0x80 };
unsigned char map02[] = { 0x82,0x8F,0x81,0xAB,0xB8,0x80,0x80,0x80,0x80 };
unsigned char map03[] = { 0x83,0x90,0xA0,0xAC,0xB9,0xC4,0x80,0x80,0x80 };
unsigned char map04[] = { 0x84,0x91,0xA1,0xAD,0xBA,0xC5,0xCE,0xD8,0xE0 };
unsigned char map05[] = { 0x85,0x92,0xA2,0xAE,0x80,0xC6,0x81,0xD9,0xE1 };
unsigned char map06[] = { 0x86,0x93,0xA3,0x80,0x80,0x80,0xCF,0xDA,0x80 };
unsigned char map07[] = { 0x87,0x94,0xA4,0xAF,0xBB,0xC7,0x80,0x80,0x80 };
unsigned char map08[] = { 0x80,0x80,0xA5,0xB0,0xBC,0xC8,0xD0,0x80,0x80 };
unsigned char map09[] = { 0x88,0x95,0xA6,0xB1,0xBD,0x81,0x81,0xDB,0x80 };
unsigned char map10[] = { 0x89,0x96,0xA7,0xB2,0xBE,0xC9,0xD1,0xDC,0x80 };
unsigned char map11[] = { 0x8A,0x97,0x81,0xB3,0xBF,0x80,0xD2,0x80,0x80 };
unsigned char map12[] = { 0x8B,0x98,0x81,0x81,0xC0,0xCA,0x80,0x80,0x80 };
unsigned char map13[] = { 0x8C,0x99,0x81,0x81,0xC1,0xCB,0xD3,0x80,0x80 };
unsigned char map14[] = { 0x80,0x9A,0x81,0xB4,0xC2,0xCC,0xD4,0xDD,0x80 };
unsigned char map15[] = { 0x80,0x9B,0xA8,0xB5,0xC3,0x80,0xD5,0xDE,0x80 };
unsigned char map16[] = { 0x80,0x9C,0xA9,0xB6,0x80,0xCD,0xD6,0x80,0x80 };
unsigned char map17[] = { 0x80,0x9D,0xAA,0x80,0x80,0x80,0xD7,0xDF,0xE2 };
unsigned char *map_map[] = {
    map00,map01,map02,map03,map04,map05,map06,map07,map08,
    map09,map10,map11,map12,map13,map14,map15,map16,map17
};
UBYTE tz_map[] = { 13,15,16,17,19,20,21,23,0,1,3,4,5,7,8,9,11,12 };


/*==========================================================================*
 |  subroutines                                                             |
 *==========================================================================*/
UBYTE get_world_map_x( UWORD map, UWORD tz )
{
    UWORD x;

    x = ((18-map)%18)*8 + ((tz+11)%24)*6;
    return( x%144 );
}

void show_world_map( UBYTE map )
{
    UBYTE x;

    for( x=0; x<18; x++ ) {
        set_bkg_tiles( x+1, 5, 1, 9, map_map[map] );
        map++; if( map>17 )  map=0;
    }

    /* set pointers */
    x = get_world_map_x( map, nvm->clock.mid_x );
    move_sprite( 10, x+11, (nvm->clock.mid_y)*6+50 );
    x = get_world_map_x( map, nvm->clock.left_x );
    move_sprite( 11, x+11, (nvm->clock.left_y)*6+50 );
    x = get_world_map_x( map, nvm->clock.right_x );
    move_sprite( 12, x+11, (nvm->clock.right_y)*6+50 );
}

void print_timer( struct time *t, struct time *o )
{
    unsigned char work[4];
    UBYTE hor;

    /* sec */
    if( o->sec != t->sec ) {
        o->sec = t->sec;
        work[0] = (t->sec/10)*2 + 0x62;
        work[1] = (t->sec%10)*2 + 0x62;
        work[2] = (t->sec/10)*2 + 0x63;
        work[3] = (t->sec%10)*2 + 0x63;
        set_bkg_tiles( 12, 1, 2, 2, work );
        if( t->sec % 2 ) {
            set_bkg_tiles(  8, 1, 1, 2, clock_on );
            set_bkg_tiles( 11, 1, 1, 2, clock_on );
        } else {
            set_bkg_tiles(  8, 1, 1, 2, clock_off );
            set_bkg_tiles( 11, 1, 1, 2, clock_off );
        }
    }

    /* min */
    if( o->min != t->min ) {
        o->min = t->min;
        work[0] = (t->min/10)*2 + 0x62;
        work[1] = (t->min%10)*2 + 0x62;
        work[2] = (t->min/10)*2 + 0x63;
        work[3] = (t->min%10)*2 + 0x63;
        set_bkg_tiles( 9, 1, 2, 2, work );
        work[0] = (t->min/10) + 0x30;
        work[1] = (t->min%10) + 0x30;
        set_bkg_tiles(  6, 16, 2, 1, work );
        set_bkg_tiles( 16, 16, 2, 1, work );
        set_sprite_tile(  8, (t->min/10)+64 );
        set_sprite_tile(  9, (t->min%10)+64 );
    }

    /* hor */
    if( o->hor != t->hor ) {
        o->hor = t->hor;
        work[0] = (t->hor/10)*2 + 0x62;
        work[1] = (t->hor%10)*2 + 0x62;
        work[2] = (t->hor/10)*2 + 0x63;
        work[3] = (t->hor%10)*2 + 0x63;
        set_bkg_tiles( 6, 1, 2, 2, work );
        hor = (24 + t->hor + nvm->clock.left_x - nvm->clock.mid_x)%24;
        work[0] = (hor/10) + 0x30;
        work[1] = (hor%10) + 0x30;
        set_bkg_tiles(  3, 16, 2, 1, work );
        hor = (24 + t->hor + nvm->clock.right_x - nvm->clock.mid_x)%24;
        work[0] = (hor/10) + 0x30;
        work[1] = (hor%10) + 0x30;
        set_bkg_tiles( 13, 16, 2, 1, work );
    }
}

void blink_pointer( UBYTE count, UBYTE sw )
{
    if( !(count%40) ) {
        if( (count/40)%2 ) {
            set_sprite_tile( 10, 76 );
            set_sprite_tile( 11, 78 );
            set_sprite_tile( 12, 80 );
        } else {
            if( sw != 1 ) {
                set_sprite_tile( 10, 75 );
            }
            if( sw != 2 ) {
                set_sprite_tile( 11, 77 );
            }
            if( sw != 3 ) {
                set_sprite_tile( 12, 79 );
            }
        }
    }
}

struct cursor_info clock_cursor0 = {  3,  2, 58, 36, 24, 0 };
UBYTE setup_clock_time( struct time *old_time )
{
    UBYTE count;
    struct cursor_pos pos;

    /* init */
    pos.x = 2; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &clock_cursor0 );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    count = 160;

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        blink_pointer( count, 0 );
        if( count ) {
            count--;
        } else {
            count = 160;
            print_timer( &time, old_time );
        }
        if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            switch( cursor.x ) {
              case 0:
                if( time.hor<23 ) time.hor++;
                else              time.hor=0;
                pda_syscall( PDA_SET_TIME, &time );
                break;
              case 1:
                if( time.min<59 ) time.min++;
                else              time.min=0;
                pda_syscall( PDA_SET_TIME, &time );
                break;
              case 2:
                time.sec = 0;
                pda_syscall( PDA_SET_TIME, &time );
                break;
            }
        }
        if( cursor.y == 1 )  break;
    }
    return( 1 );
}

struct cursor_info clock_cursor1 = {  2,  3, 14, 44, 136, 0 };
UBYTE setup_world_map( struct time *old_time )
{
    UBYTE count;
    struct cursor_pos pos;

    /* init */
    pos.x = 1; pos.y = 1;
    pda_syscall( PDA_SET_CURSOR_MAP, &clock_cursor1 );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    count = 160;

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        blink_pointer( count, 0 );
        if( count ) {
            count--;
        } else {
            count = 160;
            print_timer( &time, old_time );
        }
        if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            switch( cursor.x ) {
              case 0:
                if( (nvm->clock.map) < 17 )  (nvm->clock.map)++;
                else                         nvm->clock.map = 0;
                show_world_map( nvm->clock.map );
                break;
              case 1:
                if( nvm->clock.map )  (nvm->clock.map)--;
                else                  nvm->clock.map = 17;
                show_world_map( nvm->clock.map );
                break;
            }
        }
        if( cursor.y == 0 )  return( 0 );
        if( cursor.y == 2 )  return( 2 );
    }
    return( 1 );
}


struct cursor_info clock_cursor2 = { 24, 13, 10, 52,  6, 6 };
UBYTE show_world_clock( struct time *old_time )
{
    UBYTE count, x, y, sw;
    struct cursor_pos pos;
    struct cursor_info info_cursor2;

    /* init */
    info_cursor2.max_x = clock_cursor2.max_x;
    info_cursor2.max_y = clock_cursor2.max_y;
    info_cursor2.off_x = clock_cursor2.off_x+((18-(nvm->clock.map))%3)*2;
    info_cursor2.off_y = clock_cursor2.off_y;
    info_cursor2.add_x = clock_cursor2.add_x;
    info_cursor2.add_y = clock_cursor2.add_y;
    pos.x = get_world_map_x( nvm->clock.map, nvm->clock.mid_x )/6;
    pos.y = nvm->clock.mid_y;
    pda_syscall( PDA_SET_CURSOR_MAP, &info_cursor2 );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    count = 160; sw = 0;

    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        blink_pointer( count, sw );
        if( count ) {
            count--;
        } else {
            count = 160;
            print_timer( &time, old_time );
        }
        if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            x = (cursor.x + tz_map[nvm->clock.map])%24;
            y = cursor.y;
            switch( sw ) {
              case 1:
                nvm->clock.mid_x = x;
                nvm->clock.mid_y = y;
                x = get_world_map_x( nvm->clock.map, x );
                move_sprite( 10, x+11, y*6+50 );
                old_time->hor = 99;
                sw = 0;
                break;
              case 2:
                nvm->clock.left_x = x;
                nvm->clock.left_y = y;
                x = get_world_map_x( nvm->clock.map, x );
                move_sprite( 11, x+11, y*6+50 );
                old_time->hor = 99;
                sw = 0;
                break;
              case 3:
                nvm->clock.right_y = y;
                nvm->clock.right_x = x;
                x = get_world_map_x( nvm->clock.map, x );
                move_sprite( 12, x+11, y*6+50 );
                old_time->hor = 99;
                sw = 0;
                break;
              default:
                if( (x==(nvm->clock.mid_x)) && (y==(nvm->clock.mid_y)) ) {
                     sw = 1;
                } else if( (x==(nvm->clock.left_x)) && (y==(nvm->clock.left_y)) ) {
                     sw = 2;
                } else if( (x==(nvm->clock.right_x)) && (y==(nvm->clock.right_y)) ) {
                     sw = 3;
                }
                break;
            }
        }
        if( keycode.make & J_B ) {
            x = (24 + time.hor + cursor.x + tz_map[nvm->clock.map]
                 - nvm->clock.mid_x)%24;
            set_sprite_tile(  5, (x/10)+64 );
            set_sprite_tile(  6, (x%10)+64 );
            x = info_cursor2.off_x + cursor.x*6;
            if( x < 14   ) x = 8;
            else           x = x-6;
            if( x > 142 )  x = 142;
            move_sprite( 5, x,    cursor.y*6+69 );
            move_sprite( 6, x+ 5, cursor.y*6+69 );
            move_sprite( 7, x+ 9, cursor.y*6+69 );
            move_sprite( 8, x+13, cursor.y*6+69 );
            move_sprite( 9, x+18, cursor.y*6+69 );
         } else {
            move_sprite( 5, 0, 0 );
            move_sprite( 6, 0, 0 );
            move_sprite( 7, 0, 0 );
            move_sprite( 8, 0, 0 );
            move_sprite( 9, 0, 0 );
        }
        if( cursor.y == 0 )  break;
    }
    move_sprite( 5, 0, 0 );
    move_sprite( 6, 0, 0 );
    move_sprite( 7, 0, 0 );
    move_sprite( 8, 0, 0 );
    move_sprite( 9, 0, 0 );

    return( 1 );
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_clock( void *arg )
{
    UBYTE  i, mode;
    struct time old_time;
    UWORD  palette[4];

    /* get RTC and update year, month, day */
    pda_syscall( PDA_ENABLE_INTERRUPT, (void *)0 );
    pda_syscall( PDA_GET_TIME, &time );
    pda_syscall( PDA_UPDATE_DATE, (void *)0 );
    pda_syscall( PDA_SET_TIME, &time );

    old_time.hor = old_time.min = old_time.sec = 99;
    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    pda_syscall( PDA_INIT_PALETTE, (void *)0 );
    set_bkg_data( 24, 16, c_clock );
    set_bkg_data( 96, 32, lcd_clock );
    set_bkg_tiles( 0, 0, 20, 18, m_clock );
    if( _cpu==CGB_TYPE ) {
        for( i=0; i<8; i++ ) {
            palette[0] = clock_palette[i*4];
            palette[1] = clock_palette[i*4+1];
            palette[2] = clock_palette[i*4+2];
            palette[3] = clock_palette[i*4+3];
            switch( i ) {
              case 1:
              case 2:
              case 4:
              case 6:
                palette[3] = ~nvm->back_color;
                break;
            }
            set_bkg_palette( i, 1, palette );
        }
        set_sprite_palette( 4, 3, clock_s_palette );
        set_bkg_attribute( 0, 0, 20, 18, clock_attribute );
    }
    set_bkg_data( 128, 99, map_clock );
    set_sprite_data( 64, 17, s_clock );
    set_sprite_tile( 10, 75 );
    set_sprite_prop( 10, 4 );
    set_sprite_tile( 11, 77 );
    set_sprite_prop( 11, 5 );
    set_sprite_tile( 12, 79 );
    set_sprite_prop( 12, 6 );
    show_world_map( nvm->clock.map );
    set_sprite_tile( 7, 74 );
    set_sprite_prop( 5, 7 );
    set_sprite_prop( 6, 7 );
    set_sprite_prop( 7, 7 );
    set_sprite_prop( 8, 7 );
    set_sprite_prop( 9, 7 );
    print_timer( &time, &old_time );
    mode = 2;

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        switch( mode ) {
          case 0:
            mode = setup_clock_time( &old_time );
            break;
          case 1:
            mode = setup_world_map( &old_time );
            break;
          case 2:
            mode = show_world_clock( &old_time );
            break;
        }
    }
}

/* EOF */
