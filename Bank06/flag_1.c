/*

 FLAG_1.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 16
  Tiles                : 0 to 23

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char flag_1[] =
{
  0xFF,0xFF,0xAA,0xAA,0xD5,0xD5,0xAA,0xAA,
  0xD5,0xD5,0xAA,0xAA,0xD5,0xD5,0xAA,0xAA,
  0xFF,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,
  0x00,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,
  0xF0,0xFF,0xB0,0xB0,0x50,0x5F,0xB0,0xB0,
  0x50,0x5F,0xB0,0xB0,0x50,0x5F,0xB0,0xB0,
  0xF0,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,
  0x00,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,
  0x00,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,
  0x00,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,
  0x00,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,
  0x00,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x01,
  0x00,0x01,0x00,0x01,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3C,
  0x00,0x7E,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,0x7E,
  0x00,0x3C,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x80,0x00,0x80,
  0x00,0x80,0x00,0x80,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x3F,0x3F,0x0F,0xCF,0x03,0x33,0xC0,0xCC,
  0xF0,0xF3,0x00,0x00,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0x00,0xF0,0xF3,
  0xC0,0xCC,0x03,0x33,0x0F,0xCF,0x3F,0x3F,
  0x81,0xBD,0x81,0xBD,0x81,0xBD,0x81,0xBD,
  0x00,0x3C,0x00,0x3C,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0x3C,0x00,0x3C,
  0x81,0xBD,0x81,0xBD,0x81,0xBD,0x81,0xBD,
  0xFC,0xFC,0xF0,0xF3,0xC0,0xCC,0x03,0x33,
  0x0F,0xCF,0x00,0x00,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0x00,0x0F,0xCF,
  0x03,0x33,0xC0,0xCC,0xF0,0xF3,0xFC,0xFC,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x00,0xF8,0x00,0xF8,0x00,0xF8,0x00,0xF8,
  0x00,0xF8,0x00,0xF8,0x00,0xF9,0x00,0xF8,
  0x00,0xF9,0x00,0xF8,0x00,0xF8,0x00,0xF8,
  0x00,0xF8,0x00,0xF8,0x00,0xF8,0x00,0xF8,
  0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x2A,
  0x00,0x3E,0x00,0x9C,0x00,0xDD,0x00,0xFF,
  0x00,0xFF,0x00,0x7F,0x00,0xFF,0x00,0x08,
  0x00,0x08,0x00,0x08,0x00,0x00,0x00,0x00,
  0x00,0x1F,0x00,0x1F,0x00,0x1F,0x00,0x1F,
  0x00,0x1F,0x00,0x9F,0x00,0xDF,0x00,0x9F,
  0x00,0xDF,0x00,0x1F,0x00,0x9F,0x00,0x1F,
  0x00,0x1F,0x00,0x1F,0x00,0x1F,0x00,0x1F,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x00,0xFF,0x00,0xFF,0x00,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
};

/* End of FLAG_1.C */
