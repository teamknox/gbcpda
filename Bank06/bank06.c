/****************************************************************************
 *  bank06.c                                                                *
 *                                                                          *
 *    SYSTEMCALL FUNCTIONS (NATIONAL FLAGS)                                 *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank06\flag_1.h"
#include "bank06\flag_2.h"
#include "bank06\flag_3.h"  /* for expansion */
#include "bank06\flag_4.h"  /* for expansion */

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank06\flag_1.c"
#include "bank06\flag_2.c"
#include "bank06\flag_3.c"  /* for expansion */
#include "bank06\flag_4.c"  /* for expansion */

unsigned char flag_space[] = { 0, 0, 0, 0, 0, 0 };
unsigned char *flag_map[] = { flag_1, flag_2, flag_3, flag_4 };

UWORD flag_palette[] = {
    flag_1CGBPal0c0, flag_1CGBPal0c1, flag_1CGBPal0c2, flag_1CGBPal0c3,
    flag_1CGBPal1c0, flag_1CGBPal1c1, flag_1CGBPal1c2, flag_1CGBPal1c3,
    flag_1CGBPal2c0, flag_1CGBPal2c1, flag_1CGBPal2c2, flag_1CGBPal2c3,
    flag_1CGBPal3c0, flag_1CGBPal3c1, flag_1CGBPal3c2, flag_1CGBPal3c3,
    flag_1CGBPal4c0, flag_1CGBPal4c1, flag_1CGBPal4c2, flag_1CGBPal4c3,
    flag_1CGBPal5c0, flag_1CGBPal5c1, flag_1CGBPal5c2, flag_1CGBPal5c3,
    flag_1CGBPal6c0, flag_1CGBPal6c1, flag_1CGBPal6c2, flag_1CGBPal6c3,
    flag_1CGBPal7c0, flag_1CGBPal7c1, flag_1CGBPal7c2, flag_1CGBPal7c3,
    flag_2CGBPal0c0, flag_2CGBPal0c1, flag_2CGBPal0c2, flag_2CGBPal0c3,
    flag_2CGBPal1c0, flag_2CGBPal1c1, flag_2CGBPal1c2, flag_2CGBPal1c3,
    flag_2CGBPal2c0, flag_2CGBPal2c1, flag_2CGBPal2c2, flag_2CGBPal2c3,
    flag_2CGBPal3c0, flag_2CGBPal3c1, flag_2CGBPal3c2, flag_2CGBPal3c3,
    flag_2CGBPal4c0, flag_2CGBPal4c1, flag_2CGBPal4c2, flag_2CGBPal4c3,
    flag_2CGBPal5c0, flag_2CGBPal5c1, flag_2CGBPal5c2, flag_2CGBPal5c3,
    flag_2CGBPal6c0, flag_2CGBPal6c1, flag_2CGBPal6c2, flag_2CGBPal6c3,
    flag_2CGBPal7c0, flag_2CGBPal7c1, flag_2CGBPal7c2, flag_2CGBPal7c3,
    flag_3CGBPal0c0, flag_3CGBPal0c1, flag_3CGBPal0c2, flag_3CGBPal0c3,
    flag_3CGBPal1c0, flag_3CGBPal1c1, flag_3CGBPal1c2, flag_3CGBPal1c3,
    flag_3CGBPal2c0, flag_3CGBPal2c1, flag_3CGBPal2c2, flag_3CGBPal2c3,
    flag_3CGBPal3c0, flag_3CGBPal3c1, flag_3CGBPal3c2, flag_3CGBPal3c3,
    flag_3CGBPal4c0, flag_3CGBPal4c1, flag_3CGBPal4c2, flag_3CGBPal4c3,
    flag_3CGBPal5c0, flag_3CGBPal5c1, flag_3CGBPal5c2, flag_3CGBPal5c3,
    flag_3CGBPal6c0, flag_3CGBPal6c1, flag_3CGBPal6c2, flag_3CGBPal6c3,
    flag_3CGBPal7c0, flag_3CGBPal7c1, flag_3CGBPal7c2, flag_3CGBPal7c3,
    flag_4CGBPal0c0, flag_4CGBPal0c1, flag_4CGBPal0c2, flag_4CGBPal0c3,
    flag_4CGBPal1c0, flag_4CGBPal1c1, flag_4CGBPal1c2, flag_4CGBPal1c3,
    flag_4CGBPal2c0, flag_4CGBPal2c1, flag_4CGBPal2c2, flag_4CGBPal2c3,
    flag_4CGBPal3c0, flag_4CGBPal3c1, flag_4CGBPal3c2, flag_4CGBPal3c3,
    flag_4CGBPal4c0, flag_4CGBPal4c1, flag_4CGBPal4c2, flag_4CGBPal4c3,
    flag_4CGBPal5c0, flag_4CGBPal5c1, flag_4CGBPal5c2, flag_4CGBPal5c3,
    flag_4CGBPal6c0, flag_4CGBPal6c1, flag_4CGBPal6c2, flag_4CGBPal6c3,
    flag_4CGBPal7c0, flag_4CGBPal7c1, flag_4CGBPal7c2, flag_4CGBPal7c3
};

#include "bank06\holidays.c"

/*==========================================================================*
 |  data                                                                    |
 |--------------------------------------------------------------------------|
 | Special thanks to tobi.                                                  |
 |                                                                          |
 | Germany, France, Italy, Russia, Switzerland:                             |
 |   Last Sunday of March at 2:00 - Last Sunday of October at 3:00 (+1:00)  |
 | UK:                                                                      |
 |   Last Sunday of March at 1:00 - Last Sunday of October at 2:00 (+1:00)  |
 | Canada:                                                                  |
 |   1st Sunday of April at 2:00 - Last Sunday of October at 2:00 (+1:00)   |
 | Australia:                                                               |
 |   Last Sunday of October at 2:00 - Last Sunday of March at 2:00 (+1:00)  |
 | New Zealand:                                                             |
 |   1st Sunday of October at 2:00- 3rd Sunday of March at 2:00 (+1:00)     |
 | Finland:                                                                 |
 |   Last Sunday of March at 3:00 - Last Sunday of October at 4:00 (+1:00)  |
 | Brazil:                                                                  |
 |   3rd Sunday of October at 2:00 - 2nd Sunday of February at 2:00 (+1:00) |
 | You know, that the USA has four time zones?                              |
 |       GMT -5:00 Eastern                                                  |
 |       GMT -6:00 Central                                                  |
 |       GMT -7:00 Mountain                                                 |
 |       GMT -8:00 Pacific                                                  |
 |   They all have the same (in notation, not in time) summer time:         |
 |   1st Sunday of April at 2:00 - Last Sunday of October at 2:00 (+1:00)   |
 *==========================================================================*/
struct country_data country_data[] = {
    { "UNITED STATES ", "US ", "WASHINGTON    ", holidays_us,
      -10, {  4,  1, 10, 28 }, 0x0101, "1  " },
    { "JAPAN         ", "JPN", "TOKYO         ", holidays_jpn,
      +18, {  0,  0,  0,  0 }, 0x0101, "81 " },
    { "UNITED KINGDOM", "UK ", "LONDON        ", holidays_uk,
        0, {  3, 25, 10, 28 }, 0x0101, "44 " },
    { "GERMANY       ", "GER", "BERLIN        ", holidays_ger,
       +4, {  3, 25, 10, 28 }, 0x0101, "49 " },
    { "FRANCE        ", "FRA", "PARIS         ", holidays_fra,
       +2, {  3, 25, 10, 28 }, 0x0101, "33 " },
    { "ITALY         ", "ITA", "ROME          ", holidays_ita,
       +2, {  3, 25, 10, 28 }, 0x0101, "39 " },
    { "CANADA        ", "CAN", "OTTAWA        ", holidays_can,
       -8, {  4,  1, 10, 28 }, 0x0101, "1  " },
    { "RUSSIA        ", "RUS", "MOSCOW        ", holidays_rus,
       +6, {  3, 25, 10, 28 }, 0x0101, "7  " },
    { "AUSTRALIA     ", "AUS", "CANBERRA      ", holidays_aus,
      +20, { 10, 28,  3, 31 }, 0x0101, "61 " },
    { "NEW ZEALAND   ", "NZL", "WELLINGTON    ", holidays_nzl,
      +24, { 10,  7,  3, 17 }, 0x0101, "64 " },
    { "KORIA         ", "KOR", "SEOUL         ", holidays_kor,
      +18, {  0,  0,  0,  0 }, 0x0101, "82 " },
    { "CHINA         ", "CHN", "BEIJING       ", holidays_chn,
      +16, {  0,  0,  0,  0 }, 0x0101, "86 " },
    { "INDIA         ", "IND", "NEW DELHI     ", holidays_ind,
      +11, {  0,  0,  0,  0 }, 0x0101, "91 " },
    { "SWITZERLAND   ", "SWI", "GENEVA        ", holidays_swi,
       +2, {  3, 25, 10, 28 }, 0x0101, "41 " },
    { "FINLAND       ", "FIN", "HELSINKI      ", holidays_fin,
       +4, {  3, 25, 10, 28 }, 0x0101, "358" },
    { "BRAZIL        ", "BRZ", "BRASILIA      ", holidays_brz,
       -6, { 10, 21,  2, 10 }, 0x0101, "55 " },
};


/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void pda_country_flag( struct national_flag *arg )
{
    UBYTE i, page, no;
    UWORD index;
    unsigned char flag_tile[6];
    struct palette palette;

    page  = country_code/8;
    index = country_code%8;

    no = arg->no;
    flag_tile[0] = 0x80;
    flag_tile[1] = 0x82;
    flag_tile[2] = 0x84;
    flag_tile[3] = 0x81;
    flag_tile[4] = 0x83;
    flag_tile[5] = 0x85;
    set_bkg_data( flag_tile[0], 6, flag_map[page]+index*96 );
    set_bkg_tiles( arg->x, arg->y, 3, 2, flag_tile );
    palette.no = no;
    palette.color0 = flag_palette[country_code*4];
    palette.color1 = flag_palette[country_code*4+1];
    palette.color2 = flag_palette[country_code*4+2];
    palette.color3 = flag_palette[country_code*4+3];
    pda_syscall( PDA_SET_PALETTE, &palette );
    for( i=0; i<6; i++ ) {
        flag_tile[i] = no;
    }
    set_bkg_attribute( arg->x, arg->y, 3, 2, flag_tile );
}

void pda_country_name( struct national_flag *arg )
{
    set_bkg_tiles( arg->x, arg->y, 14, 1, country_data[country_code].name );
}

void pda_country_name3( struct national_flag *arg )
{
    set_bkg_tiles( arg->x, arg->y, 3, 1, country_data[country_code].name3 );
    if( country_data[country_code].name3[2] == ' ' ) {
        arg->no = 1;  /* 2 char name */
    } else {
        arg->no = 0;
    }
}

void pda_country_city( struct national_flag *arg )
{
    set_bkg_tiles( arg->x, arg->y, 14, 1, country_data[country_code].city );
}

void pda_country_timezone( BYTE *arg )
{
    *arg = country_data[country_code].timezone;
}

void pda_country_phone( unsigned char *arg )
{
    *arg++ = country_data[country_code].phone[0];
    *arg++ = country_data[country_code].phone[1];
    *arg++ = country_data[country_code].phone[2];
}

void pda_country_summertime( struct summertime *arg )
{
    arg->start_month = country_data[country_code].summertime.start_month;
    arg->start_day   = country_data[country_code].summertime.start_day;
    arg->end_month   = country_data[country_code].summertime.end_month;
    arg->end_day     = country_data[country_code].summertime.end_day;
}

void pda_check_holidays( struct date *arg )
{
    struct date *hd;

    hd = country_data[country_code].holidays;
    while( hd->year || hd->month || hd->day ) {
        if( ((arg->month)==hd->month) && ((arg->day)==hd->day) ) {
            if( (hd->year==0) || ((arg->year)==hd->year) ) {
                arg->day = 1;
                return;
            }
        }
        hd++;
    }
    arg->day = 0;
}

/* EOF */