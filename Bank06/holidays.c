/****************************************************************************
 *  holidays.c                                                              *
 *                                                                          *
 *    Copyright(C)  1999 - 2002    TeamKNOx                                 *
 ****************************************************************************/

struct date holidays_none[] = {
  {    0,  0,  0 }
};
struct date holidays_us[] = {  /* for United States */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  2, 12 },  /* Lincoln's Birthday */
  {    0,  7,  4 },  /* Independence Day */
  {    0, 11, 11 },  /* Veterans Day */
  {    0, 12, 24 },  /* Christmas Eve */
  {    0, 12, 25 },  /* Christmas Day */
  {    0, 12, 26 },  /* After Christmas */
  {    0, 12, 31 },  /* New Year's Eve */
  { 2001,  1, 15 },  /* Martin Luther King Day */
  { 2001,  2, 12 },  /* Washington's Birthday 
  { 2001,  5, 28 },  /* Memorial Day */
  { 2001,  9,  3 },  /* Labor Day */
  { 2001, 10,  8 },  /* Columbus Day */
  { 2001, 11, 22 },  /* Thanksgiving */
  { 2002,  1, 21 },  /* Martin Luther King Day */
  { 2002,  2, 18 },  /* Washington's Birthday 
  { 2002,  5, 27 },  /* Memorial Day */
  { 2002,  9,  2 },  /* Labor Day */
  { 2002, 10, 14 },  /* Columbus Day */
  { 2002, 11, 28 },  /* Thanksgiving */
  { 2003,  1, 20 },  /* Martin Luther King Day */
  { 2003,  2, 17 },  /* Washington's Birthday 
  { 2003,  5, 26 },  /* Memorial Day */
  { 2003,  9,  1 },  /* Labor Day */
  { 2003, 10, 13 },  /* Columbus Day */
  { 2003, 11, 27 },  /* Thanksgiving */
  { 2004,  1, 19 },  /* Martin Luther King Day */
  { 2004,  2, 16 },  /* Washington's Birthday 
  { 2004,  5, 31 },  /* Memorial Day */
  { 2004,  9,  6 },  /* Labor Day */
  { 2004, 10, 11 },  /* Columbus Day */
  { 2004, 11,  2 },  /* General Election Day */
  { 2004, 11, 25 },  /* Thanksgiving */
  { 2005,  1, 17 },  /* Martin Luther King Day */
  { 2005,  2, 21 },  /* Washington's Birthday 
  { 2005,  5, 30 },  /* Memorial Day */
  { 2005,  9,  5 },  /* Labor Day */
  { 2005, 10, 10 },  /* Columbus Day */
  { 2005, 11, 24 },  /* Thanksgiving */
  {    0,  0,  0 }
};
struct date holidays_jpn[] = {  /* for Japan */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  2, 11 },  /* National Foundation Day */
  {    0,  4, 29 },  /* Greenery Day */
  {    0,  5,  3 },  /* Constitution(Memorial) Day */
  {    0,  5,  4 },  /* National Holiday */
  {    0,  5,  5 },  /* Children's Day */
  {    0,  9, 23 },  /* Autumnal Equinox Day */
  {    0, 11,  3 },  /* (National) Culture Day */
  {    0, 11, 23 },  /* Labor Thanksgiving Day */
  {    0, 12, 23 },  /* The Emperor's Birthday */
  { 2001,  1,  8 },  /* Coming-of-Age Day */
  { 2001,  3, 20 },  /* Vernal Equinox Day */
  { 2001,  7, 20 },  /* Marine Day */
  { 2001,  9, 24 },  /* Autumnal Equinox Day */
  { 2001,  9, 15 },  /* Respect-for-the-Aged Day */
  { 2001, 10,  8 },  /* Health-(and-)Sports Day */
  { 2002,  1, 14 },  /* Coming-of-Age Day */
  { 2002,  3, 21 },  /* Vernal Equinox Day */
  { 2002,  5,  6 },  /* Children's Day */
  { 2002,  7, 20 },  /* Marine Day */
  { 2002,  9, 16 },  /* Respect-for-the-Aged Day */
  { 2002, 10, 14 },  /* Health-(and-)Sports Day */
  { 2002, 11,  4 },  /* Culture Day */
  { 2003,  1, 13 },  /* Coming-of-Age Day */
  { 2003,  3, 21 },  /* Vernal Equinox Day */
  { 2003,  7, 21 },  /* Marine Day */
  { 2003,  9, 15 },  /* Respect-for-the-Aged Day */
  { 2003, 10, 13 },  /* Health-(and-)Sports Day */
  { 2003, 11, 24 },  /* Labor Thanksgiving Day */
  { 2004,  1, 12 },  /* Coming-of-Age Day */
  { 2004,  3, 20 },  /* Vernal Equinox Day */
  { 2004,  7, 19 },  /* Marine Day */
  { 2004,  9, 20 },  /* Respect-for-the-Aged Day */
  { 2004, 10, 11 },  /* Health-(and-)Sports Day */
  { 2005,  1, 10 },  /* Coming-of-Age Day */
  { 2005,  3, 21 },  /* Vernal Equinox Day */
  { 2005,  7, 18 },  /* Marine Day */
  { 2005,  9, 19 },  /* Respect-for-the-Aged Day */
  { 2005, 10, 10 },  /* Health-(and-)Sports Day */
  {    0,  0,  0 }
};
struct date holidays_uk[] = {  /* for Great Britain */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  3, 29 },  /* Good Friday */
  {    0,  3, 31 },  /* Easter */
  {    0,  7, 12 },  /* -Battle of the Boyne */
  {    0, 12, 25 },  /* Christmas Day */
  {    0, 12, 26 },  /* Boxing Day */
  { 2002,  4,  1 },  /* Easter Monday */
  { 2002,  5,  6 },  /* Early May Bank Holiday */
  { 2002,  6,  3 },  /* Golden Jubilee Bank Holiday */
  { 2002,  6,  4 },  /* Spring Bank Holiday */
  { 2002,  8, 26 },  /* Summer Bank Holiday */
  {    0,  0,  0 }
};
struct date holidays_ger[] = {  /* for Germany */
  {    0,  1,  1 },  /* Neujahr */
  {    0,  1,  6 },  /* -Heilige Drei Koenige */
  {    0,  2, 12 },  /* -Fastnacht */
  {    0,  3, 31 },  /* Osternsonntag */
  {    0,  5,  1 },  /* Maifeiertag */
  {    0,  5,  9 },  /* Christi Himmelfahrt */
  {    0,  5, 19 },  /* Pfingstsonntag */
  {    0,  5, 30 },  /* Fronleichnam */
  {    0,  8, 15 },  /* Assumption */
  {    0, 10,  3 },  /* Tag der Deutschen Einheit */
  {    0, 11,  1 },  /* -Allerheiligen */
  {    0, 11, 20 },  /* -Day of Prayer and Repentance */
  {    0, 12, 25 },  /* Weihnachtstag */
  {    0, 12, 26 },  /* Weihnachtstag */
  { 2002,  3, 29 },  /* Karfreitag */
  { 2002,  4,  1 },  /* Osternmontag */
  { 2002,  5, 20 },  /* Pfingstmontag */
  {    0,  0,  0 }
};
struct date holidays_fra[] = {  /* for France */
  {    0,  1,  1 },  /* Jour de L'an */
  {    0,  3, 31 },  /* Paques */
  {    0,  5,  1 },  /* Fete de Travail */
  {    0,  5,  8 },  /* Anniversaire de 1945 */
  {    0,  5,  9 },  /* Ascension */
  {    0,  5, 19 },  /* Pentecote */
  {    0,  7, 14 },  /* Fete Nationale */
  {    0,  8, 15 },  /* Assomption */
  {    0, 11,  1 },  /* Toussaint */
  {    0, 11, 11 },  /* Armistice de 1918 */
  {    0, 12, 25 },  /* Noel */
  { 2002,  4,  1 },  /* Lundi de Paques */
  { 2002,  5, 20 },  /* Lundi de Pentecote */
  {    0,  0,  0 }
};
struct date holidays_ita[] = {  /* for Italy */
  {    0,  1,  1 },  /* Capo D'anno */
  {    0,  1,  6 },  /* Epifania */
  {    0,  3, 31 },  /* Pasqua */
  {    0,  4, 25 },  /* Anniversario Della Liberazione */
  {    0,  5,  1 },  /* Festa del Lavoro */
  {    0,  6, 29 },  /* -Ricorrenza Di St.Pietro e Paolo */
  {    0,  8, 15 },  /* Ferragost */
  {    0,  8, 16 },  /* -Festa in Lombardia */
  {    0, 11,  1 },  /* Tutti i Santi */
  {    0, 12,  7 },  /* -Sant' Ambrogio */
  {    0, 12,  8 },  /* Immacolata Concezione */
  {    0, 12, 25 },  /* Natale */
  {    0, 12, 26 },  /* San Stefano */
  { 2002,  4,  1 },  /* Lunedi Dell'angelo */
  {    0,  0,  0 }
};
struct date holidays_can[] = {  /* for Canada */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  2, 18 },  /* -Family Day */
  {    0,  3, 17 },  /* -Saint Patrick's Day */
  {    0,  3, 31 },  /* Easter */
  {    0,  5, 20 },  /* Victoria Day */
  {    0,  6, 24 },  /* -Quebec National Day */
  {    0,  7,  1 },  /* Memorial Day */
  {    0,  8,  5 },  /* -Civic Holiday */
  {    0,  8, 19 },  /* -Discovery Day */
  {    0,  9,  2 },  /* Labor Day */
  {    0, 10, 14 },  /* Thanksgiving Day */
  {    0, 11, 11 },  /* Remembrance Day */
  {    0, 12, 25 },  /* Christmas Day */
  {    0, 12, 26 },  /* Boxing Day */
  { 2002,  3, 29 },  /* Good Friday */
  { 2002,  4,  1 },  /* Easter Monday */
  {    0,  0,  0 }
};
struct date holidays_rus[] = {  /* for Russia */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  1,  2 },  /* New Year's Day (2nd) */
  {    0,  2, 23 },  /* Day of defenders of the fayher land */
  {    0,  3,  8 },  /* International Women's Day */
  {    0,  5,  1 },  /* Spring and Labor Day */
  {    0,  5,  2 },  /* Spring and Labor Day (2nd) */
  {    0,  5,  9 },  /* Victory Day */
  {    0,  6, 12 },  /* Independence Day */
  {    0, 11,  7 },  /* Day of Accord and Conciliation */
  {    0, 12, 12 },  /* Constitution Day */
  {    0, 12, 25 },  /* -Christmas Day */
  {    0,  0,  0 }
};
struct date holidays_aus[] = {  /* for Australia */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  4, 25 },  /* Anzac Day */
  {    0,  6, 10 },  /* Queen's Birthday */
  {    0,  8,  5 },  /* -Bank Holiday */
  {    0, 10,  7 },  /* Labour Day */
  {    0, 12, 25 },  /* Christmas Day */
  {    0, 12, 26 },  /* Boxing Day */
  { 2002,  1, 28 },  /* Australia Day */
  { 2002,  3, 29 },  /* Good Friday */
  { 2002,  3, 30 },  /* Easter Saturday */
  { 2002,  4,  1 },  /* Easter Monday */
  {    0,  0,  0 }
};
struct date holidays_nzl[] = {  /* for New Zealand */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  1,  2 },  /* Day after New Year's Day */
  {    0,  1, 28 },  /* Auckland/Northland Anniversary */
  {    0,  2,  6 },  /* Waitangi Day */
  {    0,  4, 25 },  /* Anzac Day */
  {    0,  6,  3 },  /* Queen's Birthday */
  {    0, 10, 28 },  /* Labour Day */
  {    0, 12,  1 },  /* -Westland Anniversary */
  {    0, 12, 25 },  /* Christmas Day */
  {    0, 12, 26 },  /* Boxing Day */
  { 2002,  3, 29 },  /* Good Friday */
  { 2002,  4,  1 },  /* Easter Monday */
  {    0,  0,  0 }
};
struct date holidays_kor[] = {  /* for Koria */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  2, 11 },  /* Lunar New Year's Day */
  {    0,  2, 12 },  /* Actual Holiday */
  {    0,  2, 13 },  /* Actual Holiday */
  {    0,  3,  1 },  /* Independence Movement Day */
  {    0,  4,  5 },  /* Arbor Day */
  {    0,  5,  1 },  /* Labour Day */
  {    0,  5,  5 },  /* Children's Day */
  {    0,  5, 19 },  /* Buddha's Birthday */
  {    0,  6,  6 },  /* Memorial Day */
  {    0,  7, 17 },  /* Constitution Day */
  {    0,  8, 15 },  /* Liberation Day */
  {    0,  9, 20 },  /* Harvest Moon Festival */
  {    0,  9, 21 },  /* Harvest Moon Festival (2nd) */
  {    0,  9, 22 },  /* Harvest Moon Festival (3rd) */
  {    0, 10,  3 },  /* National Founding Day */
  {    0, 12, 25 },  /* Christmas Day */
  {    0,  0,  0 }
};
struct date holidays_chn[] = {  /* for China(HongKong) */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  2, 12 },  /* Lunar New Year's Day */
  {    0,  2, 13 },  /* Actual Holiday */
  {    0,  2, 14 },  /* Actual Holiday */
  {    0,  4,  5 },  /* Ching Ming Festival */
  {    0,  5,  1 },  /* Labour Day */
  {    0,  5, 20 },  /* Buddha's Birthday */
  {    0,  6, 15 },  /* Tuen Ng Festival */
  {    0,  7,  1 },  /* HongKong Day */
  {    0,  9, 21 },  /* The day following Chinese Mid-Autum Day */
  {    0, 10,  1 },  /* National Day */
  {    0, 10, 14 },  /* Chung Yeung Festival */
  {    0, 12, 25 },  /* Christmas Day */
  {    0, 12, 26 },  /* After Christmas */
  { 2002,  3, 29 },  /* Good Friday */
  { 2002,  3, 30 },  /* The day following Good Friday */
  { 2002,  4,  1 },  /* Easter Monday */
  {    0,  0,  0 }
};
struct date holidays_ind[] = {  /* for India */
  {    0,  1, 26 },  /* Republic Day */
  {    0,  2, 23 },  /* Bakri Id */
  {    0,  3, 12 },  /* Mahashivratri */
  {    0,  3, 25 },  /* Moharrum */
  {    0,  4, 13 },  /* -Gudhi Padwa */
  {    0,  4, 25 },  /* Mahavir Jayanti */
  {    0,  5,  1 },  /* Maharashtra Day */
  {    0,  5, 25 },  /* Id-E-Milad */
  {    0,  8, 15 },  /* Independence Day */
  {    0,  9, 10 },  /* Ganesh Chaturthi */
  {    0, 10,  2 },  /* Mahatma Gandhi Jayanti */
  {    0, 10, 15 },  /* Dashra */
  {    0, 11,  4 },  /* Diwali Amavasya(Laxmi Pujan) */
  {    0, 11, 19 },  /* Guru Nanak Jayanti */
  {    0, 12,  7 },  /* Ramazan Idy */
  {    0, 12, 25 },  /* Christmas Day */
  { 2002,  3, 29 },  /* Good Friday */
  {    0,  0,  0 }
};
struct date holidays_swi[] = {  /* for Switzerland */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  1,  2 },  /* Day after New Year's Day */
  {    0,  2, 10 },  /* -Quinquagesima Sunday */
  {    0,  3, 29 },  /* Good Friday */
  {    0,  3, 31 },  /* -Easter */
  {    0,  4, 15 },  /* -Sechselauten */
  {    0,  5,  1 },  /* Labor Day */
  {    0,  5,  9 },  /* Ascension Day */
  {    0,  5, 20 },  /* Whitmonday */
  {    0,  5, 30 },  /* -Corpus Christi Day */
  {    0,  8,  1 },  /* National Day */
  {    0,  8, 15 },  /* -Assumption */
  {    0,  9,  9 },  /* Knabenschiessen */
  {    0, 11,  1 },  /* -All Saints' Day */
  {    0, 12,  8 },  /* -Immaculate Conception */
  {    0, 12, 25 },  /* Christmas Day */
  {    0, 12, 26 },  /* Boxing Day */
  { 2002,  4,  1 },  /* Easter Monday */
  { 2002,  8,  5 },  /* -First Monday in August */
  {    0,  0,  0 }
};
struct date holidays_fin[] = {  /* for Finland */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  1,  6 },  /* Epiphany */
  {    0,  2, 28 },  /* -Kalevala Day */
  {    0,  3, 31 },  /* Easter */
  {    0,  5,  1 },  /* Labor Day */
  {    0,  5,  9 },  /* Ascension Day */
  {    0,  5, 19 },  /* -Memorial Day */
  {    0, 10, 10 },  /* -Aleksis Kivi Day */
  {    0, 11,  2 },  /* -All Saints' Day */
  {    0, 11,  6 },  /* -Swedish Day */
  {    0, 12,  6 },  /* Independence Day */
  {    0, 12, 24 },  /* Christmas Eve */
  {    0, 12, 25 },  /* Christmas Day */
  {    0, 12, 26 },  /* Boxing Day */
  { 2002,  3, 24 },  /* -Plam Sunday */
  { 2002,  3, 28 },  /* -Maundy Thursday */
  { 2002,  3, 29 },  /* Good Friday */
  { 2002,  4,  1 },  /* Easter Monday */
  {    0,  0,  0 }
};
struct date holidays_brz[] = {  /* for Brazil */
  {    0,  1,  1 },  /* New Year's Day */
  {    0,  1, 20 },  /* -Rio de Janeiro Anniversary */
  {    0,  1, 25 },  /* -Sao Paulo Anniversary */
  {    0,  2, 11 },  /* Carnival (1st day) */
  {    0,  2, 12 },  /* Carnival (2nd day) */
  {    0,  2, 13 },  /* Cinzas */
  {    0,  4, 21 },  /* Tiradentes Day */
  {    0,  4, 23 },  /* -Dia do Trabalho */
  {    0,  5,  1 },  /* Labor Day */
  {    0,  5, 30 },  /* Corpus Christi Day */
  {    0,  7,  9 },  /* -Revolucia do Brasil */
  {    0,  8, 15 },  /* -Assumption */
  {    0,  9,  7 },  /* Independence Day */
  {    0, 10, 12 },  /* Nossa Senhora de Aparecida */
  {    0, 10, 21 },  /* -Dia do Comercio */
  {    0, 11,  2 },  /* Finados */
  {    0, 11, 15 },  /* Republic Day */
  {    0, 11, 20 },  /* -Dia de Zumbi */
  {    0, 12,  8 },  /* -Immaculate Conception */
  {    0, 12, 24 },  /* Christmas Eve */
  {    0, 12, 25 },  /* Christmas Day */
  { 2002,  3, 28 },  /* Endoencas */
  { 2002,  3, 29 },  /* Good Friday */
  {    0,  0,  0 }
};

/* EOF */