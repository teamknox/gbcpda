/****************************************************************************
 *  bank26.c                                                                *
 *                                                                          *
 *    GB-PDC  V2.00      2002/12/22                                         *
 *                                                                          *
 *    Copyright(C)  2002   TeamKNOx                                         *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "fixed\gb232.h"
#include "fixed\pdc.h"
#include "bank26\i_pdc.h"
#include "bank26\c_pdc.h"

extern void gb_pdc();

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_26[FUNC_NUM] = {
    { "GB-PDC  ", "2.01", gb_pdc, i_pdc, i_pdcCGBPal0c1, i_pdcCGBPal0c2 }
};
struct description desc_26 = { FUNC_NUM, (struct function *)func_26 };

/*==========================================================================*
 |  definitions                                                             |
 *==========================================================================*/
#define PDC_MODE_SELECT   0
#define PDC_MODE_LEVEL    1
#define PDC_MODE_RECEIVE  2
#define PDC_MODE_SEND     3
#define PDC_MODE_CHECK    4
#define PDC_MODE_MONITOR  5

#define PDC_NOT_CONNECT   0
#define PDC_CONNECTED     1

typedef struct pdc_t {
  UBYTE  connect;
  UBYTE  pos;
  UBYTE  level;
  UBYTE  log[36];
  UBYTE  x;
  UBYTE  y;
} _pdc_t;

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank26\i_pdc.c"
#include "bank26\b_pdc.c"
#include "bank26\c_pdc.c"
#include "bank26\r_pdc.c"
#include "bank26\n_pdc.c"

unsigned char pdc_head[] = {
  0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03
};
unsigned char pdc_line[] = {
  0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04
};
unsigned char pdc_tail[] = {
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char pdc_btn[] = {
  0x80,0x81,0x82,0x89,0x8A,0x8B,0x92,0x93,0x94,0x9B,0x9C,0x9D,0xA4,0xA5,0xA6,
  0x83,0x84,0x85,0x8C,0x8D,0x8E,0x95,0x96,0x97,0x9E,0x9F,0xA0,0xA7,0xA8,0xA9,
  0x86,0x87,0x88,0x8F,0x90,0x91,0x98,0x99,0x9A,0xA1,0xA2,0xA3,0xAA,0xAB,0xAC
};
unsigned char pdc_btn_a[]  = {
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2
};
unsigned char pdc_disconnect[] = { 0xFA,0xFB,0xFC };
unsigned char pdc_connect[]    = { 0xFD,0xFE,0xFF };
unsigned char pdc_disconnect_a[] = { 0,0,0 };
unsigned char pdc_connect_a[]    = { 4,4,4 };


unsigned char pdc_space[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char pdc_msg_title[]  = { "GB-PDC          " };
unsigned char pdc_copyright1[] = { "`2001 FACTOR R&D" }; 
unsigned char pdc_copyright2[] = { " `2002 TeamKNOx " };
unsigned char pdc_hlp_level[]    = { "Radio Level                     " };
unsigned char pdc_hlp_receive[]  = { "PickUp                          " };
unsigned char pdc_hlp_database[] = { "Database                        " };
unsigned char pdc_hlp_password[] = { "PIN Detector                    " };
unsigned char pdc_hlp_monitor[]  = { "Monitor                         " };
unsigned char *pdc_helps[] = {
  pdc_hlp_level,
  pdc_hlp_receive,
  pdc_hlp_database,
  pdc_hlp_password,
  pdc_hlp_monitor
};
UBYTE pdc_bar0[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4 };
UBYTE pdc_bar1[] = { 0,0,0,0,0,0,0,0,0,1,2,3,4,4,4,4,4 };
UBYTE pdc_bar2[] = { 0,0,0,0,0,1,2,3,4,4,4,4,4,4,4,4,4 };
UBYTE pdc_bar3[] = { 0,1,2,3,4,4,4,4,4,4,4,4,4,4,4,4,4 };

unsigned char pdc_radio_title[] = { "     RadioLevel     " };
unsigned char pdc_radio_value[]  = { " Level:   ~   db  " };
unsigned char pdc_radio_u[] = { 0xF3 };
unsigned char pdc_radio_anttn[] = { 0xF0,0xF1,0x00,0xF2 };
unsigned char pdc_radio_bar_a[] = {
  3,3,7,7,7,7,6,6,6,6,5,5,5,5,4,4,4,4,
  3,3,7,7,7,7,6,6,6,6,5,5,5,5,4,4,4,4
};
unsigned char pdc_radio_graph_head[] = {
  0xE0,0xE1,0xE1,0xE1,0xE1,0xE1,0xE1,0xE1,0xE1,0xE1,
  0xE1,0xE1,0xE1,0xE1,0xE1,0xE1,0xE1,0xE1,0xE1,0xE2
};
unsigned char pdc_radio_graph_line[] = {
  0xE3,0xB0,0xB0,0xB0,0xB0,0xB0,0xB0,0xB0,0xB0,0xB0,
  0xB0,0xB0,0xB0,0xB0,0xB0,0xB0,0xB0,0xB0,0xB0,0xE4
};
unsigned char pdc_radio_graph_tail[] = {
  0xE5,0xE6,0xE6,0xE6,0xE6,0xE6,0xE6,0xE6,0xE6,0xE6,
  0xE6,0xE6,0xE6,0xE6,0xE6,0xE6,0xE6,0xE6,0xE6,0xE7
};
unsigned char pdc_radio_graph_a[] = {
  4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
  5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
  6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
  7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7
};

unsigned char pdc_receive_title[]     = { "       PickUp       " };
unsigned char pdc_receive_wait[]       = { "    Waiting...    " };
unsigned char pdc_receive_connect[]    = { "   Connected !!   " };
unsigned char pdc_receive_disconnect[] = { "   Terminated !   " };
unsigned char pdc_receive_num[] = { 0xF8,0xF9 };

unsigned char pdc_database_title[] = { "     Data Base      " };
unsigned char pdc_database_read[]   = { "   Scanning...    " };
unsigned char pdc_database_write[]  = { "   Writing...     " };
unsigned char pdc_database_done[]   = { "     Done !!      " };

unsigned char pdc_pass_title[] = { "    PIN Detector    " };
unsigned char pdc_pass_search[] = { "   Searching...   " };
unsigned char pdc_pass_found[]  = { "     Found !!     " };
unsigned char pdc_pass_error[]  = { "     Error:       " };
unsigned char pdc_pass_num[] = {
  0xE8,0xE9,0xE9,0xEA,0xEB,0xB0,0xB2,0xEC,
  0xEB,0xB1,0xB3,0xEC,0xED,0xEE,0xEE,0xEF
};

unsigned char pdc_mon_title[] = { "      Monitor       " };
unsigned char pdc_mon_sign[]  = { "> " };
unsigned char pdc_mon_flag[]  = { ">>" };
unsigned char pdc_mon_space[] = { "  " };
unsigned char pdc_mon_head[] = {
  0xE8,0xE9,0xE9,0xE9,0xE9,0xE9,0xE9,0xE9,0xE9,0xE9,
  0xE9,0xE9,0xE9,0xE9,0xE9,0xE9,0xE9,0xE9,0xE9,0xEA
};
unsigned char pdc_mon_line[] = {
  0xEB,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0xEC
};
unsigned char pdc_mon_tail[] = {
  0xED,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
  0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEF
};
unsigned char pdc_mon_line_a[] = { 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 };

unsigned char pdc_debug_0[] = { "0" };
unsigned char pdc_debug_1[] = { "1" };
unsigned char pdc_debug_2[] = { "2" };
unsigned char pdc_debug_3[] = { "3" };
unsigned char pdc_debug_4[] = { "4" };
unsigned char pdc_debug_5[] = { "5" };
unsigned char pdc_debug_6[] = { "6" };
unsigned char pdc_debug_7[] = { "7" };

unsigned char pda_conv_tbl_1[] = {
  " .[], WAIUEOYYYT-AIUEOKKKKKSSSSSTTTTTNNNNNHHHHHMMMMMYYYRRRRRWN  "
};
unsigned char pda_conv_tbl_2[] = {
  "      O     AUOU      AIUEOAIUEOAIUEOAIUEOAIUEOAIUEOAUOAIUEOA   "
};
unsigned char pda_conv_tbl_1a[] = {
  " .[], WAIUEOYYYT-AIUEOGGGGGZZZZZDDDDDNNNNNBBBBBMMMMMYYYRRRRRWN  "
};
unsigned char pda_conv_tbl_1b[] = {
  " .[], WAIUEOYYYT-AIUEOKKKKKSSSSSTTTTTNNNNNPPPPPMMMMMYYYRRRRRWN  "
};


UWORD pdc_palette[] = {
    c_pdcCGBPal1c0, c_pdcCGBPal1c1, c_pdcCGBPal1c2, c_pdcCGBPal1c3,
    c_pdcCGBPal2c0, c_pdcCGBPal2c1, c_pdcCGBPal2c2, c_pdcCGBPal2c3,
    c_pdcCGBPal3c0, c_pdcCGBPal3c1, c_pdcCGBPal3c2, c_pdcCGBPal3c3,
    c_pdcCGBPal4c0, c_pdcCGBPal4c1, c_pdcCGBPal4c2, c_pdcCGBPal4c3,
    c_pdcCGBPal5c0, c_pdcCGBPal5c1, c_pdcCGBPal5c2, c_pdcCGBPal5c3
};

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void pdc_check_connect( struct pdc_t *pdc )
{
  UBYTE  i, j;
  unsigned char tmp[16];
  struct db_file data;
  struct search_name name;

  /* phone number */
  for( i=0; i<11; i++ )  gRcvData[i] = 0;
  send_gb232( PDC_REQUEST_NUMBER );
  if( !receive_gb232(12,1) ) {
    tmp[0] = 0xF8;    tmp[1] = 0xF9;
    for( i=0, j=2; i<11; i++, j++ ) {
      tmp[j] = gRcvData[i] & 0x0F;
      tmp[j] = tmp[j]%10 + '0';
      if( (i==2) || (i==6) ) {
        j++;  tmp[j] = '-';
      }
    }
    set_bkg_tiles( 2, 8, 15, 1, tmp );
    if( tmp[2]=='0' && tmp[4]=='0' ) {
      /* detected PDC */
      pdc->connect = PDC_CONNECTED;

      /* add to ram data base for GB-PHONE */
      for( i=0; i<18; i++ ) {
        if( i < 13 )  data.name[i] = data.phone[i] = tmp[i+2];
        else          data.name[i] = data.phone[i] = 0;
      }
      name.name = data.name;
      pda_syscall( PDA_SEARCH_PHONE_NAME, &name );
      if( name.no == 0xFFFF )  pda_syscall( PDA_SET_RAM_DATA, &data );
    }
  }

  if( pdc->connect != PDC_CONNECTED ) {
    set_bkg_tiles( 1, 15, 1, 3, pdc_disconnect );
    set_bkg_attribute( 1, 15, 1, 3, pdc_disconnect_a );
  } else {
    set_bkg_tiles( 1, 15, 1, 3, pdc_connect );
    set_bkg_attribute( 1, 15, 1, 3, pdc_connect_a );
  }
}

void pdc_disp_frame( UBYTE y, UBYTE len )
{
  set_bkg_tiles( 0, y++, 20, 1, pdc_mon_head );
  while( len-- ) {
    set_bkg_tiles( 0, y, 20, 1, pdc_mon_line );
    set_bkg_attribute( 1, y++, 18, 1, pdc_mon_line_a );
  }
  set_bkg_tiles( 0, y, 20, 1, pdc_mon_tail );
}

void pdc_disp_level( struct pdc_t *pdc )
{
  UBYTE  i, c0, c1;
  unsigned char buff[33];

  /* level */
  c0 = pdc->level;
  if( c0 )  c0--;
  for( i=0; i<=c0; i++ ) {
    buff[i]    = 0xF4;
    buff[i+16] = 0xF5;
  }
  for( ; i<16; i++ ) {
    buff[i]    = 0xF6;
    buff[i+16] = 0xF7;
  }
  set_bkg_tiles( 3, 3, 16, 2, buff );
  if( c0 / 5 )  buff[0] = c0 / 5 + '0';
  else          buff[0] = 0;
  buff[1] = (c0 % 5)*2 + '0';
  set_bkg_tiles( 9, 7, 2, 1, buff );
  if( c0 > 14 ) {
    set_bkg_tiles( 12, 7, 2, 1, pdc_space );
  } else {
    buff[1] ++;
    set_bkg_tiles( 12, 7, 2, 1, buff );
  } 

  /* log */
  for( i=0; i<18; i++ ) {
    c0 = pdc->log[i*2];
    c1 = pdc->log[i*2+1];
    buff[0] = pdc_bar0[c0]*5+pdc_bar0[c1]+0xB0;
    buff[1] = pdc_bar1[c0]*5+pdc_bar1[c1]+0xB0;
    buff[2] = pdc_bar2[c0]*5+pdc_bar2[c1]+0xB0;
    buff[3] = pdc_bar3[c0]*5+pdc_bar3[c1]+0xB0;
    set_bkg_tiles( i+1, 10, 1, 4, buff );
  }
}

void pdc_dump_log( struct pdc_t *pdc, unsigned char *p, UBYTE flag )
{
  UBYTE  x, y;
  unsigned char tmp[2];

  x = pdc->x;
  y = pdc->y;

  set_bkg_tiles( x*2+2, y+3, 2, 1, pdc_mon_space );
  if( x < 7 ) {
    x++;
  } else {
    x = 0;
    if( y < 9 )  y++;
    else         y=0;
  }
  while( (*p) != 0xFF ) {
    tmp[0] = (((*p)>>4)&0x0F) | 0xC0;
    tmp[1] = ((*p)&0x0F) | 0xD0;
    set_bkg_tiles( x*2+2, y+3, 2, 1, tmp );
    p++;
    if( x < 7 ) {
      x++;
    } else {
      x = 0;
      if( y < 9 )  y++;
      else         y=0;
    }
  }
  if( flag )  set_bkg_tiles( x*2+2, y+3, 2, 1, pdc_mon_flag );
  else        set_bkg_tiles( x*2+2, y+3, 2, 1, pdc_mon_sign );
  pdc->x = x;
  pdc->y = y;
}

void pdc_clear_level( struct pdc_t *pdc )
{
  UBYTE  i;

  for( i=0; i<36; i++ ) {
    pdc->log[i] = 0;
  }
}

void pdc_add_level( struct pdc_t *pdc )
{
  UBYTE  i;

  for( i=0; i<35; i++ ) {
    pdc->log[i] = pdc->log[i+1];
  }
  pdc->log[35] = (pdc->level);
}

void pdc_disp_4num( UBYTE a, UBYTE b, UBYTE c, UBYTE d )
{
  unsigned char tmp[4];

  /* a */
  tmp[0] = a*4+0xB0;
  tmp[1] = tmp[0]+2;
  tmp[2] = tmp[0]+1;
  tmp[3] = tmp[0]+3;
  set_bkg_tiles( 3, 9, 2, 2, tmp );

  /* b */
  tmp[0] = b*4+0xB0;
  tmp[1] = tmp[0]+2;
  tmp[2] = tmp[0]+1;
  tmp[3] = tmp[0]+3;
  set_bkg_tiles( 7, 9, 2, 2, tmp );

  /* c */
  tmp[0] = c*4+0xB0;
  tmp[1] = tmp[0]+2;
  tmp[2] = tmp[0]+1;
  tmp[3] = tmp[0]+3;
  set_bkg_tiles( 11, 9, 2, 2, tmp );

  /* d */
  tmp[0] = d*4+0xB0;
  tmp[1] = tmp[0]+2;
  tmp[2] = tmp[0]+1;
  tmp[3] = tmp[0]+3;
  set_bkg_tiles( 15, 9, 2, 2, tmp );
}

void pdc_disp_3num( UBYTE a, UBYTE b, UBYTE c )
{
  unsigned char tmp[4];

  /* a */
  tmp[0] = a*4+0xB0;
  tmp[1] = tmp[0]+2;
  tmp[2] = tmp[0]+1;
  tmp[3] = tmp[0]+3;
  set_bkg_tiles( 5, 9, 2, 2, tmp );

  /* b */
  tmp[0] = b*4+0xB0;
  tmp[1] = tmp[0]+2;
  tmp[2] = tmp[0]+1;
  tmp[3] = tmp[0]+3;
  set_bkg_tiles( 9, 9, 2, 2, tmp );

  /* c */
  tmp[0] = c*4+0xB0;
  tmp[1] = tmp[0]+2;
  tmp[2] = tmp[0]+1;
  tmp[3] = tmp[0]+3;
  set_bkg_tiles( 13, 9, 2, 2, tmp );
}

void pdc_shift_buff( UBYTE len, unsigned char *p )
{
  UBYTE  i;
  unsigned char *n;

  n = p;  n++;
  for( i=0; i<len; i++ ) {
    *p++ = *n++;
  }
  *p = 0;
} 

void pdc_filter_buff( UBYTE len, unsigned char *p )
{
  UBYTE  i;

  for( i=1; i<=len; i++ ) {
    if( ((*p)&0xC0) == 0xC0 ) {
      pdc_shift_buff( len-i, p );
    } else {
      p++;
    }
  }
}

UBYTE pdc_conv_kana2roma( unsigned char *to, unsigned char *from )
{
  UBYTE  i, len;
  unsigned char  c1, c2;

  len = 0;
  while( *from ) {
    c1 = *from;  from++;
    if( c1 < 0xA0 ) {
      *to = c1;  to++;
    } else {
      c1 = c1 - 0xA0;
      c2 = *from;
      switch( c2 ) {
        case 0xDE:  /* ba */
          from++;
          *to = pda_conv_tbl_1a[c1];
          break;
        case 0xDF:  /* pa */
          from++;
          *to = pda_conv_tbl_1b[c1];
          break;
        default:
          *to = pda_conv_tbl_1[c1];
          break;
      }
      to++; len++;
      if( pda_conv_tbl_2[c1] != ' ' ) {
        *to = pda_conv_tbl_2[c1];  to++;  len++;
      }
    }
  }
  return( len );
}

void pdc_ring()
{
  NR50_REG = 0x77;
  NR51_REG = 0x11;
  NR52_REG = 0x8F;
  NR10_REG = 0x4F;
  NR11_REG = 0x00;
  NR12_REG = 0xF4;
  NR13_REG = 0xFF;
  NR14_REG = 0x87;
}

/*--------------------------------------------------------------------------*
 |  pass check procedure (normal mode)                                      |
 *--------------------------------------------------------------------------*/
UBYTE pdc_searching( struct pdc_t *pdc )
{
  UBYTE  i, n1000, n100, n10, n1, retry;
  unsigned char tmp[4], p1000, p100, p10, p1;

  for( n1000=0; n1000<10; n1000++ ) {
    if( n1000 )  p1000 = 0x90 | n1000;
    else         p1000 = 0x9A;
    tmp[0] = n1000*4+0xB0;
    tmp[1] = tmp[0]+2;
    tmp[2] = tmp[0]+1;
    tmp[3] = tmp[0]+3;
    set_bkg_tiles( 3, 9, 2, 2, tmp );
    for( n100=0; n100<10; n100++ ) {
      if( n100 )  p100 = 0x90 | n100;
      else        p100 = 0x9A;
      tmp[0] = n100*4+0xB0;
      tmp[1] = tmp[0]+2;
      tmp[2] = tmp[0]+1;
      tmp[3] = tmp[0]+3;
      set_bkg_tiles( 7, 9, 2, 2, tmp );
      for( n10=0; n10<10; n10++ ) {
        if( n10 )  p10 = 0x90 | n10;
        else       p10 = 0x9A;
        tmp[0] = n10*4+0xB0;
        tmp[1] = tmp[0]+2;
        tmp[2] = tmp[0]+1;
        tmp[3] = tmp[0]+3;
        set_bkg_tiles( 11, 9, 2, 2, tmp );
        for( n1=0; n1<10; n1++ ) {
          if( n1 )  p1 = 0x90 | n1;
          else      p1 = 0x9A;
          tmp[0] = n1*4+0xB0;
          tmp[1] = tmp[0]+2;
          tmp[2] = tmp[0]+1;
          tmp[3] = tmp[0]+3;
          set_bkg_tiles( 15, 9, 2, 2, tmp );

          /* attack */
          retry = 1;
          while( retry ) {
            gRcvData[0] = 0;  gRcvData[1] = 0;
            send_gb232( 0xB1 );   send_gb232( 0x01 );
            send_gb232( p1000 );  send_gb232( p100 );
            send_gb232( p10 );    send_gb232( p1 );  send_gb232( 0x86 );

            /* result (0x86 or 0x87) */
            while( receive_gb232(1,1) );
            if( gRcvData[0] == 0x86 )  return(  0 );  /* match */
            if( gRcvData[0] == 0x87 )  retry = 0;

            if( joypad() )  return( 10 );  /* abort */
          }
        }
      }
    }
  }
  return( 9 );  /* unmatch */
}

/*--------------------------------------------------------------------------*
 |  radio level                                                             |
 *--------------------------------------------------------------------------*/
UBYTE pdc_radio_level( struct pdc_t *pdc )
{
  UWORD  i, j;

  /* setup bkg data */
  set_bkg_data( 0xB0, 25, r_pdc );
  set_bkg_tiles( 0, 0, 20, 1, pdc_radio_title );
  set_bkg_tiles( 0, 1, 20, 1, pdc_space );
  pdc_disp_frame( 2, 2 );
  set_bkg_attribute( 1, 3, 18, 2, pdc_radio_bar_a );
  set_bkg_tiles( 1, 3, 2, 2, pdc_radio_anttn );
  pdc_disp_frame( 6, 1 );
  set_bkg_tiles( 1, 7, 18, 1, pdc_radio_value );
  set_bkg_tiles( 17, 7, 1, 1, pdc_radio_u );
  set_bkg_tiles( 0, 9, 20, 1, pdc_radio_graph_head );
  for( i=0; i<4; i++ ) {
    set_bkg_tiles( 0, i+10, 20, 1, pdc_radio_graph_line );
  }
  set_bkg_tiles( 0, 14, 20, 1, pdc_radio_graph_tail );
  set_bkg_attribute( 1, 10, 18, 4, pdc_radio_graph_a );
  pdc_clear_level( pdc );
  pdc->level = 0;
  j = 15;

  /* loop */
  while( !joypad() ) {
    if( pdc->connect == PDC_CONNECTED ) {
      for( i=0; i<3; i++ ) {
        if( !receive_gb232(1,1) && ((gRcvData[0]&0xF0)==PDC_S_LEVEL_CMD) ) {
          pdc->level = (gRcvData[0] & PDC_S_LEVEL_MASK)+1;
          j = 0;
          break;
        }
      }
      if( i >= 3 ) {
        if( j == 15 )  pdc->level = 0;
        else           j++;
        pdc_add_level( pdc );
        pdc_disp_level( pdc );
      } else {
        pdc_add_level( pdc );
        pdc_disp_level( pdc );
        delay( 150-i*50 );
      }
    } else {
      pdc->level = rand();
      pdc->level = (pdc->level & PDC_S_LEVEL_MASK)+1;
      pdc_add_level( pdc );
      pdc_disp_level( pdc );
      delay( 250 );
    }
  }
  return( PDC_MODE_SELECT );
}

/*--------------------------------------------------------------------------*
 |  receive phone database from PDC                                         |
 *--------------------------------------------------------------------------*/
UBYTE pdc_receive_data( struct pdc_t *pdc )
{
  UBYTE  i, j;
  unsigned char tmp[16];
  struct db_file data;
  struct search_name name;

  /* init screen */
  for( i=0; i<14; i++ ) {
    set_bkg_tiles( 0, i+1, 20, 1, pdc_space );
  }
  set_bkg_tiles( 0, 0, 20, 1, pdc_receive_title );
  pdc_disp_frame( 4, 1 );
  pdc_disp_frame( 9, 1 );
  set_bkg_tiles( 2, 10, 2, 1, pdc_receive_num );
  gRcvData[0] = 0;

  /* loop */
  while( 1 ) {

    /* waiting the call */
    set_bkg_tiles( 1, 5, 18, 1, pdc_receive_wait );
    while( gRcvData[0] != 0xA5 ) {
      if( joypad() )  return( PDC_MODE_SELECT );
      receive_gb232( 1, 1 );
    }

    /* get phone number */
    if( !receive_gb232(13,1) ) {
      for( i=0, j=1; i<13; i++ ) {
        if( gRcvData[j]==0xAA ) {
          tmp[i] = 0;
        } else {
          tmp[i] = gRcvData[j++] & 0x0F;
          tmp[i] = tmp[i]%10 + '0';
        }
      }
    } else {
      for( i=0; i<13; i++ )  tmp[i] = '-';
    }

    /* connect the call */
    send_gb232( PDC_OFF_HOOK );
    set_bkg_tiles( 5, 10, 13, 1, tmp );
    pdc_ring();

//    /* reservation */
//    send_gb232( PDC_SUSPEND );

    /* waiting the disconnect */
    set_bkg_tiles( 1, 5, 18, 1, pdc_receive_connect );
    while( gRcvData[0] != 0xA6 ) {
      if( joypad() )  {
        send_gb232( PDC_ON_HOOK );
        return( PDC_MODE_SELECT );
      }
      receive_gb232( 1, 1 );
    }

    /* add to ram data base for GB-PHONE */
    if( tmp[0] != '-' ) {
      for( i=0; i<18; i++ ) {
        if( i < 13 )  data.name[i] = data.phone[i] = tmp[i];
        else          data.name[i] = data.phone[i] = 0;
      }
      name.name = data.name;
      pda_syscall( PDA_SEARCH_PHONE_NAME, &name );
      if( name.no == 0xFFFF )  pda_syscall( PDA_SET_RAM_DATA, &data );
    }

    /* on hook */
    set_bkg_tiles( 1, 5, 18, 1, pdc_receive_disconnect );
    send_gb232( PDC_ON_HOOK );
    while( gRcvData[0] != 0xAF ) {
      if( joypad() )  return( PDC_MODE_SELECT );
      receive_gb232( 1, 1 );
    }
    pdc_ring();
  }
  return( PDC_MODE_SELECT );
}

/*--------------------------------------------------------------------------*
 |  receive phone database from PDC                                         |
 *--------------------------------------------------------------------------*/
UBYTE pdc_send_data( struct pdc_t *pdc )
{
  UBYTE  j, k, x, y, retry, len, num, odd, grp;
  UWORD  i;
  unsigned char tmp[4], kana[16];
  struct db_file data;
  struct search_name name;

  /* setup bkg data */
  for( i=0; i<15; i++ ) {
    set_bkg_tiles( 0, i, 20, 1, pdc_space );
  }
  set_bkg_data( 0xB0, 40, n_pdc );
  set_bkg_tiles( 0, 0, 20, 1, pdc_database_title );
  pdc_disp_frame( 3, 1 );
  set_bkg_tiles( 1, 4, 18, 1, pdc_database_read );
  for( i=0; i<3; i++ ) {
    set_bkg_tiles( i*4+4, 8, 4, 4, pdc_pass_num );
  }

  /* from #000 to #999 */
  for( k=0; k<10; k++ ) {
    for( j=0; j<100; j++ ) {
      tmp[0] = k;
      tmp[1] = j / 10;
      tmp[2] = j % 10;
      pdc_disp_3num( tmp[0], tmp[1], tmp[2] );
      if( (pdc->connect) & PDC_CONNECTED ) {
        retry = 1;
        while( retry ) {
          for( i=0; i<120; i++ )  gRcvData[i] = 0;
          send_gb232( 0xB3 );  send_gb232( 0x02 );  send_gb232( 0x00 );
          send_gb232( k );  /* record bank */
          send_gb232( (j>>4) & 0x0F );  /* record address upper 4 bit */
          send_gb232(  j     & 0x0F );  /* record address lower 4 bit */
          receive_gb232( 80, 1 );
          if( gRcvData[0] == 0x6C )  retry = 0;
          if( gRcvData[1] == 0x06 ) {

            /* get name */
            len = (gRcvData[8]<<4) + gRcvData[9];
            for( i=0; i<(len/2); i++ ) {
              kana[i] = (gRcvData[10+i*2]<<4) + gRcvData[11+i*2];
            }
            kana[i] = 0;
            for( i=0; i<18; i++ )  data.name[i] = 0;
            pdc_conv_kana2roma( data.name, kana );

            /* get number */
            num = (gRcvData[10+len*2]<<4)+gRcvData[11+len*2]-1;
            odd = gRcvData[12+len*2];
            for( i=0; i<9; i++ ) {
              if( i < num ) {
                data.phone[i*2]   = 0x30 + gRcvData[15+(len+i)*2]%10;
                if( (i==(num-1)) && odd ) {
                  data.phone[i*2+1] = 0;
                } else {
                  data.phone[i*2+1] = 0x30 + gRcvData[14+(len+i)*2]%10;
                }
              } else {
                data.phone[i*2]   = 0;
                data.phone[i*2+1] = 0;
              }
            }

            /* get group */
            grp = ((gRcvData[14+(len+num)*2])<<4) + gRcvData[15+(len+num)*2];

            /* add to ram data base for GB-PHONE */
            if( len ) {
//            set_bkg_tiles( 1, 12, 18, 1, data.name );
//            set_bkg_tiles( 1, 13, 18, 1, data.phone );
              name.name = data.name;
              pda_syscall( PDA_SEARCH_PHONE_NAME, &name );
              if( name.no == 0xFFFF )  pda_syscall( PDA_SET_RAM_DATA, &data );
            }
            retry = 0;
          }
          if( joypad() )  return( PDC_MODE_SELECT );
        }
      } else {
        if( joypad() )  return( PDC_MODE_SELECT );
        delay( 50 );
      }
    }
  }
  set_bkg_tiles( 1, 4, 18, 1, pdc_database_done );

  while( !joypad() );
  while( joypad() );

  return( PDC_MODE_SELECT );
}

/*--------------------------------------------------------------------------*
 |  password checker                                                        |
 *--------------------------------------------------------------------------*/
UBYTE pdc_password_checker( struct pdc_t *pdc )
{
  UWORD  i, j;
  UBYTE  err;
  unsigned char tmp[4];

  /* setup bkg data */
  for( i=0; i<15; i++ ) {
    set_bkg_tiles( 0, i, 20, 1, pdc_space );
  }
  set_bkg_data( 0xB0, 40, n_pdc );
  set_bkg_tiles( 0, 0, 20, 1, pdc_pass_title );
  pdc_disp_frame( 3, 1 );
  set_bkg_tiles( 1, 4, 18, 1, pdc_pass_search );
  for( i=0; i<4; i++ ) {
    set_bkg_tiles( i*4+2, 8, 4, 4, pdc_pass_num );
  }

  if( (pdc->connect) & PDC_CONNECTED ) {
    err = pdc_searching( pdc );  /* normal mode */
    if( err ) {
      if( err < 10 ) {
        set_bkg_tiles( 1, 4, 18, 1, pdc_pass_error );
        tmp[0] = err + '0';
        set_bkg_tiles( 13, 4, 1, 1, tmp );
        while( !joypad() );
      }
      while( joypad() );
      return( PDC_MODE_SELECT );
    }
  } else {
    j = rand();
    j = j % 10000;
    for( i=0; i<j; i++ ) {
      tmp[0] = (i/1000)%10;
      tmp[1] = (i/100)%10;
      tmp[2] = (i/10)%10;
      tmp[3] = i%10;
      pdc_disp_4num( tmp[0], tmp[1], tmp[2], tmp[3] );
      if( joypad() ) {
        while( joypad() );
        return( PDC_MODE_SELECT );
      }
    }
  }
  set_bkg_tiles( 1, 4, 18, 1, pdc_pass_found );

  while( !joypad() );
  while( joypad() );
  return( PDC_MODE_SELECT );
}

/*--------------------------------------------------------------------------*
 |  PDC monitor                                                             |
 *--------------------------------------------------------------------------*/
UBYTE pdc_monitor( struct pdc_t *pdc )
{
  UBYTE  i, j, k, flag;
  unsigned char tmp[64];

  /* init screen */
  set_bkg_tiles( 0, 0, 20, 1, pdc_mon_title );
  set_bkg_tiles( 0, 1, 20, 1, pdc_space );
  pdc_disp_frame( 2, 10 );
  set_bkg_tiles( 0, 14, 20, 1, pdc_space );
  set_bkg_tiles( 2, 3, 2, 1, pdc_mon_sign );
  pdc->x = pdc->y = 0;
  i = 0;  j = 0;  k = 0; flag = 0;

  /* monitoring loop */
  while( !(k&(J_START|J_SELECT|J_UP|J_DOWN|J_LEFT|J_RIGHT)) ) {
    if( !receive_gb232(1,1) ) {
      if( ((gRcvData[0]&0xF0)!=PDC_S_LEVEL_CMD) || flag ) {
        tmp[i] = gRcvData[0];
        i++;  j = 0;
        if( i > 62 ) {  /* buffer full */
          tmp[i] = 0xFF;  /* EOR */
          pdc_dump_log( pdc, tmp, flag );
          i = 0;
        }
      }
    } else if( i ) {
      if( j > 2 ) { // 4
        tmp[i] = 0xFF;  /* EOR */
        pdc_dump_log( pdc, tmp, flag );
        i = 0;
      } else {
        j++;
      }
    }
    if( k & J_B ) {
      pdc_disp_frame( 2, 10 );
      pdc->x = pdc->y = 0;
      if( flag )  set_bkg_tiles((pdc->x)*2+2,(pdc->y)+3,2,1,pdc_mon_flag);
      else        set_bkg_tiles((pdc->x)*2+2,(pdc->y)+3,2,1,pdc_mon_sign);
      while( joypad() & J_B );
    } else if( k & J_A ) {
      flag = 1 - flag;
      if( flag )  set_bkg_tiles((pdc->x)*2+2,(pdc->y)+3,2,1,pdc_mon_flag);
      else        set_bkg_tiles((pdc->x)*2+2,(pdc->y)+3,2,1,pdc_mon_sign);
      while( joypad() & J_A );
    }
    k = joypad();
  }
  return( PDC_MODE_SELECT );
}

/*--------------------------------------------------------------------------*
 |  select menu                                                             |
 *--------------------------------------------------------------------------*/
struct cursor_info pdc_select_cursor = { 5, 1, 38, 148, 24, 0 };
UBYTE pdc_select_menu( struct pdc_t *pdc )
{
  UBYTE  i;
  UWORD  j;
  struct cursor_pos pos;
  struct button btn;

  /* frame */
  set_bkg_tiles( 0, 0, 20, 1, pdc_head );
  for( i=0; i<13; i++ ) {
    set_bkg_tiles( 0, i+1, 20, 1, pdc_line );
    set_bkg_attribute( 0, i+1, 20, 1, pdc_space );
  }
  set_bkg_tiles( 0, 14, 20, 1, pdc_tail );
  set_bkg_data( 0xC0, 64, c_pdc );

  /* button */
  for( i=0, j=0; i<5; i++ ) {
    btn.no = i;
    btn.on = 0;  /* off */
    btn.data = b_pdc + j;
    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
    j += 128;
  }
  set_bkg_tiles( 3, 15, 15, 3, pdc_btn );
  set_bkg_attribute( 3, 15, 15, 3, pdc_btn_a );
  set_bkg_tiles( 2, 2, 16, 1, pdc_msg_title );
  set_bkg_tiles( 2, 11, 16, 1, pdc_copyright1 );
  set_bkg_tiles( 2, 12, 16, 1, pdc_copyright2 );
  pdc_check_connect( pdc );

  /* loop */
  pos.x = pdc->pos;
  pos.y = 0;
  set_bkg_tiles( 2, 4, 16, 2, pdc_helps[pos.x] );
  pda_syscall( PDA_SET_CURSOR_MAP, &pdc_select_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( cursor.x != pos.x ) {
      pos.x = cursor.x;
      set_bkg_tiles( 2, 4, 16, 2, pdc_helps[pos.x] );
    }
    if( keycode.make & J_A ) {
      btn.no = cursor.x;
      btn.on = 1; // on
      btn.data = b_pdc+btn.no*128+64;
      pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 0 );
      }
      pdc->pos = cursor.x;
      return( PDC_MODE_LEVEL + cursor.x );
    }
    if( keycode.brk & J_B ) {
      keycode.brk &= ~J_B;
      if( (cursor.x==3) && ((pdc->connect)==PDC_CONNECTED) ) {
        pdc->pos = cursor.x;
        pdc->connect |= 0x80;
        return( PDC_MODE_LEVEL + cursor.x );
      }
    }
  }
  return( PDC_MODE_SELECT );
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void pdc_init( struct pdc_t *pdc )
{
  UBYTE  i;
  UWORD  palette[4];

  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

  /* clear screen */
  for( i=0; i<5 ; i++ ) {
    palette[0] = pdc_palette[i*4];
    palette[1] = pdc_palette[i*4+1];
    palette[2] = pdc_palette[i*4+2];
    palette[3] = pdc_palette[i*4+3];
    set_bkg_palette( i+3, 1, palette );
  }

  set_gb232( SPEED_600 | DATA_8 | STOP_1 | PARITY_EVEN, ADJUST_0P );
  init_gb232();
  send_gb232( PDC_SERVICE_START );  /* make PDC service connection */

  pdc->connect = 0;
  pdc->pos     = 0;
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_pdc()
{
  UBYTE  mode;
  struct pdc_t pdc;

  pdc_init( &pdc );

  mode = PDC_MODE_SELECT;
  while( !(keycode.brk & J_EXIT) ) {
    switch( mode ) {
      case PDC_MODE_SELECT:
        mode = pdc_select_menu( &pdc );
        break;
      case PDC_MODE_LEVEL:
        mode = pdc_radio_level( &pdc );
        break;
      case PDC_MODE_RECEIVE:
        mode = pdc_receive_data( &pdc );
        break;
      case PDC_MODE_SEND:
        mode = pdc_send_data( &pdc );
        break;
      case PDC_MODE_CHECK:
        mode = pdc_password_checker( &pdc );
        break;
      default:
        mode = pdc_monitor( &pdc );
        break;
    }
  }
}

/* EOF */
