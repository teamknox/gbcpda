/****************************************************************************
 *  bank03.c                                                                *
 *                                                                          *
 *    SYSTEMCALL FUNCTIONS                                                  *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "fixed\gb232.h"
#include "fixed\pdc.h"
#include "bank03\bank03.h"

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
unsigned char space[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
UBYTE month_len[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

/* 109 Japanese keyboard data */
UBYTE key_table[] = {
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  /* 0x00 */
  0x00,0x00,0x00,0x00,0x00,0x09,0x00,0x00,
  0x00,0x00,0x0F,0x00,0x00,0x71,0x31,0x00,  /* 0x10 */
  0x00,0x00,0x7A,0x73,0x61,0x77,0x32,0x00,
  0x00,0x63,0x78,0x64,0x65,0x34,0x33,0x00,  /* 0x20 */
  0x00,0x20,0x76,0x66,0x74,0x72,0x35,0x00,
  0x00,0x6E,0x62,0x68,0x67,0x79,0x36,0x00,  /* 0x30 */
  0x00,0x00,0x6D,0x6A,0x75,0x37,0x38,0x00,
  0x00,0x2C,0x6B,0x69,0x6F,0x30,0x39,0x00,  /* 0x40 */
  0x00,0x2E,0x2F,0x6C,0x3B,0x70,0x2D,0x00,
  0x00,0x5C,0x3A,0x00,0x40,0x5E,0x00,0x00,  /* 0x50 */
  0x00,0x0F,0x0D,0x5B,0x00,0x5D,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,  /* 0x60 */
  0x00,0x31,0x5C,0x34,0x37,0x00,0x00,0x00,
  0x30,0x2E,0x32,0x35,0x36,0x38,0x00,0x00,  /* 0x70 */
  0x00,0x2B,0x33,0x2D,0x2A,0x39,0x00,0x00
};

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/

/*--------------------------------------------------------------------------*
 |  no operation                                                            |
 *--------------------------------------------------------------------------*/
void pda_nop( void *arg )
{
}

/*--------------------------------------------------------------------------*
 |  wait seconds                                                            |
 *--------------------------------------------------------------------------*/
void pda_wait( UBYTE *arg )
{
    UBYTE sec, i, j;

    sec = *arg;
    if( sec==0 ) return;
    for( i=0; i<sec; i++ ) {
        for( j=0; j<10; j++ ) {
            delay( 150 );
        }
    }
}

/*--------------------------------------------------------------------------*
 |  initialize nvm data                                                     |
 *--------------------------------------------------------------------------*/
void pda_init_nvm( void *arg )
{
    UWORD i;
    UBYTE *p;

    /* init system environment */
    nvm->build_id   = BUILD_ID;
    nvm->date.year  = BUILD_YEAR;
    nvm->date.month = BUILD_MONTH;
    nvm->date.day   = BUILD_DAY;
    nvm->week_day   = 0;  /* Sun */
    nvm->fore_color = (UWORD)CLR_RGB(23,0,0);
//  nvm->back_color = ~(UWORD)CLR_RGB( 0,23,23); /* 4.0 */
//  nvm->back_color = ~(UWORD)CLR_RGB(18,15,31); /* 4.1 */
//  nvm->back_color = ~(UWORD)CLR_RGB(31,15,15); /* 4.3 */
    nvm->back_color = ~(UWORD)CLR_RGB(15,31,15); /* 5.0 */

    /* reset apps icon mapping */
    for( i=0; i<MAX_APPS; i++ ) {
        nvm->apps[i] = i;
    }

    /* clear pasword */
    for( i=0; i<8; i++ ) {
        nvm->password[i] = 0;
    }

    /* set data base flag (phone & memo) */
    p = nvm_phone_flag;
    for( i=0; i<(64+4); i++ ) {
        *p++ = 0x00;
    }

    /* clear phone data base pointer */
    nvm->data_base[0].name[0] = '!';

    /* clear memo data base (10 pages) */
    for( i=0; i<1800; i++ ) {
        *(nvm_memo_data+i) = 0;
    }
    nvm->tag_mode = 0;
    nvm->tag_page = 0;

    /* clear hi score */
    nvm->dscan_hi_score  = 0;
    nvm->gunpey_hi_score = 0;

    /* localization */
#if 1 /* JPN */
    nvm->country_code  =  1;
    nvm->clock.map     =  7;
    nvm->clock.mid_x   =  9;
    nvm->clock.mid_y   =  6;
    nvm->clock.left_x  =  0;
    nvm->clock.left_y  =  4;
    nvm->clock.right_x = 16;
    nvm->clock.right_y =  5;
#else /* UK */
    nvm->country_code  =  2;
    nvm->clock.map     =  0;
    nvm->clock.mid_x   =  0;
    nvm->clock.mid_y   =  4;
    nvm->clock.left_x  = 19;
    nvm->clock.left_y  =  5;
    nvm->clock.right_x =  9;
    nvm->clock.right_y =  6;
#endif

    /* clear remscan! data */
    for( i=0; i<14; i++ ) {
        nvm->rem_code[i] = 0;
    }

    /* clear draw data counter */
    nvm->draw_num = 0;
}

/*--------------------------------------------------------------------------*
 |  clear screen                                                            |
 *--------------------------------------------------------------------------*/
void pda_clear_screen( void *arg )
{
    UBYTE i;
    UWORD palette[4];

    for( i=0; i<18; i++ ) {
        set_bkg_tiles( 0, i, 20, 1, space );
        set_bkg_attribute( 0, i, 20, 1, space );
    }
    pda_syscall( PDA_INIT_COLOR, (void *)0 );
    SHOW_BKG;

    for( i=0; i<40; i++ ) {
      set_sprite_tile( i, 0 );
      move_sprite( i, 0, 0 );
      set_sprite_prop( i, 0 );
    }
    SHOW_SPRITES;
}

/*--------------------------------------------------------------------------*
 |  scan joypad                                                             |
 *--------------------------------------------------------------------------*/
void pda_scan_pad( void *arg )
{
    if( keycode.code & J_SELECT ) {
        if( !(keycode.make & J_START) ) {
            keycode.make |= J_SELECT;
        }
    } else if( keycode.make & J_SELECT ) {
        keycode.make &= ~J_SELECT;
        keycode.brk |= J_SELECT;
    }
    if( keycode.code & J_A ) {
        keycode.make |= J_A;
        if( keycode.make & J_SELECT ) {
            keycode.code &= ~J_A;
        }
    } else if( keycode.make & J_A ) {
        keycode.make &= ~J_A;
        if( keycode.make & J_SELECT ) {
            keycode.brk |= J_SHIFT_A;
        } else {
            keycode.brk |= J_A;
        }
    }
    if( keycode.code & J_B ) {
        keycode.make |= J_B;
        if( keycode.make & J_SELECT ) {
            keycode.code &= ~J_B;
        }
    } else if( keycode.make & J_B ) {
        keycode.make &= ~J_B;
        if( keycode.make & J_SELECT ) {
            keycode.brk |= J_SHIFT_B;
        } else {
            keycode.brk |= J_B;
        }
    }
    if( keycode.code & J_START ) {
        keycode.make |= J_START;
        if( keycode.make & J_SELECT ) {
            keycode.brk |= J_EXIT;
        }
    } else if( keycode.make & J_START ) {
        keycode.make &= ~J_START;
        keycode.brk |= J_START;
    }
}

/*--------------------------------------------------------------------------*
 |  show system cursor                                                      |
 *--------------------------------------------------------------------------*/
void pda_show_cursor( void *arg )
{
    UBYTE x, y;

    /* animation */
    if( !cursor.sp ) {
        cursor.sp = 50;
        if( cursor.ani>=7 ) {
            cursor.ani = 0;
        } else {
            cursor.ani++;
        }
        set_sprite_tile( 1, cursor.ani*4+1 );
        set_sprite_tile( 2, cursor.ani*4+2 );
        set_sprite_tile( 3, cursor.ani*4+3 );
        set_sprite_tile( 4, cursor.ani*4+4 );
    } else {
        cursor.sp--;
    }

    x = cursor.info.off_x + cursor.info.add_x * cursor.x;
    y = cursor.info.off_y + cursor.info.add_y * cursor.y;
    if( nvm->cursor_type == 4 ) {
      move_sprite( 1, x,   y-8 );
      move_sprite( 2, x,   y   );
      move_sprite( 3, x+8, y-8 );
      move_sprite( 4, x+8, y   );
    } else {
      move_sprite( 1, x,   y   );
      move_sprite( 2, x,   y+8 );
      move_sprite( 3, x+8, y   );
      move_sprite( 4, x+8, y+8 );
    }
}

/*--------------------------------------------------------------------------*
 |  move system cursor                                                      |
 *--------------------------------------------------------------------------*/
void pda_move_cursor( void *arg )
{
    keycode.code = joypad();
    if( keycode.make & J_A ) {
        ;
    } else if( keycode.flag ) {
        if( !(keycode.code&(J_UP|J_DOWN|J_LEFT|J_RIGHT)) ) {
            keycode.flag = 0;
        }
    } else {
        if( keycode.code & J_UP ) {
            if( cursor.y > 0 ) {
                cursor.y--;
            }
        } else if( keycode.code & J_DOWN ) {
            if( cursor.y < (cursor.info.max_y-1) ) {
                cursor.y++;
            }
        } else if( keycode.code & J_LEFT ) {
            if( cursor.x > 0 ) {
                cursor.x--;
            }
        } else if( keycode.code & J_RIGHT ) {
            if( cursor.x < (cursor.info.max_x-1) ) {
                cursor.x++;
            }
        } else {
            pda_show_cursor( (void *)0 );
            pda_scan_pad( (void *)0 );
            return;
        }
        keycode.flag = 1;
        keycode.brk = 0;
    }
    pda_show_cursor( (void *)0 );
    pda_scan_pad( (void *)0 );
}

/*--------------------------------------------------------------------------*
 |  set system time to RTC                                                  |
 *--------------------------------------------------------------------------*/
void pda_set_time( struct time *arg )
{
    if( arg->flag ) {
        SWITCH_RAM_MBC1( 0x08 );
        *(UBYTE *)0xA000 = arg->sec;
        SWITCH_RAM_MBC1( 0x09 );
        *(UBYTE *)0xA000 = arg->min;
        SWITCH_RAM_MBC1( 0x0A );
        *(UBYTE *)0xA000 = arg->hor;
        SWITCH_RAM_MBC1( 0x0B );
        *(UBYTE *)0xA000 = arg->day_l;
        SWITCH_RAM_MBC1( 0x0C );
        *(UBYTE *)0xA000 = arg->day_h;
        SWITCH_RAM_MBC1( 0 );
    }
}

/*--------------------------------------------------------------------------*
 |  get system time from RTC                                                |
 *--------------------------------------------------------------------------*/
void pda_get_time( struct time *arg )
{
    if( arg->flag ) {
        if( (*(UBYTE *)0x6000) & 0x01 ) {
            *(UBYTE *)0x6000 = 0;
        }
        *(UBYTE *)0x6000 = 1;
        delay( 4UL );

        SWITCH_RAM_MBC1( 0x08 );
        arg->sec = *(UBYTE *)0xA000;
        SWITCH_RAM_MBC1( 0x09 );
        arg->min = *(UBYTE *)0xA000;
        SWITCH_RAM_MBC1( 0x0A );
        arg->hor = *(UBYTE *)0xA000;
        SWITCH_RAM_MBC1( 0x0B );
        arg->day_l = *(UBYTE *)0xA000;
        SWITCH_RAM_MBC1( 0x0C );
        arg->day_h = *(UBYTE *)0xA000;
        SWITCH_RAM_MBC1( 0 );
    }
}

/*--------------------------------------------------------------------------*
 |  set palette color                                                       |
 *--------------------------------------------------------------------------*/
void pda_set_palette( struct palette *arg )
{
    if( _cpu == CGB_TYPE ) {
        if( (arg->no) < 8 ) {
            set_bkg_palette( arg->no, 1, &arg->color0 );
        } else {
            set_sprite_palette( (arg->no)-8, 1, &arg->color0 );
        }
    }
}

/*--------------------------------------------------------------------------*
 |  get day length of month                                                 |
 *--------------------------------------------------------------------------*/
void pda_get_day_length( struct date *arg )
{
    if( ((arg->month)==2) && !((arg->year)%4) ) {
        if( ((arg->year)%100) || !((arg->year)%400) ) {
            arg->day = 29;
        } else {
            arg->day = month_len[(arg->month)-1];
        }
    } else {
        arg->day = month_len[(arg->month)-1];
    }
}

/*--------------------------------------------------------------------------*
 |  update date                                                             |
 *--------------------------------------------------------------------------*/
void pda_update_date( void *arg )
{
    UWORD days;
    UBYTE subs;
    struct date date;

    days = time.day_h*256 + time.day_l - 1;
    time.day_h = 0;
    time.day_l = 1;
    if( days > 366 ) { /* invalid days (for emu, non RTC...) */
        days = 0;
    }

    while( days ) {
        date.year  = nvm->date.year;
        date.month = nvm->date.month;
        pda_syscall( PDA_GET_DAY_LENGTH, &date );
        subs = date.day - nvm->date.day;
        if( days > subs ) {
            days -= subs;
            nvm->date.day = 0;
            (nvm->date.month)++;
            if( (nvm->date.month) > 12 ) {
                nvm->date.month = 1;
                (nvm->date.year)++;
            }
        } else {
            (nvm->date.day) += days;
            days = 0;
        }
    }
    if( (nvm->date.year) > 9999 ) {
        nvm->date.year = 0;
    }
}

/*--------------------------------------------------------------------------*
 |  initialize DTMF tone                                                    |
 *--------------------------------------------------------------------------*/
void pda_init_dial( void *arg )
{
    NR52_REG = 0x83U;
    NR51_REG = 0x00U;
    NR50_REG = 0x77U;
    NR24_REG = 0x87U;
    NR22_REG = 0xffU;
    NR21_REG = 0xbfU;
    NR14_REG = 0x87U;
    NR12_REG = 0xffU;
    NR11_REG = 0xbfU;
    NR10_REG = 0x04U;

    NR13_REG = 0x00U;
    NR23_REG = 0x00U;	
}

/*--------------------------------------------------------------------------*
 |  sound engine for DTMF                                                   |
 *--------------------------------------------------------------------------*/
void pda_dialtone( unsigned char *arg )
{
    UBYTE i;
    UWORD dtmf_on, dtmf_off;

    pda_syscall( PDA_INIT_DIAL, (void *)0 );

    dtmf_on  = DTMF_ON;
    dtmf_off = DTMF_OFF;
    while( *arg ){
        switch( *arg ){
          case '1':
            NR13_REG = R1;
            NR23_REG = C1; 
            break;
          case '2':
            NR13_REG = R1;
            NR23_REG = C2;
            break;
          case '3':
            NR13_REG = R1;
            NR23_REG = C3;	
            break;
          case 'A':
          case 'a':
            NR13_REG = R1;
            NR23_REG = C4;  
            break;
          case '4':
            NR13_REG = R2;
            NR23_REG = C1;	
            break;
          case '5':
            NR13_REG = R2;
            NR23_REG = C2;	
            break;
          case '6':
            NR13_REG = R2;
            NR23_REG = C3;	
            break;
          case 'B':
          case 'b':
            NR13_REG = R2;
            NR23_REG = C4;	
            break;
          case '7':
            NR13_REG = R3;
            NR23_REG = C1;	
            break;
          case '8':
            NR13_REG = R3;
            NR23_REG = C2;	
            break;
          case '9':
            NR13_REG = R3;
            NR23_REG = C3;	
            break;
          case 'C':
          case 'c':
            NR13_REG = R3;
            NR23_REG = C4;	
            break;
          case '*':
            NR13_REG = R4;
            NR23_REG = C1;	
            break;
          case '0':
            NR13_REG = R4;
            NR23_REG = C2;	
            break;
          case '#':
            NR13_REG = R4;
            NR23_REG = C3;	
            break;
          case 'D':
          case 'd':
            NR13_REG = R4;
            NR23_REG = C4;	
            break;
          case ',':
            delay( dtmf_on );
            delay( dtmf_off );
          default:
            NR51_REG = 0x00U;
            goto skip;
        }
        NR24_REG = 0x87U;
        NR51_REG = 0x33U;
        delay( dtmf_on );
        NR51_REG = 0x00U;
        delay( dtmf_off );
skip:
        arg++;
    }
}

/*--------------------------------------------------------------------------*
 |  pdc on hook                                                             |
 *--------------------------------------------------------------------------*/
void pda_pdc_on_hook( void *arg )
{
    send_gb232( PDC_ON_HOOK );
}

/*--------------------------------------------------------------------------*
 |  pdc dialing                                                             |
 *--------------------------------------------------------------------------*/
void pda_pdc_dialing( unsigned char *arg )
{
    set_gb232( SPEED_600 | DATA_8 | STOP_1 | PARITY_EVEN, ADJUST_0P );
    init_gb232();
    send_gb232( PDC_OFF_HOOK );
    send_gb232( PDC_DIAL_OK );

    while( *arg ){
        switch( *arg ){
          case '0':
            send_gb232( PDC_DIAL_0 );
            break;
          case '1':
            send_gb232( PDC_DIAL_1 );
            break;
          case '2':
            send_gb232( PDC_DIAL_2 );
            break;
          case '3':
            send_gb232( PDC_DIAL_3 );
            break;
          case '4':
            send_gb232( PDC_DIAL_4 );
            break;
          case '5':
            send_gb232( PDC_DIAL_5 );
            break;
          case '6':
            send_gb232( PDC_DIAL_6 );
            break;
          case '7':
            send_gb232( PDC_DIAL_7 );
            break;
          case '8':
            send_gb232( PDC_DIAL_8 );
            break;
          case '9':
            send_gb232( PDC_DIAL_9 );
            break;
          case 'a':
          case 'A':
            send_gb232( PDC_DIAL_A );
            break;
          case 'b':
          case 'B':
            send_gb232( PDC_DIAL_B );
            break;
          case 'c':
          case 'C':
            send_gb232( PDC_DIAL_C );
            break;
          case 'd':
          case 'D':
            send_gb232( PDC_DIAL_D );
            break;
          case '*':
            send_gb232( PDC_DIAL_AS );
            break;
          case '#':
            send_gb232( PDC_DIAL_SH );
            break;
          case '-':
          default:
            break;
        }
        arg++;
    }
    send_gb232( PDC_DIAL_END );
}

/*--------------------------------------------------------------------------*
 |  play remocon data                                                       |
 *--------------------------------------------------------------------------*/
void pda_play_remocon( UBYTE *arg )
{
#if 0
    switch( remcon_hz ) {
      case REMCON_00HZ:
        driver_play_remcon00( *arg );
        break;
      case REMCON_33HZ:
        driver_play_remcon33( *arg );
        break;
      case REMCON_35HZ:
        driver_play_remcon35( *arg );
        break;
      case REMCON_38HZ:
        driver_play_remcon38( *arg );
        break;
      case REMCON_40HZ:
        driver_play_remcon40( *arg );
        break;
      case REMCON_44HZ:
        driver_play_remcon44( *arg );
        break;
      case REMCON_48HZ:
        driver_play_remcon48( *arg );
        break;
      case REMCON_52HZ:
        driver_play_remcon52( *arg );
        break;
    }
#else
    driver_play_remcon38( *arg );
#endif
}

/*--------------------------------------------------------------------------*
 |  record remocon data                                                     |
 *--------------------------------------------------------------------------*/
void pda_rec_remocon( void *arg )
{
    driver_rec_remcon();
}

/*--------------------------------------------------------------------------*
 |  get keyboard data (physical code)                                       |
 *--------------------------------------------------------------------------*/
void pda_get_keyboard( struct read_data *arg )
{
    driver_get_keyboard();
    arg->flag = key_raw_ir[7];
    arg->data = key_raw_ir[0];
}

/*--------------------------------------------------------------------------*
 |  get keyboard data with conversion                                       |
 *--------------------------------------------------------------------------*/
void pda_get_real_keyboard( unsigned char *arg )
{
    driver_get_keyboard();

    *arg = 0;  /* nul code */
    if( key_raw_ir[7] ) {  /* if get any code from keyboard */
        switch( key_raw_ir[0] ) {  /* physical key code */
          case 0xF0:
            pkey_flag |= PKF_BREAK;
            break;
          case 0xE0:
            pkey_flag |= PKF_EXTEND;
            break;
          default:
            if( pkey_flag & PKF_BREAK ) {
            } else if( pkey_flag & PKF_EXTEND ) {
                switch( key_raw_ir[0] ) {
                  case 0x75:
                    keycode.code |= J_UP;
                    break;
                  case 0x72:
                    keycode.code |= J_DOWN;
                    break;
                  case 0x68:
                    keycode.code |= J_LEFT;
                    break;
                  case 0x74:
                    keycode.code |= J_RIGHT;
                    break;
                }
            } else {
                switch( key_raw_ir[0] ) {
                  case 0x22:
                    keycode.code |= J_A;
                    break;
                  case 0x1A:
                    keycode.code |= J_B;
                    break;
                  case 0x1B:
                    keycode.code |= J_START;
                    break;
                  case 0x1C:
                    keycode.code |= J_SELECT;
                    break;
                  default:
                    *arg = key_table[key_raw_ir[0]];
                    break;
                }
            }
            pkey_flag &= ~(PKF_BREAK | PKF_EXTEND);        
            break;
        }
    }
}

/*--------------------------------------------------------------------------*
 |  init irda settings                                                      |
 *--------------------------------------------------------------------------*/
void pda_init_irda( void *arg )
{
    irda_speed  = 0; /* 0:2400bps, 1:9600bps */
    irda_parity = 0; /* 0:non, 1:odd, 2:even */
}

/*--------------------------------------------------------------------------*
 |  send irda data                                                          |
 *--------------------------------------------------------------------------*/
void pda_send_irda( unsigned char *arg )
{
    driver_irda_snd( *arg );
}

/*--------------------------------------------------------------------------*
 |  recieve irda data                                                       |
 *--------------------------------------------------------------------------*/
void pda_recv_irda( struct read_data *arg )
{
    arg->flag = driver_irda_rcv();
    if( arg->flag == 1 ) {
        arg->data = irda_buff;
    } else { /* 0:unreceived, 2:parity error */ 
        arg->data = 0;
    }
}

/*--------------------------------------------------------------------------*
 |  disable interrupt handler                                               |
 *--------------------------------------------------------------------------*/
void pda_disable_interrupt( void *arg )
{
    disable_interrupts();
    set_interrupts( VBL_IFLAG );
    enable_interrupts();
}

/*--------------------------------------------------------------------------*
 |  enable interrupt handler                                                |
 *--------------------------------------------------------------------------*/
void pda_enable_interrupt( void *arg )
{
    /* disable interrupt */
    disable_interrupts();

    /* Handle TIM interrupt (32 counts/sec) */
    vbl_cnt = 0;
    TMA_REG = 0x00U;  /* Set TMA to divide clock by 0x100 */
    TAC_REG = 0x04U;  /* Set clock to 4096 Hertz */

    /* add interrupt handler */
//  add_VBL( vbl );  /* for clock    */
//  add_SIO( sio );  /* for keyboard */
    add_TIM( tim );  /* for clock    */
    set_interrupts( VBL_IFLAG | TIM_IFLAG );
    enable_interrupts();
}

/* EOF */