/****************************************************************************
 *  bank03.h                                                                *
 ****************************************************************************/

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define C1 0x94U /* 1209Hz, 1213Hz */
#define C2 0x9EU /* 1336Hz, 1337Hz */
#define C3 0xA7U /* 1477Hz, 1472Hz */
#define C4 0xB0U /* 1633Hz, 1638Hz */
#define R1 0x44U /*  697Hz,  697Hz */
#define R2 0x56U /*  770Hz,  770Hz */
#define R3 0x66U /*  852Hz,  851Hz */
#define R4 0x75U /*  941Hz,  942Hz */ 
#define DTMF_ON		50UL	/* Tone on time		*/
#define DTMF_OFF	50UL	/* Tone off time	*/

#define PKF_BREAK       0x80
#define PKF_EXTEND      0x40

/*==========================================================================*
 |  system work area (non backup)                                           |
 *==========================================================================*/
extern UBYTE ram[4][127];       /* for remocon driver */
extern UBYTE irda_speed;        /* in fixed/irda_rcv.s */
extern UBYTE irda_parity;       /* in fixed/irda_rcv.s */
extern UBYTE irda_buff;         /* in fixed/irda_rcv.s */
extern UBYTE key_raw_ir[8];     /* in fixed/kb.s */
extern UBYTE pkey_flag;         /* in fixed/fixed.c */
extern UBYTE pkey_code;         /* in fixed/fixed.c */

extern UBYTE vbl_cnt;           /* in fixed/fixed.c */
extern void vbl();              /* in fixed/fixed.c */
extern void tim();              /* in fixed/fixed.c */
extern void sio();              /* in fixed/fixed.c */
extern void dummy_hdr();        /* in fixed/fixed.c */
