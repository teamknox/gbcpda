/****************************************************************************
 *  bank21.c (part 2/2)                                                     *
 *                                                                          *
 *    RoReCon                                                               *
 *                                                                          *
 *    Copyright(C)  2000, 2001, 2002   TeamKNOx                             *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank20\bank20.h"

extern void bank_21();  // dummy


/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        0
unsigned char i_none[1] = { 0x00 };
struct function func_21[1] = {
    { "NONE    ", "1.00", bank_21, i_none }
};
struct description desc_21 = { FUNC_NUM, (struct function *)func_21 };


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
/* button */
unsigned char rrc2_btn_break1[] = { 0x10,0x11,0x12,0x13 };
unsigned char rrc2_btn_break2[] = { 0x14,0x15,0x16,0x17 };
unsigned char rrc2_btn_press1[] = { 0x18,0x19,0x1A,0x1B };
unsigned char rrc2_btn_press2[] = { 0x1C,0x1D,0x1E,0x1F };
unsigned char rrc2_btn2_break[] = { 0xAE };
unsigned char rrc2_btn2_press[] = { 0xAF };

/* selection bkg_palette */
unsigned char rrc2_attrib0[] =
                        { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char rrc2_attrib1[] =
                        { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
unsigned char rrc2_attrib2[] = { 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2 };
unsigned char rrc2_attrib3[] = { 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 };
unsigned char rrc2_attrib4[] =
                        { 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4 };
unsigned char rrc2_attrib5[] = { 5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5 };
unsigned char rrc2_attrib6[] = { 6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6 };
unsigned char rrc2_attrib7[] = { 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7 };
unsigned char rrc2_ris[]  = { 6,7,7,5,7,7,7,7,7,7,7,7 };
unsigned char rrc2_genu[] = {
  4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,2,4,4,2,4,4,2,4,4,4,4,4,4,4,4,4,4,
  4,4,4,4,4,4,4,4,4,4,2,4,4,2,4,4,2,4,4,4,4,4,4,4,4,4,4,4,2,4,4,2,4,4,2,4,
  4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,2,4,4,4,4,4,2,4,4,4,4,4,4,4,4,4,4,
  4,2,4,4,2,4,4,2,4,4,6,4,4,4,4,4,4,4,6,3,6,6,6,4,4,2,4,4,4,4,4,4,4,4,4,4
};
unsigned char rrc2_msg_credit[]  = { "RoReCon 00/03/19TeamKNOx" };
unsigned char rrc2_msg_clr[]     = { "                        " };
unsigned char rrc2_msg_err3[]    = { "Sorry.. RecordinCant Use" };
unsigned char rrc2_msg_rcx[][16] = { "RCX say 'Roger!'","RCX say '-----'" };
unsigned char rrc2_msg_song[][9] = { "StarWars","        ","        " };
UBYTE rrc2_play_tone[] = { 0x23, 3 };
UBYTE rrc2_remocon[]   = { 0xD2, 2 };

/* remocon parameter */
UWORD rrc2_para[] = {
  0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080,
  0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000, 0x8000,
  0x0000, 0x0000, 0x0000
};

/* RCX Piano data */
UWORD rrc2_black[] = {
  RRC_FS3, RRC_GS3, RRC_AS3, 0, RRC_CS4, RRC_DS4, 0, RRC_FS4, RRC_GS4,
  RRC_AS4, 0, RRC_CS5, RRC_DS5, 0, RRC_FS5, RRC_GS5, RRC_AS5
};
UWORD rrc2_white[] = {
  RRC_F3, RRC_G3, RRC_A3, RRC_B3, RRC_C4, RRC_D4, RRC_E4, RRC_F4, RRC_G4,
  RRC_A4, RRC_B4, RRC_C5, RRC_D5, RRC_E5, RRC_F5, RRC_G5, RRC_A5, RRC_B5
};
UWORD rrc2_song1[] = {  /* star wars */
  RRC_G4, RRC_L4/3, RRC_G4, RRC_L4/3,  RRC_G4, RRC_L4/3, RRC_C5, RRC_L4/3,
  RRC_R, RRC_L4/3, RRC_C5, RRC_L4/3, RRC_G5, RRC_L2, RRC_F5, RRC_L4/3,
  RRC_E5, RRC_L4/3, RRC_D5, RRC_L4/3, RRC_C6, RRC_L2, RRC_G5, RRC_L4, RRC_F5,
  RRC_L4/3, RRC_E5, RRC_L4/3, RRC_D5, RRC_L4/3, RRC_C6, RRC_L2, RRC_G5,
  RRC_L4, RRC_F5, RRC_L4/3, RRC_E5, RRC_L4/3, RRC_F5, RRC_L4/3, RRC_D5,
  RRC_L2H, 0};
UWORD rrc2_song2[] = { 0 };
UWORD rrc2_song3[] = { 0 };
UBYTE rrc2[] = { 3, 16, 6 };

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void rrc2_press_button( UBYTE x, UBYTE y )
{
  set_bkg_tiles( x,   y,   3, 1, rrc2_btn_press1 );
  set_bkg_tiles( x,   y+1, 1, 1, &rrc2_btn_press1[3] );
  set_bkg_tiles( x+2, y+1, 1, 1, rrc2_btn_press2 );
  set_bkg_tiles( x,   y+2, 3, 1, &rrc2_btn_press2[1] );
}

void rrc2_break_button( UBYTE x, UBYTE y )
{
  set_bkg_tiles( x,   y,   3, 1, rrc2_btn_break1 );
  set_bkg_tiles( x,   y+1, 1, 1, &rrc2_btn_break1[3] );
  set_bkg_tiles( x+2, y+1, 1, 1, rrc2_btn_break2 );
  set_bkg_tiles( x,   y+2, 3, 1, &rrc2_btn_break2[1] );
}

void rrc2_press_black( UBYTE x )
{
  UBYTE i;

  for( i=0; i<3; i++ ) {
    set_sprite_tile( 6+i, 37 );
    set_sprite_prop( 6+i, 2 );
    move_sprite( 6+i, x*8+22, i*8+48 );
  }
}

void rrc2_break_black( UBYTE x )
{
  UBYTE i;

  for( i=0; i<3; i++ ) {
    move_sprite( 6+i, 0, 0 );
  }
}

void rrc2_press_white( UBYTE x )
{
  set_bkg_attribute( x+1, 4, 1, 4, rrc2_attrib5 );
}

void rrc2_break_white( UBYTE x )
{
  set_bkg_attribute( x+1, 4, 1, 4, rrc2_attrib0 );
}

void rrc2_press_key( UBYTE x, UBYTE y )
{
  set_bkg_tiles( x, y, 1, 1, rrc2_btn2_press );
}

void rrc2_break_key( UBYTE x, UBYTE y )
{
  set_bkg_tiles( x, y, 1, 1, rrc2_btn2_break );
}

void rrc2_lock_on()
{
  UBYTE i;

  for( i=0; i<8; i++ ) {
    set_sprite_tile( 8+i, 33+(i%4) );
    set_sprite_prop( 8+i, 1 );
  }
}

void rrc2_lock_off()
{
  UBYTE i;

  for( i=0; i<8; i++ ) {
    move_sprite( 8+i, 0, 0 );
  }
  rrc2_break_key( 11, 6 );
  rrc2_break_key( 11, 8 );
  rrc2_break_key( 17, 6 );
  rrc2_break_key( 17, 8 );
}

void rrc_move_cursor( UBYTE i, UBYTE x, UBYTE y )
{
    move_sprite( i*4+8,  x,   y );
    move_sprite( i*4+9,  x,   y+8 );
    move_sprite( i*4+10, x+8, y );
    move_sprite( i*4+11, x+8, y+8 );
}

void rrc2_rcx_cntl( struct rrc_rcx_cntrl *rcx )
{
  UBYTE snd_data[16];
  UBYTE i, j, len, chksum, rsv_flag;
  struct read_data data;

  if( rcx->mode != RRC_MODE_GENUINE )  rcx->rrc->tglbit3 ^= 0x08;

  /* Header */
  snd_data[0] = 0x55;
  snd_data[1] = 0xFF;
  snd_data[2] = 0x00;

  /* Command */
  snd_data[3] = *(rcx->cmd);
  if( rcx->mode != RRC_MODE_GENUINE )  snd_data[3] |= rcx->rrc->tglbit3;
  snd_data[4] = ~snd_data[3];
  chksum = snd_data[3];
  len = *(rcx->cmd+1);
  i = 5;

  /* Parameter */ 
  if( len > 0 ) {
    snd_data[i] = rcx->p1;
    snd_data[i+1] = ~snd_data[i];
    chksum += snd_data[i];
    i++;
    i++;
  }
  if( len > 1 ) {
    snd_data[i] = rcx->p2;
    snd_data[i+1] = ~snd_data[i];
    chksum += snd_data[i];
    i++;
    i++;
  }
  if( len > 2 ) {
    snd_data[i] = rcx->p3;
    snd_data[i+1] = ~snd_data[i];
    chksum += snd_data[i];
    i++;
    i++;
  }

  /* Checksum */ 
  snd_data[i++] = chksum;
  snd_data[i++] = ~chksum;

  /* Ir send */
  for( j=0; j<i; j++ ) {
    pda_syscall( PDA_SEND_IRDA, &snd_data[j] );
  }

  /* rcx msg */
  if( rcx->mode != RRC_MODE_GENUINE ) {
    for( i=0; i<5; i++ ) { // why 5 byte ???
      data.flag = 0;
      pda_syscall( PDA_RECV_IRDA, &data );
    }
    if( (data.flag==0) && (snd_data[3]==data.data) ) {
      set_bkg_tiles( 11, 15, 8, 2, rrc2_msg_rcx[0] );
    } else {
      set_bkg_tiles( 11, 15, 8, 2, rrc2_msg_rcx[1] );
    }
  }
}

/*--------------------------------------------------------------------------*
 |  mode selection area                                                     |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc2_mode_cursor = { 1, 2, 14, 30, 0, 0 };
UBYTE rrc2_mode_select( struct rrc_t *rrc )
{
  struct cursor_pos pos;

  if( rrc->mode != RRC_MODE_GENUINE ) {
    set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_credit );
    set_bkg_attribute( 11, 14, 8, 3, rrc2_attrib4 );
  }

  pos.x = pos.y = 0;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc2_mode_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 0 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 0 );
    if( keycode.make & J_A ) {
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 0 );
      }
      if( rrc->mode != RRC_MODE_GENUINE ) {
        set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_clr );
      }
      rrc->mode = rrc->mode + 1;
      return( 0 );
    }
  }
  if( rrc->mode != RRC_MODE_GENUINE ) {
    set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_clr );
  }
  return( 1 );
}

/*--------------------------------------------------------------------------*
 |  black key                                                               |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_black_cursor = { 17, 3, 18, 66, 8, 0 };
UBYTE rrc_black_key( struct rrc_t *rrc, struct cursor_pos *pos )
{
  struct rrc_rcx_cntrl rcx;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_PIANO;
  rcx.cmd  = rrc2_play_tone;
  rcx.p3   = 0xA0 >> rrc->tone_len;
  pos->y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_black_cursor );
  pda_syscall( PDA_INIT_CURSOR, pos );
  set_bkg_attribute( 11, 14, 8, 3, rrc2_attrib0 );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 1 );
    if( keycode.make & J_A ) {
      if( (cursor.x!=3)&&(cursor.x!=6)&&(cursor.x!=10)&&(cursor.x!=13) ) {
        rrc2_press_black( cursor.x );
        rcx.p1  = rrc2_black[cursor.x];
        rcx.p2  = rrc2_black[cursor.x]>>8;
        rrc2_rcx_cntl( &rcx );
        while( keycode.make & J_A ) {
          pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
          if( keycode.brk & J_EXIT )  return( 1 );
        }
        rrc2_break_black( cursor.x );
        set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_clr );
      }
    }
  }
  pos->x = cursor.x;
  if( cursor.y == 2 )  return( 2 );
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  white key                                                               |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_white_cursor = { 18, 3, 14, 74, 8, 0 };
UBYTE rrc_white_key( struct rrc_t *rrc, struct cursor_pos *pos )
{
  struct rrc_rcx_cntrl rcx;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_PIANO;
  rcx.cmd  = rrc2_play_tone;
  rcx.p3   = 0xA0 >> rrc->tone_len;
  pos->y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_white_cursor );
  pda_syscall( PDA_INIT_CURSOR, pos );
  set_bkg_attribute( 11, 14, 8, 3, rrc2_attrib0 );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 2 );
    if( keycode.make & J_A ) {
      rrc2_press_white( cursor.x );
      rcx.p1  = rrc2_white[cursor.x];
      rcx.p2  = rrc2_white[cursor.x]>>8;
      rrc2_rcx_cntl( &rcx );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 2 );
      }
      rrc2_break_white( cursor.x );
      set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_clr );
    }
  }
  if( cursor.x > 16 )  cursor.x = 16;
  pos->x = cursor.x;
  if( cursor.y == 2 )  return( 3 );
  return( 1 );
}

/*--------------------------------------------------------------------------*
 |  rest button                                                             |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_rest_cursor = { 2, 3, 22, 104, 0, 0 };
UBYTE rrc_rest_button( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_PIANO;
  rcx.cmd  = rrc2_play_tone;
  rcx.p1   = rcx.p2   = 0;
  rcx.p3   = 0xA0 >> rrc->tone_len;
  pos.x = 0;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_rest_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  set_bkg_attribute( 11, 14, 8, 3, rrc2_attrib0 );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 3 );
    if( cursor.x == 1 )  return( 4 );
    if( keycode.make & J_A ) {
      rrc2_press_button( 1, 9 );
      rrc2_rcx_cntl( &rcx );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 3 );
      }
      rrc2_break_button( 1, 9 );
      set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_clr );
    }
  }
  if( cursor.y == 2 )  return( 6 );
  return( 2 );
}

/*--------------------------------------------------------------------------*
 |  length select                                                           |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_length_cursor = { 7, 3, 46, 102, 8, 0 };
UBYTE rrc_length_select( struct rrc_t *rrc )
{
  struct cursor_pos pos;

  pos.x = rrc->tone_len+1;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_length_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 4 );
    if( cursor.x == 0 )  return( 3 );
    if( cursor.x == 6 )  return( 5 );
    if( keycode.make & J_A ) {
      set_bkg_attribute( 6, 10, 5, 1, rrc2_attrib0 );
      set_bkg_attribute( cursor.x+5, 10, 1, 1, rrc2_attrib1 );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 4 );
      }
      rrc->tone_len = cursor.x-1;
    }
  }
  if( cursor.y == 2 )  return( 6 );
  return( 2 );
}

/*--------------------------------------------------------------------------*
 |  clear and play button                                                   |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_clearplay_cursor = { 3, 3, 94, 104, 24, 0 };
UBYTE rrc_clear_play( struct rrc_t *rrc )
{
  struct cursor_pos pos;

  pos.x = 1;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_clearplay_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_err3 );
  set_bkg_attribute( 11, 14, 8, 3, rrc2_attrib1 );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 5 );
    if( cursor.x == 0 ) {
      set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_clr );
      return( 4 );
    }
    if( keycode.make & J_A ) {
      rrc2_press_button( cursor.x*3+10, 9 );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 5 );
      }
      rrc2_break_button( cursor.x*3+10, 9 );
    }
  }
  set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_clr );
  if( cursor.y == 2 )  return( 6 );
  return( 2 );
}

/*--------------------------------------------------------------------------*
 |  song button                                                             |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_song_cursor = { 3, 2, 22, 144, 24, 0 };
UBYTE rrc_song_button( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;
  UBYTE  i;
  UWORD  *song;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_PIANO;
  rcx.cmd  = rrc2_play_tone;
  pos.x = 0;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_song_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  set_bkg_attribute( 11, 14, 8, 3, rrc2_attrib0 );
  set_bkg_tiles( 11, 14, 8, 1, rrc2_msg_song[pos.x] );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 6 );
    if( pos.x != cursor.x ) {
      pos.x = cursor.x;
      set_bkg_tiles( 11, 14, 8, 1, rrc2_msg_song[pos.x] );
    }
    if( keycode.make & J_A ) {
      rrc2_press_button( cursor.x*3+1, 14 );
      switch( pos.x ) {
        case 0:
          song = rrc2_song1;
          break;
        case 1:
          song = rrc2_song2;
          break;
        case 2:
          song = rrc2_song3;
          break;
      }
      i = 0;
      while( song[i] ) {
        rcx.p1   = song[i];
        rcx.p2   = song[i]>>8;
        rcx.p3   = song[i+1];
        rrc2_rcx_cntl( &rcx );
        delay( 100 );
        i++;  i++;
      }
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 6 );
      }
      rrc2_break_button( cursor.x*3+1, 14 );
      set_bkg_tiles( 11, 14, 8, 3, rrc2_msg_clr );
    }
  }
  return( 3 );
}

/*==========================================================================*
 |  piano mode                                                              |
 *==========================================================================*/
void rrc_mode_piano( struct rrc_t *rrc )
{
  UBYTE area;
  struct cursor_pos pos;

  area = pos.x = 0;

  while( rrc->mode == RRC_MODE_PIANO ) {
    if( keycode.brk & J_EXIT )  return;
    switch( area ) {
      case 0:
        area = rrc2_mode_select( rrc );
        break;
      case 1:
        area = rrc_black_key( rrc, &pos );
        break;
      case 2:
        area = rrc_white_key( rrc, &pos );
        break;
      case 3:
        area = rrc_rest_button( rrc );
        break;
      case 4:
        area = rrc_length_select( rrc );
        break;
      case 5:
        area = rrc_clear_play( rrc );
        break;
      case 6:
        area = rrc_song_button( rrc );
        break;
    }
  }
}


/*--------------------------------------------------------------------------*
 |  genuine 1 2 3 key                                                       |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_123_cursor = { 3, 3, 94, 42, 24, 0 };
UBYTE rrc_genuine_123( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_GENUINE;
  rcx.cmd  = rrc2_remocon;
  pos.x = 0;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_123_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 1 );
    if( keycode.make & J_A ) {
      rrc2_press_key( cursor.x*3+11, 3 );
      rcx.p1 = rrc2_para[cursor.x]>>8;
      rcx.p2 = rrc2_para[cursor.x];
      rcx.p3 = 0;
      rrc2_rcx_cntl( &rcx );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 1 );
        rrc2_rcx_cntl( &rcx );
      }
      rrc2_break_key( cursor.x*3+11, 3 );
      rcx.p1 = rcx.p2 = rcx.p3 = 0;
      rrc2_rcx_cntl( &rcx );
    }
  }
  if( cursor.y == 2 )  return( 2 );
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  genuine a b c key                                                       |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_abc_cursor = { 1, 3, 118, 76, 0, 0 };
UBYTE rrc_genuine_abc( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;
  UBYTE  lockon, pad, y1, y2, yy1, yy2;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_GENUINE;
  rcx.cmd  = rrc2_remocon;
  pos.x = 0;  pos.y = 1;  lockon = RRC_OFF;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_abc_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 2 );
    if( keycode.make & J_SELECT ) {  /* lockon */
      while( keycode.make & J_SELECT ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 2 );
      }
      lockon = RRC_ON;
    }
    if( (lockon==RRC_ON) || (keycode.make & J_A) ) {
      rrc2_lock_on();
      y1 = y2 = yy1 = yy2 = 1;
      rrc_move_cursor( 0,  94, y1*8+68 );
      rrc_move_cursor( 1, 142, y2*8+68 );
      keycode.code = J_A;
      while( (lockon==RRC_ON) || (keycode.code & J_A) ) {
        keycode.code = joypad();
        if( keycode.code & J_UP )          y1 = y2 = 0;
        else if( keycode.code & J_DOWN )   y1 = y2 = 2;
        else                               y1 = y2 = 1;
        if( keycode.code & J_LEFT ) {
          if( y1 == 1 )  y1 = 2;
          else           y1 = 1;
          if( y2 != 2 )  y2 = 0;
        } else if( keycode.code & J_RIGHT ) {
          if( y1 != 2 )  y1 = 0;
          if( y2 == 1 )  y2 = 2;
          else           y2 = 1;
        }
        cursor.sp = 0;
        pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
        pda_syscall( PDA_SCAN_PAD, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 2 );
        if( keycode.make & J_SELECT ) {  /* lockon */
          while( keycode.make & J_SELECT ) {
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return( 2 );
          }
          lockon = RRC_OFF;
        }
        if( y1 != yy1 ) {
          if( yy1 != 1 )  rrc2_break_key( 11, 6+yy1 );
          yy1 = y1;
          rrc_move_cursor( 0,  94, y1*8+68 );
          if( yy1 != 1 )  rrc2_press_key( 11, 6+yy1 );
        }
        if( y2 != yy2 ) {
          if( yy2 != 1 )  rrc2_break_key( 17, 6+yy2 );
          yy2 = y2;
          rrc_move_cursor( 1, 142, y2*8+68 );
          if( yy2 != 1 )  rrc2_press_key( 17, 6+yy2 );
        }
        rcx.p1 = (rrc2_para[rrc2[y1]] | rrc2_para[rrc2[y2]])>>8;
        rcx.p2 = rrc2_para[rrc2[y1]] | rrc2_para[rrc2[y2]];
        rcx.p3 = 0;
        rrc2_rcx_cntl( &rcx );
      }
      rrc2_lock_off();
    }
  }
  if( cursor.y == 2 )  return( 3 );
  return( 1 );
}

/*--------------------------------------------------------------------------*
 |  genuine p1 p2 key                                                       |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_p1p2_cursor = { 2, 3, 94, 106, 48, 0 };
UBYTE rrc_genuine_p1p2( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_GENUINE;
  rcx.cmd  = rrc2_remocon;
  pos.x = 0;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_p1p2_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 3 );
    if( keycode.make & J_A ) {
      rrc2_press_key( cursor.x*6+11, 11 );
      rcx.p1 = rrc2_para[cursor.x+9]>>8;
      rcx.p2 = rrc2_para[cursor.x+9];
      rcx.p3 = 0;
      rrc2_rcx_cntl( &rcx );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 3 );
        rrc2_rcx_cntl( &rcx );
      }
      rrc2_break_key( cursor.x*6+11, 11 );
      rcx.p1 = rcx.p2 = rcx.p3 = 0;
      rrc2_rcx_cntl( &rcx );
    }
  }
  if( cursor.y == 2 )  return( 4 );
  return( 2 );
}

/*--------------------------------------------------------------------------*
 |  genuine p3 p4 p5 key                                                    |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_p3p4p5_cursor = { 3, 3, 94, 122, 24, 0 };
UBYTE rrc_genuine_p3p4p5( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_GENUINE;
  rcx.cmd  = rrc2_remocon;
  pos.x = 0;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_p3p4p5_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 4 );
    if( keycode.make & J_A ) {
      rrc2_press_key( cursor.x*3+11, 13 );
      rcx.p1 = rrc2_para[cursor.x+11]>>8;
      rcx.p2 = rrc2_para[cursor.x+11];
      rcx.p3 = 0;
      rrc2_rcx_cntl( &rcx );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 4 );
        rrc2_rcx_cntl( &rcx );
      }
      rrc2_break_key( cursor.x*3+11, 13 );
      rcx.p1 = rcx.p2 = rcx.p3 = 0;
      rrc2_rcx_cntl( &rcx );
    }
  }
  if( cursor.y == 2 )  return( 5 );
  return( 3 );
}

/*--------------------------------------------------------------------------*
 |  genuine stop key                                                        |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_stop_cursor = { 2, 2, 94, 138, 48, 0 };
UBYTE rrc_genuine_stop( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;

  rcx.rrc  = rrc;
  rcx.mode = RRC_MODE_GENUINE;
  rcx.cmd  = rrc2_remocon;
  pos.x = 0;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_stop_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 5 );
    if( keycode.make & J_A ) {
      rrc2_press_key( cursor.x*6+11, 15 );
      rcx.p1 = rrc2_para[cursor.x+14]>>8;
      rcx.p2 = rrc2_para[cursor.x+14];
      rcx.p3 = 0;
      rrc2_rcx_cntl( &rcx );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 5 );
        rrc2_rcx_cntl( &rcx );
      }
      rrc2_break_key( cursor.x*6+11, 15 );
      rcx.p1 = rcx.p2 = rcx.p3 = 0;
      rrc2_rcx_cntl( &rcx );
    }
  }
  return( 4 );
}

/*==========================================================================*
 |  genuine mode                                                            |
 *==========================================================================*/
void rrc_mode_genuine( struct rrc_t *rrc )
{
  UBYTE area;

  area = 0;

  while( rrc->mode == RRC_MODE_GENUINE ) {
    if( keycode.brk & J_EXIT )  return;
    switch( area ) {
      case 0:
        area = rrc2_mode_select( rrc );
        break;
      case 1:
        area = rrc_genuine_123( rrc );
        break;
      case 2:
        area = rrc_genuine_abc( rrc );
        break;
      case 3:
        area = rrc_genuine_p1p2( rrc );
        break;
      case 4:
        area = rrc_genuine_p3p4p5( rrc );
        break;
      case 5:
        area = rrc_genuine_stop( rrc );
        break;
    }
  }
}

/*==========================================================================*
 |  dummy                                                                   |
 *==========================================================================*/
void bank_21()
{
}

/* EOF */
