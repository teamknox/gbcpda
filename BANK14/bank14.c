/****************************************************************************
 *  bank14.c                                                                *
 *                                                                          *
 *    GB-DATA  V2.12    2002/05/25                                          *
 *                                                                          *
 *    Copyright(C)  1999 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank14\i_data.h"
#include "bank14\c_data.h"
#include "bank14\s_data.h"

extern void gb_data( void *arg );

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank14\i_data.c"
#include "bank14\s_data.c"
#include "bank14\ss_data.c"
#include "bank14\c_data.c"
#include "bank14\cc_data.c"
#include "bank14\b_data.c"

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_14[FUNC_NUM] = {
  { "GB-DATA ", "2.12", gb_data, i_data, i_dataCGBPal0c1, i_dataCGBPal0c2 }
};
struct description desc_14 = { FUNC_NUM, (struct function *)func_14 };

/*==========================================================================*
 |  data                                                                    |
 *==========================================================================*/
unsigned char data_space[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char md_btn[] = {
    0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,
    0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,
    0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,
    0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,0xA1,0xA2,0xA3
};

unsigned char md_head[] = {
    0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x03,0x00,0x00,0x00,0x00,0x00,0x00,
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x04,0x00,0x30,0x30,0x30,0x00,0x00,
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x04,0x00,0x2F,0x30,0x30,0x30,0x00,
    0x04,0x00,0x00,0x00,0x0E,0x0E,0x0E,0x0E,0x0E,0x0E,
    0x0E,0x0E,0x00,0x05,0x02,0x02,0x02,0x02,0x02,0x03,
    0x05,0x02,0x02,0x03,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0C,0x04
};
unsigned char md_line[] = {
    0x10,0x11,0x12,0x04,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x13,0x00,0x14,0x04,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x15,0x16,0x17,0x04,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04
};
unsigned char md_tail[] = {
    0x10,0x11,0x12,0x04,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x13,0x00,0x14,0x04,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x15,0x16,0x17,0x04,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0D,0x04,
    0x00,0x00,0x00,0x05,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char md_line_a[] = {
    2,2,2,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
    2,2,2,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
    2,2,2,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0
};
unsigned char md_tail_a[] = { 2,2,2,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0 };
unsigned char md_space_a[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char md_abort_break_a[] = { 2,2,2,2,2,2,2,2,2,2,2,2,2,2 };

unsigned char md_abort_break[] = {
    0x10,0x11,0x11,0x11,0x11,0x11,0x12,0x15,0x16,0x16,0x16,0x16,0x16,0x17
};
unsigned char md_abort_press[] = {
    0x18,0x19,0x19,0x19,0x19,0x19,0x1A,0x1D,0x1E,0x1E,0x1E,0x1E,0x1E,0x1F
};
unsigned char md_gb[] = {
    0xE0,0xE2,0xE8,0xEA,0xE1,0xE3,0xE9,0xEB,
    0xE4,0xE6,0xEC,0xEE,0xE5,0xE7,0xED,0xEF
};
unsigned char md_pc[] = {
    0xF0,0xF2,0xF8,0xFA,0xF1,0xF3,0xF9,0xFB,
    0xF4,0xF6,0xFC,0xFE,0xF5,0xF7,0xFD,0xFF
};

unsigned char md_load[] = { "Load FILEs   from PC to GB" };
unsigned char md_save[] = { "Save FILEs   from GB to PC" };
UBYTE md_cr[] = { 0x0D };
UBYTE md_lf[] = { 0x0A };
UBYTE md_tab[] = { 0x08 };
UBYTE md_cnm[] = { ',' };
UBYTE md_sp[] = { ' ' };

struct palette data_palette[] = {
     9, s_dataCGBPal0c0, s_dataCGBPal0c1, s_dataCGBPal0c2, s_dataCGBPal0c3,
    10, s_dataCGBPal1c0, s_dataCGBPal1c1, s_dataCGBPal1c2, s_dataCGBPal1c3,
    11, s_dataCGBPal2c0, s_dataCGBPal2c1, s_dataCGBPal2c2, s_dataCGBPal2c3,
    12, s_dataCGBPal3c0, s_dataCGBPal3c1, s_dataCGBPal3c2, s_dataCGBPal3c3,
    13, s_dataCGBPal4c0, s_dataCGBPal4c1, s_dataCGBPal4c2, s_dataCGBPal4c3,
    14, s_dataCGBPal5c0, s_dataCGBPal5c1, s_dataCGBPal5c2, s_dataCGBPal5c3,
    15, s_dataCGBPal6c0, s_dataCGBPal6c1, s_dataCGBPal6c2, s_dataCGBPal6c3
};

#define FOLDER_ROOT   0
#define FOLDER_PHONE  1
#define FOLDER_MEMO   2
#define FOLDER_DATE   3
#define FOLDER_DRAW   4
unsigned char data_passw[] = { "NEW PASSWORD" };
unsigned char data_root[]  = { "FOLDERS      " };
unsigned char data_phone[] = { "PHONE        " };
unsigned char data_memo[]  = { "MEMO         " };
unsigned char data_date[]  = { "DATE         " };
unsigned char data_draw[]  = { "DRAW         " };
unsigned char data_none[]  = { "No Data      " };
unsigned char *data_menu[] = {
  data_root, data_phone, data_memo, data_date, data_draw
};


/*==========================================================================*
 |  work ram                                                                |
 *==========================================================================*/
typedef struct data_work {
    UBYTE folder;
    UWORD total;
    UWORD y;
    struct cursor_pos pos;
    UWORD max_date_tag;
} _data_work;

/*==========================================================================*
 |  subroutines                                                             |
 *==========================================================================*/
void data_move_4sprite( UBYTE no, UBYTE x, UBYTE y )
{
    move_sprite( no++, x,   y   );
    move_sprite( no++, x,   y+8 );
    move_sprite( no++, x+8, y   );
    move_sprite( no,   x+8, y+8 );
}

void data_clear_4sprite( UBYTE no )
{
    data_move_4sprite( no, 0, 0 );
} 

void data_set_4sprite( UBYTE no, UBYTE t, UBYTE c, UBYTE x, UBYTE y )
{
    set_sprite_tile( no,  t++ );
    move_sprite( no, x,   y   );
    set_sprite_prop( no++, c );
    set_sprite_tile( no,  t++ );
    move_sprite( no, x,   y+8 );
    set_sprite_prop( no++, c );
    set_sprite_tile( no,  t++ );
    move_sprite( no, x+8, y   );
    set_sprite_prop( no++, c );
    set_sprite_tile( no,  t );
    move_sprite( no, x+8, y+8 );
    set_sprite_prop( no, c );
}

void init_data()
{
    UBYTE  i;
    struct button btn;
    struct palette palette;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_data( 12, 4, c_data );
    set_bkg_data( 224, 32, cc_data );
    set_sprite_data( 64, 64, s_data );
    set_bkg_tiles( 0, 0, 20, 5, md_head );
    for( i=0; i<3; i++ ) {
        set_bkg_tiles( 0, i*3+5, 20, 3, md_line );
        set_bkg_attribute( 0, i*3+5, 20, 3, md_line_a );
    }
    set_bkg_tiles( 0, 14, 20, 4, md_tail );
    set_bkg_attribute( 0, 14, 20, 2, md_line_a );
    set_bkg_attribute( 0, 16, 20, 1, md_tail_a );

    for( i=0; i<7; i++ ) {
        palette.no = data_palette[i].no;
        palette.color1 = data_palette[i].color1;
        palette.color2 = data_palette[i].color2;
        palette.color3 = data_palette[i].color3;
        pda_syscall( PDA_SET_PALETTE, &palette );
    }

    /* make up button */
    btn.on = 0; // off
    btn.data = b_data;
    for( i=0; i<4; i++ ) {
        btn.no   = i;
        btn.data = b_data + 128*(UWORD)i;
        pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
    }
    set_bkg_tiles( 0, 5, 3, 12, md_btn );

    /* secretive protection */
    if( nvm->password[0] ) {
        data_set_4sprite( 34, 72, 4, 12, 132 );  /* 2: key */
    } else {
        data_clear_4sprite( 34 );
    }
}

void set_total_file( struct data_work *dw )
{
    UWORD  id[127];
    struct search_schedule tag;

    /* get schedule tag */
    tag.table = id;
    tag.max   = 127;
    pda_syscall( PDA_SEARCH_SCHEDULE_TAG, &tag );
    dw->max_date_tag = (UWORD)tag.max;

    switch( dw->folder ) {
      case FOLDER_ROOT:
        dw->total = (UWORD)4;
        break;
      case FOLDER_PHONE:
        dw->total = (UWORD)max_rom_file + max_ram_file - (dw->max_date_tag);
        break;
      case FOLDER_MEMO:
        dw->total = (UWORD)max_rom_memo_page + 10;
        break;
      case FOLDER_DATE:
        dw->total = (UWORD)dw->max_date_tag;
        break;
      case FOLDER_DRAW:
        if( nvm->draw_num )  dw->total = (UWORD)1;
        else                 dw->total = (UWORD)0;
        break;
    }
}

void data_space_line( UBYTE y )
{
    set_bkg_tiles( 6, y*2+5, 13, 1, data_space );
    data_clear_4sprite( y*4+10 );
}

void data_set_sprite_icon( UBYTE y, UBYTE tile, UBYTE color )
{
    UBYTE  no, y16;

    no = y*4+10;  y16 = y*16+52;
    data_set_4sprite( no, tile, color, 39, y16 );
}

/*--------------------------------------------------------------------------*
 |  show files on main window                                               |
 *--------------------------------------------------------------------------*/
void show_file( struct data_work *dw )
{
    UBYTE  i;
    UWORD  y, n, m;
    struct phone_number pn;
    unsigned char buff[18];

    /* count / total */
    buff[0] = ((dw->total)/100)%10+'0';
    buff[1] = ((dw->total)/10)%10+'0';
    buff[2] = (dw->total)%10+'0';
    set_bkg_tiles( 16, 2, 3, 1, buff );
    buff[0] = (((dw->y)+(dw->pos.y)+1)/100)%10+'0';
    buff[1] = (((dw->y)+(dw->pos.y)+1)/10)%10+'0';
    buff[2] = ((dw->y)+(dw->pos.y)+1)%10+'0';
    set_bkg_tiles( 15, 1, 3, 1, buff );

    /* folder */
    if( (dw->folder) ) {
        data_set_4sprite( 6, 68, 1, 20, 28 );  /* 1: FOLDER */
    } else {
        data_set_4sprite( 6, 64, 1, 20, 28 );  /* 0: ROOT */
    }
    set_bkg_tiles( 4, 2, 9, 1, data_menu[dw->folder] );

    n = max_ram_file - (dw->max_date_tag);

    for( i=0; i<6 ; i++ ) {
        m = (dw->y) + i;
        switch( dw->folder ) {
          case FOLDER_ROOT:
            if( i < (dw->total) ) {
                set_bkg_tiles( 6, i*2+5, 13, 1, data_menu[i+1] );
                data_set_sprite_icon( i, 68, 1 );  /* folder */
            } else {
                data_space_line( i );
            }
            break;
          case FOLDER_PHONE:
            if( m < n ) {
                pn.no  = m + 0x8000;
                pn.str = buff;
                pda_syscall( PDA_GET_PHONE_NAME, &pn );
                set_bkg_tiles( 6, i*2+5, 13, 1, pn.str );
                data_set_sprite_icon( i, 76, 2 );  /* ram data */
            } else if( (m-n) < max_rom_file ) {
                pn.no  = m - n;
                pn.str = buff;
                pda_syscall( PDA_GET_PHONE_NAME, &pn );
                set_bkg_tiles( 6, i*2+5, 13, 1, pn.str );
                if( *(nvm_phone_flag+((pn.no)/8)) & (1<<((pn.no)%8)) ) {
                    data_set_sprite_icon( i, 84, 2 );  /* rom hide */
                } else {
                    data_set_sprite_icon( i, 80, 2 );  /* rom data */
                }
            } else {
                data_space_line( i );
            }
            break;
          case FOLDER_MEMO:
          case FOLDER_DRAW:
            if( i < (dw->total) ) {
                buff[0] = 'P';
                buff[1] = 'A';
                buff[2] = 'G';
                buff[3] = 'E';
                buff[4] = '-';
                buff[5] = '0' + (m+1)/10;
                buff[6] = '0' + (m+1)%10;
                set_bkg_tiles( 6, i*2+5, 7, 1, buff );
                if( (dw->folder) == FOLDER_DRAW ) {
                    data_set_sprite_icon( i, 76, 2 );  /* ram data */
                } else if( m < max_rom_memo_page ) {
                    m = m + 10;
                    if( *(nvm_memo_flag+m/8) & (1<<(m%8)) ) {
                        data_set_sprite_icon( i, 84, 2 );  /* rom hide */
                    } else {
                        data_set_sprite_icon( i, 80, 2 );  /* rom data */
                    }
                } else {
                    m = m - max_rom_memo_page;
                    if( *(nvm_memo_flag+m/8) & (1<<(m%8)) ) {
                        data_set_sprite_icon( i, 88, 3 );  /* ram hide */
                    } else {
                        data_set_sprite_icon( i, 76, 3 );  /* ram data */
                    }
                }
            } else {
                data_space_line( i );
            }
            break;
          case FOLDER_DATE:
            if( i < (dw->total) ) {
                pn.no  = m + n + 0x8000;
                pn.str = buff;
                pda_syscall( PDA_GET_PHONE_NAME, &pn );
                set_bkg_tiles( 6, i*2+5, 13, 1, &buff[1] );
                data_set_sprite_icon( i, 76, 2 );  /* ram data */
            } else {
                data_space_line( i );
            }
            break;
        }
    }
    if( !(dw->total) ) {
        buff[0] = '0';
        set_bkg_tiles( 17, 1, 1, 1, buff );
        set_bkg_tiles( 6, 5, 13, 1, data_none );
    }
}

/*--------------------------------------------------------------------------*
 |  init screen of IrDA transfering                                         |
 *--------------------------------------------------------------------------*/
struct cursor_info data_abort_cursor = {  1, 1, 94, 136, 0, 0 };
void data_init_irda()
{
    UBYTE  i;
    struct cursor_pos pos;

    /* init IrDA parameters */
    pda_syscall( PDA_INIT_IRDA, (void *)0 );

    /* init sprite data and display screen */
    set_sprite_data( 72, 20, ss_data );
    set_bkg_attribute( 5,  6, 13, 1, md_space_a );
    set_bkg_attribute( 5,  7, 13, 1, md_space_a );
    set_bkg_tiles(     8, 14,  7, 2, md_abort_break );
    set_bkg_attribute( 8, 14,  7, 2, md_abort_break_a );
    for( i=0; i<16; i++ ) {  /* ABORT */
        set_sprite_tile( i+10, i+72 );
        if( i < 5 ) {
            move_sprite( i+10, i*8+80, 132 );
            set_sprite_prop( i+10, 0 );
        } else if( i<13 ) {
            set_sprite_prop( i+10, 6 );
        } else {
            set_sprite_prop( i+10, 7 );
        }
    }
    set_bkg_tiles(  5, 9, 4, 4, md_pc );
    set_bkg_attribute( 5, 9, 4, 4, md_space_a );
    set_bkg_tiles( 13, 9, 4, 4, md_gb );
    set_bkg_attribute( 13, 9, 4, 4, md_space_a );
    pos.x = pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &data_abort_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
}

/*--------------------------------------------------------------------------*
 |  load data from PC by IrDA                                               |
 *--------------------------------------------------------------------------*/
void data_load_irda()
{
    UBYTE  i, flag, x, ti;
    UBYTE  tmp[38];  /* size of struct db_file */
    struct read_data data;

    /* init screen */
    flag = 0;  x = 0;
    for( i=0; i<6; i++ )  data_space_line( i );
    set_bkg_tiles(     5,  6, 13, 2, md_load );
    data_init_irda();

    /* ready for recieving data on GB */
    data_move_4sprite( 15, 116, 98 );

#if 1
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    ti = 1;
    pda_syscall( PDA_WAIT, &ti );
#endif

    /* recieve files */
    while( flag != 3 ) {
        /* recieve a data to use IrDA */
        for( i=0; i<38; i++ ) {
            data.flag = 0;
            while( data.flag == 0 ) {
                pda_syscall( PDA_RECV_IRDA, &data );
#if 0
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return;
                if( keycode.make & J_A ) {
                    set_bkg_tiles( 8, 14, 7, 2, md_abort_press );
                    while( keycode.make & J_A ) {
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                        if( keycode.brk & J_EXIT )  return;
                    }
                    return;
                }
                if( flag != 0 ) {
                    move_sprite( 23, 80+x, 108 );
                    x++;  if( x > 24 )  x=0;
                }
#else
                if( data.flag == 0 ) {
                    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    if( keycode.brk & J_EXIT )  return;
                    if( keycode.make & J_A ) {
                        set_bkg_tiles( 8, 14, 7, 2, md_abort_press );
                        while( keycode.make & J_A ) {
                            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                            if( keycode.brk & J_EXIT )  return;
                        }
                        return;
                    }
                    if( flag != 0 ) {
                        move_sprite( 23, 80+x, 108 );
                        x++;  if( x > 24 )  x=0;
                    }
                }
#endif
            }
            tmp[i] = data.data;
            if( (i==0) && (tmp[i]=='!') ) {
                /* get EOF */
                flag = 3;
            } else if( tmp[i] < 0x20 ) {
                /* skip special(control) character */
                i--;
            }
            if( data.flag == 2 ) {  /* detected error */
                flag = 2;
                set_sprite_tile( 15, 88 );
                set_sprite_tile( 16, 89 );
                set_sprite_tile( 17, 90 );
                set_sprite_tile( 18, 91 );
            } else if( flag == 0 ) {  /* recieved first data */
                flag = 1;
                data_move_4sprite( 19, 58, 93 );  /* PC */
            }
        }
        if( flag == 1 ) {
            /* set recieved data into ram data base */
            pda_syscall( PDA_SET_RAM_DATA, &tmp[1] );
        }
    }
}

/*--------------------------------------------------------------------------*
 |  save data to PC by IrDA                                                 |
 *--------------------------------------------------------------------------*/
void data_save_irda()
{
    UWORD  w;
    UBYTE  i, flag, x;
    UBYTE  tmp[36];  /* size of struct db_file */
    struct phone_number pn;

    /* init screen */
    flag = 0;  x = 0;
    for( i=0; i<6; i++ )  data_space_line( i );
    set_bkg_tiles(     5,  6, 13, 2, md_save );
    data_init_irda();

    /* ready for recieving data on PC */
    data_move_4sprite( 19, 58, 93 );

    /* ready for sending data on GB */
    data_move_4sprite( 15, 116, 98 );

    /* save all data */
    for( w=0; w<max_ram_file; w++ ) {
        /* make send buffer from data base */
        pn.no = w+0x8000;
        pn.str = (unsigned char *)tmp;
        pda_syscall( PDA_GET_PHONE_NAME, &pn );
        pn.str = (unsigned char *)&tmp[18];
        pda_syscall( PDA_GET_PHONE_NUMBER, &pn );

        /* send data to use IrDA */
        pda_syscall( PDA_SEND_IRDA, &md_sp );
        for( i=0; i<36; i++ ) {
            pda_syscall( PDA_SEND_IRDA, &tmp[i] );
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return;
            if( keycode.make & J_A ) {
                set_bkg_tiles( 8, 14, 7, 2, md_abort_press );
                while( keycode.make & J_A ) {
                    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    if( keycode.brk & J_EXIT )  return;
                }
                return;
            }
            move_sprite( 24, 104-x, 108 );
            x++;  if( x > 24 )  x=0;
        }
#if 0
        pda_syscall( PDA_SEND_IRDA, &md_cr );
        pda_syscall( PDA_SEND_IRDA, &md_lf );
#else
        pda_syscall( PDA_SEND_IRDA, &md_cnm );
#endif
    }
}

/*--------------------------------------------------------------------------*
 |  change password                                                         |
 *--------------------------------------------------------------------------*/
void data_password()
{
    UBYTE i;
    unsigned char tmp[8];
    struct password pw;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_tiles( 4, 1, 12, 1, data_passw );

    pw.match = 0; /* false */
    pw.password = tmp;
    pda_syscall( PDA_PASSWORD, &pw );
    for( i=0; i<8; i++ ) {
        nvm->password[i] = tmp[i];
    }
}

/*--------------------------------------------------------------------------*
 |  button                                                                  |
 *--------------------------------------------------------------------------*/
struct cursor_info data_cursor0 = { 2, 4, 13, 70, 0, 24 };
UBYTE data_area0( struct data_work *dw )
{
    struct cursor_pos pos;
    struct button btn;

    pos.x = 0; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &data_cursor0 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            btn.no = cursor.y;
            btn.on = 1; // on
            btn.data = b_data+cursor.y*128+64;
            pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
            while( keycode.make & J_A ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( 0 );
            }
            switch( cursor.y ) {
              case 0: /* root */
                dw->folder = dw->y = dw->pos.y = 0;
                set_total_file( dw );
                show_file( dw );
                break;
              case 1: /* load from PC */
                data_load_irda();
                if( keycode.brk & J_EXIT )  return( 0 );
                init_data();
                show_file( dw );
                return( 0 );
              case 2: /* save to PC */
                data_save_irda();
                if( keycode.brk & J_EXIT )  return( 0 );
                init_data();
                show_file( dw );
                return( 0 );
              case 3: /* password */
                data_password();
                init_data();
                show_file( dw );
                return( 0 );
            }
            btn.on = 0; // off
            btn.data = b_data+cursor.y*128;
            pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
        }
        if( cursor.x == 1 ) {
            if( dw->total ) {
                return( 1 );
            } else {
                cursor.x = 0;
            }
        }
    }
    return( 0 );
}

/*--------------------------------------------------------------------------*
 |  main window                                                             |
 *--------------------------------------------------------------------------*/
struct cursor_info data_cursor1 = { 3, 6, 62, 60, 0, 16 };
UBYTE data_area1( struct data_work *dw )
{
    UWORD  n, no;
    struct cursor_pos pos;

    pos.x = 1;  pos.y = dw->pos.y;
    pda_syscall( PDA_SET_CURSOR_MAP, &data_cursor1 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        no = (dw->y)+cursor.y;
        n = max_ram_file - (dw->max_date_tag);
        if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            switch( dw->folder ) {
              case FOLDER_ROOT:
                dw->folder = no+1;
                set_total_file( dw );
                show_file( dw );
                if( (dw->total) == 0 ) {
                    return( 0 );
                }
                break;
              case FOLDER_PHONE:
                if( no >= n ) {
                    no = no - n;
                    pda_syscall( PDA_REVIVAL_ROM_DATA, &no );
                    show_file( dw );
                }
                break;
              case FOLDER_MEMO:
                if( no < max_rom_memo_page ) {
                    no = no + 0x100A;
                } else {
                    no = no - max_rom_memo_page + 0x1000;
                }
                pda_syscall( PDA_REVIVAL_ROM_DATA, &no );
                show_file( dw );
                break;
            }
        } else if( keycode.brk & J_B ) {
            keycode.brk &= ~J_B;
            switch( dw->folder ) {
              case FOLDER_PHONE:
                if( no < n ) {
                    if( (((dw->y)+7)>(dw->total)) && (dw->y) ) {
                        (dw->y)--;
                    }
                    no = no + 0x8000;
                    pda_syscall( PDA_REMOVE_RAM_DATA, &no );
                } else {
                    no = no - n;
                    pda_syscall( PDA_REMOVE_RAM_DATA, &no );
                }
                set_total_file( dw );
                show_file( dw );
                break;
              case FOLDER_MEMO:
                if( no < max_rom_memo_page ) {
                    no = no + 0x100A;
                } else {
                    no = no - max_rom_memo_page + 0x1000;
                }
                pda_syscall( PDA_REMOVE_RAM_DATA, &no );
                show_file( dw );
                break;
              case FOLDER_DATE:
                if( (((dw->y)+7)>(dw->total)) && (dw->y) ) {
                    (dw->y)--;
                }
                no = no + 0x8000 + max_ram_file - (dw->max_date_tag);
                pda_syscall( PDA_REMOVE_RAM_DATA, &no );
                set_total_file( dw );
                show_file( dw );
                break;
              case FOLDER_DRAW:
                if( (((dw->y)+7)>(dw->total)) && (dw->y) ) {
                    (dw->y)--;
                }
                nvm->draw_num = 0;
                set_total_file( dw );
                show_file( dw );
                break;
            }
        }

        if( cursor.y == 0 ) {
            if( (dw->y) > 0 ) {
                cursor.y = 1;
                pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
                (dw->y)--;
                show_file( dw );
            }
        } else if( (cursor.y+1) > (dw->total) ) {
            if( (dw->total) == 0 ) {
                cursor.y = 0;
            } else {
                cursor.y = (dw->total)-1;
            }
            pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
        } else if( cursor.y == 5 ) {
            if( (dw->y) < ((dw->total)-6) ) {
                cursor.y = 4;
                pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
                (dw->y)++;
                show_file( dw );
            }
        }
        if( dw->pos.y != cursor.y ) {
            dw->pos.y = cursor.y;
            show_file( dw );
        }
        if( cursor.x == 0 ) {
            return( 0 );
        } else if( cursor.x == 2 ) {
            if( (dw->total) > 6 ) {
                return( 2 );
            } else {
                cursor.x = 1;
            }
        }
    }
    return( 1 );
}

/*--------------------------------------------------------------------------*
 |  scroll                                                                  |
 *--------------------------------------------------------------------------*/
struct cursor_info data_cursor2 = { 2, 2, 150, 48, 0, 96 };
UBYTE data_area2( struct data_work *dw )
{
    struct cursor_pos pos;

    pos.x = 1; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &data_cursor2 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            if( cursor.y == 0 ) {
                if( (dw->y) > 6 ) {
                    (dw->y) -= 6;
                    if( (dw->pos.y) == 5 ) {
                         dw->pos.y = 4;
                    }
                } else {
                    dw->y = 0;
                }
            } else if( (dw->total) > 6 ) {
                if( ((dw->y)+12) < (dw->total) ) {
                    (dw->y) += 6;
                    if( (dw->pos.y) == 0 ) {
                         dw->pos.y = 1;
                    }
                } else {
                    dw->y = (dw->total)-6;
                }
            }
            show_file( dw );
        }
        if( cursor.x == 0 ) {
            return( 1 );
        }
    }
    return( 2 );
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_data( void *arg )
{
    UBYTE mode;
    struct data_work dw;

    init_data();
    dw.folder = dw.y = dw.pos.y = 0;
    set_total_file( &dw );
    show_file( &dw );

    mode = 1;
    while( !(keycode.brk & J_EXIT) ) { 
        switch( mode ) {
          case 0: /* button */
            mode = data_area0( &dw );
            break;
          case 1: /* main */
            mode = data_area1( &dw );
            break;
          case 2: /* scroll */
            mode = data_area2( &dw );
            break;
        }
    }
}

/* EOF */