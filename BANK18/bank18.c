/****************************************************************************
 *  bank18.c                                                                *
 *                                                                          *
 *    GB-CALC   V2.10    2002/05/19                                         *
 *                                                                          *
 *    Copyright(C)  1998, 2001, 2002   TeamKNOx                             *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"

#include "bank18\i_calc.h"
#include "bank18\s_calc.h"

extern void gb_calc( void *arg );

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_18[FUNC_NUM] = {
  { "GB-CALC ", "2.10", gb_calc, i_calc, i_calcCGBPal0c1, i_calcCGBPal0c2 }
};
struct description desc_18 = { FUNC_NUM, (struct function *)func_18 };

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define CALC_MODE_EXIT    0
#define CALC_MODE_CALC    1
#define CALC_MODE_HEX     2
#define CALC_MODE_FUNC    3

#define MAX_COLUM         15 // displayed: +1(sign)
#define MAX_BUFF          (MAX_COLUM*2)

typedef struct calc_t {
    UBYTE  flag;
    UBYTE  dec_hex;
    struct cursor_pos pos;
    UBYTE  area;
    UBYTE  buff[MAX_BUFF+1];
    UBYTE  reg[MAX_BUFF+1];
    UBYTE  hex[MAX_BUFF+1];
} _calc_t;


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank18\i_calc.c"
#include "bank18\c_calc.c"
#include "bank18\s_calc.c"
#include "bank18\ss_calc.c"

unsigned char calc_head[] = {
    0x00,0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,0x00,
    0x00,0x04,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
    0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x04,0x00,
    0x00,0x04,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,
    0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x04,0x00,
    0x00,0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06,0x00
};
unsigned char calc_head_a[] = {
    0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,
    0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0
};
unsigned char calc_line[] = {
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};
unsigned char calc_line_a[] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

unsigned char calc_break3[] = { 0x10,0x11,0x12,0x13,0x00,0x14,0x15,0x16,0x17 };
unsigned char calc_press3[] = { 0x18,0x19,0x1A,0x1B,0x00,0x1C,0x1D,0x1E,0x1F };
unsigned char calc_button_a[] = { 2,2,2,2,2,2,2,2,2 };
UBYTE calc_lcd_code[] = {
    0x07,0x08,0x09,0x0A,0x04,0x05,0x06,0x0B,
    0x01,0x02,0x03,0x0C,0x00,0x0F,0x0E,0x0D
};

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
struct cursor_info calc_cursor0 = { 5, 4,  30, 72, 24, 24 };
struct cursor_info calc_cursor1 = { 2, 4, 134, 72,  0, 24 };

void calc_disp_lcd( UBYTE *str );

/*--------------------------------------------------------------------------*
 |  calc engine                                                             |
 *--------------------------------------------------------------------------*/
void calc_copy_buff( UBYTE *to, UBYTE *from )
{
    UBYTE i;

    for( i=0; i<=MAX_BUFF; i++ ) {
        *to++ = *from++;
    }
}

void calc_add( UBYTE *a, UBYTE *b )
{
    UBYTE i, *reg, *val, c[MAX_BUFF+1];

    calc_copy_buff( c, b );
    reg = a + MAX_BUFF;
    val = c + MAX_BUFF;
    for( i=0; i<MAX_BUFF; i++ ) {
        *reg = (*reg) + (*val);
        val--;
        if( (*reg) > 9 ) {
            *reg = (*reg) - 10;
            *val = (*val) + 1;
            if( (*reg) > 9 ) {
                *reg = (*reg) - 10;
                *val = (*val) + 1;
            }
        }
        reg--;
    }
}

void calc_sub( UBYTE *a, UBYTE *b )
{
    UBYTE i, sub, *reg, *val, c[MAX_BUFF+1];

    calc_copy_buff( c, b );
    reg = a + MAX_BUFF;
    val = c + MAX_BUFF;
    for( i=0; i<MAX_BUFF; i++ ) {
        sub = (*val); val--;
        if( (*reg) < sub ) {
            *reg = (*reg) + 10;
            *val = (*val) + 1;
        }
        *reg = (*reg) - sub;
        reg--;
    }
}

UBYTE calc_check_abs( UBYTE *a, UBYTE *b )
{
    UBYTE i;

    a++;  b++;
    for( i=0; i<MAX_BUFF; i++ ) {
        if( (*a) != (*b) ) {
            if( (*a) > (*b) ) {
                return( 0 );
            } else {
                return( 1 );
            }
        }
        a++;  b++;
    }
    return( 0 );
}

UBYTE calc_check_sign( UBYTE *a, UBYTE *b )
{
    if( ((*a)&0x80) == ((*b)&0x80) ) {
        return( 1 );
    }
    return( 0 );
}

UBYTE calc_get_ftp_len( UBYTE *a )
{
    UBYTE i;

    i = (*a) & 0x7F; if( i )  i--;
    return( i );
}

UBYTE calc_get_int_len( UBYTE *a )
{
    UBYTE i, fp;

    fp = calc_get_ftp_len( a++ );
    for( i=MAX_BUFF; i>0; i-- ) {
        if( (*a++) ) {
            if( i > fp )  return( i - fp );
            return( 1 );
        }
    }
    return( 1 );
}

UBYTE calc_get_shift_len( UBYTE *a )
{
    UBYTE i, fp;

    a++;
    for( i=MAX_BUFF; i>0; i-- ) {
        if( (*a++) ) {
            return( i );
        }
    }
    return( 1 );
}

void calc_shift_l_buff( UBYTE *a, UBYTE n )
{
    UBYTE i, *b;

    if( !n )  return;
    i = calc_get_ftp_len(a) + 1;
    *a = ((*a) & 0x80) | (i + n);

    a++;  b = a + n;
    for( i=0; i<MAX_BUFF; i++ ) {
        if( i < (MAX_BUFF-n) ) {
            *a++ = *b++;
        } else {
            *a++ = 0x00;
        }
    }
}

void calc_shift_r_buff( UBYTE *a, UBYTE n )
{
    UBYTE i, *b;

    if( !n )  return;
    i = calc_get_ftp_len(a) + 1;
    if( i > n )  i = i - n;
    else         i = 0;
    *a = ((*a) & 0x80) | i;

    a = a + MAX_BUFF;  b = a - n;
    for( i=0; i<MAX_BUFF; i++ ) {
        if( i < (MAX_BUFF-n) ) {
            *a-- = *b--;
        } else {
            *a-- = 0x00;
        }
    }
}

void calc_sort_buff( UBYTE *a )
{
    UBYTE i, a_int, a_ftp, a_len, *b;

    a_int = calc_get_int_len( a );
    a_ftp = calc_get_ftp_len( a );
    a_len = a_int + a_ftp;
    if( a_int > (MAX_COLUM-1) ) {
        calc_shift_r_buff( a, a_ftp );  // overflow
        *a = 0xFF;
        return;
    } else if( a_len > (MAX_COLUM-1) ) {
        calc_shift_r_buff( a, a_len+1-MAX_COLUM ); 
    }

    a_ftp = calc_get_ftp_len( a );
    if( a_ftp ) {
        b = a + MAX_BUFF;
        for( i=0; i<a_ftp; i++ ) {
            if( (*b--) )  break;
        }
        calc_shift_r_buff( a, i ); 
    }
}

void calc_addsub( UBYTE *a, UBYTE *b )
{
    UBYTE i;

    /* make same fp point */
    i = calc_get_ftp_len( a );
    calc_shift_l_buff( a, MAX_COLUM-2-i ); 
    i = calc_get_ftp_len( b );
    calc_shift_l_buff( b, MAX_COLUM-2-i ); 

    /* add/sub check */
    if( calc_check_sign(a,b) ) {
        calc_add( a, b );
    } else if( calc_check_abs(a,b) ) {
        calc_sub( b, a );
        calc_copy_buff( a, b );
    } else {
        calc_sub( a, b );
    }
    calc_sort_buff( a );
}

void calc_mul( UBYTE *a, UBYTE *b )
{
    UBYTE i, j, *p, c[MAX_BUFF+1];

    /* calc fp */
    i = calc_get_ftp_len(a) + calc_get_ftp_len(b);
    if( calc_check_sign(a,b) )  j = 0x00;
    else                        j = 0x80;
    if( i )  i++;
    *a = j | i;

    /* mul */
    calc_copy_buff( c, a ); /* copy a to c */
    p = a + 1;  for( i=0; i<MAX_BUFF; i++ )  *p++ = 0; /* a=0 */
    for( i=0; i<(MAX_COLUM-1); i++ ) {
        for( j=0; j<c[MAX_BUFF-i]; j++ ) {
            calc_add( a, b );
        }
        calc_shift_l_buff( b, 1 ); /* x10 */
    }
    calc_sort_buff( a );
}

void calc_debug( UBYTE *p )
{
    UBYTE  i;
    unsigned char c[MAX_BUFF+1];

    c[0] = *p++;
    c[1] = (c[0]&0x0F) | 0x30; if( c[1] > '9' )  c[1] += 7;
    c[0] = ((c[0]>>4)&0x0F) | 0x30;   if( c[0] > '9' )  c[0] += 7;
    set_bkg_tiles( 0, 0, 2, 1, c );
    for( i=0; i<MAX_BUFF; i++ ) {
      c[i] = (unsigned char)*p++;
      c[i] = (c[i]&0x0F) | 0x30;
    }
    set_bkg_tiles( 3, 0, 16, 1, c );
    while( !joypad() );  waitpadup(); // debug
}

void calc_div( UBYTE *a, UBYTE *b )
{
    UBYTE i, j, *p, c[MAX_BUFF+1];

    /* calc fp */
    i = calc_get_int_len(a) + calc_get_ftp_len(a);
    calc_shift_l_buff( a, MAX_BUFF-1-i );
    j = calc_get_int_len(b) + calc_get_ftp_len(b);
    i = calc_get_shift_len( b );
    if( i != j )  j = calc_get_ftp_len(b) - i + 1;  // 0.000x
    else          j = 0;
    calc_shift_l_buff( b, MAX_BUFF-1-i );

    i = calc_get_ftp_len(a) + calc_get_int_len(b) - j;
    if( calc_check_sign(a,b) )  j = 0x00;
    else                        j = 0x80;
    if( i )  i++;
    *a = j | i;

    /* div */
    calc_copy_buff( c, a ); /* copy a to c */
    p = a + 1;
    for( i=0; i<MAX_BUFF; i++ ) {
        *p = 0;
        while( !calc_check_abs(c,b) ) {
            calc_sub( c, b );
            *p = (*p) + 1;
            if( (*p) > 9 ) {
                *a = 0xFF;
                return; /* div zreo */
            }
        }
        calc_shift_l_buff( c, 1 ); /* x10 */
        p++;
    }
    calc_sort_buff( a );
}

void calc_divmod( UBYTE *a, UBYTE *b )
{
    UBYTE i, *p, c[MAX_BUFF+1];

    /* calc fp */
    i = calc_get_ftp_len(a);
    calc_shift_r_buff( a, MAX_BUFF-1-i );
    i = calc_get_ftp_len(b);
    calc_shift_r_buff( b, MAX_BUFF-1-i );
    i = calc_get_ftp_len(a) + calc_get_int_len(b);
    if( calc_check_sign(a,b) )  *a = 0x00;
    else                        *a = 0x80;

    /* div */
    calc_copy_buff( c, a ); /* copy a to c */
    p = a + 1;
    for( i=0; i<MAX_BUFF; i++ ) {
        *p = 0;
        while( !calc_check_abs(c,b) ) {
            calc_sub( c, b );
            *p = (*p) + 1;
            if( (*p) > 9 ) {
                *a = 0xFF;
                return; /* div zreo */
            }
        }


        calc_shift_l_buff( c, 1 ); /* x10 */
        p++;
    }
    calc_sort_buff( a );
}

/*--------------------------------------------------------------------------*
 |  functions                                                               |
 *--------------------------------------------------------------------------*/
void calc_break( UBYTE no )
{
    UBYTE x, y;
    unsigned char data;

    x = (no%5)*3+2; if( x==14 ) x=15;
    y = (no/5)*3+5;
    data = (unsigned char)(0x30+no);
    set_bkg_tiles( x, y, 3, 3, calc_break3 );
    set_bkg_attribute( x, y, 3, 3, calc_button_a );
    set_sprite_tile( 8+no, data );
    move_sprite( 8+no, x*8+16, y*8+24 );
    set_sprite_prop( 8+no, 2 );
}

void calc_press( UBYTE no )
{
    UBYTE x, y;
    unsigned char data;

    x = (no%5)*3+2; if( x==14 ) x=15;
    y = (no/5)*3+5;
    set_bkg_tiles( x, y, 3, 3, calc_press3 );
    set_sprite_prop( 8+no, 3 );
}

void calc_disp_lcd( UBYTE *str )
{
    UBYTE i, j, k, flag, x, *data;
    unsigned char tmp[2];

    if( (*str) == 0xFF ) { /* error ? */
        tmp[0] = 0xA4;  tmp[1] = 0xA5;  /* - */
        for( i=0; i<=MAX_COLUM; i++ ) {
            set_bkg_tiles( 17+i-MAX_COLUM, 2, 1, 2, tmp );
        }
        return;
    }

    data = str + MAX_COLUM + 2;  flag = 0;  x = 17 - MAX_COLUM;
    k = calc_get_int_len(str) - 1;
    j = k + calc_get_ftp_len(str) + 1;
    tmp[0] = 0x80;  tmp[1] = 0x81;  /* null */
    for( i=0; i<(MAX_COLUM-1-j); i++ ) {
        set_bkg_tiles( x++, 2, 1, 2, tmp );
        data++;
    }
    if( (*str) & 0x80 ) {
        tmp[0] = 0xA4;  tmp[1] = 0xA5;  /* -(sign) */
    }
    set_bkg_tiles( x++, 2, 1, 2, tmp );
    for( i=0; i<j; i++ ) {
        tmp[0] = (*data++)*2+0x82;  tmp[1] = tmp[0]+1;
        set_bkg_tiles( x++, 2, 1, 2, tmp );
        if( i == k ) {
            tmp[0] = 0xA2;  tmp[1] = 0xA3;  /* . */
            set_bkg_tiles( x++, 2, 1, 2, tmp );
        }
    }
}

UBYTE calc_check( UBYTE *str )
{
    UBYTE i, *data;

    data = str + MAX_COLUM + 1;
    for( i=0; i<MAX_COLUM; i++ ) {
        if( *data ) {
            return( 0x80 );
        }
        data++;
    }
    return( (*str) & 0x7F );
}

void calc_init_buff( UBYTE *str )
{
    UBYTE i, *to;

    to = str;
    for( i=0; i<=MAX_BUFF; i++ ) {
        *to++ = 0;
    }
}

void calc_clear( UBYTE *str )
{
    calc_init_buff( str );
    calc_disp_lcd( str );
}

void calc_ins_x( UBYTE *str, UBYTE data )
{
    UBYTE i, *to, *from;

    to   = str + MAX_COLUM + 2;  // safty protection(not +1)
    from = to  + 1;
    if( (*to==0) && (((*str)&0x7F)<(MAX_COLUM-1)) ) { /* check overflow */
        for( i=2; i<MAX_COLUM; i++ ) {
            *to++ = *from++;
        }
        *to = data;
        if( (*str) & 0x7F ) {
            *str = (*str) + 1;
        }
    }
}

void calc_ins( UBYTE *str, UBYTE data )
{
    UBYTE i, *to, *from;

    to   = str + MAX_COLUM + 2;  // safty protection(not +1)
    from = to  + 1;
    if( (*to==0) && (((*str)&0x7F)<(MAX_COLUM-1)) ) { /* check overflow */
        for( i=2; i<MAX_COLUM; i++ ) {
            *to++ = *from++;
        }
        *to = data;
        if( (*str) & 0x7F ) {
            *str = (*str) + 1;
        }
        calc_disp_lcd( str );
    }
}

void calc_del( UBYTE *str )
{
    UBYTE i, *to, *from;

    if( (*str) & 0x7F ) {
        *str = (*str) - 1;
        if( !((*str) & 0x7F) ) {
            return;
        }
    }
    to   = str + MAX_COLUM * 2;
    from = to  - 1;
    for( i=1; i<MAX_COLUM; i++ ) {
        *to-- = *from--;
    }
    *to = 0;
    calc_disp_lcd( str );
}

void calc_sign( UBYTE *str )
{
    if( *str & 0x80 ) {
        *str = (*str) & 0x7F;
    } else {
        *str = (*str) | 0x80;
    }
    calc_disp_lcd( str );
}

void calc_conv_dec2hex( struct calc_t *c )
{
    UBYTE  i, *p;
    UBYTE  a[MAX_BUFF+1], b[MAX_BUFF+1];
    UBYTE  hex[MAX_BUFF+1];

    c->dec_hex = 1;
    calc_init_buff( hex );  /* 17592186044416 */
    calc_ins_x( hex, 1 );  calc_ins_x( hex, 6 );
    calc_shift_r_buff( c->buff, calc_get_ftp_len(c->buff) );
    calc_init_buff( a );

    for( i=0; i<12; i++ ) {

    }
//    calc_disp_lcd( a );
//    calc_copy_buff( c->buff, a );
}

void calc_conv_hex2dec( struct calc_t *c )
{
    UBYTE  i, *p;
    UBYTE  a[MAX_BUFF+1], b[MAX_BUFF+1];
    UBYTE  hex[MAX_BUFF+1];

    c->dec_hex = 0;
    calc_init_buff( hex );
    calc_ins_x( hex, 1 );  calc_ins_x( hex, 6 );
    calc_init_buff( a );
    p = c->buff;  p++;
    for( i=0; i<MAX_BUFF; i++ ) {
        calc_copy_buff( b, hex );
        calc_mul( a, b );
        calc_init_buff( b );
        calc_ins_x( b, *p++ );
        calc_addsub( a, b );
    }
    calc_disp_lcd( a );
    calc_copy_buff( c->buff, a );
}


/*--------------------------------------------------------------------------*
 |  calc loop                                                               |
 *--------------------------------------------------------------------------*/
UBYTE calc_area0( struct calc_t *c )
{ 
    UBYTE  i, lcd;

    pda_syscall( PDA_SET_CURSOR_MAP, &calc_cursor0 );
    pda_syscall( PDA_INIT_CURSOR, &c->pos );

    while( cursor.x != 4 ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )    return( CALC_MODE_EXIT );
        if( keycode.brk & J_SELECT )  return( CALC_MODE_HEX );
        c->pos.x = cursor.x;  c->pos.y = cursor.y;
        lcd = calc_lcd_code[cursor.y*4+cursor.x];
        if( keycode.make & J_A ) {
            calc_press( cursor.y*5+cursor.x );
            while( keycode.make & J_A ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( CALC_MODE_EXIT );
            }
            calc_break( cursor.y*5+cursor.x );
            if( cursor.x < 3 ) {
                if( cursor.y < 3 ) {
                    if( !(c->flag) ) {
                        c->flag = 1;
                        calc_init_buff( c->buff );
                    }
                    calc_ins( c->buff, lcd );
                } else {
                    switch( cursor.x ) {
                      case 0: /* 0 */
                        if( !(c->flag) ) {
                            c->flag = 1;
                            calc_init_buff( c->buff );
                        }
                        if( calc_check(c->buff) )  calc_ins( c->buff, lcd );
                        break;
                      case 1: /* +/- */
                        calc_sign( c->buff );
                        break;
                      case 2: /* .(fp) */
                        if( !(c->flag) ) {
                            c->flag = 1;
                            calc_init_buff( c->buff );
                        }
                        if( ((c->buff[0])&0x7F) == 0 ) {
                            c->buff[0] |= 0x01;
                            calc_disp_lcd( c->buff );
                        }
                        break;
                    }
                }
            } else if( cursor.x == 3 ) {
                /* calc---1 */
                switch( c->flag ) {
                  case 2: /* div */
                    calc_div( c->reg, c->buff );
                    calc_copy_buff( c->buff, c->reg );
                    calc_disp_lcd( c->buff );
                    break;
                  case 3: /* mul */
                    calc_mul( c->buff, c->reg );
                    calc_disp_lcd( c->buff );
                    break;
                  case 4: /* sub */
                    calc_sign( c->buff );
                  case 5: /* add */
                    calc_addsub( c->buff, c->reg );
                    calc_disp_lcd( c->buff );
                    break;
                }
                for( i=0; i<4; i++ )  calc_break( i*5 + 3 );
                if( (c->buff[0]) == 0xFF ) {
                    calc_init_buff( c->buff );
                    c->flag = 0;
                } else {
                    switch( cursor.y ) {
                      case 0: /* div */
                        set_sprite_prop( 11, 4 );
                        c->flag = 2;
                        break;
                      case 1: /* mul */
                        set_sprite_prop( 16, 4 );
                        c->flag = 3;
                        break;
                      case 2: /* sub */
                        set_sprite_prop( 21, 4 );
                        c->flag = 4;
                        break;
                      case 3: /* add */
                        set_sprite_prop( 26, 4 );
                        c->flag = 5;
                        break;
                    }
                }
                calc_copy_buff( c->reg, c->buff );
                calc_init_buff( c->buff );
            }
        }
    }
    c->area  = 1;
    c->pos.x = 1;
    return( CALC_MODE_CALC );
} 

UBYTE calc_area1( struct calc_t *c )
{ 
    UBYTE  i, lcd;

    pda_syscall( PDA_SET_CURSOR_MAP, &calc_cursor1 );
    pda_syscall( PDA_INIT_CURSOR, &c->pos );
    while( cursor.x != 0 ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )    return( CALC_MODE_EXIT );
        if( keycode.brk & J_SELECT )  return( CALC_MODE_HEX );
        c->pos.x = cursor.x;  c->pos.y = cursor.y;
        if( keycode.make & J_A ) {
            calc_press( cursor.y*5+4 );
            while( keycode.make & J_A ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( CALC_MODE_EXIT );
            }
            calc_break( cursor.y*5+4 );
            switch( cursor.y ) {
              case 0: /* CE */
                for( i=0; i<4; i++ )  calc_break( i*5 + 3 );
                c->flag = 1;
                calc_clear( c->buff );
                break;
              case 1: /* C */
                calc_clear( c->buff );
                break;
              case 2: /* BS */
                if( !(c->flag) ) {
                    c->flag = 1;
                    calc_init_buff( c->buff );
                }
                calc_del( c->buff );
                break;
              case 3: /* CALC */
                for( i=0; i<4; i++ )  calc_break( i*5 + 3 );
                switch( c->flag ) {
                  case 2: /* div */
                    calc_div( c->reg, c->buff );
                    calc_copy_buff( c->buff, c->reg );
                    calc_disp_lcd( c->buff );
                    calc_init_buff( c->reg );
                    break;
                  case 3: /* mul */
                    calc_mul( c->buff, c->reg );
                    calc_disp_lcd( c->buff );
                    calc_init_buff( c->reg );
                    break;
                  case 4: /* sub */
                    calc_sign( c->buff );
                  case 5: /* add */
                    calc_addsub( c->buff, c->reg );
                    calc_disp_lcd( c->buff );
                    calc_init_buff( c->reg );
                    break;
                }
                if( (c->buff[0]) == 0xFF )  calc_init_buff( c->buff );
                c->flag = 0;
                break;
            }
        }
    }
    c->area  = 0;
    c->pos.x = 3;
    return( CALC_MODE_CALC );
}

UBYTE calc_loop( struct calc_t *c )
{
    UBYTE  i, mode, lcd;

    /* init screen */
    for( i=5; i<40; i++ )  move_sprite( i, 0, 0 );
    set_sprite_data( 0x30, 20, s_calc );
    for( i=0; i<20; i++ )  calc_break( i );

    switch( c->flag ) {
      case 2: /* div */
        set_sprite_prop( 11, 4 );
        break;
      case 3: /* mul */
        set_sprite_prop( 16, 4 );
        break;
      case 4: /* sub */
        set_sprite_prop( 21, 4 );
        break;
      case 5: /* add */
        set_sprite_prop( 26, 4 );
        break;
    }

    /* loop */
    mode = CALC_MODE_CALC;
    while( mode == CALC_MODE_CALC ) {
        switch( c->area ) {
          case 0:
            mode = calc_area0( c );
            break;
          case 1:
            mode = calc_area1( c );
            break;
        }
    }
    return( mode );
}

/*--------------------------------------------------------------------------*
 |  hex loop                                                                |
 *--------------------------------------------------------------------------*/
UBYTE calc_hex_area0( struct calc_t *c )
{ 
    UBYTE  i, lcd;

    pda_syscall( PDA_SET_CURSOR_MAP, &calc_cursor0 );
    pda_syscall( PDA_INIT_CURSOR, &c->pos );

    while( cursor.x != 4 ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )    return( CALC_MODE_EXIT );
        if( keycode.brk & J_SELECT )  return( CALC_MODE_CALC );
        c->pos.x = cursor.x;  c->pos.y = cursor.y;
        lcd = calc_lcd_code[cursor.y*4+cursor.x];
        if( keycode.make & J_A ) {
            calc_press( cursor.y*5+cursor.x );
            while( keycode.make & J_A ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( CALC_MODE_EXIT );
            }
            calc_break( cursor.y*5+cursor.x );
            if( (cursor.x==0) && (cursor.y==3) ) {
                if( !(c->flag) ) {
                    c->flag = 1;
                    calc_init_buff( c->buff );
                }
                if( calc_check(c->buff) )  calc_ins( c->buff, lcd );
            } else {
                if( !(c->flag) ) {
                    c->flag = 1;
                    calc_init_buff( c->buff );
                }
                calc_ins( c->buff, lcd );
            }
        }
    }
    c->area  = 1;
    c->pos.x = 1;
    return( CALC_MODE_HEX );
} 

UBYTE calc_hex_area1( struct calc_t *c )
{ 
    UBYTE  i, lcd;

    pda_syscall( PDA_SET_CURSOR_MAP, &calc_cursor1 );
    pda_syscall( PDA_INIT_CURSOR, &c->pos );
    while( cursor.x != 0 ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )    return( CALC_MODE_EXIT );
        if( keycode.brk & J_SELECT )  return( CALC_MODE_CALC );
        c->pos.x = cursor.x;  c->pos.y = cursor.y;
        if( keycode.make & J_A ) {
            calc_press( cursor.y*5+4 );
            while( keycode.make & J_A ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( CALC_MODE_EXIT );
            }
            calc_break( cursor.y*5+4 );
            switch( cursor.y ) {
              case 0: /* CE */
                for( i=0; i<4; i++ )  calc_break( i*5 + 3 );
                c->flag = 1;
                calc_clear( c->buff );
                break;
              case 1: /* C */
                calc_clear( c->buff );
                break;
              case 2: /* BS */
                if( !(c->flag) ) {
                    c->flag = 1;
                    calc_init_buff( c->buff );
                }
                calc_del( c->buff );
                break;
              case 3: /* CALC */
                for( i=0; i<4; i++ )  calc_break( i*5 + 3 );
                if( (c->buff[0]) == 0xFF )  calc_init_buff( c->buff );
                c->flag = 0;
                break;
            }
        }
    }
    c->area  = 0;
    c->pos.x = 3;
    return( CALC_MODE_HEX );
}

UBYTE calc_hex_loop( struct calc_t *c )
{
    UBYTE  i, mode;

    /* init screen */
    for( i=5; i<40; i++ )  move_sprite( i, 0, 0 );
    set_sprite_data( 0x30, 20, ss_calc );
    for( i=0; i<20; i++ )  calc_break( i );
    calc_conv_dec2hex( c );

    /* loop */
    mode = CALC_MODE_HEX;
    while( mode == CALC_MODE_HEX ) {
        switch( c->area ) {
          case 0:
            mode = calc_hex_area0( c );
            break;
          case 1:
            mode = calc_hex_area1( c );
            break;
        }
    }
    calc_conv_hex2dec( c );

    return( mode );
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void init_calc( struct calc_t *calc )
{
    UWORD  palette[4];

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_data( 0x80, 64, c_calc );
    set_bkg_tiles( 0, 1, 20, 4, calc_head );
    set_bkg_attribute( 0, 2, 20, 2, calc_head_a );

    palette[3] = (UWORD)CLR_RGB( 0, 0, 0);
    set_sprite_palette( 2, 1, palette );
    if( nvm->button_type == 1 )  palette[3] = ~(nvm->back_color);
    else                         palette[3] = (UWORD)CLR_RGB( 31, 31, 31 );
    set_sprite_palette( 3, 1, palette );
    palette[3] = nvm->fore_color;
    set_sprite_palette( 4, 1, palette );

    calc->flag    = 1;
    calc->dec_hex = 0;
    calc->pos.x   = 0;
    calc->pos.y   = 0;
    calc->area    = 0;
    calc_clear( calc->buff );
}


/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_calc( void *arg )
{
    UBYTE mode;
    struct calc_t calc;

    init_calc( &calc );

    mode = CALC_MODE_CALC;
    while( mode != CALC_MODE_EXIT ) {
        switch( mode ) {
          case CALC_MODE_HEX:
            mode = calc_hex_loop( &calc );
            break;
          case CALC_MODE_CALC:
          default:
            mode = calc_loop( &calc );
            break;
        }
    }
}

/* EOF */