/****************************************************************************
 *  bank10.c                                                                *
 *                                                                          *
 *    GB-PHONE  V2.10    2002/05/04                                         *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank10\i_phone.h"

extern void gb_phone( void *arg );

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank10\i_phone.c"
#include "bank10\b_phone.c"

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_10[] = {
    { "GB-PHONE", "2.10", gb_phone, i_phone, i_phoneCGBPal0c1, i_phoneCGBPal0c2 }
};
struct description desc_10 = { FUNC_NUM, (struct function *)func_10 };

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define PHONE_A2Z	0
#define PHONE_NAME	1
#define PHONE_TOOLS	2
#define MAX_DB_BUFF	24

unsigned char ph_head[] = {
    0x01,0x02,0x09,0x02,0x09,0x02,0x09,0x02,0x09,0x02,
    0x09,0x02,0x09,0x02,0x09,0x02,0x09,0x02,0x03,0x0E
};
unsigned char ph_tag[] = {
    0x07,0x02,0x0A,0x02,0x0A,0x02,0x0A,0x02,0x0A,0x02,
    0x0A,0x02,0x0A,0x02,0x0A,0x02,0x0A,0x02,0x0A,0x03
};
unsigned char ph_line[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x07,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x08
};
unsigned char ph_tail[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char ph_btn[]  = {
    0x00,0x00,0x80,0x81,0x82,0x89,0x8A,0x8B,0x92,0x93,
    0x94,0x9B,0x9C,0x9D,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x83,0x84,0x85,0x8C,0x8D,0x8E,0x95,0x96,
    0x97,0x9E,0x9F,0xA0,0x00,  60,  48,  48,  62,0x00,
    0x00,0x00,0x86,0x87,0x88,0x8F,0x90,0x91,0x98,0x99,
    0x9A,0xA1,0xA2,0xA3,0x00,0x00,0x00,0x00,0x00,0x00
};
unsigned char ph_tag_a[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2 };
unsigned char ph_ln1_a[] = { 0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0 };
unsigned char ph_btn_a[] = { 0,0,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0 };
unsigned char a_tile[]    = {
    0x04,0x00,0x04,0x41,0x04,0x42,0x04,0x43,0x04,0x44,
    0x04,0x45,0x04,0x46,0x04,0x47,0x04,0x48,0x04,0x0F
};
unsigned char i_tile[]    = {
    0x04,0x49,0x04,0x4A,0x04,0x4B,0x04,0x4C,0x04,0x4D,
    0x04,0x4E,0x04,0x4F,0x04,0x50,0x04,0x51,0x04,0x0F
};
unsigned char r_tile[]    = {
    0x04,0x52,0x04,0x53,0x04,0x54,0x04,0x55,0x04,0x56,
    0x04,0x57,0x04,0x58,0x04,0x59,0x04,0x5A,0x04,0x0F
};
unsigned char *tag_tile[] = { a_tile, i_tile, r_tile };
unsigned char tag_clr0[]  = { 0x04,0x00,0x05 };
unsigned char tag_clr1[]  = { 0x06,0x00,0x05 };

/* edit screen */
unsigned char ed_head0[] = { "DATA ENTRY" };
unsigned char ed_head1[] = {
    0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x4E,0x61,
    0x6D,0x65,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03
};
unsigned char ed_head2[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char ed_head3[] = {
    0x01,0x02,0x02,0x02,0x50,0x68,0x6F,0x6E,0x65,0x20,
    0x4E,0x75,0x6D,0x62,0x65,0x72,0x02,0x02,0x02,0x03
};


/*==========================================================================*
 |  sub-routines                                                            |
 *==========================================================================*/
void show_phone_tag( UBYTE t_page, UBYTE t_x )
{
    set_bkg_tiles( 0, 1, 20, 1, tag_tile[t_page] );
    set_bkg_tiles( 0, 2, 20, 1, ph_tag );
    if( t_x ) {
        set_bkg_tiles( t_x*2, 2, 3, 1, tag_clr1 );
    } else {
        set_bkg_tiles( 0, 2, 3, 1, tag_clr0 );
    }
}

void show_phone_page( UBYTE page )
{
    unsigned char tmp_buff[1];

    tmp_buff[0] = 0x30 + page;
    set_bkg_tiles( 17, 16, 1, 1, tmp_buff );
}

void show_phone_line( UWORD *table )
{
    UBYTE i;
    struct search_phone phone;

    for( i=0; i<6; i++ ) {
        phone.tag = i*2+3;
        phone.table = table++;
        pda_syscall( PDA_SHOW_PHONE_LINE, &phone );
    }
}

void init_phone()
{
    UBYTE i;
    struct button btn;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

    /* setup bkg data */
    btn.no = 0;
    btn.on = 0; // off
    btn.data = b_phone;
    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
    btn.no = 1;
    btn.data = b_phone+128;
    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
    btn.no = 2;
    btn.data = b_phone+256;
    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
    btn.no = 3;
    btn.data = b_phone+384;
    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );

    set_bkg_tiles( 0, 0, 20, 1, ph_head );
    set_bkg_attribute( 0, 0, 20, 1, ph_tag_a );
    set_bkg_attribute( 0, 1, 20, 1, ph_tag_a );
    for( i=0; i<5; i++ ) {
        set_bkg_tiles( 0, i*2+3, 20, 2, ph_line );
        set_bkg_attribute( 0, i*2+3, 20, 1, ph_ln1_a );
    }
    set_bkg_tiles( 0, 13, 20, 2, ph_tail );
    set_bkg_attribute( 0, 13, 20, 1, ph_ln1_a );
    set_bkg_tiles( 0, 15, 20, 3, ph_btn );
    set_bkg_attribute( 0, 15, 20, 1, ph_btn_a );
    set_bkg_attribute( 0, 16, 20, 1, ph_btn_a );
    set_bkg_attribute( 0, 17, 20, 1, ph_btn_a );

    pda_syscall( PDA_PROBE_DATA_BASE, (void *)0 );
    pda_syscall( PDA_INIT_DIAL, (void *)0 );
}

void init_edit()
{
    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_tiles( 5,  1, 10, 1, ed_head0 );
    set_bkg_tiles( 0,  3, 20, 1, ed_head1 );
    set_bkg_tiles( 0,  4, 20, 2, ed_head2 );
    set_bkg_attribute( 0, 4, 20, 1, ph_ln1_a );
    set_bkg_tiles( 0,  7, 20, 1, ed_head3 );
    set_bkg_tiles( 0,  8, 20, 2, ed_head2 );
    set_bkg_attribute( 0, 8, 20, 1, ph_ln1_a );
}

/*==========================================================================*
 |  data base edit routine                                                  |
 *==========================================================================*/
void edit_file( unsigned char tag, UWORD no )
{
    UBYTE i;
    struct db_file data;
    struct input_string input_string;
    struct phone_number pn;

    init_edit();

    /* set strings */
    if( no != 0xFFFF ) {
        pn.no = no;
        pn.str = data.name;
        pda_syscall( PDA_GET_PHONE_NAME, &pn );
        set_bkg_tiles( 1, 4, 18, 1, data.name );
        pn.str = data.phone;
        pda_syscall( PDA_GET_PHONE_NUMBER, &pn );
        set_bkg_tiles( 1, 8, 18, 1, data.phone );
        pda_syscall( PDA_REMOVE_RAM_DATA, &no );
    } else {
        for( i=0; i<18; i++ ) {
            data.name[i]  = 0;
            data.phone[i] = 0;
        }
        data.name[0] = tag;
    }

    /* get name field */
    input_string.x = 1;
    input_string.y = 4;
    input_string.max = 18;
    input_string.mode = 0;
    input_string.data = data.name;
    input_string.def_x = 0;
    for( i=0; i<18; i++ ) {
        if( data.name[i] ) {
            input_string.def_x = i+1;
        }
    }
    pda_syscall( PDA_INPUT_STRING, &input_string );
    if( data.name[0] == 0 ) return;

    /* get number field */
    caps_lock = 0;  keypad_pos.x = 6;  keypad_pos.y = 0; /* '7' */
    input_string.x = 1;
    input_string.y = 8;
    input_string.max = 18;
    input_string.mode = 0x04;
    input_string.data = data.phone;
    input_string.def_x = 0;
    for( i=0; i<18; i++ ) {
        if( data.phone[i] ) {
            input_string.def_x = i+1;
        }
    }
    pda_syscall( PDA_INPUT_STRING, &input_string );
    if( data.phone[0] == 0 ) return;

    /* set ram data base */
    pda_syscall( PDA_SET_RAM_DATA, &data );
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
struct cursor_info phone1_cursor = { 10,  2, 14, 28, 16,  0 };
struct cursor_info phone2_cursor = {  2,  8, 14, 28, 52, 16 };
struct cursor_info phone3_cursor = {  6,  2, 30,144, 24,  0 };

void gb_phone( void *arg )
{
    UBYTE i, y;
    UBYTE y_mode, mode, page;
    UBYTE t_page, t_x;
    unsigned char str_buff[20];
    UWORD db_table[MAX_DB_BUFF];
    struct cursor_pos pos;
    struct button btn;
    struct search_phone phone;
    struct phone_number number;

    init_phone();
    mode = 0;
    btn.no = mode;
    btn.on = 1; // on;
    btn.data = b_phone+mode*128+64;
    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );

    /* 1st print */
    t_page = t_x = 0;
    show_phone_tag( t_page, t_x );

    phone.tag = (t_page*9 + t_x +0x40);
    phone.table = db_table;
    phone.max = MAX_DB_BUFF;
    pda_syscall( PDA_SEARCH_PHONE_TAG, &phone );
    pda_syscall( PDA_SORT_PHONE_NAME, &phone );

    page = 0;
    show_phone_page( page );
    show_phone_line( &db_table[page*6] );

    y_mode = PHONE_A2Z;
    while( 1 ) {
        switch( y_mode ) {
          case PHONE_A2Z:
            pos.x = t_x;  pos.y = 0;
            pda_syscall( PDA_SET_CURSOR_MAP, &phone1_cursor );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( cursor.y == 0 ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return;
                if( keycode.brk & J_A ) {
                    keycode.brk &= ~J_A;
                    if( cursor.x == 9 ) {
                        t_page++; if( t_page>2 )  t_page = 0;
                    } else {
                        t_x = cursor.x;
                    }
                    show_phone_tag( t_page, t_x );
                    phone.tag = (t_page*9 + t_x +0x40);
                    phone.table = db_table;
                    phone.max = MAX_DB_BUFF;
                    pda_syscall( PDA_SEARCH_PHONE_TAG, &phone );
                    pda_syscall( PDA_SORT_PHONE_NAME, &phone );
                    page = 0;
                    show_phone_page( page );
                    show_phone_line( &db_table[page*6] );
                }
            }
            y_mode = PHONE_NAME;
            y = 1;
            break;
          case PHONE_NAME:
            pos.x = 1;  pos.y = y;
            pda_syscall( PDA_SET_CURSOR_MAP, &phone2_cursor );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( (cursor.y >0) && (cursor.y < 7) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return;
                y = cursor.y;
                if( keycode.brk & J_A ) {
                    keycode.brk &= ~J_A;
                    if( mode == 3 ) {
                        pos.x = cursor.x;  pos.y = y;
                        edit_file( phone.tag, db_table[page*6+y-1] );
                        init_phone();
                        pda_syscall( PDA_SET_CURSOR_MAP, &phone2_cursor );
                        pda_syscall( PDA_INIT_CURSOR, &pos );
                        btn.no = mode;
                        btn.on = 1; // on;
                        btn.data = b_phone+mode*128+64;
                        pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
                        show_phone_tag( t_page, t_x );
                        show_phone_page( page );
                        phone.tag = (t_page*9 + t_x +0x40);
                        phone.table = db_table;
                        phone.max = MAX_DB_BUFF;
                        pda_syscall( PDA_SEARCH_PHONE_TAG, &phone );
                        pda_syscall( PDA_SORT_PHONE_NAME, &phone );
                        show_phone_line( &db_table[page*6] );
                    } else if( db_table[page*6+y-1] < 0xFF00 ) {
                        number.mode = mode;
                        number.no = db_table[page*6+y-1];
                        number.str = str_buff;
                        pda_syscall( PDA_MAKE_DIAL_NUMBER, &number );
                        set_bkg_tiles( 1, y*2+1, 18, 1, str_buff );
                        pda_syscall( PDA_PDC_DIALING, str_buff );
                        pda_syscall( PDA_DIALTONE, str_buff );
                    }
                } else if( keycode.brk & J_B ) {
                    keycode.brk &= ~J_B;
                    phone.tag = y*2+1;
                    phone.table = &db_table[page*6+y-1];
                    if( mode == 3 ) {
                        pda_syscall( PDA_REMOVE_RAM_DATA, phone.table );
                        phone.tag = (t_page*9 + t_x +0x40);
                        phone.table = db_table;
                        phone.max = MAX_DB_BUFF;
                        pda_syscall( PDA_SEARCH_PHONE_TAG, &phone );
                        pda_syscall( PDA_SORT_PHONE_NAME, &phone );
                        show_phone_line( &db_table[page*6] );
                    } else {
                        pda_syscall( PDA_SHOW_PHONE_LINE, &phone );
                        pda_syscall( PDA_PDC_ON_HOOK, (void *)0 );
                    }
                }
            }
            if( cursor.y == 0 )  y_mode = PHONE_A2Z;
            else                 y_mode = PHONE_TOOLS;
            break;
          case PHONE_TOOLS:
            pos.x = mode;  pos.y = 1;
            pda_syscall( PDA_SET_CURSOR_MAP, &phone3_cursor );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( cursor.y != 0 ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return;
                if( keycode.brk & J_A ) {
                    keycode.brk &= ~J_A;
                    if( cursor.x > 3 ) {
                        if( (cursor.x==4) && (page>0) )       page--;
                        else if( (cursor.x==5) && (page<3) )  page++;
                        show_phone_page( page );
                        show_phone_line( &db_table[page*6] );
                    } else if( cursor.x != mode ) {
                        btn.no = mode;
                        btn.on = 0; // off;
                        btn.data = b_phone+mode*128;
                        pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
                        mode = cursor.x;
                        btn.no = mode;
                        btn.on = 1; // on;
                        btn.data = b_phone+mode*128+64;
                        pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
                    }
                }
            }
            y_mode = PHONE_NAME;
            y = 6;
            break;
        }
    }
}

/* EOF */