@echo off

rem GB-PDA SYSTEM ROUTINE
..\bin\lcc -Wa-l -c -o fix.o fixed\fixed.c
..\bin\lcc -Wa-l -c -o kb.o fixed\kb.s
..\bin\lcc -Wa-l -c -o rec.o fixed\recrem.s
..\bin\lcc -Wa-l -c -o p00.o fixed\playrm00.s
..\bin\lcc -Wa-l -c -o p33.o fixed\playrm33.s
..\bin\lcc -Wa-l -c -o p35.o fixed\playrm35.s
..\bin\lcc -Wa-l -c -o p38.o fixed\playrm38.s
..\bin\lcc -Wa-l -c -o p40.o fixed\playrm40.s
..\bin\lcc -Wa-l -c -o p44.o fixed\playrm44.s
..\bin\lcc -Wa-l -c -o p48.o fixed\playrm48.s
..\bin\lcc -Wa-l -c -o p52.o fixed\playrm52.s
..\bin\lcc -Wa-l -c -o irs.o fixed\irda_snd.s
..\bin\lcc -Wa-l -c -o irr.o fixed\irda_rcv.s
..\bin\lcc -Wa-l -c -o rs.o  fixed\gb232.s
..\bin\lcc -Wa-l -Wf-ba0 -c -o b00.o bank00\bank00.c
..\bin\lcc -Wa-l -Wf-bo1 -c -o b01.o bank01\bank01.c
..\bin\lcc -Wa-l -Wf-bo2 -c -o b02.o bank02\bank02.c
..\bin\lcc -Wa-l -Wf-bo3 -c -o b03.o bank03\bank03.c
..\bin\lcc -Wa-l -Wf-bo4 -c -o b04.o bank04\bank04.c
..\bin\lcc -Wa-l -Wf-bo5 -c -o b05.o bank05\bank05.c

rem DATA BASE
..\bin\lcc -Wa-l -Wf-bo6 -c -o b06.o bank06\bank06.c
..\bin\lcc -Wa-l -Wf-bo7 -DUSER_DATA -c -o b07.o bank07\bank07.c

rem GB-PDA APPs
..\bin\lcc -Wa-l -Wf-bo8 -c -o b08.o bank08\bank08.c
..\bin\lcc -Wa-l -Wf-bo9 -c -o b09.o bank09\bank09.c
..\bin\lcc -Wa-l -Wf-bo10 -c -o b10.o bank10\bank10.c
..\bin\lcc -Wa-l -Wf-bo11 -c -o b11.o bank11\bank11.c
..\bin\lcc -Wa-l -Wf-bo12 -c -o b12.o bank12\bank12.c
..\bin\lcc -Wa-l -Wf-bo13 -c -o b13.o bank13\bank13.c
..\bin\lcc -Wa-l -Wf-bo14 -c -o b14.o bank14\bank14.c
..\bin\lcc -Wa-l -Wf-bo15 -c -o b15.o bank15\bank15.c
..\bin\lcc -Wa-l -Wf-bo16 -c -o b16.o bank16\bank16.c
..\bin\lcc -Wa-l -Wf-bo17 -c -o b17.o bank17\bank17.c
..\bin\lcc -Wa-l -Wf-bo18 -c -o b18.o bank18\bank18.c
..\bin\lcc -Wa-l -Wf-bo19 -c -o b19.o bank19\bank19.c
..\bin\lcc -Wa-l -Wf-bo20 -c -o b20.o bank20\bank20.c
..\bin\lcc -Wa-l -Wf-bo21 -c -o b21.o bank21\bank21.c
..\bin\lcc -Wa-l -Wf-bo22 -c -o b22.o bank22\bank22.c
..\bin\lcc -Wa-l -Wf-bo23 -c -o b23.o bank23\bank23.c
..\bin\lcc -Wa-l -Wf-bo24 -c -o b24.o bank24\bank24.c
..\bin\lcc -Wa-l -Wf-bo25 -c -o b25.o bank25\bank25.c

rem USERS APPs
..\bin\lcc -Wa-l -Wf-bo26 -c -o b26.o bank26\bank26.c
..\bin\lcc -Wa-l -Wf-bo27 -c -o b27.o bank27\bank27.c
..\bin\lcc -Wa-l -Wf-bo28 -c -o b28.o bank28\bank28.c
..\bin\lcc -Wa-l -Wf-bo29 -c -o b29.o bank29\bank29.c
..\bin\lcc -Wa-l -Wf-bo30 -c -o b30.o bank30\bank30.c
..\bin\lcc -Wa-l -Wf-bo31 -c -o b31.o bank31\bank31.c

rem LINK
..\bin\lcc -Wl-m -Wl-yt3 -Wl-yo32 -Wl-ya1 -Wl-yp0x143=0x80 -o pda.gb @pda.l

rem CLEAN
del *.lst
del *.o
rem del *.map
