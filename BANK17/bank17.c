/****************************************************************************
 *  bank17.c                                                                *
 *                                                                          *
 *    NAMIHEY  V2.01    2000/01/27                                          *
 *                                                                          *
 *    Copyright(C)  1999, 2000   TeamKNOx                                   *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank17\i_gunpey.h"

#define GUNPEY_MODE   0     /* 0:Namihey, 1:Gunpey */

extern void gb_gunpey();


/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM      1
struct function func_17[FUNC_NUM] = {
    { "NAMIHEY ", "2.01", gb_gunpey, i_gunpey, i_gunpeyCGBPal0c1, i_gunpeyCGBPal0c2 }
};
struct description desc_17 = { FUNC_NUM, (struct function *)func_17 };


/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define MODE_EXIT     0
#define MODE_MENU     1
#define MODE_ENDLESS  2

/* panel condition */
#define PANEL_0       0x00  /* non line */
#define PANEL_1       0x01  /* down left, up right */
#define PANEL_2       0x02  /* up       , down     */
#define PANEL_3       0x04  /* up       , up       */
#define PANEL_4       0x08  /* down     , down     */
#define PANEL_FLASH   0x10  /* flash(rainbow) bit */
#define PANEL_BLINK   0x80  /* blink bit */

/* flag condition */
#define NONCHECK      0
#define SKIP          1
#define CONNECT       2

/* timer value */
#if GUNPEY_MODE /* hard mode (gunpey) */
#define SHIFTUP_TIME  350UL  /* first speed */
#define MAX_SHIFTUP   120UL  /* max speed */
#define FLASH_TIME    300UL  /* fixed */
#define BLINK_TIME    120UL  /* fixed */
#define RAINBOW_TIME  2UL    /* fixed */
#define BLINK_CYCLE   0x08UL /* fixed */
#define CHECK_DELAY   4UL    /* connect_chk() delay */
#else           /* easy mode (namihey) */
#define SHIFTUP_TIME  500UL  /* first speed */
#define MAX_SHIFTUP   150UL  /* max speed */
#define FLASH_TIME    350UL  /* fixed */
#define BLINK_TIME    180UL  /* fixed */
#define RAINBOW_TIME  4UL    /* fixed */
#define BLINK_CYCLE   0x10UL /* fixed */
#define CHECK_DELAY   6UL    /* connect_chk() delay */
#endif

/* work area */
typedef struct gunpey_t {
  UBYTE mode;
  UBYTE cursor_x;
  UBYTE cursor_y;
  UBYTE keycode_flag;
  UBYTE keycode_off;
  UWORD panel_total;
  UWORD score_total;
  UWORD s_timer;
  UWORD b_timer;
  UWORD f_timer;
  UWORD r_timer;
  UWORD s_level;
  UWORD bkg_rainbow[4];
  UWORD rainbow;
  UBYTE *panel;  /* [7][12] */
  UBYTE *panel2; /* [7][12] */
  UBYTE *flag;   /* [7][12] */
} _gunpey_t;

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank17\obj.h"
#include "bank17\i_gunpey.c"
#include "bank17\obj.c"
#include "bank17\ank_char.c"

/* panel design */
#if GUNPEY_MODE
#include "bank17\p151.h"
#include "bank17\p151.c" /* gunpey style */
#else
#include "bank17\p15.h"
#include "bank17\p15.c"  /* namihey style */
#endif

/* palette data */
UWORD bkg_palette[] = {
    p15CGBPal1c0, p15CGBPal1c1, p15CGBPal1c2, p15CGBPal1c3,
    p15CGBPal6c0, p15CGBPal6c1, p15CGBPal6c2, p15CGBPal6c3,
    p15CGBPal7c0, p15CGBPal7c1, p15CGBPal7c2, p15CGBPal7c3
};
UWORD rainbow_palette[] = {
    0x001F, 0x00FF, 0x01FF, 0x02FF, /* red     -> yellow-  */
    0x03FF, 0x03F7, 0x03EF, 0x03E7, /* yellow  -> green-   */
    0x03E0, 0x1FE0, 0x3FE0, 0x5FE0, /* green   -> cyan-    */
    0x7FE0, 0x7EE0, 0x7DE0, 0x7CE0, /* cyan    -> blue-    */
    0x7C00, 0x7C07, 0x7C0F, 0x7C17, /* blue    -> magenta- */
    0x7C1F, 0x5C1F, 0x3C1F, 0x1C1F  /* magenta -> red-     */
};
UWORD obj_palette[] = {
    objCGBPal0c0, objCGBPal0c1, objCGBPal0c2, objCGBPal0c3,
    objCGBPal1c0, objCGBPal1c1, objCGBPal1c2, objCGBPal1c3
};

/* demo data */
unsigned char demo_panel[6][9] = {
    { 1, 2, 0, 8, 1, 0, 0, 0, 0 }, /* G */
    { 0, 0, 2, 0, 2, 1, 0, 0, 0 }, /* U */
    { 0, 1, 2, 0, 2, 0, 0, 0, 0 }, /* N */
    { 1, 2, 0, 2, 1, 2, 0, 0, 0 }, /* P */
    { 1, 2, 0, 0, 1, 4, 0, 0, 0 }, /* E */
    { 0, 2, 4, 0, 0, 2, 0, 0, 0 }  /* Y */
};
UBYTE demo_x[6] = { 1, 2, 4, 1, 3, 4 };
UBYTE demo_y[6] = { 5, 2, 1, 8, 7, 4 };

/* wall pattern */
#if GUNPEY_MODE
unsigned char gpy_head[]  = {
    0x8D,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8F,
    0x81,0x82,0x83,0x84,0x82,0x83,0x84,0x82,0x83,0x84,0x82,0x85,
};
#else
unsigned char gpy_head[]  = {
    0x8D,0x8E,0x8F,0x8D,0x8E,0x8F,0x8D,0x8E,0x8F,0x8D,0x8E,0x8F,
    0x81,0x82,0x83,0x84,0x82,0x83,0x84,0x82,0x83,0x84,0x82,0x85,
};
#endif
unsigned char gpy_line[] = {
    0x86,0x96,0x99,0x96,0x99,0x96,0x99,0x96,0x99,0x96,0x99,0x87,
    0x86,0x9C,0xA0,0x9C,0xA0,0x9C,0xA0,0x9C,0xA0,0x9C,0xA0,0x87,
    0x86,0x90,0x93,0x90,0x93,0x90,0x93,0x90,0x93,0x90,0x93,0x87
};
unsigned char gpy_tail[] = {
    0x88,0x89,0x8A,0x8B,0x89,0x8A,0x8B,0x89,0x8A,0x8B,0x89,0x8C
};
unsigned char gpy_head_a[] = { 7,7,7,7,7,7,7,7,7,7,7,7 };
unsigned char gpy_line_a[] = { 7,1,1,1,1,1,1,1,1,1,1,7 };

/* messages */
unsigned char gmsg_score[] = { "SCORE" };
unsigned char gmsg_panel[] = { "PANEL" };
unsigned char gmsg_top[]   = { "TOP" };
unsigned char gmsg_clr[]   = { "                    " };
unsigned char gmsg_0[]     = { "0" };
unsigned char gmsg_zzz[]   = { "!" };  /* for debug */
unsigned char gmsg_over[]  = {  0, 1, 2, 3, 4, 5, 3, 6 };
unsigned char gmsg_bonus[] = {  7, 8, 3, 1, 6, 9,10,10,10 };
unsigned char gmsg_pause[] = { 11, 1,12,13, 3 };

/* tables for route checking */
BYTE  route1_x[4] = { -1,  0,  0,  1 }; 
BYTE  route1_y[4] = {  0, -1,  1,  0 };
UBYTE route1_p[4] = {  1,  3,  0,  2 };
BYTE  route2_x[4] = { -1,  1, -1,  1 }; 
BYTE  route2_y[4] = { -1, -1,  1,  1 };
UBYTE route2_p[4] = {  3,  2,  1,  0 };
BYTE  route3_x[4] = {  0,  1, -1,  0 }; 
BYTE  route3_y[4] = { -1,  0,  0,  1 };
UBYTE route3_p[4] = {  2,  0,  3,  1 };
UBYTE new_p[4][5] = {
   { 0, 0, 4, 2, 0 }, /* p=0 */
   { 0, 3, 0, 1, 0 }, /* p=1 */
   { 0, 2, 0, 0, 4 }, /* p=2 */
   { 0, 0, 1, 0, 3 }  /* p=3 */
};

/* tables for x1.5 display driver */
int pan_tbl[32] = { 0, 1, 2, 0, 3, 0, 0, 0, 4, 0,0,0,0,0,0,0,
                    0, 5, 6, 0, 7, 0, 0, 0, 8, 0,0,0,0,0,0,0 };
unsigned char lower_left[9]  = {
    0x90,0x91,0x92,0x92,0x91,0xA4,0xA5,0xA5,0xA4 };
unsigned char lower_right[9] = {
    0x93,0x94,0x95,0x94,0x95,0xA6,0xA7,0xA6,0xA7 };
unsigned char upper_left[9]  = {
    0x96,0x97,0x98,0x98,0x97,0xA8,0xA9,0xA9,0xA8 };
unsigned char upper_right[9] = {
    0x99,0x9A,0x9B,0x9A,0x9B,0xAA,0xAB,0xAA,0xAB };
unsigned char midle_left[81]  = {
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 0 */
    0x9D,0x9D,0x9F,0x9F,0x9D,0x9D,0xB2,0xB2,0x9D,   /* 1 */
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 2 */
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 3 */
    0x9D,0x9D,0x9F,0x9F,0x9D,0x9D,0xB2,0xB2,0x9D,   /* 4 */
    0xAC,0xAC,0xB3,0xB3,0xAC,0xAC,0xAE,0xAE,0xAC,   /* 5 */
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 6 */
    0x9C,0x9C,0x9E,0x9E,0x9C,0x9C,0xAD,0xAD,0x9C,   /* 7 */
    0xAC,0xAC,0xB3,0xB3,0xAC,0xAC,0xAE,0xAE,0xAC }; /* 8 */
unsigned char midle_right[81] = {
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 0 */
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 1 */
    0xA1,0xA3,0xA1,0xA3,0xA1,0xB4,0xA1,0xB4,0xA1,   /* 2 */
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 3 */
    0xA1,0xA3,0xA1,0xA3,0xA1,0xB4,0xA1,0xB4,0xA1,   /* 4 */
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 5 */
    0xAF,0xB5,0xAF,0xB5,0xAF,0xB1,0xAF,0xB1,0xAF,   /* 6 */
    0xA0,0xA2,0xA0,0xA2,0xA0,0xB0,0xA0,0xB0,0xA0,   /* 7 */
    0xAF,0xB5,0xAF,0xB5,0xAF,0xB1,0xAF,0xB1,0xAF }; /* 8 */


/*==========================================================================*
 |  x1.5 panel display drivers                                              |
 *==========================================================================*/
void x15driver( struct gunpey_t *gp, int x, int y )
{
  int xx, yy, pan;
  unsigned char tiles[4];

  xx = x * 2 - 1;
  yy = ((y-1)*3)/2+2;
  pan = pan_tbl[*((gp->panel)+x+y*7)&0x1F];

  if( y % 2 ) {
    tiles[0] = upper_left[pan];
    tiles[1] = upper_right[pan];
    pan = pan * 9 + pan_tbl[*((gp->panel)+x+(y+1)*7)&0x1F];
    tiles[2] = midle_left[pan];
    tiles[3] = midle_right[pan];
    set_bkg_tiles( xx, yy, 2, 2, tiles );
  } else {
    tiles[2] = lower_left[pan];
    tiles[3] = lower_right[pan];
    pan = pan + pan_tbl[*((gp->panel)+x+(y-1)*7)&0x1F] * 9;
    tiles[0] = midle_left[pan];
    tiles[1] = midle_right[pan];
    set_bkg_tiles( xx, yy, 2, 2, tiles );
  }
}

int obj_x15( struct gunpey_t *gp, int x, int y, int n )
{
  int xx, yy, pan;
  unsigned char tiles[4];

  if( n > 38L )  return( 40L ); /* overflow */
  xx = (x*2-1)*8 + 7;
  yy = (((y-1)*3)/2+2)*8 + 15;
  pan = pan_tbl[*((gp->panel2)+x+y*7)&0x1F];

  if( y % 2 ) {
    tiles[0] = upper_left[pan];
    tiles[1] = upper_right[pan];
    set_sprite_tile( n  , tiles[0]-0x80 );
    set_sprite_tile( n+1, tiles[1]-0x80 );
    move_sprite( n  , xx,   yy );
    move_sprite( n+1, xx+8, yy );
    n += 2L;
    if( n > 38L )  return( 40L );
    if( *((gp->panel2)+x+(y+1)*7) == 0 ) {
      pan = pan * 9;
      tiles[2] = midle_left[pan];
      tiles[3] = midle_right[pan];
      set_sprite_tile( n  , tiles[2]-0x80 );
      set_sprite_tile( n+1, tiles[3]-0x80 );
      move_sprite( n  , xx,   yy+8 );
      move_sprite( n+1, xx+8, yy+8 );
      n += 2L;
    }
  } else {
    tiles[2] = lower_left[pan];
    tiles[3] = lower_right[pan];
    pan = pan + pan_tbl[*((gp->panel2)+x+(y-1)*7)&0x1F] * 9;
    tiles[0] = midle_left[pan];
    tiles[1] = midle_right[pan];
    set_sprite_tile( n  , tiles[0]-0x80 );
    set_sprite_tile( n+1, tiles[1]-0x80 );
    move_sprite( n  , xx,   yy );
    move_sprite( n+1, xx+8, yy );
    if( n > 36L )  return( 40L );
    set_sprite_tile( n+2, tiles[2]-0x80 );
    set_sprite_tile( n+3, tiles[3]-0x80 );
    move_sprite( n+2, xx,   yy+8 );
    move_sprite( n+3, xx+8, yy+8 );
    n += 4L;
  }
  return( n );
}

void cls_obj( int n )
{
  int i;

  for( i=n; i<40; i++ ) {
    move_sprite( i, 0, 0 );
  }
}

/*==========================================================================*
 |  display value (dword)                                                   |
 *==========================================================================*/
void disp_value( UBYTE x, UBYTE y, UWORD d )
{
  UBYTE i, f;
  unsigned char data[6];

  for( i=0; i<6; i++ ) {
    data[5-i] = d % 10;
    d = d / 10;
  }
  f = 0;
  for( i=0; i<6; i++ ) {
    if( f || data[i] ) {
      f = 1;
      data[i] += '0';
    } else {
      data[i] = ' ';
    }
  }
  set_bkg_tiles( x, y, 6, 1, data );
}

/*==========================================================================*
 |  panel controls                                                          |
 *==========================================================================*/
void disp_panel( struct gunpey_t *gp )
{
  UBYTE x, y;
  unsigned char tiles[2];

  for( x=1; x<6; x++ ) {
    tiles[0] = tiles[1] = x;
    for( y=2; y<17; y++ ) {
      set_bkg_attribute( 11-x*2, y, 2, 1, tiles );
    }
    for( y=1; y<11; y++ ) {
      x15driver( gp, 6-x, y );
    }
  }
}

void disp_panel2( struct gunpey_t *gp )
{
  UBYTE x, y;
  int   n;

  n = 6L;
  for( y=1; y<11; y++ ) {
    for( x=1; x<6; x++ ) {
      if ( *((gp->panel2)+x+y*7) & 0x1F ) {
        n = obj_x15( gp, x, y, n );
      }
    }
  }
  cls_obj( n );
}

void clear_panel( struct gunpey_t *gp )
{
  UBYTE x, y;

  for( y=1; y<11; y++ ) {
    for( x=1; x<6; x++ ) {
      *((gp->panel)+x+y*7) = *((gp->panel2)+x+y*7) = 0;
    }
  }
  disp_panel( gp );
  cls_obj( 6 );
}

/*==========================================================================*
 |  player(box) controls                                                    |
 *==========================================================================*/
void hide_box()
{
  UBYTE i;

  for( i=0; i<6; i++ ) {
    move_sprite( i, 0, 0 );
  }
}

void show_box( struct gunpey_t *gp )
{
  UBYTE x, y;

  x = 16 * gp->cursor_x;
  y = 12 * gp->cursor_y + 20;
  move_sprite( 0, x,   y    );
  move_sprite( 1, x,   y+8  );
  move_sprite( 2, x,   y+16 );
  move_sprite( 3, x+8, y    );
  move_sprite( 4, x+8, y+8  );
  move_sprite( 5, x+8, y+16 );
}

/*==========================================================================*
 |  pad driver                                                              |
 *==========================================================================*/
void move_box( struct gunpey_t *gp )
{
  UBYTE i, key;

  key = joypad();
  if( gp->keycode_flag == 0 ) {
    if( key & (J_UP|J_DOWN|J_LEFT|J_RIGHT|J_A|J_B|J_START) ) {
      gp->keycode_flag = 1;
      if( key & J_UP ) {
        if( gp->cursor_y > 1 )      gp->cursor_y--;
      } else if( key & J_DOWN ) {
        if( gp->cursor_y < 9 )      gp->cursor_y++;
      } else if( key & J_LEFT ) {
        if( gp->cursor_x > 1 )      gp->cursor_x--;
      } else if( key & J_RIGHT ) {
        if( gp->cursor_x < 5 )      gp->cursor_x++;
      } else if( key & J_A ) {
        gp->keycode_off |= J_A;
      } else if( key & J_B ) {
        gp->keycode_off |= J_B;
      } else if( key & J_START ) {
        gp->keycode_off |= J_START;
      }
      show_box( gp );
    }
  } else if( !(key & (J_UP|J_DOWN|J_LEFT|J_RIGHT|J_A|J_B|J_START)) ) {
    gp->keycode_flag = 0;
  }
  if( key == (J_SELECT|J_START) ) {
    gp->mode = MODE_EXIT;
  }
}

/*==========================================================================*
 |  route check (recursive engine)                                          |
 *==========================================================================*/
void check_panel( struct gunpey_t *gp, UBYTE x, UBYTE y, UBYTE p, UBYTE *connect_flags )
{
  UBYTE s;

  if( (x<1) || (x>5) || (y<1) || (y>10) )  return;

  if( p=new_p[p][pan_tbl[*((gp->panel)+x+y*7)&0x0F]] ) {
    p--;
    s = *connect_flags; /* save check counter */
    if( (x==5) && (p&0x01) ) {
      *(connect_flags+1) = CONNECT;
      *((gp->flag)+x+y*7) = s;
      *connect_flags += 1;
    } else if( (x==1) && !(p&0x01) && *(connect_flags+1) ) {
      *((gp->flag)+x+y*7) = s;
      *connect_flags += 1;
    } else if( *((gp->flag)+x+y*7) == NONCHECK ) {
      *((gp->flag)+x+y*7) = s;
    } else if( *((gp->flag)+x+y*7) == SKIP ) {
      return;
    } else if( *((gp->flag)+x+y*7) < s ) {
      *connect_flags += 1;
      return;
    } else {
      return;
    }
    check_panel( gp, x+route1_x[p], y+route1_y[p], route1_p[p], connect_flags );
    check_panel( gp, x+route2_x[p], y+route2_y[p], route2_p[p], connect_flags );
    check_panel( gp, x+route3_x[p], y+route3_y[p], route3_p[p], connect_flags );
    if( s == *connect_flags ) {
      *((gp->flag)+x+y*7) = SKIP;
    }
#if 1 /* for debug */
    if( s > 50 ) {
      set_bkg_tiles( 19, 16, 1, 1, gmsg_zzz );      
      disp_value( 13, 17, (int)s );
      waitpad( J_START );
    }
#endif
  }
}

/*==========================================================================*
 |  connection check                                                        |
 *==========================================================================*/
void connect_chk( struct gunpey_t *gp )
{
  UBYTE x, y, connect_flags[2];

  /* clear flag table */
  for( y=1; y<11; y++ ) {
    for( x=1; x<6; x++ ) {
      *((gp->flag)+x+y*7) = NONCHECK;
    }
  }

  /* check the route */
  connect_flags[0] = CONNECT; /* init check counter */
  for( y=1; y<12; y++ ) {
    move_box( gp );
    connect_flags[1] = NONCHECK; /* init connect flag */
    check_panel( gp, 1, y-1, 2, connect_flags );
    check_panel( gp, 1, y,   0, connect_flags );
  }

  /* judge */
  for( y=1; y<11; y++ ) {
    for( x=1; x<6; x++ ) {
      if( *((gp->flag)+x+y*7) >= CONNECT ) {
        if( !gp->f_timer ) {  /* first time */
          gp->f_timer = FLASH_TIME;
        }
        *((gp->panel)+x+y*7) |=  PANEL_FLASH;  /* rainbow */
        *((gp->panel)+x+y*7) &= ~PANEL_BLINK;  /* stop blinking */
        x15driver( gp, x, y );
      }
    }
  }
}

/*==========================================================================*
 |  shiftup and panel generate                                              |
 *==========================================================================*/
void shift2add( struct gunpey_t *gp )
{
  UBYTE x, y, r, n, retry;

  if( !gp->s_timer ) {
    gp->s_timer = gp->s_level;
    for( x=1; x<6; x++ ) {
      if( *((gp->panel)+x+7) != 0 ) {
        gp->mode = MODE_MENU;  /* game over */
      }
    }
    for( y=1; y<10; y++ ) {
      for ( x=1; x<6; x++ ) {
        *((gp->panel)+x+y*7) = *((gp->panel)+x+(y+1)*7) & ~PANEL_BLINK;
        x15driver( gp, x, y );
      } 
    }
    if( gp->cursor_y > 1 ) {
      gp->cursor_y--;
      show_box( gp );
    }

    /* panel generation engine */
    retry = 1;
    while( retry ) {
      for( x=1; x<6; x++ ) {
        r = rand() % 8;
        if( r < 4 ) {
          *((gp->panel)+x+70) = (0x01 << r) & 0x0F | PANEL_BLINK;
          retry = 0;
        } else {
          *((gp->panel)+x+70) = PANEL_0;
        }
        x15driver( gp, x, 10 );
      }
    }
    gp->b_timer = BLINK_TIME;
    connect_chk( gp );
  } else {
    gp->s_timer--;
    delay( CHECK_DELAY );
  }
}

/*==========================================================================*
 |  panel delete                                                            |
 *==========================================================================*/
void flash2del( struct gunpey_t *gp )
{
  UBYTE i, x, y, bonus;
  int   new;
  unsigned char *p;

  /* delete panels */
  if( gp->f_timer ) {
    if( gp->s_timer ) {
      gp->s_timer = MAX_SHIFTUP;
    }
    gp->f_timer--;
    if( gp->f_timer == 0 ) {
      new = bonus = 0;
      for( y=1; y<11; y++ ) {
        for( x=1; x<6; x++ ) {
          if( *((gp->flag)+x+y*7) >= CONNECT ) {
            new++;
            *((gp->panel)+x+y*7) = *((gp->panel2)+x+y*7);
            *((gp->panel2)+x+y*7) = 0;
            x15driver( gp, x, y );
          }
          bonus |= *((gp->panel)+x+y*7);
          bonus |= *((gp->panel2)+x+y*7);
        }
      }
      disp_panel2( gp );
      if( new > 0 ) {
        if( !bonus ) {
          gp->score_total += 100L;  /* clean bonus */
          /* clear 1000 */
          set_sprite_data( 16, 16, ank_char );
          for( i=0; i<9; i++ ) {
            set_sprite_prop( i+6, 2 );
            set_sprite_tile( i+6, gmsg_bonus[i]+16 );
            move_sprite( i+6, i*9+15, 88 );
          }
          for( i=0; i<200; i++ ) {
            move_box( gp );
            delay( 0x0010 );
          }
          for( i=0; i<9; i++ ) {
            move_sprite( i+6, 0, 0 );
            set_sprite_prop( i+6, 1 );
          }
          p = p15;
          p += 256; /* offset */
          set_sprite_data( 16, 16, p ); /* reload */
          clear_panel( gp ); /* safty code */
          /* speed down */
          gp->s_level = SHIFTUP_TIME;
        } else {
          /* speed up */
          gp->s_level = gp->panel_total * (rand()%3+2);
          if( gp->s_level > (SHIFTUP_TIME - MAX_SHIFTUP) ) {
            gp->s_level = MAX_SHIFTUP;
          } else {
            gp->s_level = SHIFTUP_TIME - gp->s_level;
          }
        }
        gp->score_total += (new*((UWORD)new-4L));
        gp->panel_total += new;
        disp_value( 12, 3, gp->score_total );
        disp_value( 13, 6, gp->panel_total );
      }
      connect_chk( gp );
    } else {
      delay( CHECK_DELAY );
    }
  } else {
    shift2add( gp );
  }

  /* blink panels */
  if( gp->b_timer ) {
    gp->b_timer--;
    if( gp->b_timer == 0 ) {
      for( x=1; x<6; x++ ) {
        *((gp->panel)+x+70) &= ~PANEL_BLINK;
        x15driver( gp, x, 10 );
      }
    } else if( gp->b_timer & BLINK_CYCLE ) {
      for( x=1; x<6; x++ ) {
        if( *((gp->panel)+x+70) & PANEL_BLINK ) {
          i = *((gp->panel)+x+70);
          *((gp->panel)+x+70) = 0;
          x15driver( gp, x, 10 );
          *((gp->panel)+x+70) = i;
        }
      }
    } else {
      for( x=1; x<6; x++ ) {
        x15driver( gp, x, 10 );
      }
    }
  }

  if( gp->r_timer ) {
    gp->r_timer--;
  } else {
    gp->r_timer = RAINBOW_TIME;
    gp->rainbow++; if( gp->rainbow>23 ) gp->rainbow=0;
    for( i=0; i<5; i++ ) {
      gp->bkg_rainbow[2] = rainbow_palette[(gp->rainbow+i)%24];
      set_bkg_palette( i+1, 1, gp->bkg_rainbow );
    }
  }
}

/*==========================================================================*
 |  main loop of game                                                       |
 *==========================================================================*/
void gunpey_game( struct gunpey_t *gp )
{
  UBYTE i, x, y;

  /* setup sprite screen */
  set_sprite_data( 0, 16, obj ); /* overwrite 0-9 */
  for( i=0; i<6; i++ ) {
    set_sprite_tile( i, 3+i );
    move_sprite( i, 0, 0 );
    set_sprite_prop( i, 0 );
  }
  set_sprite_palette( 1, 1, &obj_palette[4] );
  for( i=6; i<40; i++ ) {
    move_sprite( i, 0, 0 );
    set_sprite_prop( i, 1 );
  }

  gp->cursor_x = 3;
  gp->cursor_y = 6;
  show_box( gp );
  clear_panel( gp );

  gp->f_timer = gp->b_timer = gp->r_timer = 0L;
  gp->s_timer = MAX_SHIFTUP;
  gp->s_level = SHIFTUP_TIME;
  gp->score_total = gp->panel_total = 0L;
  set_bkg_tiles( 13, 3, 5, 1, gmsg_clr );
  set_bkg_tiles( 13, 6, 5, 1, gmsg_clr );
  set_bkg_tiles( 18, 6, 1, 1, gmsg_0 );

  while( gp->mode == MODE_ENDLESS ) {
    flash2del( gp );
    move_box( gp );
    if( gp->keycode_off & J_A ) {
      gp->keycode_off &= ~J_A;
      x = gp->cursor_x;
      y = gp->cursor_y;
      if( *((gp->flag)+x+y*7) < CONNECT ) {
        i = (UBYTE)*((gp->panel)+x+y*7) & (UBYTE)~PANEL_FLASH;
        if( *((gp->flag)+x+(y+1)*7) < CONNECT ) {
          *((gp->panel)+x+y*7) = (UBYTE)*((gp->panel)+x+(y+1)*7) & (UBYTE)~PANEL_FLASH;
          *((gp->panel)+x+(y+1)*7) = i;
          x15driver( gp, x, y+1 );
        } else {
          *((gp->panel)+x+y*7) = *((gp->panel2)+x+(y+1)*7);
          *((gp->panel2)+x+(y+1)*7) = i;
          disp_panel2( gp );
        }
        x15driver( gp, x, y );
        i |= *((gp->panel)+x+y*7);
      } else {
        i = *((gp->panel2)+x+y*7);
        if( *((gp->flag)+x+(y+1)*7) < CONNECT ) {
          *((gp->panel2)+x+y*7) = (UBYTE)*((gp->panel)+x+(y+1)*7) & (UBYTE)~PANEL_FLASH;
          *((gp->panel)+x+(y+1)*7) = i;
          x15driver( gp, x, y+1 );
        } else {
          *((gp->panel2)+x+y*7) = *((gp->panel2)+x+(y+1)*7);
          *((gp->panel2)+x+(y+1)*7) = i;
        }
        disp_panel2( gp );
        i |= *((gp->panel2)+x+y*7);
      }
      if( i ) {
        connect_chk( gp );
      } else {
        delay( CHECK_DELAY );
      }
    } else if( gp->keycode_off & J_B ) {
      gp->keycode_off &= ~J_B;
      gp->f_timer = 1;
      gp->s_timer = 0;
    } else if( gp->keycode_off & J_START ) {
      if( gp->mode == MODE_ENDLESS ) {
        gp->keycode_off &= ~J_START;
        hide_box();
        delay( 0x0040 );
        /* pause */
        set_sprite_data( 0, 16, ank_char );
        for( i=0; i<5; i++ ) {
          set_sprite_prop( i, 2 );
          set_sprite_tile( i, gmsg_pause[i] );
          move_sprite( i, i*11+30, 88 );
        }
        waitpadup();
        waitpad( J_START );
        if( joypad() == (J_SELECT|J_START) ) {
          gp->mode = MODE_EXIT;
        }
        waitpadup();
        set_sprite_data( 0, 16, obj ); /* overwrite 0-9 */
        for( i=0; i<6; i++ ) {
          set_sprite_tile( i, 3+i );
          move_sprite( i, 0, 0 );
          set_sprite_prop( i, 0 );
        }
        disp_panel( gp );
        show_box( gp );
      }
    } else {
      delay( CHECK_DELAY );
    }
  }
}

/*==========================================================================*
 |  demo                                                                    |
 *==========================================================================*/
void gunpey_demo( struct gunpey_t *gp )
{
#if GUNPEY_MODE
  UBYTE i, j, x, y, n;

  for( i=0; i<6; i++ ) {
    for( j=5; j>=demo_x[i]; j-- ) {
      n = 0;
      for( x=0; x<3; x++ ) {
        if( x < (6-j) ) {
          for( y=0; y<3; y++ ) {
            *((gp->panel)+x+j+(demo_y[i]+y)*7) = demo_panel[i][n];
            x15driver( gp, j+x, demo_y[i]+y );
            n++;
          }
        }
      }
      delay( 0x0040 );
    }
  }
#endif
}

/*==========================================================================*
 |  menu (game over)                                                        |
 *==========================================================================*/
void gunpey_menu( struct gunpey_t *gp )
{
  UBYTE i;
  fixed seed;

  hide_box();

  /* game over */
  set_sprite_data( 0, 16, ank_char );
  for( i=0; i<8; i++ ) {
    set_sprite_prop( i, 2 );
    set_sprite_tile( i, gmsg_over[i] );
    move_sprite( i, i*9+20, 88 );
  }

  if( (nvm->gunpey_hi_score) < (gp->score_total) ) {
    nvm->gunpey_hi_score = gp->score_total;
  }
  disp_value( 12, 14, nvm->gunpey_hi_score );

  gp->keycode_flag = gp->keycode_off = gp->rainbow = 0;
  waitpadup();
  waitpad( J_START );
  if( joypad() == (J_SELECT|J_START) ) {
    gp->mode = MODE_EXIT;
  } else {
    gp->mode = MODE_ENDLESS;

    /* init rand */
    seed.b.l = DIV_REG;
    waitpadup();
    seed.b.h = DIV_REG;
    initarand( seed.w );
  }
  set_sprite_data( 0, 16, obj ); /* overwrite 0-9 */
}

/*==========================================================================*
 |  initialize                                                              |
 *==========================================================================*/
void gunpey_init( struct gunpey_t *gp )
{
  UBYTE i;

  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

  /* load characters */
  set_bkg_data(  128, 64, p15 );
  set_sprite_data( 0, 64, p15 );

  /* setup palettes */
  gp->bkg_rainbow[0] = bkg_palette[0];
  gp->bkg_rainbow[1] = bkg_palette[1];
  gp->bkg_rainbow[2] = bkg_palette[2];
  gp->bkg_rainbow[3] = bkg_palette[3];
  for( i=1; i<=5; i++ ) {
    set_bkg_palette( i, 1, gp->bkg_rainbow );
  }
  set_bkg_palette( 6, 1, &bkg_palette[4] );
  set_bkg_palette( 7, 1, &bkg_palette[8] );
  set_sprite_palette( 0, 1, &obj_palette[0] );
  set_sprite_palette( 1, 1, &obj_palette[4] );
  set_sprite_palette( 2, 1, &bkg_palette[4] );

  set_bkg_tiles( 0, 0, 12, 2, gpy_head );
  for( i=0; i<5; i++ ) {
    set_bkg_tiles( 0, i*3+2, 12, 3, gpy_line );
    set_bkg_attribute( 0, i*3+2, 12, 1, gpy_line_a );
    set_bkg_attribute( 0, i*3+3, 12, 1, gpy_line_a );
    set_bkg_attribute( 0, i*3+4, 12, 1, gpy_line_a );
  }
  set_bkg_tiles( 0, 17, 12, 1, gpy_tail );
  set_bkg_attribute( 0,  0, 12, 1, gpy_head_a );
  set_bkg_attribute( 0,  1, 12, 1, gpy_head_a );
  set_bkg_attribute( 0, 17, 12, 1, gpy_head_a );

  set_bkg_tiles( 13,  2,  5, 1, gmsg_score );
  set_bkg_tiles( 18,  3,  1, 1, gmsg_0 );
  set_bkg_tiles( 13,  5,  5, 1, gmsg_panel );
  set_bkg_tiles( 18,  6,  1, 1, gmsg_0 );
  set_bkg_tiles( 13, 13,  3, 1, gmsg_top );
  disp_value( 12, 14, nvm->gunpey_hi_score );
  set_bkg_tiles( 18, 14,  1, 1, gmsg_0 );

  clear_panel( gp );
  gunpey_demo( gp );
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_gunpey()
{
  UBYTE mode;
  struct gunpey_t gp;
  UBYTE panel[7][12];  /* [7][12] */
  UBYTE panel2[7][12]; /* [7][12] */
  UBYTE flag[7][12];   /* [7][12] */

  /* setup work area on the stack */
  gp.mode = MODE_MENU;
  gp.panel  = (UBYTE *)panel;
  gp.panel2 = (UBYTE *)panel2;
  gp.flag   = (UBYTE *)flag;
  gunpey_init( &gp );
  gp.score_total = 0L;

  while( gp.mode != MODE_EXIT ) {
    switch( gp.mode ) {
      case MODE_MENU:
        gunpey_menu( &gp );
        break;
      case MODE_ENDLESS:
        gunpey_game( &gp );
        break;
    }
  }
  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
}

/* EOF */