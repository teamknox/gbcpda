/*

 OBJ.H

 Include File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 8

  Palette colors       : Included.
  SGB Palette          : Constant per entry.
  CGB Palette          : 1 Byte per entry.

  Convert to metatiles : No.

 This file was generated by GBTD v2.1

*/


/* Bank of tiles. */
#define objBank 0

/* Super Gameboy palette 0 */
#define objSGBPal0c0 25368
#define objSGBPal0c1 0
#define objSGBPal0c2 4104
#define objSGBPal0c3 6

/* Super Gameboy palette 1 */
#define objSGBPal1c0 0
#define objSGBPal1c1 0
#define objSGBPal1c2 4104
#define objSGBPal1c3 6

/* Super Gameboy palette 2 */
#define objSGBPal2c0 0
#define objSGBPal2c1 0
#define objSGBPal2c2 4104
#define objSGBPal2c3 6

/* Super Gameboy palette 3 */
#define objSGBPal3c0 0
#define objSGBPal3c1 0
#define objSGBPal3c2 4104
#define objSGBPal3c3 6

/* Gameboy Color palette 0 */
#define objCGBPal0c0 15360
#define objCGBPal0c1 31
#define objCGBPal0c2 31775
#define objCGBPal0c3 32767

/* Gameboy Color palette 1 */
#define objCGBPal1c0 15360
#define objCGBPal1c1 15360
#define objCGBPal1c2 24
#define objCGBPal1c3 25368

/* Gameboy Color palette 2 */
#define objCGBPal2c0 6108
#define objCGBPal2c1 8935
#define objCGBPal2c2 6628
#define objCGBPal2c3 6368

/* Gameboy Color palette 3 */
#define objCGBPal3c0 6108
#define objCGBPal3c1 8935
#define objCGBPal3c2 6628
#define objCGBPal3c3 6368

/* Gameboy Color palette 4 */
#define objCGBPal4c0 6108
#define objCGBPal4c1 8935
#define objCGBPal4c2 6628
#define objCGBPal4c3 6368

/* Gameboy Color palette 5 */
#define objCGBPal5c0 6108
#define objCGBPal5c1 8935
#define objCGBPal5c2 6628
#define objCGBPal5c3 6368

/* Gameboy Color palette 6 */
#define objCGBPal6c0 6108
#define objCGBPal6c1 8935
#define objCGBPal6c2 6628
#define objCGBPal6c3 6368

/* Gameboy Color palette 7 */
#define objCGBPal7c0 6108
#define objCGBPal7c1 8935
#define objCGBPal7c2 6628
#define objCGBPal7c3 6368

/* SGB palette entries. */
#define objSGB0 0
#define objSGB1 0
#define objSGB2 0
#define objSGB3 0
#define objSGB4 0
#define objSGB5 0
#define objSGB6 0
#define objSGB7 0
#define objSGB8 0
/* CGBpalette entries. */
extern unsigned char objCGB[];
/* Start of tile array. */
extern unsigned char obj[];

/* End of OBJ.H */
