/*

 OBJ.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 8

  Palette colors       : Included.
  SGB Palette          : Constant per entry.
  CGB Palette          : 1 Byte per entry.

  Convert to metatiles : No.

 This file was generated by GBTD v2.1

*/

/* CGBpalette entries. */
unsigned char objCGB[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00
};
/* Start of tile array. */
unsigned char obj[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x10,0x00,0x18,0x00,0x1C,0x00,0x1E,
  0x00,0x1C,0x00,0x18,0x00,0x10,0x00,0x00,
  0x00,0x08,0x00,0x18,0x00,0x38,0x00,0x78,
  0x00,0x38,0x00,0x18,0x00,0x08,0x00,0x00,
  0x00,0xFF,0x00,0x80,0x00,0x80,0x00,0x80,
  0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,
  0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,
  0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,
  0x00,0x80,0x00,0x80,0x00,0x80,0x00,0x80,
  0x00,0x80,0x00,0x80,0x00,0xFF,0x00,0x00,
  0x00,0xFE,0x00,0x02,0x00,0x02,0x00,0x02,
  0x00,0x02,0x00,0x02,0x00,0x02,0x00,0x02,
  0x00,0x02,0x00,0x02,0x00,0x02,0x00,0x02,
  0x00,0x02,0x00,0x02,0x00,0x02,0x00,0x02,
  0x00,0x02,0x00,0x02,0x00,0x02,0x00,0x02,
  0x00,0x02,0x00,0x02,0x00,0xFE,0x00,0x00
};

/* End of OBJ.C */
