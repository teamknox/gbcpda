/****************************************************************************
 *  bank07.c                                                                *
 *                                                                          *
 *    SYSTEMCALL FUNCTIONS (PHONE DATA BASE)                                *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"

#define DB_DATE_TAG   0x7E
#define DB_EOF_TAG    0x7F

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank07\rom_file.c"

unsigned char line_space[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

/*==========================================================================*
 |  subroutines                                                             |
 *==========================================================================*/
UBYTE comp_string( unsigned char *src, unsigned char *tag, UBYTE len )
{
    UBYTE i;
    unsigned char s, t;

    for( i=0; i<len; i++ ) {
        s = *src++;
        t = *tag++;
#if 0 /* big : small */
        if( (0x60<s)&&(s<0x7B) ) {
            s -= 0x20;
        }
        if( (0x60<t)&&(t<0x7B) ) {
            t -= 0x20;
        }
#endif
        if( s > t ) {
            return( 1 );
        } else if( s < t ) {
            return( 0 );
        }
    }
    return( 0 ); /* EQ */
}

void swap_ram_data( unsigned char *src, unsigned char *tag )
{
    UBYTE i;
    unsigned char work;

    for( i=0; i<sizeof(struct db_file); i++ ) {
        work = *src;
        *src = *tag;
        *tag = work;
        src++; tag++;
    }
}

void sort_ram_db_file()
{
    UBYTE i, j;
    unsigned char *src, *tag;

    if( max_ram_file < 2 )  return;

    src = (unsigned char *)nvm->data_base[0].name;
    for( i=0; i<(max_ram_file-1); i++ ) {
        tag = (unsigned char *)nvm->data_base[i+1].name;
        for( j=(i+1); j<max_ram_file; j++ ) {
            if( comp_string(src,tag,18) ) {
                swap_ram_data( src, tag );
            }
            tag += sizeof(struct db_file);
        }
        src += sizeof(struct db_file);
    }
}

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void pda_search_phone_tag( struct search_phone *arg )
{
    UWORD no, *p;
    UBYTE hit;

    p = arg->table;
    for( no=0; no<(arg->max); no++ ) {
        *(p++) = 0xFFFF;
    }

    /* search rom data base */
    p = arg->table;
    hit = no = 0;
    while( (data_base[no].name[0]!='!') && (hit<(arg->max)) ) {
        if( *(nvm_phone_flag+(no/8)) & (1<<(no%8)) ) {
            /* no opration */
        } else if( (arg->tag) == '@' ) {
            if( (data_base[no].name[0]<'A')||('Z'<data_base[no].name[0]) ) {
                *(p++) = no;
                hit++;
            }
        } else if( data_base[no].name[0] == (arg->tag) ) {
            *(p++) = no;
            hit++;
        }
        no++;
    }

    /* search ram data base */
    no = 0;
    while( ((nvm->data_base[no].name[0])!='!')&&(hit<(arg->max)) ) {
        if( (nvm->data_base[no].name[0]) == DB_DATE_TAG ) {
            ;
        } else if( (arg->tag) == '@' ) {
            if( ((nvm->data_base[no].name[0])<'A')||('Z'<(nvm->data_base[no].name[0])) ) {
                *(p++) = no+0x8000;
                hit++;
            }
        } else if( (nvm->data_base[no].name[0]) == (arg->tag) ) {
            *(p++) = no+0x8000;
            hit++;
        }
        no++;
    }
    arg->max = hit;
}

void pda_sort_phone_name( struct search_phone *arg )
{
    UBYTE i, j;
    UWORD no, *from, *to, *p;
    unsigned char *src, *tag;

    p = arg->table;
    from = p++;
    for( i=0; i<((arg->max)-1); i++ ) {
        if( *from < 0x8000 )  src = data_base[*from].name;
        else                  src = nvm->data_base[(*from)-0x8000].name;
        to = p++;
        for( j=(i+1); j<(arg->max); j++ ) {
            if( *to < 0x8000 )  tag = data_base[*to].name;
            else                tag = nvm->data_base[(*to)-0x8000].name;
            if( comp_string(src,tag,18) ) {
                no = *from; *from = *to; *to = no;
                src = tag;
            }
            to++;
        }
        from++;
    }
}

void pda_get_phone_name( struct phone_number *arg )
{
    UBYTE i;
    unsigned char *f, *t;

    t = arg->str;
    if( (arg->no) < 0x8000 ) {
        f = data_base[(arg->no)].name;
    } else if( (arg->no) < 0xFF00 ) {
        f = nvm->data_base[(arg->no)-0x8000].name;
    } else {
        f = line_space;
    }

    for( i=0; i<18; i++ ) {
        *t = *f;
        t++;  f++;
    }
    for( i=0; i<18; i++ ) {
        t--;
        if( *t == ' ' ) {
            *t = 0;
        }
    }
}

void pda_get_phone_number( struct phone_number *arg )
{
    UBYTE i;
    unsigned char *f, *t;

    t = arg->str;
    if( (arg->no) < 0x8000 ) {
        f = data_base[(arg->no)].phone;
    } else if( (arg->no) < 0xFF00 ) {
        f = nvm->data_base[(arg->no)-0x8000].phone;
    } else {
        f = line_space;
    }

    for( i=0; i<18; i++ ) {
        *t = *f;
        t++;  f++;
    }
    for( i=0; i<18; i++ ) {
        t--;
        if( *t == ' ' ) {
            *t = 0;
        }
    }
}

void pda_show_phone_line( struct search_phone *arg )
{
    unsigned char *p;

    if( *(arg->table) < 0x8000 ) {
        p = data_base[*(arg->table)].name;
    } else if( *(arg->table) < 0xFF00 ) {
        p = nvm->data_base[*(arg->table)-0x8000].name;
    } else {
        p = line_space;
    }
    if( *p == '@' ) {
        p++;
        set_bkg_tiles(  1, arg->tag, 17, 1, p );
        set_bkg_tiles( 18, arg->tag,  1, 1, line_space );
    } else {
        set_bkg_tiles( 1, arg->tag, 18, 1, p );
    }
}

void pda_make_dial_number( struct phone_number *arg )
{
    UBYTE i, j;
    unsigned char *addr;

    addr = arg->str;
    for( i=0; i<20; i++ ) {
        *addr++ = 0;
    }
    j = 10;

    if( (arg->no) < 0x8000 ) { /* rom data base */
        for( i=0; i<18; i++ ) {
            if( data_base[arg->no].phone[i]==' ' ) {
                return;
            } else if( data_base[arg->no].phone[i]=='+' ) {
                j = 0;
                if( (arg->mode) == 2 ) {
                    *(arg->str++) = data_base[arg->no].phone[i];
                }
            } else if( (((arg->mode)==0)&&(j<1)) || (((arg->mode)==1)&&(j<2)) ) {
                if( data_base[arg->no].phone[i]=='-' ) {
                    j++;
                }
            } else if( (arg->mode)==2 ) {
                if( (j==1) && (data_base[arg->no].phone[i]=='-') ) {
                    j = 2;
                    *(arg->str++) = data_base[arg->no].phone[i];
                    if( data_base[arg->no].phone[i+1]=='0' ) {
                        i++;
                    }
                } else {
                    if( j==0 ) {
                        if( (data_base[arg->no].phone[i]=='1')
                         && (data_base[arg->no].phone[i+1]=='-') ) {
                            j = 2; /* USA (+1-XXX-XXX-XXXX)*/
                        } else {
                            j = 1; /* other */
                        }
                    }
                    *(arg->str++) = data_base[arg->no].phone[i];
                }
            } else {
                *(arg->str++) = data_base[arg->no].phone[i];
            }
        }
    } else { /* ram data base */
        (arg->no) -= 0x8000;
        for( i=0; i<18; i++ ) {
            if( nvm->data_base[arg->no].phone[i]==' ' ) {
                return;
            } else if( (nvm->data_base[arg->no].phone[i])=='+' ) {
                j = 0;
                if( (arg->mode)==2 ) {
                    *(arg->str++) = nvm->data_base[arg->no].phone[i];
                }
            } else if( (((arg->mode)==0)&&(j<1)) || (((arg->mode)==1)&&(j<2)) ) {
                if( (nvm->data_base[arg->no].phone[i])=='-' ) {
                    j++;
                }
            } else if( (arg->mode)==2 ) {
                if( (j==1) && ((nvm->data_base[arg->no].phone[i])=='-') ) {
                    j = 2;
                    *(arg->str++) = nvm->data_base[arg->no].phone[i];
                    if( (nvm->data_base[arg->no].phone[i+1])=='0' ) {
                        i++;
                    }
                } else {
                    if( j==0 ) {
                        if( ((nvm->data_base[arg->no].phone[i])=='1')
                         && ((nvm->data_base[arg->no].phone[i+1])=='-') ) {
                            j = 2; /* USA (+1-XXX-XXX-XXXX)*/
                        } else {
                            j = 1; /* other */
                        }
                    }
                    *(arg->str++) = nvm->data_base[arg->no].phone[i];
                }
            } else {
                *(arg->str++) = nvm->data_base[arg->no].phone[i];
            }
        }
    }
}

void pda_probe_data_base( void *arg )
{
    UBYTE  i, flag;

    /* search rom data base */
    max_rom_file = 0;
    while( data_base[max_rom_file].name[0]!='!' ) {
      max_rom_file++;
    }

    /* search ram data base */
    max_ram_file = 0;
    while( (nvm->data_base[max_ram_file].name[0]) != '!' ) {
      if( (nvm->data_base[max_ram_file].name[0]) == 0  ) {
          break;
      }
      max_ram_file++;
    }

    /* search rom memo data */
    max_rom_memo_page = 0;  flag = 1;
    while( flag ) {
       flag = 0;
       for( i=0; i<18; i++ ) {
           if( memo_rom_data[max_rom_memo_page][0][i] != '!' ) {
               flag = 1;
           }
       }
       if( flag == 1 )  max_rom_memo_page++;
    }
}

void pda_set_ram_data( struct db_file *arg )
{
    UBYTE i;

    for( i=0; i<(sizeof(struct db_file)); i++ ) {
        *(&(nvm->data_base[max_ram_file])+i) = *(arg+i);
    }
    max_ram_file++;
    nvm->data_base[max_ram_file].name[0] = '!';

    sort_ram_db_file();  /* sort (1)phone, (2)date... */
}

void pda_remove_ram_data( UWORD *arg )
{
    UWORD no;
    UBYTE i, j, flag;
    unsigned char *dbp;

    if( (*arg) < 0x1000 ) {
        no = (*arg) & 0x01FF; /* mask 512 /8 = 64 */
        flag = *(nvm_phone_flag+(no/8)) | (1<<(no%8));
        *(nvm_phone_flag+(no/8)) = flag; 
    } else if( (*arg) < 0x8000 ) {
        no = (*arg) & 0x000F; /* mask 16 /8 = 2 */
        flag = *(nvm_memo_flag+(no/8)) | (1<<(no%8));
        *(nvm_memo_flag+(no/8)) = flag; 
    } else {
        no = (*arg) - 0x8000;
        if( no < max_ram_file ) {
            max_ram_file--;
            dbp = (unsigned char *)&(nvm->data_base[no]);
            for( i=no; i<max_ram_file; i++ ) {
                for( j=0; j<(sizeof(struct db_file)); j++ ) {
                    *(dbp) = *(dbp+sizeof(struct db_file));
                    dbp++;
                }
            }
            nvm->data_base[max_ram_file].name[0] = '!';
        }
    }
}

void pda_revival_rom_data( UWORD *arg )
{
    UWORD no;
    UBYTE flag;

    if( (*arg) < 0x1000 ) {
        no = (*arg) & 0x01FF; /* mask 512 */
        flag = *(nvm_phone_flag+(no/8)) & ~(1<<(no%8));
        *(nvm_phone_flag+(no/8)) = flag; 
    } else if( (*arg) < 0x8000 ) {
        no = (*arg) & 0x000F; /* mask 16 */
        flag = *(nvm_memo_flag+(no/8)) & ~(1<<(no%8));
        *(nvm_memo_flag+(no/8)) = flag; 
    }
}

void pda_search_schedule_tag( struct search_schedule *arg )
{
    UWORD no, *p;
    UBYTE hit, flag, hour;
    struct date *date;

    p = arg->table;
    for( no=0; no<(arg->max); no++ ) {
        *(p++) = 0xFFFF;
    }

    /* set flag */
    if( (arg->max) > 1 )  flag = 1;
    else                  flag = 0;

    /* search ram data base */
    p = arg->table;
    hit = 0;
    no = 0L;
    while( ((nvm->data_base[no].name[0])!='!')&&(hit<(arg->max)) ) {
        if( (nvm->data_base[no].name[0]) == DB_DATE_TAG ) {
            if( flag ) {
                *(p++) = no+0x8000;
                hit++;
            } else {
                date = (struct date *)&(nvm->data_base[no].phone[0]);
                hour = (UBYTE)nvm->data_base[no].phone[4];
                if( ((arg->date.year)==(date->year))
                  &&((arg->date.month)==(date->month))
                  &&((arg->date.day)==(date->day)) ) {
                    if( ((arg->hour)==hour) || ((arg->hour)==0xFF) ) {
                        *(p++) = no+0x8000;
                        hit++;
                    }
                }
            }
        }
        no++;
    }
    arg->max = hit;
}

void pda_get_rom_memo_data( struct memo_data *arg )
{
    UBYTE i;
    unsigned char *p;

    p = arg->buff;
    for( i=0; i<18; i++ ) {
        *p++ = (unsigned char)memo_rom_data[arg->page][arg->row][i];
    }
}

UBYTE pdax_comp_string( unsigned char *a, unsigned char *b )
{
    UBYTE  i;

    for( i=0; i<18; i++ ) {
        if( *a++ != *b++ )  return( 0 );
    }
    return( 1 );  // found
}

void pda_search_phone_name( struct search_name *arg )
{
    UWORD  no;

    /* search rom data base */
    no = 0;
    while( data_base[no].name[0] != '!' ) {
        if( pdax_comp_string(data_base[no].name,arg->name) ) {
            arg->no = no;
            return;
        }
        no++;
    }

    /* search ram data base */
    no = 0;
    while( (nvm->data_base[no].name[0]) != '!' ) {
        if( (nvm->data_base[no].name[0]) != DB_DATE_TAG ) {
            if( pdax_comp_string(nvm->data_base[no].name,arg->name) ) {
                arg->no = no + 0x8000;
                return;
            }
        }
        no++;
    }
    arg->no = 0xFFFF;  // not found
}

/* EOF */