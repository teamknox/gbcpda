/****************************************************************************
 *  bank02.c                                                                *
 *                                                                          *
 *    CONFIGURATION                                                         *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "fixed\fixed.h"
#include "bank02\s_config.h"

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank02\c_config.c"
#include "bank02\s_config.c"

unsigned char config_head[] = {
    0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x10,0x12,0x00,0x04,
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x15,0x17,0x00,0x04
};
unsigned char config_line[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
};
unsigned char config_tail[] = {
    0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char config_page1a[] = {
    0x07,0x02,0x02,0x02,0x02,0x02,0x02,0x09,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x08
};
unsigned char config_page1b[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x00,0x31,
    0x00,0x32,0x00,0x33,0x00,0x34,0x00,0x35,0x00,0x04
};
unsigned char config_page1c[] = {
    0x07,0x02,0x02,0x02,0x02,0x02,0x02,0x0B,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x08
};
unsigned char config_page1d[] = {
    0x04,0x46,0x4F,0x52,0x45,0x00,0x00,0x04,0x02,0x02,
    0x02,0x00,0x02,0x02,0x02,0x00,0x02,0x02,0x02,0x04,
    0x07,0x02,0x02,0x02,0x02,0x02,0x02,0x0B,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x08,
    0x04,0x42,0x41,0x43,0x4B,0x00,0x00,0x04,0x02,0x02,
    0x02,0x00,0x02,0x02,0x02,0x00,0x02,0x02,0x02,0x04,
    0x07,0x02,0x02,0x02,0x02,0x02,0x02,0x0A,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x08,
    0x04,0x46,0x52,0x45,0x45,0x00,0x4D,0x45,0x4D,0x00,
    0x00,0x00,0x00,0x00,0x00,0x42,0x59,0x54,0x45,0x04
};
unsigned char config_page2a[] = {
    0x07,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x08
};

unsigned char config_title1[] = { "DESIGN" };
unsigned char config_title2[] = { "APPs" };
unsigned char config_msg1[] = { "FONT  " };
unsigned char config_msg2[] = { "FRAME " };
unsigned char config_msg3[] = { "BUTTON" };
unsigned char config_msg4[] = { "CURSOR" };
unsigned char config_msg5[] = { "PAGE   /" };

unsigned char msg_clear[] = { " " };
unsigned char msg_left[]  = { "<" };
unsigned char msg_right[] = { ">" };
unsigned char config_break[] = { 0x10,0x12,0x15,0x17 };
unsigned char config_press[] = { 0x18,0x1A,0x1D,0x1F };
unsigned char config_space[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char config_btn_a[] = { 2,2,2,2 };
unsigned char config_txt_a[] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };

unsigned char config_icon[]   = { 0x80,0x82,0x81,0x83 };
unsigned char config_iconx[]  = { 0x84,0x86,0x85,0x87 };
unsigned char config_icon6[]  = {
    0x88,0x8A,0x80,0x82,0x8C,0x8E,0x89,0x8B,0x81,0x83,0x8D,0x8F
};
unsigned char config_icon6x[] = {
    0x88,0x8A,0x84,0x86,0x8C,0x8E,0x89,0x8B,0x85,0x87,0x8D,0x8F
};

struct cursor_info config_cursor0 = { 1, 2, 138,  32,  0, 16 };
struct cursor_info config_cursor1 = { 5, 6,  78,  36, 16, 16 };
struct cursor_info config_cursor2 = { 3, 3,  78, 100, 32, 16 };
struct cursor_info config_cursor3 = { 1, 8,  50,  44,  0,  8 };

UWORD config_palette[] = {
    s_configCGBPal0c0, s_configCGBPal0c1, s_configCGBPal0c2, s_configCGBPal0c3,
    s_configCGBPal1c0, s_configCGBPal1c1, s_configCGBPal1c2, s_configCGBPal1c3,
    s_configCGBPal2c0, s_configCGBPal2c1, s_configCGBPal2c2, s_configCGBPal2c3,
    s_configCGBPal3c0, s_configCGBPal3c1, s_configCGBPal3c2, s_configCGBPal3c3,
    s_configCGBPal4c0, s_configCGBPal4c1, s_configCGBPal4c2, s_configCGBPal4c3
};

void show_config_page1()
{
    UBYTE i, x;
    UWORD memory;
    unsigned char buff[4];

    /* display screen */
    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_sprite_data( 64, 37, s_config );
    set_sprite_palette( 1, 5, config_palette );
    set_sprite_tile( 7, 32+64 );
    move_sprite( 7, 141, 28 );

    set_bkg_tiles( 0, 0, 20, 3, config_head );
    set_bkg_tiles( 7, 1,  6, 1, config_title1 );
    set_bkg_attribute( 16,  1, 2, 2, config_btn_a );
    set_bkg_tiles( 0, 3, 20, 1, config_page1a );
    for( i=0; i<4; i++ ) {
      set_bkg_tiles( 0, i*2+4, 20, 1, config_page1b );
      set_bkg_tiles( 0, i*2+5, 20, 1, config_page1c );
    }
    set_bkg_tiles( 1,  4,  6, 1, config_msg1 );
    set_bkg_tiles( 1,  6,  6, 1, config_msg2 );
    set_bkg_tiles( 1,  8,  6, 1, config_msg3 );
    set_bkg_tiles( 1, 10,  6, 1, config_msg4 );
    set_bkg_tiles( 0, 12, 20, 5, config_page1d );
    set_bkg_attribute( 10, 16, 4, 1, config_txt_a );
    set_bkg_tiles( 0, 17, 20, 1, config_tail );

    if( _cpu == CGB_TYPE ) {
        set_sprite_prop( 10, 1 );
        set_sprite_prop( 11, 2 );
        set_sprite_prop( 12, 3 );
        set_sprite_prop( 13, 1 );
        set_sprite_prop( 14, 2 );
        set_sprite_prop( 15, 3 );
    }
    set_sprite_tile( 10, 33+64 );
    set_sprite_tile( 11, 33+64 );
    set_sprite_tile( 12, 33+64 );
    set_sprite_tile( 13, 33+64 );
    set_sprite_tile( 14, 33+64 );
    set_sprite_tile( 15, 33+64 );
    move_sprite( 10,  ((nvm->fore_color)&0x001F)/2     + 72, 112 );
    move_sprite( 11, (((nvm->fore_color)>>5)&0x001F)/2 +104, 112 );
    move_sprite( 12, (((nvm->fore_color)>>10)&0x001F)/2+136, 112 );
    move_sprite( 13,  (~(nvm->back_color)&0x001F)/2     + 72, 128 );
    move_sprite( 14, ((~(nvm->back_color)>>5)&0x001F)/2 +104, 128 );
    move_sprite( 15, ((~(nvm->back_color)>>10)&0x001F)/2+136, 128 );

    for( i=0; i<4; i++ ) {
        x = *(&(nvm->font_type)+i);
        set_bkg_tiles( x*2+8,  i*2+4, 1, 1, msg_left );
        set_bkg_tiles( x*2+10, i*2+4, 1, 1, msg_right );
    }
    memory = (UWORD)(8191-64-2-1800-768) - sizeof(struct backup_ram0);
    memory -= max_ram_file * sizeof(struct db_file);
    buff[3] = (memory%10)+'0'; memory /= 10;
    buff[2] = (memory%10)+'0'; memory /= 10;
    buff[1] = (memory%10)+'0'; memory /= 10;
    buff[0] = (memory%10)+'0';
    if( !buff[0] ) {
        buff[0] = 0;
        if( !buff[1] ) {
            buff[1] = 0;
            if( !buff[2] ) {
                buff[2] = 0;
            }
        }
    }
    set_bkg_tiles( 10, 16, 4, 1, buff );
}

void show_config_icon_a( UBYTE y )
{
    UBYTE x;

    x = ((y-1)%7)%3;
    y = ((y-1)%7)/3;
    if( y == 2 ) {
        move_sprite( 12, 16+56, y*16+104 );
        move_sprite( 13, 16+56, y*16+112 );
        move_sprite( 14, 16+64, y*16+104 );
        move_sprite( 15, 16+64, y*16+112 );
    } else {
        move_sprite( 12, x*16+56, y*16+104 );
        move_sprite( 13, x*16+56, y*16+112 );
        move_sprite( 14, x*16+64, y*16+104 );
        move_sprite( 15, x*16+64, y*16+112 );
    }
}

void show_config_icon( UBYTE y )
{
    unsigned char c;
    UBYTE i;

    if( !y )  return;
    show_config_icon_a( y );

    y = ((y-1)/7)*7;
    for( i=0; i<7; i++ ) {
        if( (y+i) < apps_num ) {
            /* disp icon */
            if( i==6 ) {
                set_bkg_tiles( 6, 15, 6, 2, config_icon6 );
            } else {
                set_bkg_tiles( (i%3)*2+6, (i/3)*2+11, 2, 2, config_icon );
            }
        } else {
            /* disp icon */
            if( i==6 ) {
                set_bkg_tiles( 6, 15, 6, 2, config_icon6x );
            } else {
                set_bkg_tiles( (i%3)*2+6, (i/3)*2+11, 2, 2, config_iconx );
            }
        }
    }
    c = '1' + y/7;
    set_bkg_tiles( 15, 15, 1, 1, &c );
}

void show_config_page2_apps( UBYTE y )
{
    UBYTE i;
    unsigned char buff[2];

    /* apps name, version */
    for( i=0; i<7; i++ ) {
        if( (y+i) < apps_num ) {
            buff[0] = (y+i+1)/10+'0';
            buff[1] = (y+i+1)%10+'0';
            set_bkg_tiles(  2, i+4, 2, 1, buff );
            set_bkg_tiles(  5, i+4, 8, 1, dispatch[nvm->apps[y+i]].name );
            set_bkg_tiles( 14, i+4, 4, 1, dispatch[nvm->apps[y+i]].version );
            set_bkg_attribute( 5, i+4, 8, 1, config_txt_a );
        } else {
            set_bkg_tiles(  2, i+4, 16, 1, config_space );
        }
    }
}

void show_config_page2()
{
    unsigned char c;
    UBYTE i;

    /* display screen */
    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_data( 0x80, 16, c_config );
    set_sprite_data( 64, 37, s_config );
    set_sprite_data( 64+40, 4, c_config );
    set_sprite_palette( 1, 5, config_palette );
    set_sprite_tile( 7, 35+64 );
    move_sprite( 7, 141, 28 );
    set_bkg_tiles( 0, 0, 20, 3, config_head );
    set_bkg_tiles( 8, 1,  4, 1, config_title2 );
    set_bkg_attribute( 16,  1, 2, 2, config_btn_a );
    set_bkg_tiles( 0, 3, 20, 1, config_page2a );
    for( i=4; i<17; i++ ) {
        set_bkg_tiles( 0, i, 20, 1, config_line );
    }
    set_bkg_tiles( 0, 17, 20, 1, config_tail );
    if( _cpu == CGB_TYPE ) {
        set_sprite_prop( 12, 4 );
        set_sprite_prop( 13, 4 );
        set_sprite_prop( 14, 4 );
        set_sprite_prop( 15, 4 );
    }
    set_sprite_tile( 12, 40+64 );
    set_sprite_tile( 13, 41+64 );
    set_sprite_tile( 14, 42+64 );
    set_sprite_tile( 15, 43+64 );

    /* page number */
    set_bkg_tiles( 13, 14, 4, 2, config_msg5 );
    c = '0' + (apps_num+6)/7;
    set_bkg_tiles( 17, 15, 1, 1, &c );

    /* apps name, version */
    show_config_page2_apps( 0 );
    show_config_icon( 1 );
}

UBYTE config_page( UBYTE *page )
{
    struct cursor_pos pos;

    pos.x = 0;  pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &config_cursor0 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( 16, 1, 2, 2, config_press );
            while( !(keycode.brk & J_EXIT) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( !(keycode.make & J_A) ) {
                    set_bkg_tiles( 16, 1, 2, 2, config_break );
                    (*page)++; if( (*page)>1 )  (*page)=0;
                    switch( *page ) {
                      case 0: /* config */
                        show_config_page1();
                        break;
                      case 1: /* apps */
                        show_config_page2();
                        break;
                    }
                    return( 0 );
                }
            }
        }
        if( cursor.y ) {
            return( (*page)*2+1 ); /* 1 or 3 */
        }
    }
    return( 0 );
}

UBYTE config_resource()
{
    struct cursor_pos pos;

    pos.x = *(&(nvm->font_type)+cursor.y-1);
    pos.y = cursor.y;
    pda_syscall( PDA_SET_CURSOR_MAP, &config_cursor1 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( cursor.y == 0 ) {
            cursor.y = 1;
            return( 0 );
        } else if( cursor.y == 5 ) {
            cursor.y = 4;
            return( 2 );
        } else if( cursor.y != pos.y ) {
            pos.y = cursor.y;
            pos.x = cursor.x = *(&(nvm->font_type)+cursor.y-1);
        }
        if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            if( cursor.x != pos.x ) {
                set_bkg_tiles( pos.x*2+8,  cursor.y*2+2, 1, 1, msg_clear );
                set_bkg_tiles( pos.x*2+10, cursor.y*2+2, 1, 1, msg_clear );
                pos.x = cursor.x;
                pda_syscall( (PDA_FONT_TYPE+cursor.y-1), (void *)&pos.x );
                set_bkg_tiles( pos.x*2+8,  cursor.y*2+2, 1, 1, msg_left );
                set_bkg_tiles( pos.x*2+10, cursor.y*2+2, 1, 1, msg_right );
                pda_syscall( PDA_INIT_COLOR, (void *)0 );
            }
        }
    }
    return( 0 );
}


UBYTE config_color()
{
    UBYTE x, y, cnt;
    UWORD rgb, data;
    struct cursor_pos pos;
    struct palette palette;

    set_sprite_prop( 5, 7 );
    set_sprite_prop( 6, 7 );
    pos.x = cursor.x; pos.y = cursor.y = 1; cnt = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &config_cursor2 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    while( !(keycode.brk & J_EXIT) ) {
        if( cursor.y == 1 ) {
            rgb = nvm->fore_color;
        } else {
            rgb = ~(nvm->back_color);
        }
        if( cursor.x == 0 ) { 
            data = rgb & 0x001F; /* red */
        } else if( cursor.x == 1 ) {
            data = (rgb>>5) & 0x001F; /* green */
        } else {
            data = (rgb>>10) & 0x001F; /* blue */
        }

        keycode.code = joypad();
        if( keycode.flag ) {
            if( !(keycode.code&(J_UP|J_DOWN|J_LEFT|J_RIGHT)) ) {
                keycode.flag = 0;
            }
        } else if( keycode.code & J_A ) {
            if( keycode.code & J_RIGHT ) {
                if( !cnt ) {
                    cnt = 10;
                    if( data < 31 ) {
                        data++;
                    }
                }
                keycode.flag = 0;
            }
            if( keycode.code & J_LEFT ) {
                if( !cnt ) {
                    cnt = 10;
                    if( data ) {
                        data--;
                    }
                }
                keycode.flag = 0;
            }
        } else {
            if( keycode.code & J_UP ) {
                 keycode.flag = 1;
                 keycode.brk = 0;
                if( cursor.y > 0 ) {
                    cursor.y--;
                }
            } else if( keycode.code & J_DOWN ) {
                 keycode.flag = 1;
                 keycode.brk = 0;
                if( cursor.y < (cursor.info.max_y-1) ) {
                    cursor.y++;
                }
            } else if( keycode.code & J_LEFT ) {
                 keycode.flag = 1;
                 keycode.brk = 0;
                if( cursor.x > 0 ) {
                    cursor.x--;
                }
            } else if( keycode.code & J_RIGHT ) {
                 keycode.flag = 1;
                 keycode.brk = 0;
                 if( cursor.x < (cursor.info.max_x-1) ) {
                    cursor.x++;
                }
            }
            if( cursor.y == 0 ) {
                move_sprite( 5, 0, 0 );
                move_sprite( 6, 0, 0 );
                return( 1 );
            } else if( cursor.y == 1 ) {
                rgb = nvm->fore_color;
            } else {
                rgb = ~(nvm->back_color);
            }
            if( cursor.x == 0 ) { 
                data = rgb & 0x001F; /* red */
            } else if( cursor.x == 1 ) {
                data = (rgb>>5) & 0x001F; /* green */
            } else {
                data = (rgb>>10) & 0x001F; /* blue */
            }
        }
        pda_syscall( PDA_SCAN_PAD, (void *)0 );

        /* animation */
        if( !cursor.sp ) {
            cursor.sp = 50;
            if( cursor.ani>=7 ) {
                cursor.ani = 0;
            } else {
                cursor.ani++;
            }
            set_sprite_tile( 1, cursor.ani*4+1 );
            set_sprite_tile( 2, cursor.ani*4+2 );
            set_sprite_tile( 3, cursor.ani*4+3 );
            set_sprite_tile( 4, cursor.ani*4+4 );
        } else {
            cursor.sp--;
        }
        x = cursor.info.off_x + cursor.info.add_x * cursor.x + data/2 -8;
        y = cursor.info.off_y + cursor.info.add_y * cursor.y;
        move_sprite( 1, x,   y   );
        move_sprite( 2, x,   y+8 );
        move_sprite( 3, x+8, y   );
        move_sprite( 4, x+8, y+8 );

        set_sprite_tile( 5, (data/10)+64 );
        set_sprite_tile( 6, (data%10)+80 );
        x = cursor.x * cursor.info.add_x + cursor.info.off_x;
        y = cursor.y * cursor.info.add_y + cursor.info.off_y + 17;
        move_sprite( 5, x,   y );
        move_sprite( 6, x+8, y );

        if( cnt ) {
            cnt--;
        }
        if( cursor.x == 2 ) { 
            rgb = (rgb & 0x03FF) | (data<<10); /* blue */
        } else if( cursor.x == 1 ) {
            rgb = (rgb & 0x7C1F) | (data<<5); /* green */
        } else {
            rgb = (rgb & 0x7FE0) | data; /* red */
        }
        if( cursor.y == 1 ) {
            nvm->fore_color = rgb;
        } else {
            nvm->back_color = ~rgb;
        }
        pda_syscall( PDA_INIT_COLOR, &palette );
        move_sprite( 10,  ((nvm->fore_color)&0x001F)/2     + 72, 112 );
        move_sprite( 11, (((nvm->fore_color)>>5)&0x001F)/2 +104, 112 );
        move_sprite( 12, (((nvm->fore_color)>>10)&0x001F)/2+136, 112 );
        move_sprite( 13,  (~(nvm->back_color)&0x001F)/2     + 72, 128 );
        move_sprite( 14, ((~(nvm->back_color)>>5)&0x001F)/2 +104, 128 );
        move_sprite( 15, ((~(nvm->back_color)>>10)&0x001F)/2+136, 128 );
    }
    return( 0 );
}

UBYTE config_apps()
{
    UBYTE i, flag, y, from;
    struct cursor_pos pos;

    if( _cpu == CGB_TYPE ) {
        set_sprite_prop( 10, 1 );
        set_sprite_prop( 11, 1 );
    }
    set_sprite_tile( 10, 36+64 );
    set_sprite_tile( 11, 36+64 );

    flag = 0;  y = 0;  pos.x = 0;  pos.y = 1;
    pda_syscall( PDA_SET_CURSOR_MAP, &config_cursor3 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( flag ) {
            move_sprite( 10, 16, (5+cursor.y)*8 );
        }
        show_config_icon( y+cursor.y );

        if( keycode.brk & J_B ) {
            keycode.brk &= ~J_B;
            if( flag ) {
                flag = 0;
                move_sprite( 10, 0, 0 );
                move_sprite( 11, 0, 0 );
            }
        } else if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            if( (y+cursor.y-1) < apps_num ) {
                if( flag ) {
                    i = nvm->apps[y+cursor.y-1];
                    nvm->apps[y+cursor.y-1] = nvm->apps[from];
                    nvm->apps[from] = i;
                    show_config_page2_apps( y );
                    move_sprite( 10, 0, 0 );
                    move_sprite( 11, 0, 0 );
                } else {
                    from = y+cursor.y-1;
                    move_sprite( 11, 16, (6+from-y)*8 );
                }
                flag = 1 - flag;
            }
        }

        if( cursor.y == 0 ) {
            move_sprite( 10, 0, 0 );
            move_sprite( 11, 0, 0 );
            return( 0 );
        } else if( cursor.y == 7 ) {
            if( y < (apps_num-7) ) {
                y++;
                cursor.y = 6;
                show_config_page2_apps( y );
                if( flag && (y<=from) && (from<(y+7)) ) {
                    move_sprite( 10, 16, (5+cursor.y)*8 );
                    move_sprite( 11, 16, (6+from-y)*8 );
                } else {
                    move_sprite( 11, 0, 0 );
                }
            }
        } else if( cursor.y == 1 ) {
            if( y > 0 ) {
                y--;
                cursor.y = 2;
                show_config_page2_apps( y );
                if( flag && (y<=from) && (from<(y+7)) ) {
                    move_sprite( 10, 16, (5+cursor.y)*8 );
                    move_sprite( 11, 16, (6+from-y)*8 );
                } else {
                    move_sprite( 11, 0, 0 );
                }
            }
        }
    }
    move_sprite( 10, 0, 0 );
    move_sprite( 11, 0, 0 );
    return( 3 );
}

/*==========================================================================*
 |  configuration                                                           |
 *==========================================================================*/
void configuration( void *arg )
{
    UBYTE section, page;
    struct cursor_pos pos;

    show_config_page1();

    /* setup cursor */
    page = 0; section = 1; pos.x = 0; pos.y = 1;
    while( !(keycode.brk & J_EXIT) ) {
        switch( section ) {
          case 0: /* page */
            section = config_page( &page );
            break;
          case 1: /* resource */
            cursor.y = pos.y;
            section = config_resource();
            pos.y = cursor.y;
            break;
          case 2: /* color */
            cursor.x = pos.x;
            section = config_color();
            pos.x = cursor.x;
            break;
          default: /* apps */
            section = config_apps();
            break;
        }
    }
}

/* EOF */