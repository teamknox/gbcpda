/*

 I_DIAL.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 32 x 32
  Tiles                : 0 to 0

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : 1 Byte per entry.

  Convert to metatiles : No.

 This file was generated by GBTD v2.1

*/

/* CGBpalette entries. */
unsigned char i_dialCGB[] =
{
  0x00
};
/* Start of tile array. */
unsigned char i_dial[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,
  0x07,0x06,0x1F,0x19,0x3E,0x27,0x3A,0x2F,
  0x39,0x2F,0x3F,0x26,0x3F,0x30,0x7F,0x73,
  0x5F,0x7C,0x5F,0x78,0x6F,0x7C,0x6F,0x7C,
  0x07,0x07,0x1F,0x18,0x7F,0x66,0xFB,0x9F,
  0xE9,0x7F,0xA7,0xFE,0x9F,0xF8,0x7F,0xE3,
  0xFF,0x8C,0xFF,0x30,0xFF,0xC0,0xFF,0x0C,
  0xFF,0x0C,0xFF,0xC0,0xFF,0xC0,0xFF,0x03,
  0x77,0x7E,0x77,0x7E,0x7B,0x7F,0x7B,0x7F,
  0x7D,0x7F,0x3D,0x3F,0x3E,0x3F,0x1E,0x1F,
  0xFF,0x00,0xFF,0x7D,0xFF,0x45,0xFF,0x45,
  0xFF,0x65,0xFF,0x65,0xFF,0x65,0xFF,0x7D,
  0xFF,0x03,0xFF,0x30,0xFF,0x30,0xFF,0x00,
  0xFF,0x80,0xFF,0x8C,0xFF,0xCC,0xFF,0xC0,
  0xFF,0x00,0xFF,0x3E,0xFF,0x22,0xFF,0x22,
  0xFF,0xBE,0xFF,0xB2,0xFF,0xB2,0xFF,0xB2,
  0x00,0x00,0xB8,0xF8,0xDE,0x7E,0xDF,0x7F,
  0xEF,0x3F,0xEF,0x3F,0xF7,0xDF,0xF7,0x1F,
  0xFB,0x0F,0xFB,0xCF,0xFD,0xC7,0xFD,0x07,
  0xFE,0x03,0xFE,0x33,0xFF,0x31,0xFF,0x01,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0xC0,0xC0,0xE0,0xE0,0xF0,0xF0,0xF8,0xF8,
  0xF8,0xF8,0xFC,0xFC,0xFC,0xFC,0xFE,0xFE,
  0xFE,0xFE,0xFE,0xFE,0x7E,0xFE,0x7E,0xFE,
  0xFF,0x00,0xFF,0x0C,0xFF,0x0C,0xFF,0xC0,
  0xFF,0xC0,0xFF,0x03,0xFF,0x03,0xFF,0x30,
  0xFF,0x00,0xFF,0x87,0xFF,0x84,0xFF,0x84,
  0xFF,0xC7,0xFF,0xC6,0xFF,0xC6,0xFF,0xF7,
  0xBE,0xFE,0xBE,0xFE,0xDE,0x7E,0xDE,0x7E,
  0xEE,0x3E,0xEC,0x3C,0xF4,0x1C,0xF0,0x18,
  0xFF,0x00,0xFF,0xDE,0xFF,0x12,0xFF,0x12,
  0xFF,0xDF,0xFF,0x19,0xFF,0x19,0xFF,0xD9
};

/* End of I_DIAL.C */
