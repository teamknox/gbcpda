/****************************************************************************
 *  bank11.c                                                                *
 *                                                                          *
 *    GB-DIAL   V2.10    2002/05/04                                         *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank11\i_dial.h"

extern void gb_dial( void *arg );

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_11[FUNC_NUM] = {
    { "GB-DIAL ", "2.10", gb_dial, i_dial, i_dialCGBPal0c1, i_dialCGBPal0c2 }
};
struct description desc_11 = { FUNC_NUM, (struct function *)func_11 };

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
/* DTMF */
#define C1 0x94U  /* 1209Hz, 1213Hz */
#define C2 0x9EU  /* 1336Hz, 1337Hz */
#define C3 0xA7U  /* 1477Hz, 1472Hz */
#define C4 0xB0U  /* 1633Hz, 1638Hz */
#define R1 0x44U  /*  697Hz,  697Hz */
#define R2 0x56U  /*  770Hz,  770Hz */
#define R3 0x66U  /*  852Hz,  851Hz */
#define R4 0x75U  /*  941Hz,  942Hz */ 
#define DTMF_ON		50UL	/* Tone on time		*/
#define DTMF_OFF	50UL	/* Tone off time	*/

/* CCITT 5 */
#define F1 0x45U  /*  700Hz,  700Hz */
#define F2 0x6EU  /*  900Hz,  897Hz */
#define F3 0x89U  /* 1100Hz, 1101Hz */
#define F4 0x9BU  /* 1300Hz, 1297Hz */
#define F5 0xA9U  /* 1500Hz, 1506Hz */
#define F6 0xB3U  /* 1700Hz, 1702Hz */
#define F7 0xC5U  /* 2200Hz, 2186Hz */
#define F8 0xC9U  /* 2400Hz, 2383Hz */
#define F9 0xCEU  /* 2600Hz, 2621Hz */
#define SOUND_ON  60UL  /* Tone on time		*/
#define NICKLE    0     /*  5 cent coin in US */
#define DIME      1     /* 10 cent coin in US */
#define QUARTER   2     /* 25 cent coin in US */
#define NICKLE_TIME   66UL
#define DIME_TIME     66UL
#define QUARTER_TIME  33UL
#define MAX_BUFF  40

#define TONE_DTMF   0
#define TONE_CCITT  1

/* mode */
#define MODE_EXIT   0
#define MODE_DIALER 1
#define MODE_PHREAK 2

typedef struct phreak_t {
    UWORD on;
    UWORD off;
    UWORD repeat;
    UWORD pause;
    UBYTE ccitt;
} _phreak_t;

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank11\i_dial.c"
#include "bank11\c_dial.c"
#include "bank11\s_dial.c"

unsigned char dial_head[] = {
    0x00,0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,0x00,
    0x00,0x04,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
    0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x04,0x00,
    0x00,0x04,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,
    0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x04,0x00,
    0x00,0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06,0x00
};
unsigned char dial_head_a[] = {
    0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,
    0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0
};
unsigned char dial_line[] = {
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};
unsigned char dial_line_a[] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

unsigned char dial_break3[] = { 0x10,0x11,0x12,0x13,0x00,0x14,0x15,0x16,0x17 };
unsigned char dial_press3[] = { 0x18,0x19,0x1A,0x1B,0x00,0x1C,0x1D,0x1E,0x1F };
unsigned char phreak_break2[] = { 0x10,0x12,0x15,0x17 };
unsigned char phreak_break3[] = { 0x10,0x11,0x12,0x15,0x16,0x17 };
unsigned char phreak_break4[] = { 0x10,0x11,0x11,0x12,0x15,0x16,0x16,0x17 };
unsigned char phreak_press2[] = { 0x18,0x1A,0x1D,0x1F };
unsigned char phreak_press3[] = { 0x18,0x19,0x1A,0x1D,0x1E,0x1F };
unsigned char phreak_press4[] = { 0x18,0x19,0x19,0x1A,0x1D,0x1E,0x1E,0x1F };
unsigned char dial_button_a[] = { 2,2,2,2,2,2,2,2,2 };

unsigned char row[4] = {R1,R2,R3,R4};	/* DTMF frequency strage of row */	
unsigned char col[4] = {C1,C2,C3,C4};	/* DTMF frequency strage of col */
unsigned char dtmf_13[16] = {C2,C1,C2,C3,C1,C2,C3,C1,C2,C3,C4,C4,C4,C4,C1,C3};
unsigned char dtmf_23[16] = {R4,R1,R1,R1,R2,R2,R2,R3,R3,R3,R1,R2,R3,R4,R4,R4};
unsigned char ccitt_13[10] = {F1,F1,F2,F1,F2,F3,F1,F2,F3,F4};
unsigned char ccitt_23[10] = {F2,F3,F3,F4,F4,F4,F5,F5,F5,F5};

UBYTE dial_lcd_code[] = { 0x02,0x03,0x04,0x0B,0x00,
                          0x05,0x06,0x07,0x0C,0x00,
                          0x08,0x09,0x0A,0x0D,0x11,
                          0x0F,0x01,0x10,0x0E,0x00 };
unsigned char dial_btn[] = { 0x00,'0','1','2','3','4','5','6','7',
                             '8','9','A','B','C','D','*','#',',' };
unsigned char lcd_phreak[] = {
    0xB8,0xBA,0xB6,0xB4,0x96,0xBC,0xAC,0x98,0x82,0xA4,
    0xB9,0xBB,0xB7,0xB5,0x97,0xBD,0xAD,0x99,0x83,0xA5
};
unsigned char lcd_dtmf[] = {
    0x9C,0xAE,0xA6,0xB0,0x80,0xA6,0x82,0x9C,0xB4,
    0x9D,0xAF,0xA7,0xB1,0x81,0xA7,0x83,0x9D,0xB5
};
unsigned char lcd_ccitt[] = {
    0x9A,0x9A,0xC0,0xAE,0xAE,0x8C,0x80,0xA6,0x82,0x9C,0xB4,
    0x9B,0x9B,0xC1,0xAF,0xAF,0x8D,0x81,0xA7,0x83,0x9D,0xB5
};
unsigned char lcd_c_start[] = {
    0x9A,0x82,0xB2,0xB0,0xC0,0xAA,0x80,0xA6,0x82,0x9C,0xB4,
    0x9B,0x83,0xB3,0xB1,0xC1,0xAB,0x81,0xA7,0x83,0x9D,0xB5
};
unsigned char lcd_ontime[] = {
    0x82,0xB2,0x80,0xAE,0xC0,0xA6,0xB4,0x80,0x80,0x80,0x80,0xBE,0xBE,0xBE,
    0x83,0xB3,0x81,0xAF,0xC1,0xA7,0xB5,0x81,0x81,0x81,0x81,0xBF,0xBF,0xBF
};
unsigned char lcd_offtime[] = {
    0x82,0xB0,0xB0,0x80,0xAE,0xC0,0xA6,0xB4,0x80,0x80,0x80,0xBE,0xBE,0xBE,
    0x83,0xB1,0xB1,0x81,0xAF,0xC1,0xA7,0xB5,0x81,0x81,0x81,0xBF,0xBF,0xBF
};
unsigned char lcd_repeat[] = {
    0xB6,0xB4,0xB8,0xB4,0x96,0xAE,0x80,0xB2,0x82,0x80,0x80,0xBE,0xBE,0xBE,
    0xB7,0xB5,0xB9,0xB5,0x97,0xAF,0x81,0xB3,0x83,0x81,0x81,0xBF,0xBF,0xBF
};
unsigned char lcd_pause[] = {
    0xB8,0x96,0xA8,0x8C,0xB4,0x80,0xAE,0xC0,0xA6,0xB4,0x80,0xBE,0xBE,0xBE,
    0xB9,0x97,0xA9,0x8D,0xB5,0x81,0xAF,0xC1,0xA7,0xB5,0x81,0xBF,0xBF,0xBF
};
unsigned char lcd_c_end[] = {
    0x80,0x9A,0x82,0xB2,0xB0,0xC0,0xAA,0x80,0xB4,0xB2,0x9C,0x80,0x80,0x80,
    0x81,0x9B,0x83,0xB3,0xB1,0xC1,0xAB,0x81,0xB5,0xB3,0x9D,0x81,0x81,0x81
};
unsigned char lcd_bar[] = { 0xAC,0xAC,0xAC,0xAD,0xAD,0xAD };
UBYTE lcd_clear[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
/*--------------------------------------------------------------------------*
 |  dialer mode                                                             |
 *--------------------------------------------------------------------------*/
struct cursor_info dial_cursor0 = { 5, 4,  30, 72, 24, 24 };
struct cursor_info dial_cursor1 = { 2, 4, 110, 72, 24, 24 };

void dial_break( UBYTE no )
{
    UBYTE x, y;
    unsigned char data;

    x = (no%5)*3+2; if( x==14 ) x=15;
    y = (no/5)*3+5;
    data = (unsigned char)(0x30+no);
    set_bkg_tiles( x, y, 3, 3, dial_break3 );
    set_bkg_attribute( x, y, 3, 3, dial_button_a );
    set_sprite_tile( 8+no, data );
    move_sprite( 8+no, x*8+16, y*8+24 );
}

void dial_press( UBYTE no )
{
    UBYTE x, y;
    unsigned char data;

    x = (no%5)*3+2; if( x==14 ) x=15;
    y = (no/5)*3+5;
    data = (unsigned char)(0x58+no);
    set_bkg_tiles( x, y, 3, 3, dial_press3 );
    set_sprite_tile( 8+no, data );
    move_sprite( 8+no, x*8+16, y*8+24 );
}

void dial_disp_lcd( UBYTE *str )
{
    UBYTE i;
    UBYTE *data;
    unsigned char tmp[2];

    data = str+4;
    for( i=4; i<20; i++ ) {
        tmp[0] = (*data)*2+0x80;
        tmp[1] = (*data)*2+0x81;
        set_bkg_tiles( i-2, 2, 1, 2, tmp );
        data++;
    }
}

void dial_clear( UBYTE *str )
{
    UBYTE i;
    UBYTE *to;

    to = str;
    for( i=0; i<20; i++ ) {
        *to++ = 0;
    }
    dial_disp_lcd( str );
}

void dial_add( UBYTE *str, UBYTE data )
{
    UBYTE i;
    UBYTE *to, *from;

    to = str;
    from = str+1;
    for( i=0; i<19; i++ ) {
        *to++ = *from++;
    }
    *to = data;
    dial_disp_lcd( str );
}

void dial_del( UBYTE *str )
{
    UBYTE i;
    UBYTE *to, *from;

    to = str+19;
    from = str+18;
    for( i=0; i<19; i++ ) {
        *to-- = *from--;
    }
    *str = 0;
    dial_disp_lcd( str );
}

void dial_play( UBYTE *str, UBYTE mode )
{
    UBYTE i, to;
    unsigned char buff[20];
    struct db_file data;
    struct search_name name;

    to = 0;
    for( i=0; i<20; i++ ) {
        if( *str ) {
            buff[to] = dial_btn[*str];
            to++;
        }
        str++;
    }
    buff[to] = 0;
    pda_syscall( PDA_PDC_DIALING, buff );  /* for PDC */
    pda_syscall( PDA_DIALTONE, buff );

    /* add to ram data base of GB-PHONE */
    if( mode ) {
        for( i=0; i<18; i++ ) {
            if( i < to ) {
                data.name[i]  = buff[i];
                data.phone[i] = buff[i];
            } else {
                data.name[i]  = 0;
                data.phone[i] = 0;
            }
        }
        /* set ram data base */
        if( data.name[0] != 0 ) {
            name.name = data.name;
            pda_syscall( PDA_SEARCH_PHONE_NAME, &name );
            if( name.no == 0xFFFF ) {
                pda_syscall( PDA_SET_RAM_DATA, &data );
            }
        }
    }
}

void show_dial_page()
{
    UBYTE i;

    for( i=5; i<40; i++ ) {
        move_sprite( i, 0, 0 );
    }
    for( i=5; i<17; i++ ) {
        set_bkg_tiles( 0, i, 20, 1, dial_line );
        set_bkg_attribute( 0, i, 20, 1, dial_line_a );
    }
    for( i=0; i<20; i++ ) {
        dial_break( i );
    }
}

UBYTE dial_loop()
{
    struct cursor_pos pos;
    UBYTE  buff[21];

    show_dial_page();
    pos.x = pos.y = 0;
    dial_clear( buff );

    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_SET_CURSOR_MAP, &dial_cursor0 );
        pda_syscall( PDA_INIT_CURSOR, &pos );
        while( cursor.x != 4 ) {
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return( MODE_EXIT );
            pos.x = cursor.x;  pos.y = cursor.y;
            if( keycode.make & J_A ) {
                pda_syscall( PDA_INIT_DIAL, (void *)0 );
                dial_press( pos.y*5+pos.x );
                dial_add( buff, dial_lcd_code[pos.y*5+pos.x] );
                /* frequncy register set up for DTMF */
                pda_syscall( PDA_INIT_DIAL, (void *)0 );
                NR13_REG = col[pos.x];
                NR23_REG = row[pos.y];
                NR24_REG = 0x87U;
                /* sound output on */
                NR51_REG = 0x33U;
                while( keycode.make & J_A ) {
                    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                }
                /* sound output off */
                NR51_REG = 0x00U;
                dial_break( pos.y*5+pos.x );
            } else if( keycode.brk & J_B ) {
                keycode.brk &= ~J_B;
                pda_syscall( PDA_PDC_ON_HOOK, (void *)0 );
            }
        }
        pos.x = 1;
        pda_syscall( PDA_SET_CURSOR_MAP, &dial_cursor1 );
        pda_syscall( PDA_INIT_CURSOR, &pos );
        while( cursor.x != 0 ) {
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return( MODE_EXIT );
            pos.x = cursor.x;  pos.y = cursor.y;
            if( keycode.make & J_A ) {
                pda_syscall( PDA_INIT_DIAL, (void *)0 );
                dial_press( pos.y*5+pos.x+3 );
                /* execute function keys */
                switch( pos.y ) {
                  case 0:
                    dial_clear( buff );
                    break;
                  case 1:
                    dial_del( buff );
                    break;
                  case 2:
                    dial_add( buff, dial_lcd_code[14] );
                    break;
                  case 3:
                    if( (buff[15]==0) && (buff[16]==3) && (buff[17]==7)
                      && (buff[18]==1) && (buff[19]==1) ) {  // 2600
                        while( keycode.make & J_A ) {
                            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                            if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        }
                        return( MODE_PHREAK ); /* change to phreak mode */
                    }
                    dial_play( buff, 1 );
                    break;
                }
                while( keycode.make & J_A ) {
                    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                }
                dial_break( pos.y*5+pos.x+3 );
            } else if( keycode.brk & J_B ) {
                keycode.brk &= ~J_B;
                pda_syscall( PDA_PDC_ON_HOOK, (void *)0 );
            }
        }
        pos.x = 3;
    }
    return( MODE_EXIT );
}

/*--------------------------------------------------------------------------*
 |  phreak mode                                                             |
 *--------------------------------------------------------------------------*/
struct cursor_info phreak_cursor0 = { 6, 5,  34,  66, 16, 16 };
struct cursor_info phreak_cursor1 = { 3, 5, 114,  66,  0, 16 };
struct cursor_info phreak_cursor2 = { 2, 5, 134,  66,  0, 16 };
struct cursor_info phreak_cursor3 = { 4, 3,  34, 114, 32, 16 };
struct cursor_info phreak_cursor4 = { 2, 5,  30,  66,  0, 16 };
struct cursor_info phreak_cursor5 = { 3, 4,  50,  66, 16, 16 };

void phreak_break( UBYTE no )
{
    UBYTE x, y;
    unsigned char data, tmp[4];
    struct button btn;

    data = (unsigned char)(0x30+no);
    if( no < 20 ) {  /* basic keys */
        x = (no%5)*2+5;
        y = (no/5)*2+5;
        if( (no%5) < 3 ) {
            btn.no = 20 + (no/5)*3+(no%5);
            btn.on = 0; // break(off)
            btn.data = s_dial + (UWORD)no * 16; // data;
            pda_syscall( PDA_MAKE_2X2BUTTON, &btn );
            tmp[0] = btn.no * 4 + 128;
            tmp[1] = tmp[0] + 1;
            tmp[2] = tmp[0] + 2;
            tmp[3] = tmp[0] + 3;
            set_bkg_tiles( x, y, 2, 2, tmp );
            set_bkg_attribute( x, y, 2, 2, dial_button_a );
        } else {
            set_bkg_tiles( x, y, 2, 2, phreak_break2 );
            set_bkg_attribute( x, y, 2, 2, dial_button_a );
            set_sprite_tile( 8+no, data );
            move_sprite( 8+no, x*8+12, y*8+20 );
        }
    } else if( no < 24 ) {  /* additional keys 1 */
        x = 15;
        y = (no-20)*2+5;
        set_bkg_tiles( x, y, 3, 2, phreak_break3 );
        set_bkg_attribute( x, y, 3, 2, dial_button_a );
        if( no == 23 ) {
            set_sprite_tile( 8+no, data );
            move_sprite( 8+no, x*8+12, y*8+20 );
            set_sprite_tile( 5, 0x50 );
            move_sprite( 5, x*8+20, y*8+20 );
        } else {
            set_sprite_tile( 8+no, data );
            move_sprite( 8+no, x*8+16, y*8+20 );
        }
    } else if( no < 32 ) {  /* additional keys 2 */
        x = ((no-24)%4)*4+2;
        y = ((no-24)/4)*2+13;
        set_bkg_tiles( x, y, 4, 2, phreak_break4 );
        set_bkg_attribute( x, y, 4, 2, dial_button_a );
        if( no == 25 ) {
            set_sprite_tile( 8+no, 0x48 );
            move_sprite( 8+no, x*8+18, y*8+20 );
            set_sprite_tile( 6, data );
            move_sprite( 6, x*8+26, y*8+20 );
        } else {
            set_sprite_tile( 8+no, data );
            move_sprite( 8+no, x*8+20, y*8+20 );
        }
    } else {  /* special function keys */
        x = 2;
        y = (no-32)*2+5;
        data = (unsigned char)(0x51+(no-32)*2);
        set_bkg_tiles( x, y, 3, 2, phreak_break3 );
        set_bkg_attribute( x, y, 3, 2, dial_button_a );
        if( no < 35 ) {
            set_sprite_tile( 8+(no-32)*5, data );
            move_sprite( 8+(no-32)*5, x*8+12, y*8+20 );
            set_sprite_tile( 9+(no-32)*5, data+1 );
            move_sprite( 9+(no-32)*5, x*8+20, y*8+20 );
        }
    }
}

void phreak_press( UBYTE no )
{
    UBYTE x, y;
    unsigned char data;
    struct button btn;

    data = (unsigned char)(0x58+no);
    if( no < 20 ) {  /* basic keys */
        x = (no%5)*2+5;
        y = (no/5)*2+5;
        if( (no%5) < 3 ) {
            btn.no = 20 + (no/5)*3+(no%5);
            btn.on = 1; // press(on)
            btn.data = s_dial + 16 * ((UWORD)no+40);
            pda_syscall( PDA_MAKE_2X2BUTTON, &btn );
        } else {
            set_bkg_tiles( x, y, 2, 2, phreak_press2 );
            set_sprite_tile( 8+no, data );
            move_sprite( 8+no, x*8+12, y*8+20 );
        }
    } else if( no < 24 ) {  /* additional keys 1 */
        x = 15;
        y = (no-20)*2+5;
        set_bkg_tiles( x, y, 3, 2, phreak_press3 );
        if( no == 23 ) {
            set_sprite_tile( 8+no, data );
            move_sprite( 8+no, x*8+12, y*8+20 );
            set_sprite_tile( 5, 0x78 );
            move_sprite( 5, x*8+20, y*8+20 );
        } else {
            set_sprite_tile( 8+no, data );
            move_sprite( 8+no, x*8+16, y*8+20 );
        }
    } else if( no < 32 ) {  /* additional keys 2 */
        x = ((no-24)%4)*4+2;
        y = ((no-24)/4)*2+13;
        set_bkg_tiles( x, y, 4, 2, phreak_press4 );
        if( no == 25 ) {
            set_sprite_tile( 8+no, 0x70 );
            move_sprite( 8+no, x*8+18, y*8+20 );
            set_sprite_tile( 6, data );
            move_sprite( 6, x*8+26, y*8+20 );
        } else {
            set_sprite_tile( 8+no, data );
            move_sprite( 8+no, x*8+20, y*8+20 );
        }
    } else {  /* special function keys */
        x = 2;
        y = (no-32)*2+5;
        data = (unsigned char)(0x79+(no-32)*2);
        set_bkg_tiles( x, y, 3, 2, phreak_press3 );
        if( no < 35 ) {
            set_sprite_tile( 8+(no-32)*5, data );
            move_sprite( 8+(no-32)*5, x*8+12, y*8+20 );
            set_sprite_tile( 9+(no-32)*5, data+1 );
            move_sprite( 9+(no-32)*5, x*8+20, y*8+20 );
        }
    }
}

void phreak_disp_lcd( UBYTE *str )
{
    UBYTE i;
    UBYTE *data;
    unsigned char tmp[2];

    data = str+(MAX_BUFF-16);
    for( i=0; i<16; i++ ) {
        tmp[0] = (*data)*2 + 0x80;
        tmp[1] = (*data)*2 + 0x81;
        set_bkg_tiles( i+2, 2, 1, 2, tmp );
        data++;
    }
}

void phreak_clear( UBYTE *str )
{
    UBYTE i;
    UBYTE *to;

    to = str;
    for( i=0; i<MAX_BUFF; i++ ) {
        *to++ = 0;
    }
    phreak_disp_lcd( str );
}

void phreak_add( UBYTE *str, UBYTE data )
{
    UBYTE i;
    UBYTE *to, *from;

    to = str;
    from = str+1;
    for( i=0; i<(MAX_BUFF-1); i++ ) {
        *to++ = *from++;
    }
    *to = data;
    phreak_disp_lcd( str );
}

void phreak_del( UBYTE *str )
{
    UBYTE i;
    UBYTE *to, *from;

    to = str+(MAX_BUFF-1);
    from = str+(MAX_BUFF-2);
    for( i=0; i<(MAX_BUFF-1); i++ ) {
        *to-- = *from--;
    }
    *str = 0;
    phreak_disp_lcd( str );
}

void phreak_coin( UBYTE coin )
{
    UBYTE i;

    switch( coin ) {
      case NICKLE:
        NR13_REG = F7;
        NR23_REG = F6;
        NR24_REG = 0x87U;
        NR51_REG = 0x33U;
        delay( NICKLE_TIME );
        NR51_REG = 0x00U;
        delay( NICKLE_TIME );
        break;
      case DIME:
        for( i=0; i<2; i++ ) {
            NR13_REG = F7;
            NR23_REG = F6;
            NR24_REG = 0x87U;
            NR51_REG = 0x33U;
            delay( DIME_TIME );
            NR51_REG = 0x00U;
            delay( DIME_TIME );
        }
        break;
      case QUARTER:
        for( i=0; i<5; i++ ) {
            NR13_REG = F7;
            NR23_REG = F6;
            NR24_REG = 0x87U;
            NR51_REG = 0x33U;
            delay( QUARTER_TIME );
            NR51_REG = 0x00U;
            delay( QUARTER_TIME );
        }
        break;
    }
}

void phreak_dial( struct phreak_t *pt, UBYTE *str )
{
    UBYTE i, to;
    UWORD repeat;
    unsigned char data, buff[MAX_BUFF];

    to = 0;
    for( i=0; i<MAX_BUFF; i++ ) {
        if( *str ) {
            buff[to] = *str;
            to++;
        }
        str++;
    }

    repeat = pt->repeat;
    while( repeat ) {
        repeat--;
        for( i=0; i<to; i++ ) {
            data = buff[i] - 1;
            if( data < 16 ) {
                NR13_REG = dtmf_13[data];
                NR23_REG = dtmf_23[data];
                NR24_REG = 0x87U;
                /* sound output on */
                NR51_REG = 0x33U;
                if( pt->on )   delay( pt->on  );  /* keep on */
                /* sound output off */
                NR51_REG = 0x00U;
                if( pt->off )  delay( pt->off );  /* keep off */
            } else if( data == 16 ) {
                /* sound output off */
                NR51_REG = 0x00U;
                if( pt->on )   delay( (pt->on)*2  );  /* keep on */
                if( pt->off )  delay( (pt->off)*2 );  /* keep off */
            } else {
                /* sound output off */
                NR51_REG = 0x00U;
            }
            if( joypad() & J_B ) { /* abort... */ 
                return;
            }
        }
		}
    if( pt->pause )  delay( pt->pause );
}


void phreak_ccitt( struct phreak_t *pt, UBYTE *str )
{
    UBYTE i, to;
    UWORD repeat;
    unsigned char data, buff[MAX_BUFF];

    to = 0;
    for( i=0; i<MAX_BUFF; i++ ) {
        if( *str ) {
            buff[to] = *str;
            to++;
        }
        str++;
    }

    repeat = pt->repeat;
    while( repeat ) {
        repeat--;
        for( i=0; i<to; i++ ) {
            data = buff[i] - 1;
            if( data < 10 ) {
                NR13_REG = ccitt_13[data];
                NR23_REG = ccitt_23[data];
                NR24_REG = 0x87U;
                /* sound output on */
                NR51_REG = 0x33U;
                if( pt->on )   delay( pt->on  );  /* keep on */
                NR51_REG = 0x00U;
                if( pt->off )  delay( pt->off );  /* keep off */
            } else if( data == 16 ) {
                /* sound output off */
                NR51_REG = 0x00U;
                if( pt->on )   delay( (pt->on)*2  );  /* keep on */
                if( pt->off )  delay( (pt->off)*2 );  /* keep off */
            } else {
                /* sound output off */
                NR51_REG = 0x00U;
            }
            /* sound output off */
            if( joypad() & J_B ) { /* abort... */ 
                return;
            }
        }
		}
    if( pt->pause )  delay( pt->pause );
}

void phreak_config( UWORD *value )
{
    struct cursor_pos pos;
    UBYTE  count, data;
    unsigned char tmp[2];

    *value = 0L;
    count = 3;
    pos.x = 1;  pos.y = 3;
    pda_syscall( PDA_SET_CURSOR_MAP, &phreak_cursor5 );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( count ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return;
        pos.x = cursor.x;  pos.y = cursor.y;
        if( keycode.make & J_A ) {
            if( (pos.y!=3) || (pos.x==1) ) {
                count--;
                phreak_press( pos.y*5+pos.x );
                data = dial_lcd_code[pos.y*5+pos.x];
                *value = ((UWORD)*value)*10+(data-1);
                tmp[0] = data*2+0x80;
                tmp[1] = data*2+0x81;
                set_bkg_tiles( 17-count, 2, 1, 2, tmp );
                while( keycode.make & J_A ) {
                    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    if( keycode.brk & J_EXIT )  return;
                }
                phreak_break( pos.y*5+pos.x );
            }
        }
    }
}

void show_phreak_page()
{
    UBYTE i;

    for( i=5; i<17; i++ ) {
        set_bkg_tiles( 0, i, 20, 1, dial_line );
        set_bkg_attribute( 0, i, 20, 1, dial_line_a );
    }

    for( i=0; i<20; i++ ) {
        move_sprite( 8+i, 0, 0 );
    }
    for( i=0; i<36; i++ ) {
        phreak_break( i );
    }
}

void phreak_set_str( UWORD n, UBYTE *str )
{
    *str++ = (BYTE)((n/100)%10+1);
    *str++ = (BYTE)((n/10)%10+1);
    *str   = (BYTE)(n%10+1);
}

void parm_init( struct phreak_t *phreak )
{
    phreak->on     = DTMF_ON;
    phreak->off    = DTMF_OFF;
    phreak->repeat = 1;
    phreak->pause  = 99;
    phreak->ccitt  = TONE_DTMF;
}

UBYTE phreak_loop()
{
    struct cursor_pos pos;
    struct phreak_t phreak;
    UBYTE  i, area, hook;
    UBYTE  buff[MAX_BUFF], status[20];

    show_phreak_page();
    phreak_clear( buff );
    set_bkg_tiles( 5, 2, 10, 2, lcd_phreak ); /* title */

    /* init params */
    parm_init( &phreak );

    area = 0;
    pos.x = 1;
    pos.y = hook = 0;
    while( !(keycode.brk & J_EXIT) ) {
        switch( area ) {
          case 0: /* dial sounds */
            pda_syscall( PDA_SET_CURSOR_MAP, &phreak_cursor0 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( (pos.x>0) && (pos.x<5) && (pos.y<4) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                pos.x = cursor.x;  pos.y = cursor.y;
                if( keycode.make & J_A ) {
                    pda_syscall( PDA_INIT_DIAL, (void *)0 );
                    phreak_press( pos.y*5+pos.x-1 );
                    phreak_add( buff, dial_lcd_code[pos.y*5+pos.x-1] );
                    /* frequncy register set up */
                    pda_syscall( PDA_INIT_DIAL, (void *)0 );
                    if( phreak.ccitt ) {
                        if( (pos.y<3) && (pos.x<4) ) {
                            NR13_REG = ccitt_13[pos.y*3+pos.x];
                            NR23_REG = ccitt_23[pos.y*3+pos.x];
                            NR24_REG = 0x87U;
                            /* sound output on */
                            NR51_REG = 0x33U;
                        } else if( (pos.y==3) && (pos.x==2) ){
                            NR13_REG = ccitt_13[0];
                            NR23_REG = ccitt_23[0];
                            NR24_REG = 0x87U;
                            /* sound output on */
                            NR51_REG = 0x33U;
                        }
                    } else {
                        NR13_REG = col[pos.x-1];
                        NR23_REG = row[pos.y];
                        NR24_REG = 0x87U;
                        /* sound output on */
                        NR51_REG = 0x33U;
                    }
                    while( keycode.make & J_A ) {
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                        if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                    }
                    /* sound output off */
                    NR51_REG = 0x00U;
                    phreak_break( pos.y*5+pos.x-1 );
                }
            }
            if( pos.x == 5 ) {
                area = 1;
            } else if( pos.x == 0 ) {
                area = 4;
            } else {
                area = 3;
                pos.x = pos.x/2;
            }
            break;
          case 1: /* dial functions */
            pos.x = 1;
            pda_syscall( PDA_SET_CURSOR_MAP, &phreak_cursor1 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( (pos.x==1) && (pos.y<4) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.code && hook ) {
                    pda_syscall( PDA_PDC_ON_HOOK, (void *)0 );
                    hook = 0;
                }
                if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                pos.x = cursor.x;  pos.y = cursor.y;
                if( keycode.make & J_A ) {
                    pda_syscall( PDA_INIT_DIAL, (void *)0 );
                    phreak_press( pos.y*5+4 );
                    switch( pos.y ) {
                      case 0:
                        phreak_clear( buff );
                        break;
                      case 1:
                        phreak_del( buff );
                        break;
                      case 2:
                        phreak_add( buff, dial_lcd_code[14] );
                        break;
                      case 3:
                        if( phreak.ccitt ) {
                            phreak_ccitt( &phreak, buff );
                        } else {
                            phreak_dial( &phreak, buff );
                            hook = 1;
                        }
                        break;
                    }
                    while( keycode.make & J_A ) {
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                        if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                    }
                    phreak_break( pos.y*5+4 );
                }
            }
            if( pos.x == 0 ) {
                area = 0;
                pos.x = 4;
            } else if( pos.x == 2 ) {
                area = 2;
            } else {
                area = 3;
                pos.x = 2;
            }
            break;
          case 2: /* phreak function 1 */
            pos.x = 1;
            pda_syscall( PDA_SET_CURSOR_MAP, &phreak_cursor2 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( (pos.x>0) && (pos.y<4) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                pos.x = cursor.x;  pos.y = cursor.y;
                if( keycode.make & J_A ) {
                    pda_syscall( PDA_INIT_DIAL, (void *)0 );
                    phreak_press( pos.y+20 );
                    switch( pos.y ) {
                      case 0: /* ? */
                        dial_disp_lcd( lcd_clear );
                        if( phreak.ccitt ) {
                            set_bkg_tiles( 5, 2, 11, 2, lcd_ccitt );
                        } else {
                            set_bkg_tiles( 6, 2, 9, 2, lcd_dtmf );
                        }
                        delay( 1000 );
                        status[4]  = 0;
                        phreak_set_str( phreak.on, &status[5] );
                        status[8]  = 0;
                        phreak_set_str( phreak.off, &status[9] );
                        status[12]  = 0;
                        phreak_set_str( phreak.repeat, &status[13] );
                        status[16] = 0;
                        phreak_set_str( phreak.pause, &status[17] );
                        dial_disp_lcd( status );
                        delay( 1000 );
                        while( keycode.make & J_A ) {
                            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                            if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        }
                        phreak_break( pos.y+20 );
                        break;
                      case 1: /* config */
                        dial_disp_lcd( lcd_clear );
                        set_bkg_tiles( 5, 2, 11, 2, lcd_c_start );
                        delay( 50 );
                        while( keycode.make & J_A ) {
                            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                            if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        }
                        phreak_break( pos.y+20 );
                        set_bkg_tiles( 4, 2, 14, 2, lcd_ontime );
                        phreak_config( &phreak.on );
                        if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        set_bkg_tiles( 4, 2, 14, 2, lcd_offtime );
                        phreak_config( &phreak.off );
                        if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        set_bkg_tiles( 4, 2, 14, 2, lcd_repeat );
                        phreak_config( &phreak.repeat );
                        if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        set_bkg_tiles( 4, 2, 14, 2, lcd_pause );
                        phreak_config( &phreak.pause );
                        if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        set_bkg_tiles( 4, 2, 14, 2, lcd_c_end );
                        delay( 1000 );
                        pda_syscall( PDA_SET_CURSOR_MAP, &phreak_cursor2 );
                        pda_syscall( PDA_INIT_CURSOR, &pos );
                        break;
                      case 2: /* dtmf/ccitt */
                        dial_disp_lcd( lcd_clear );
                        phreak.ccitt = 1 - phreak.ccitt;
                        if( phreak.ccitt ) {
                            set_bkg_tiles( 5, 2, 11, 2, lcd_ccitt );
                        } else {
                            set_bkg_tiles( 6, 2, 9, 2, lcd_dtmf );
                        }
                        delay( 50 );
                        while( keycode.make & J_A ) {
                            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                            if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        }
                        phreak_break( pos.y+20 );
                        pda_syscall( PDA_INIT_DIAL, (void *)0 );
                        break;
                      case 3: /* 2600 */
                        NR13_REG = F9;
                        NR23_REG = F9;
                        NR24_REG = 0x87U;
                        NR51_REG = 0x33U;
                        while( keycode.make & J_A ) {
                            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                            if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                        }
                        phreak_break( pos.y+20 );
                        NR51_REG = 0x00U;
                        break;
                    }
                    phreak_disp_lcd( buff );
                }
            }
            if( pos.x == 0 ) {
                area = 1;
            } else {
                area = 3;
                pos.x = 3;
            }
            break;
          case 3: /* phreak function 2 */
            pos.y = 1;
            pda_syscall( PDA_SET_CURSOR_MAP, &phreak_cursor3 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( pos.y > 0 ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                pos.x = cursor.x;  pos.y = cursor.y;
                if( keycode.make & J_A ) {
                    pda_syscall( PDA_INIT_DIAL, (void *)0 );
                    phreak_press( pos.y*4+pos.x+20 );
                    if( pos.y == 1 ) {
                        switch( pos.x ) {
                          case 0: /* ST */
                            NR13_REG = F5;
                            NR23_REG = F6;
                            break;
                          case 1: /* STp */
                            NR13_REG = F2;
                            NR23_REG = F6;
                            break;
                          case 2: /* 2p */
                            NR13_REG = F4;
                            NR23_REG = F6;
                            break;
                          case 3: /* 3p */
                            NR13_REG = F1;
                            NR23_REG = F6;
                            break;
                        }
                        NR24_REG = 0x87U;
                        NR51_REG = 0x33U;
                        delay( SOUND_ON );
                        NR51_REG = 0x00U;
                        delay( SOUND_ON );
                    } else {
                        switch( pos.x ) {
                          case 0: /* Kp */
                            NR13_REG = F3;
                            NR23_REG = F6;
                            NR24_REG = 0x87U;
                            NR51_REG = 0x33U;
                            delay( SOUND_ON );
                            NR51_REG = 0x00U;
                            delay( SOUND_ON );
                            break;
                          case 1: /* 5 cent */
                            phreak_coin( NICKLE );
                            break;
                          case 2: /* 10 cent */
                            phreak_coin( DIME );
                            break;
                          case 3: /* 25 cent */
                            phreak_coin( QUARTER );
                            break;
                        }
                    }
                    while( keycode.make & J_A ) {
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                        if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                    }
                    phreak_break( pos.y*4+pos.x+20 );
                }
            }
            if( pos.x == 0 ) {
                area = 4;
                pos.y = 3;
            } else if( pos.x == 1 ) {
                area = 0;
                pos.x = 2;
                pos.y = 3;
            } else if( pos.x == 2 ) {
                area = 0;
                pos.x = 4;
                pos.y = 3;
            } else {
                area = 2;
                pos.y = 3;
            }
            break;
          case 4: /* phreak function 1 */
            pos.x = 0;
            pda_syscall( PDA_SET_CURSOR_MAP, &phreak_cursor4 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( (pos.x<1) && (pos.y<4) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                pos.x = cursor.x;  pos.y = cursor.y;
                if( keycode.make & J_A ) {
                    phreak_press( pos.y+32 );
                    switch( pos.y ) {
                      case 0: /* fastest dialing setting */
                        parm_init( &phreak );
                        phreak.on  = 25;
                        phreak.off = 35;
                        break;
                      case 1: /* answering machine cracking */
                        parm_init( &phreak );
                        phreak.on     = 20;
                        phreak.off    = 0;
                        phreak.repeat = 4;
                        phreak.pause  = 99;
                        phreak_clear( buff );
                        for( i=0; i<9; i++ ) {
                            buff[MAX_BUFF-10+i] = i+2;
                        }
                        buff[MAX_BUFF-1] = 1;
                        break;
                      case 2: /* default setting */
                        parm_init( &phreak );
                        break;
                      case 3: /* reserved */
                        break;
                    }
                    while( keycode.make & J_A ) {
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                        if( keycode.brk & J_EXIT )  return( MODE_EXIT );
                    }
                    phreak_break( pos.y+32 );
                    phreak_disp_lcd( buff );
                }
            }
            if( pos.x == 1 ) {
                area = 0;
            } else {
                area = 3;
                pos.x = 0;
            }
            break;
        }
    }
    return( MODE_EXIT );
}

/*--------------------------------------------------------------------------*
 |  init                                                                    |
 *--------------------------------------------------------------------------*/
void init_dial()
{
    struct palette palette;
    UBYTE i;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_data( 0x80, 66, c_dial );
    set_sprite_data( 0x30, 80, s_dial );
    set_bkg_tiles( 0, 1, 20, 4, dial_head );
    set_bkg_attribute( 0, 2, 20, 2, dial_head_a );

    palette.no = 9;
    switch( nvm->button_type ) {
      case 1:
        palette.color1 = ~(nvm->back_color);
        palette.color2 = CLR_RGB(31,31,0); /* yellow */
        palette.color3 = CLR_BLACK;
        break;
      case 2:
        palette.color1 = CLR_WHITE;
        palette.color2 = CLR_RGB(31,31,0); /* yellow */
        palette.color3 = CLR_BLACK;
        break;
      default:
        palette.color1 = ~(nvm->back_color);
        palette.color2 = CLR_RGB(31,31,0); /* yellow */
        palette.color3 = CLR_BLACK;
        break;
    }
    pda_syscall( PDA_SET_PALETTE, &palette );
    for( i=5; i<40; i++ ) {
        set_sprite_prop( i, 1 );
    }
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_dial( void *arg )
{
    UBYTE mode;

    init_dial();

    mode = MODE_DIALER;
    while( mode != MODE_EXIT ) {
        switch( mode ) {
          case MODE_PHREAK:
            mode = phreak_loop();
            break;
          case MODE_DIALER:
          default:
            mode = dial_loop();
            break;
        }
    }

    /* sound output off */
    NR51_REG = 0x00U;
}

/* EOF */