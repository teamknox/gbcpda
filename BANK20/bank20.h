/****************************************************************************
 *  bank20.h                                                                *
 *                                                                          *
 *    Copyright(C)  2000, 2001   TeamKNOx                                   *
 ****************************************************************************/

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define RRC_MODE_JOYSTICK       0
#define RRC_MODE_PIANO          1
#define RRC_MODE_GENUINE        2
#define RRC_FLAG_OFF            0
#define RRC_FLAG_ON             1
#define RRC_OFF                 0
#define RRC_ON                  1
#define RRC_MOVEMENU            1
#define RRC_SELECTION           2
#define RRC_TASK                3
#define RRC_DRIVE_LR            0
#define RRC_DRIVE_DS            1
#define RRC_MOTOR_A             0x01
#define RRC_MOTOR_B             0x02
#define RRC_MOTOR_C             0x04
#define RRC_MOTORS              (RRC_MOTOR_A | RRC_MOTOR_B | RRC_MOTOR_C)
#define RRC_REVERSE             0x00
#define RRC_FORWARD             0x80
#define RRC_MOTOR_ON            0x80
#define RRC_MOTOR_OFF           0x40
#define RRC_F3                  175
#define RRC_FS3                 185
#define RRC_G3                  196
#define RRC_GS3                 208
#define RRC_A3                  220
#define RRC_AS3                 233
#define RRC_B3                  247
#define RRC_C4                  262
#define RRC_CS4                 277
#define RRC_D4                  294
#define RRC_DS4                 311
#define RRC_E4                  330
#define RRC_F4                  350
#define RRC_FS4                 370
#define RRC_G4                  392
#define RRC_GS4                 416
#define RRC_A4                  440
#define RRC_AS4                 466
#define RRC_B4                  494
#define RRC_C5                  523
#define RRC_CS5                 554
#define RRC_D5                  587
#define RRC_DS5                 622
#define RRC_E5                  660
#define RRC_F5                  699
#define RRC_FS5                 741
#define RRC_G5                  785
#define RRC_GS5                 832
#define RRC_A5                  880
#define RRC_AS5                 932
#define RRC_B5                  988
#define RRC_C6                  1047
#define RRC_R                   65535
#define RRC_L1                  160
#define RRC_L2H                 120
#define RRC_L2                  80
#define RRC_L4H                 60
#define RRC_L4                  40
#define RRC_L8H                 30
#define RRC_L8                  20
#define RRC_L16H                15
#define RRC_L16                 10

/*==========================================================================*
 |  work area                                                               |
 *==========================================================================*/
typedef struct rrc_t {
  UBYTE mode;
  UBYTE drive_mode;
  UBYTE motor_sel[2];
  UBYTE motor_dir[2];
  UBYTE motor_pwr;
  UBYTE motor_pwr_old;
  UBYTE task_num;
  UBYTE tone_len;
  UBYTE tglbit3;
} _rrc_t;

typedef struct rrc_rcx_cntrl {
  struct rrc_t *rrc;
  UBYTE *cmd;
  UBYTE p1;
  UBYTE p2;
  UBYTE p3;
  UBYTE mode;
} _rrc_rcx_cntrl;

/* EOF */