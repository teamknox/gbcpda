/*

 C_RRC.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 19

  Palette colors       : None.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char c_rrc[] =
{
  0xFF,0x00,0xFF,0x01,0xFF,0x03,0xFF,0x07,
  0xFF,0x0F,0xFF,0x1F,0xFF,0x3F,0xFF,0x7F,
  0xFF,0x7F,0xFF,0x3F,0xFF,0x1F,0xFF,0x0F,
  0xFF,0x07,0xFF,0x03,0xFF,0x01,0xFF,0x00,
  0xFF,0x00,0xFF,0x40,0xFF,0x60,0xFF,0x70,
  0xFF,0x78,0xFF,0x7C,0xFF,0x7E,0xFF,0x7F,
  0xFF,0x7F,0xFF,0x7E,0xFF,0x7C,0xFF,0x78,
  0xFF,0x70,0xFF,0x60,0xFF,0x40,0xFF,0x00,
  0xFF,0x00,0xC0,0x3F,0x80,0x5F,0x80,0x60,
  0x80,0x60,0x80,0x60,0x80,0x60,0x80,0x60,
  0xFF,0x00,0x00,0xFF,0x00,0xFF,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0x00,0x03,0xFC,0x03,0xFA,0x07,0x06,
  0x07,0x06,0x07,0x06,0x07,0x06,0x07,0x06,
  0x80,0x60,0x80,0x60,0x80,0x60,0x80,0x60,
  0x80,0x60,0x80,0x60,0x80,0x60,0x80,0x60,
  0x07,0x06,0x07,0x06,0x07,0x06,0x07,0x06,
  0x07,0x06,0x07,0x06,0x07,0x06,0x07,0x06,
  0x80,0x60,0x80,0x60,0x80,0x60,0x80,0x60,
  0x80,0x60,0x9F,0x5F,0xFF,0x3F,0xFF,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,
  0x07,0x06,0x07,0x06,0x07,0x06,0x07,0x06,
  0x07,0x06,0xFB,0xFA,0xFF,0xFC,0xFF,0x00,
  0xFF,0x00,0xFF,0x3F,0xDF,0x5F,0xE0,0x60,
  0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0x60,
  0xFF,0x00,0xFF,0xFF,0xFF,0xFF,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0x00,0xFF,0xFC,0xF9,0xFA,0x01,0x06,
  0x01,0x06,0x01,0x06,0x01,0x06,0x01,0x06,
  0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0x60,
  0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0x60,
  0x01,0x06,0x01,0x06,0x01,0x06,0x01,0x06,
  0x01,0x06,0x01,0x06,0x01,0x06,0x01,0x06,
  0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0x60,
  0xE0,0x60,0xC0,0x5F,0xC0,0x3F,0xFF,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0xFF,0x00,0xFF,0xFF,0x00,
  0x01,0x06,0x01,0x06,0x01,0x06,0x01,0x06,
  0x01,0x06,0x01,0xFA,0x03,0xFC,0xFF,0x00
};

/* End of C_RRC.C */
