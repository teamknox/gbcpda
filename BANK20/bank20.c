/****************************************************************************
 *  bank20.c (part 1/2)                                                     *
 *                                                                          *
 *    RoReCon  V2.10    2002/03/31 (Base: 2000/03 version)                  *
 *                                                                          *
 *    Copyright(C)  2000, 2001, 2002   TeamKNOx                             *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank20\i_rrc.h"
#include "bank20\s_rrc.h"
#include "bank20\bank20.h"

extern void gb_rrc( void *arg );
extern UBYTE irda_speed;
extern UBYTE irda_parity;

/* extern (bank 21) */
extern void rrc_mode_piano( struct rrc_t *rrc );
extern void rrc_mode_genuine( struct rrc_t *rrc );


/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_20[FUNC_NUM] = {
    { "RoReCon ", "2.10", gb_rrc, i_rrc, i_rrcCGBPal0c1, i_rrcCGBPal0c2 }
};
struct description desc_20 = { FUNC_NUM, func_20 };

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank20\i_rrc.c"
#include "bank20\c_rrc.c"
#include "bank20\s_rrc.c"
#include "bank20\joystick.c"
#include "bank20\piano.c"
#include "bank20\genuine.c"

UWORD c_rrc_palette[] = {
  i_rrcCGBPal2c0, i_rrcCGBPal2c1, i_rrcCGBPal2c2, i_rrcCGBPal2c3,
  i_rrcCGBPal3c0, i_rrcCGBPal3c1, i_rrcCGBPal3c2, i_rrcCGBPal3c3,
  i_rrcCGBPal4c0, i_rrcCGBPal4c1, i_rrcCGBPal4c2, i_rrcCGBPal4c3,
  i_rrcCGBPal5c0, i_rrcCGBPal5c1, i_rrcCGBPal5c2, i_rrcCGBPal5c3,
  i_rrcCGBPal6c0, i_rrcCGBPal6c1, i_rrcCGBPal6c2, i_rrcCGBPal6c3,
  i_rrcCGBPal7c0, i_rrcCGBPal7c1, i_rrcCGBPal7c2, i_rrcCGBPal7c3,
};
UWORD s_rrc_palette[] = {
  s_rrcCGBPal0c0, s_rrcCGBPal0c1, s_rrcCGBPal0c2, s_rrcCGBPal0c3,
  s_rrcCGBPal1c0, s_rrcCGBPal1c1, s_rrcCGBPal1c2, s_rrcCGBPal1c3
};

unsigned char rrc_joystick_form[] = {
  0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,
  0x04,0x80,0x00,0x00,0x52,0x43,0x58,0x00,0x4A,0x6F,
  0x79,0x73,0x74,0x69,0x63,0x6B,0x00,0x00,0x00,0x04,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06,
  0x00,0x10,0x11,0x12,0x10,0x11,0x12,0x10,0x11,0x12,
  0x01,0x4D,0x6F,0x64,0x65,0x02,0x02,0x02,0x02,0x03,
  0x00,0x13,0x82,0x14,0x13,0x83,0x14,0x13,0x84,0x14,
  0x04,0x4C,0x2D,0x52,0x00,0x00,0x44,0x2D,0x53,0x04,
  0x00,0x15,0x16,0x17,0x15,0x16,0x17,0x15,0x16,0x17,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06,
  0x00,0x10,0x11,0x12,0x18,0x19,0x1A,0x10,0x11,0x12,
  0x01,0x4D,0x6F,0x74,0x6F,0x72,0x02,0x02,0x02,0x03,
  0x00,0x13,0x85,0x14,0x1B,0x86,0x1C,0x13,0x87,0x14,
  0x04,0x00,0x3A,0x41,0x42,0x43,0x3A,0x3C,0x3E,0x04,
  0x00,0x15,0x16,0x17,0x1D,0x1E,0x1F,0x15,0x16,0x17,
  0x04,0x00,0x3A,0x41,0x42,0x43,0x3A,0x3C,0x3E,0x04,
  0x00,0x10,0x11,0x12,0x10,0x11,0x12,0x10,0x11,0x12,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06,
  0x00,0x13,0x88,0x14,0x13,0x89,0x14,0x13,0x8A,0x14,
  0x01,0x53,0x70,0x65,0x65,0x64,0x02,0x02,0x02,0x03,
  0x00,0x15,0x16,0x17,0x15,0x16,0x17,0x15,0x16,0x17,
  0x04,0x00,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x04,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06,
  0x01,0x54,0x61,0x73,0x6B,0x02,0x02,0x02,0x02,0x02,
  0x09,0x4D,0x65,0x73,0x73,0x61,0x67,0x65,0x02,0x03,
  0x04,0x00,0x00,0x00,0x10,0x11,0x12,0x10,0x11,0x12,
  0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
  0x04,0x3C,0x00,0x3E,0x13,0x8B,0x14,0x13,0x86,0x14,
  0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
  0x04,0x00,0x00,0x00,0x15,0x16,0x17,0x15,0x16,0x17,
  0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x0A,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};

unsigned char rrc_piano_form[] = {
  0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,
  0x04,0x87,0x00,0x00,0x52,0x43,0x58,0x00,0x50,0x69,
  0x61,0x6E,0x6F,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06,
  0x00,0x84,0x84,0x84,0x84,0x84,0x84,0x84,0x84,0x84,
  0x84,0x84,0x84,0x84,0x84,0x84,0x84,0x84,0x84,0x00,
  0x00,0x80,0x83,0x83,0x82,0x80,0x83,0x82,0x80,0x83,
  0x83,0x82,0x80,0x83,0x82,0x80,0x83,0x83,0x82,0x00,
  0x00,0x80,0x83,0x83,0x82,0x80,0x83,0x82,0x80,0x83,
  0x83,0x82,0x80,0x83,0x82,0x80,0x83,0x83,0x82,0x00,
  0x00,0x80,0x83,0x83,0x82,0x80,0x83,0x82,0x80,0x83,
  0x83,0x82,0x80,0x83,0x82,0x80,0x83,0x83,0x82,0x00,
  0x00,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,
  0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x81,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x10,0x11,0x12,0x00,0x01,0x02,0x02,0x02,0x02,
  0x02,0x03,0x00,0x10,0x11,0x12,0x10,0x11,0x12,0x00,
  0x00,0x13,0x85,0x14,0x00,0x04,0x89,0x8A,0x8B,0x8C,
  0x8D,0x04,0x00,0x13,0x86,0x14,0x13,0x88,0x14,0x00,
  0x00,0x15,0x16,0x17,0x00,0x05,0x02,0x02,0x02,0x02,
  0x02,0x06,0x00,0x15,0x16,0x17,0x15,0x16,0x17,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x01,0x53,0x6F,0x6E,0x67,0x02,0x02,0x02,0x02,0x02,
  0x09,0x4D,0x65,0x73,0x73,0x61,0x67,0x65,0x02,0x03,
  0x04,0x10,0x11,0x12,0x10,0x11,0x12,0x10,0x11,0x12,
  0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
  0x04,0x13,0x31,0x14,0x13,0x32,0x14,0x13,0x33,0x14,
  0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
  0x04,0x15,0x16,0x17,0x15,0x16,0x17,0x15,0x16,0x17,
  0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x0A,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};

unsigned char rrc_genuine_form[] = {
  0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0xCD,
  0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xD0,
  0x04,0xD5,0x00,0x80,0x81,0x82,0x83,0x84,0x00,0xCE,
  0xA4,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xA4,0xA4,0xD1,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0xCE,
  0xA4,0xAA,0xA4,0xA4,0xAB,0xA4,0xA4,0xAC,0xA4,0xD1,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xCE,
  0xAD,0xAE,0xAD,0xAD,0xAE,0xAD,0xAD,0xAE,0xAD,0xD1,
  0x00,0x00,0x00,0x85,0x85,0x85,0x85,0x00,0x00,0xCE,
  0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xD1,
  0x00,0x00,0x00,0x86,0x88,0x8A,0x8C,0x00,0x00,0xCE,
  0xB0,0xB1,0xB2,0xB0,0xB1,0xB2,0xB0,0xB1,0xB2,0xD1,
  0x00,0x00,0x00,0x87,0x89,0x8B,0x8D,0x00,0x00,0xCE,
  0xB6,0xAE,0xB7,0xB6,0xAE,0xB7,0xB6,0xAE,0xB7,0xD1,
  0x00,0x00,0x00,0x85,0x85,0x85,0x85,0x00,0x00,0xCE,
  0xBA,0xA4,0xB7,0xBB,0xA4,0xB7,0xBC,0xA4,0xB7,0xD1,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xCE,
  0xB6,0xAE,0xB7,0xB6,0xAE,0xB7,0xB6,0xAE,0xB7,0xD1,
  0x00,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0xCE,
  0xB3,0xB4,0xB5,0xB3,0xB4,0xB5,0xB3,0xB4,0xB5,0xD1,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xCE,
  0xA4,0xBD,0xA4,0xC2,0xC3,0xC4,0xA4,0xBE,0xA4,0xD1,
  0x00,0x00,0x00,0x96,0x97,0x97,0x96,0x00,0x00,0xCE,
  0xA4,0xAE,0xA4,0xC5,0xC6,0xC7,0xA4,0xAE,0xA4,0xD1,
  0x00,0x00,0x00,0x9B,0x9A,0x9A,0x9C,0x00,0x00,0xCE,
  0xA4,0xBF,0xA4,0xA4,0xC0,0xA4,0xA4,0xC1,0xA4,0xD1,
  0x00,0x00,0x00,0xA0,0x98,0x98,0xA2,0x00,0x00,0xCE,
  0xA4,0xAE,0xA4,0xA4,0xAE,0xA4,0xA4,0xAE,0xA4,0xD1,
  0x00,0x00,0x00,0xA1,0x99,0x99,0xA1,0x00,0x00,0xCE,
  0xA4,0xD6,0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xD1,
  0x00,0x00,0x00,0x9E,0x9D,0x9D,0x9F,0x00,0x00,0xCE,
  0xD7,0xAE,0xC8,0xC9,0xCA,0xCB,0xCC,0xAE,0xA4,0xD1,
  0x00,0x00,0x00,0x96,0x96,0x96,0x96,0x00,0x00,0xCE,
  0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xA4,0xD1,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xD2,
  0xD3,0xD3,0xD3,0xD3,0xD3,0xD3,0xD3,0xD3,0xD3,0xD4
};

/* button */
unsigned char rrc_btn_break1[] = { 0x10,0x11,0x12,0x13 };
unsigned char rrc_btn_break2[] = { 0x14,0x15,0x16,0x17 };
unsigned char rrc_btn_press1[] = { 0x18,0x19,0x1A,0x1B };
unsigned char rrc_btn_press2[] = { 0x1C,0x1D,0x1E,0x1F };
#if 0
unsigned char rrc_btn2_break[] = { 47 };
unsigned char rrc_btn2_press[] = { 48 };
#endif

/* selection bkg_palette */
unsigned char rrc_attrib0[] =
                        { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char rrc_attrib1[] =
                        { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
unsigned char rrc_attrib2[] = { 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2 };
unsigned char rrc_attrib3[] = { 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 };
unsigned char rrc_attrib4[] =
                        { 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4 };
unsigned char rrc_attrib5[] = { 5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5 };
unsigned char rrc_attrib6[] = { 6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6 };
unsigned char rrc_attrib7[] = { 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7 };
unsigned char rrc_ris[]  = { 6,7,7,5,7,7,7,7,7,7,7,7 };
unsigned char rrc_genu[] = {
  4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,2,4,4,2,4,4,2,4,4,4,4,4,4,4,4,4,4,
  4,4,4,4,4,4,4,4,4,4,2,4,4,2,4,4,2,4,4,4,4,4,4,4,4,4,4,4,2,4,4,2,4,4,2,4,
  4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,2,4,4,4,4,4,2,4,4,4,4,4,4,4,4,4,4,
  4,2,4,4,2,4,4,2,4,4,6,4,4,4,4,4,4,4,6,3,6,6,6,4,4,2,4,4,4,4,4,4,4,4,4,4
};

/* message */
unsigned char rrc_msg_default[]  = { "Default Data Set" };
unsigned char rrc_msg_pusha[]    = { "PUSH A" };
unsigned char rrc_msg_d_mode[][3]= { "LR","DS" };
unsigned char rrc_msg_clr[]      = { "                        " };
unsigned char rrc_msg_err1[]     = { "Warning!TwoMotorOverlap " };
unsigned char rrc_msg_err2[]     = { "Sorry.. Mode D-SCant Use" };
unsigned char rrc_msg_rcx[][16]  = { "RCX say 'Roger!'","RCX say '-----'" };
unsigned char rrc_msg_credit[]   = { "RoReCon 00/03/19TeamKNOx" };

/* RCX Command */
UBYTE rrc_set_motor_dir[]   = { 0xE1, 1 };
UBYTE rrc_set_motor_onoff[] = { 0x21, 1 };
UBYTE rrc_set_motor_power[] = { 0x13, 3 };
UBYTE rrc_start_task[]      = { 0x71, 1 };
UBYTE rrc_stop_task[]       = { 0x81, 1 };


/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void rrc_press_button( UBYTE x, UBYTE y )
{
  set_bkg_tiles( x,   y,   3, 1, rrc_btn_press1 );
  set_bkg_tiles( x,   y+1, 1, 1, &rrc_btn_press1[3] );
  set_bkg_tiles( x+2, y+1, 1, 1, rrc_btn_press2 );
  set_bkg_tiles( x,   y+2, 3, 1, &rrc_btn_press2[1] );
}

void rrc_break_button( UBYTE x, UBYTE y )
{
  set_bkg_tiles( x,   y,   3, 1, rrc_btn_break1 );
  set_bkg_tiles( x,   y+1, 1, 1, &rrc_btn_break1[3] );
  set_bkg_tiles( x+2, y+1, 1, 1, rrc_btn_break2 );
  set_bkg_tiles( x,   y+2, 3, 1, &rrc_btn_break2[1] );
}

void rrc_button_pallete()
{
  UWORD palette[4];

  palette[0] = CLR_L_GLAY;
  palette[1] = ~(nvm->back_color);
  palette[2] = CLR_WHITE;
  palette[3] = CLR_BLACK;
  set_bkg_palette( 2, 1, palette );
  palette[2] = CLR_RGB( 31, 0, 0 );
  set_bkg_palette( 3, 1, palette );
  palette[0] = ~(nvm->back_color);
  palette[3] = CLR_RGB( 0, 0, 31 );
  set_bkg_palette( 4, 1, palette );
  palette[1] = CLR_RGB( 0, 31, 0 );
  palette[2] = CLR_WHITE;
  palette[3] = CLR_BLACK;
  set_bkg_palette( 5, 1, palette );

  set_sprite_palette( 1, 2, s_rrc_palette );
}

void rrc_disp_value( UBYTE x, UBYTE y, UWORD value )
{
  unsigned char data;

  data = '0' + value % 10;
  set_bkg_tiles( x, y, 1, 1, &data );
}

void rrc_rcx_cntl( struct rrc_rcx_cntrl *rcx )
{
  UBYTE snd_data[16];
  UBYTE i, j, len, chksum, rsv_flag;
  struct read_data data;

  if( rcx->mode != RRC_MODE_GENUINE )  rcx->rrc->tglbit3 ^= 0x08;

  /* Header */
  snd_data[0] = 0x55;
  snd_data[1] = 0xFF;
  snd_data[2] = 0x00;

  /* Command */
  snd_data[3] = *(rcx->cmd);
  if( rcx->mode != RRC_MODE_GENUINE )  snd_data[3] |= rcx->rrc->tglbit3;
  snd_data[4] = ~snd_data[3];
  chksum = snd_data[3];
  len = *(rcx->cmd+1);
  i = 5;

  /* Parameter */ 
  if( len > 0 ) {
    snd_data[i] = rcx->p1;
    snd_data[i+1] = ~snd_data[i];
    chksum += snd_data[i];
    i++;
    i++;
  }
  if( len > 1 ) {
    snd_data[i] = rcx->p2;
    snd_data[i+1] = ~snd_data[i];
    chksum += snd_data[i];
    i++;
    i++;
  }
  if( len > 2 ) {
    snd_data[i] = rcx->p3;
    snd_data[i+1] = ~snd_data[i];
    chksum += snd_data[i];
    i++;
    i++;
  }

  /* Checksum */ 
  snd_data[i++] = chksum;
  snd_data[i++] = ~chksum;

  /* Ir send */
  for( j=0; j<i; j++ ) {
    pda_syscall( PDA_SEND_IRDA, &snd_data[j] );
  }

  /* rcx msg */
  if( rcx->mode != RRC_MODE_GENUINE ) {
    for( i=0; i<5; i++ ) { // why 5 byte ???
      data.flag = 0;
      pda_syscall( PDA_RECV_IRDA, &data );
    }
    if( (data.flag==0) && (snd_data[3]==data.data) ) {
      set_bkg_tiles( 11, 15, 8, 2, rrc_msg_rcx[0] );
    } else {
      set_bkg_tiles( 11, 15, 8, 2, rrc_msg_rcx[1] );
    }
  }
}

/*--------------------------------------------------------------------------*
 |  mode selection area                                                     |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_mode_cursor = { 1, 2, 14, 30, 0, 0 };
UBYTE rrc_mode_select( struct rrc_t *rrc )
{
  struct cursor_pos pos;

  if( rrc->mode != RRC_MODE_GENUINE ) {
    set_bkg_tiles( 11, 14, 8, 3, rrc_msg_credit );
    set_bkg_attribute( 11, 14, 8, 3, rrc_attrib4 );
  }

  pos.x = pos.y = 0;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_mode_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 0 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 0 );
    if( keycode.make & J_A ) {
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 0 );
      }
      if( rrc->mode != RRC_MODE_GENUINE ) {
        set_bkg_tiles( 11, 14, 8, 3, rrc_msg_clr );
        set_bkg_attribute( 11, 14, 8, 3, rrc_attrib0 );
      }
      rrc->mode = rrc->mode + 1;
      return( 0 );
    }
  }
  if( rrc->mode != RRC_MODE_GENUINE ) {
    set_bkg_tiles( 11, 14, 8, 3, rrc_msg_clr );
  }
  return( 1 );
}

/*--------------------------------------------------------------------------*
 |  movement area                                                           |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_movement_cursor1 = { 2, 3, 46, 78, 0, 0 };
struct cursor_info rrc_movement_cursor2 = { 3, 3, 22, 54, 24, 24 };
void rrc_lock_off()
{
  UBYTE x, y;

  for( y=0; y<3; y++ ) {
    for( x=0; x<3; x++ ) {
      set_bkg_attribute( x*3+2, y*3+4, 1, 1, rrc_attrib2 );
    }
  }
}

void rrc_lock_on()
{
  UBYTE x, y;

  for( y=0; y<3; y++ ) {
    for( x=0; x<3; x++ ) {
      set_bkg_attribute( x*3+2, y*3+4, 1, 1, rrc_attrib3 );
    }
  }
}

UBYTE rrc_movement( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;
  UBYTE  lockon, pad, flag;
  UBYTE  d0, d1, s0, s1;

  rcx.rrc = rrc;
  rcx.mode = RRC_MODE_JOYSTICK;
  if( rrc->drive_mode == RRC_DRIVE_DS ) {
    set_bkg_attribute( 11, 14, 8, 3, rrc_attrib1 );
    set_bkg_tiles( 11, 14, 8, 3, rrc_msg_err2 );
    flag = RRC_OFF;
  } else if( rrc->motor_sel[0] == rrc->motor_sel[1] ) {
    set_bkg_attribute( 11, 14, 8, 3, rrc_attrib1 );
    set_bkg_tiles( 11, 14, 8, 3, rrc_msg_err1 );
    flag = RRC_OFF;
  } else {
    set_bkg_attribute( 11, 14, 8, 3, rrc_attrib0 );
    flag = RRC_ON;
  }
  pos.x = 0;  pos.y = 1;  lockon = RRC_OFF;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_movement_cursor1 );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 1 );
    if( cursor.x > 0 ) {
      set_bkg_tiles( 11, 14, 8, 3, rrc_msg_clr );
      return( 4 );
    }
    if( flag == RRC_ON ) {
      if( keycode.make & J_SELECT ) {  /* lockon */
        while( keycode.make & J_SELECT ) {
          pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
          if( keycode.brk & J_EXIT )  return( 1 );
        }
        lockon = RRC_ON;
      }
      if( (lockon==RRC_ON) || (keycode.make & J_A) ) {
        rrc_lock_on();
        pos.x = 1;  pos.y = 1;
        pda_syscall( PDA_SET_CURSOR_MAP, &rrc_movement_cursor2 );
        pda_syscall( PDA_INIT_CURSOR, &pos );
        keycode.code = J_A;
        while( (lockon==RRC_ON) || (keycode.code & J_A) ) {
          keycode.code = joypad();
          if( keycode.code & J_UP )          cursor.y = 0;
          else if( keycode.code & J_DOWN )   cursor.y = 2;
          else                               cursor.y = 1;
          if( keycode.code & J_LEFT )        cursor.x = 0;
          else if( keycode.code & J_RIGHT )  cursor.x = 2;
          else                               cursor.x = 1;
          cursor.sp = 0;
          pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
          pda_syscall( PDA_SCAN_PAD, (void *)0 );
          if( keycode.brk & J_EXIT )  return( 1 );
          if( keycode.make & J_SELECT ) {  /* lockon */
            while( keycode.make & J_SELECT ) {
              pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return( 1 );
            }
            lockon = RRC_OFF;
          } else if( (pos.x!=cursor.x) || (pos.y!=cursor.y) ) {
            rrc_break_button( pos.x*3+1, pos.y*3+3 );
            pos.x = cursor.x;  pos.y = cursor.y;
            rrc_press_button( pos.x*3+1, pos.y*3+3 );
          }
          if( rrc->motor_pwr != rrc->motor_pwr_old ) {
            rrc->motor_pwr_old = rrc->motor_pwr;
            rcx.cmd = rrc_set_motor_power;
            rcx.p1  = RRC_MOTORS;
            rcx.p2  = 2;
            rcx.p3  = rrc->motor_pwr;
            rrc_rcx_cntl( &rcx );
          }
          d0 = 0x80 * (1-rrc->motor_dir[0]);
          d1 = 0x80 * (1-rrc->motor_dir[1]);
          s0 = 0x01 << rrc->motor_sel[0];
          s1 = 0x01 << rrc->motor_sel[1];
          switch( cursor.x + cursor.y * 3 ) {
            case 0:
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_OFF | s0;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d1 | s1;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_ON | s1;
              rrc_rcx_cntl( &rcx );
              break;
            case 1:  /* up */
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d0 | s0;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d1 | s1;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_ON | s0 | s1;
              rrc_rcx_cntl( &rcx );
              break;
            case 2:  /* up right */
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d0 | s0;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_OFF | s1;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_ON | s0;
              rrc_rcx_cntl( &rcx );
              break;
            case 3:  /* left */
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d0^0x80 | s0;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d1 | s1;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_ON | s0 | s1;
              rrc_rcx_cntl( &rcx );
              break;
            case 4:  /* center */
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = RRC_MOTOR_OFF | s0 | s1;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              break;
            case 5:  /* right */
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d0 | s0;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d1^0x80 | s1;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_ON | s0 | s1;
              rrc_rcx_cntl( &rcx );
              break;
            case 6:  /* down left */
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_OFF | s0;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d1^0x80 | s1;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_ON | s1;
              rrc_rcx_cntl( &rcx );
              break;
            case 7:  /* down */
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d0^0x80 | s0;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d1^0x80 | s1;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_ON | s0 | s1;
              rrc_rcx_cntl( &rcx );
              break;
            case 8:  /* down right */
              rcx.cmd = rrc_set_motor_dir;
              rcx.p1  = d0^0x80 | s0;
              rcx.p2  = rcx.p3  = 0;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_OFF | s1;
              rrc_rcx_cntl( &rcx );
              rcx.cmd = rrc_set_motor_onoff;
              rcx.p1  = RRC_MOTOR_ON | s0;
              rrc_rcx_cntl( &rcx );
              break;
          }
        }
        rrc_break_button( pos.x*3+1, pos.y*3+3 );
        pos.x = 0;  pos.y = 1;
        pda_syscall( PDA_SET_CURSOR_MAP, &rrc_movement_cursor1 );
        pda_syscall( PDA_INIT_CURSOR, &pos );
        rrc_lock_off();
        rrc_press_button( 4, 6 );
      }
    }
  }
  set_bkg_tiles( 11, 14, 8, 3, rrc_msg_clr );
  return( cursor.y );
}

/*--------------------------------------------------------------------------*
 |  task area                                                               |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_task_cursor1 = { 4, 2, 22, 140, 24, 0 };
struct cursor_info rrc_task_cursor2 = { 3, 2, 14, 140, 16, 0 };
UBYTE rrc_task( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  struct rrc_rcx_cntrl rcx;

  rcx.rrc = rrc;
  rcx.mode = RRC_MODE_JOYSTICK;
  pos.x = 1;  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_task_cursor1 );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  set_bkg_attribute( 11, 14, 8, 3, rrc_attrib0 );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 2 );
    if( cursor.x == 3 )  return( 5 );
    if( cursor.x == 0 ) {
      pos.x = 1;  pos.y = 1;
      pda_syscall( PDA_SET_CURSOR_MAP, &rrc_task_cursor2 );
      pda_syscall( PDA_INIT_CURSOR, &pos );
      while( cursor.x < 2 ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 2 );
        if( cursor.y == 0 )  return( 1 );
        if( keycode.make & J_A ) {
          while( keycode.make & J_A ) {
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return( 2 );
          }
          if( cursor.x == 0 ) {
            if( rrc->task_num > 0 )  rrc->task_num--;
            else                     rrc->task_num=9;
          } else {
            if( rrc->task_num < 9 )  rrc->task_num++;
            else                     rrc->task_num=0;
          }
          rrc_disp_value( 2, 15, rrc->task_num );
        }
      }
      pos.x = 1;  pos.y = 1;
      pda_syscall( PDA_SET_CURSOR_MAP, &rrc_task_cursor1 );
      pda_syscall( PDA_INIT_CURSOR, &pos );
    }
    if( keycode.make & J_A ) {
      rrc_press_button( cursor.x*3+1, 14 );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 2 );
      }
      if( cursor.x == 1 ) {
        rcx.cmd = rrc_start_task;
        rcx.p1  = rrc->task_num;
        rcx.p2  = rcx.p3  = 0;
        rrc_rcx_cntl( &rcx );
      } else {
        rcx.cmd = rrc_stop_task;
        rcx.p1  = rrc->task_num;
        rcx.p2  = rcx.p3  = 0;
        rrc_rcx_cntl( &rcx );
      }
      rrc_break_button( cursor.x*3+1, 14 );
    }
  }
  return( 1 );
}

/*--------------------------------------------------------------------------*
 |  drive mode area                                                         |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_drive_mode_cursor = { 3, 3, 62, 52, 40, 0 };
UBYTE rrc_drive_mode( struct rrc_t *rrc )
{
  struct cursor_pos pos;

  pos.x = rrc->drive_mode + 1;
  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_drive_mode_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 3 );
    if( cursor.x == 0 )  return( 1 );
    if( keycode.make & J_A ) {
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 3 );
      }
      rrc->drive_mode = cursor.x - 1;
      set_bkg_attribute( 11, 4, 8, 1, rrc_attrib0 );
      set_bkg_attribute( 11+(rrc->drive_mode)*5, 4, 3, 1, rrc_attrib1 );
      set_bkg_tiles( 11, 7, 1, 2, rrc_msg_d_mode[rrc->drive_mode] );
    }
  }
  if( cursor.y == 2 )  return( 4 );
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  motor area                                                              |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_motor_cursor1 = { 5, 4, 102, 66, 8, 8 };
struct cursor_info rrc_motor_cursor2 = { 3, 4, 134, 66, 8, 8 };
UBYTE rrc_motor( struct rrc_t *rrc )
{
  struct cursor_pos pos;
  UBYTE  y;

  pos.x = pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_motor_cursor1 );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( (cursor.y==1) || (cursor.y==2) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 4 );
    if( cursor.x == 0 )  return( 1 );
    if( cursor.x == 4 ) {
      pos.x = 1;  pos.y = cursor.y;
      pda_syscall( PDA_SET_CURSOR_MAP, &rrc_motor_cursor2 );
      pda_syscall( PDA_INIT_CURSOR, &pos );
      while( cursor.x > 0 ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 4 );
        if( cursor.y == 0 )  return( 3 );
        if( cursor.y == 3 )  return( 5 );
        if( keycode.make & J_A ) {
          while( keycode.make & J_A ) {
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            if( keycode.brk & J_EXIT )  return( 4 );
          }
          y = cursor.y - 1;
          rrc->motor_dir[y] = cursor.x - 1;
          set_bkg_attribute( 17, y+7, 2, 1, rrc_attrib0 );
          set_bkg_attribute( rrc->motor_dir[y]+17, y+7, 1, 1, rrc_attrib1);
        }
      }
      pos.x = 3;  pos.y = cursor.y;
      pda_syscall( PDA_SET_CURSOR_MAP, &rrc_motor_cursor1 );
      pda_syscall( PDA_INIT_CURSOR, &pos );
    }
    if( keycode.make & J_A ) {
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 4 );
      }
      y = cursor.y - 1;
      rrc->motor_sel[y] = cursor.x;
      set_bkg_attribute( 13, y+7, 3, 1, rrc_attrib0 );
      set_bkg_attribute( rrc->motor_sel[y]+12, y+7, 1, 1, rrc_attrib1 );
    }
  }
  if( cursor.y == 3 )  return( 5 );
  return( 3 );
}

/*--------------------------------------------------------------------------*
 |  speed area                                                              |
 *--------------------------------------------------------------------------*/
struct cursor_info rrc_speed_cursor = { 8, 3, 94, 108, 8, 0 };
UBYTE rrc_speed( struct rrc_t *rrc )
{
  struct cursor_pos pos;

  pos.x = rrc->motor_pwr;
  pos.y = 1;
  pda_syscall( PDA_SET_CURSOR_MAP, &rrc_speed_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.y == 1 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 5 );
    if( cursor.x == 0 )  return( 1 );
    if( keycode.make & J_A ) {
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 5 );
      }
      rrc->motor_pwr = cursor.x;
      set_bkg_attribute( 12, 11, 7, 1, rrc_attrib2 );
      set_bkg_attribute( 12, 11, rrc->motor_pwr, 1, rrc_attrib3 );
    }
  }
  if( cursor.y == 2 )  return( 2 );
  return( 4 );
}

/*==========================================================================*
 |  joystick mode                                                           |
 *==========================================================================*/
void rrc_disp_joystick( struct rrc_t *rrc )
{
  UBYTE  i;

  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
  rrc_button_pallete();

  set_bkg_data( 128, 32, rrc_joystick );
  set_bkg_tiles( 0, 0, 20, 18, rrc_joystick_form );
  set_bkg_tiles( 11, 7, 1, 2, rrc_msg_d_mode[rrc->drive_mode] );
  rrc_disp_value( 2, 15, rrc->task_num );

  /* attributes */
  for( i=3; i<12; i++ ) {
    set_bkg_attribute( 1, i, 9, 1, rrc_attrib2 );
  }
  for( i=14; i<17; i++ ) {
    set_bkg_attribute( 4, i, 6, 1, rrc_attrib2 );
  }
  set_bkg_attribute( 5, 15, 1, 1, rrc_attrib3 );
  set_bkg_attribute( 8, 15, 1, 1, rrc_attrib3 );
  set_bkg_attribute( (rrc->drive_mode)*5+11, 4, 3, 1, rrc_attrib1 );
  set_bkg_attribute( rrc->motor_sel[0]+12, 7, 1, 1, rrc_attrib1 );
  set_bkg_attribute( rrc->motor_sel[1]+12, 8, 1, 1, rrc_attrib1 );
  set_bkg_attribute( rrc->motor_dir[0]+17, 7, 1, 1, rrc_attrib1 );
  set_bkg_attribute( rrc->motor_dir[1]+17, 8, 1, 1, rrc_attrib1 );
  set_bkg_attribute( 12, 11, 7, 1, rrc_attrib2 );
  set_bkg_attribute( 12, 11, rrc->motor_pwr, 1, rrc_attrib3 );
}

void rrc_mode_joystick( struct rrc_t *rrc )
{
  UBYTE area;

  rrc_disp_joystick( rrc );
  area = 0;

  while( rrc->mode == RRC_MODE_JOYSTICK ) {
    if( keycode.brk & J_EXIT )  return;
    switch( area ) {
      case 0:
        area = rrc_mode_select( rrc );
        break;
      case 1:
        area = rrc_movement( rrc );
        break;
      case 2:
        area = rrc_task( rrc );
        break;
      case 3:
        area = rrc_drive_mode( rrc );
        break;
      case 4:
        area = rrc_motor( rrc );
        break;
      case 5:
        area = rrc_speed( rrc );
        break;
    }
  }
}

/*==========================================================================*
 |  piano mode                                                              |
 *==========================================================================*/
void rrc_disp_piano( struct rrc_t *rrc )
{
  UBYTE i;

  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
  rrc_button_pallete();

  set_bkg_data( 128, 16, rrc_piano );
  set_bkg_tiles( 0, 0, 20, 18, rrc_piano_form );

  /* attributes */
  for( i=9; i<12; i++ ) {
    set_bkg_attribute(  1, i, 3, 1, rrc_attrib2 );
    set_bkg_attribute( 13, i, 6, 1, rrc_attrib2 );
  }
  for( i=14; i<17; i++ ) {
    set_bkg_attribute( 1, i, 9, 1, rrc_attrib2 );
  }
  set_bkg_attribute( 17, 10, 1, 1, rrc_attrib3 );
  set_bkg_attribute( rrc->tone_len+6, 10, 1, 1, rrc_attrib1 );
}

/*==========================================================================*
 |  genuine mode                                                            |
 *==========================================================================*/
void rrc_disp_genuine( struct rrc_t *rrc )
{
  UBYTE i;

  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
  set_bkg_palette( 2, 6, c_rrc_palette );
  set_bkg_data( 128, 100, rrc_genuine );
  set_bkg_tiles( 0, 0, 20, 18, rrc_genuine_form );

  set_bkg_attribute( 3,  4, 4, 4, rrc_attrib6 );
  set_bkg_attribute( 3, 11, 4, 3, rrc_attrib7 );
  set_bkg_attribute( 3, 14, 4, 3, rrc_ris );
  set_bkg_attribute( 10, 1, 9, 16, rrc_genu );
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void rrc_init( struct rrc_t *rrc )
{
  set_bkg_data( 12, 20, c_rrc );
  set_sprite_data( 33, 5, s_rrc );

  /* init IrDA parameters */
  irda_speed  = 0;  /* 2400bps */
  irda_parity = 1;  /* odd parity */

  /* initialize work parameters */
  rrc->drive_mode    = RRC_DRIVE_LR;
  rrc->motor_sel[0]  = 1;  /* RRC_MOTOR_A */
  rrc->motor_sel[1]  = 3;  /* RRC_MOTOR_C */
  rrc->motor_dir[0]  = 0;  /* RRC_REVERSE */
  rrc->motor_dir[1]  = 0;  /* RRC_REVERSE */
  rrc->motor_pwr_old = rrc->motor_pwr = 4;
  rrc->task_num      = 0;
  rrc->tone_len      = 2;
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_rrc( void *arg )
{
  struct rrc_t rrc;

  /* setup work area on the stack */
  pda_syscall( PDA_DISABLE_INTERRUPT, (void *)0 );
  rrc.mode = RRC_MODE_JOYSTICK;
  rrc_init( &rrc );

  while( !(keycode.brk & J_EXIT) ) {
    rrc.tglbit3 = 0;
    switch( rrc.mode ) {

      case RRC_MODE_PIANO:
        rrc_disp_piano( &rrc );
        pda_longjmp( 21, rrc_mode_piano, &rrc );
        break;

      case RRC_MODE_GENUINE:
        rrc_disp_genuine( &rrc );
        pda_longjmp( 21, rrc_mode_genuine, &rrc );
        break;

      case RRC_MODE_JOYSTICK:
      default:
        rrc.mode = RRC_MODE_JOYSTICK;
        rrc_mode_joystick( &rrc );
        break;
    }
  }
}

/* EOF */