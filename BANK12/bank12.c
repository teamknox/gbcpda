/****************************************************************************
 *  bank12.c                                                                *
 *                                                                          *
 *    GB-WORLD  V2.01    2000/11/12                                         *
 *                                                                          *
 *    Copyright(C)  1999, 2000, 2001   TeamKNOx                             *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank12\i_world.h"

extern void gb_world();

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank12\i_world.c"
#include "bank12\c_world.c"
#include "bank12\s_world.c"

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_12[1] = {
    { "GB-WORLD", "2.01", gb_world, i_world, i_worldCGBPal0c1, i_worldCGBPal0c2 }
};
struct description desc_12 = { FUNC_NUM, (struct function *)func_12 };

/*==========================================================================*
 |  data                                                                    |
 *==========================================================================*/
unsigned char world_head[] = {
    0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,
    0x04,0x10,0x12,0x00,0x00,0x10,0x12,0x00,0x10,0x12,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x04,0x15,0x17,0x30,0x30,0x15,0x17,0x00,0x15,0x17,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x07,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x08
};
unsigned char world_line[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04
};
unsigned char world_tail[] = {
    0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char world_break[] = { 0x10,0x12,0x15,0x17 };
unsigned char world_press[] = { 0x18,0x1A,0x1D,0x1F };
unsigned char world_btn_a[] = { 2,2,2,2 };
unsigned char world_name3[] = { "() " };
unsigned char world_current[]   = { "CURRENT" };
unsigned char world_current_n[] = { "       " };
unsigned char world_current_a[] = { 1,1,1,1,1,1,1 };
unsigned char world_sign[5] = { '-', 0xA8, '+', ':', '/' };
unsigned char world_none[]  = { "NONE          " };
unsigned char world_city[]  = { 0x90,0x92,0x91,0x93 };
unsigned char world_timez[] = { 0x94,0x96,0x95,0x97 };
unsigned char world_call[]  = { 0x98,0x9A,0x99,0x9B };
unsigned char world_stime[] = { 0x9C,0x9E,0x9D,0x9F };
unsigned char world_hld[]   = { 0xA0,0xA2,0xA1,0xA3 };
unsigned char world_pos[]   = { 0xA4,0xA6,0xA5,0xA7 };


/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void show_flag_page()
{
    struct national_flag flag;
    unsigned char buff[3];
    BYTE timez;
    struct summertime date;

    country_code = country_code % 16;
    flag.no = 3;
    flag.x = 1;
    flag.y = 4;
    pda_syscall( PDA_COUNTRY_FLAG, &flag );

    buff[0] = '0'+((country_code+1)/10);
    buff[1] = '0'+((country_code+1)%10);
    set_bkg_tiles( 3, 2, 2, 1, buff );

    flag.x = 5;
    flag.y = 4;
    pda_syscall( PDA_COUNTRY_NAME, &flag );
    set_bkg_tiles( 5, 5, 1, 1, world_name3 );
    flag.x = 6;
    flag.y = 5;
    pda_syscall( PDA_COUNTRY_NAME3, &flag );
    if( flag.no ) {
        set_bkg_tiles( 8, 5, 2, 1, &world_name3[1] );
    } else {
        set_bkg_tiles( 9, 5, 1, 1, &world_name3[1] );
    }
    if( country_code == nvm->country_code ) {
        set_bkg_tiles( 11, 5, 7, 1, world_current );
        set_bkg_attribute( 11,5, 7, 1, world_current_a );
    } else {
        set_bkg_tiles( 11, 5, 7, 1, world_current_n );
    }

    flag.x = 4;
    flag.y = 7;
    pda_syscall( PDA_COUNTRY_CITY, &flag );
    pda_syscall( PDA_COUNTRY_TIMEZONE, &timez );
    if( timez < 0 ) {
        set_bkg_tiles( 4, 9, 1, 1, &world_sign[0] );
        timez = -timez;
    } else if( timez == 0 ) {
        set_bkg_tiles( 4, 9, 1, 1, &world_sign[1] );
    } else {
        set_bkg_tiles( 4, 9, 1, 1, &world_sign[2] );
    }
    buff[0] = '0' + timez/20;
    buff[1] = '0' + (timez/2)%10;
    set_bkg_tiles( 5, 9, 2, 1, buff );
    set_bkg_tiles( 7, 9, 1, 1, &world_sign[3] );
    buff[0] = '0' + (timez%2)*3;
    buff[1] = '0';
    set_bkg_tiles( 8, 9, 2, 1, buff );
    set_bkg_tiles( 15, 9, 1, 1, &world_sign[2] );

    pda_syscall( PDA_COUNTRY_PHONE, buff );
    set_bkg_tiles( 16, 9, 3, 1, buff );

    pda_syscall( PDA_COUNTRY_SUMMERTIME, &date );
    if( date.start_month ) {
        buff[0] = '0' + (date.start_month)/10;
        buff[1] = '0' + (date.start_month)%10;
        set_bkg_tiles( 4, 11, 2, 1, buff );
        set_bkg_tiles( 6, 11, 1, 1, &world_sign[4] );
        buff[0] = '0' + (date.start_day)/10;
        buff[1] = '0' + (date.start_day)%10;
        set_bkg_tiles( 7, 11, 2, 1, buff );
        set_bkg_tiles( 10, 11, 1, 1, &world_sign[0] );
        buff[0] = '0' + (date.end_month)/10;
        buff[1] = '0' + (date.end_month)%10;
        set_bkg_tiles( 12, 11, 2, 1, buff );
        set_bkg_tiles( 14, 11, 1, 1, &world_sign[4] );
        buff[0] = '0' + (date.end_day)/10;
        buff[1] = '0' + (date.end_day)%10;
        set_bkg_tiles( 15, 11, 2, 1, buff );
    } else {
        set_bkg_tiles( 4, 11, 14, 1, world_none );
    }
}

struct cursor_info world_cursor0 = { 3, 1, 18, 34, 32, 0 };
UBYTE select_world_control()
{
    struct cursor_pos pos;

    pos.x = 1; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &world_cursor0 );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( cursor.x*4+1, 1, 2, 2, world_press );
            while( !(keycode.brk & J_EXIT) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( !(keycode.make & J_A) ) {
                    set_bkg_tiles( cursor.x*4+1, 1, 2, 2, world_break );
                    switch( cursor.x ) {
                      case 0:
                        if( country_code ) {
                            country_code--;
                        } else {
                            country_code = 15;
                        }
                        break;
                      case 1:
                        if( country_code < 15 ) {
                            country_code++;
                        } else {
                            country_code = 0;
                        }
                        break;
                    }
                    show_flag_page();
                    break;
                }
            }
        }
        if( cursor.x == 2 ) {
            return( 1 );
        }
    }
    return( 0 );
}

struct cursor_info world_cursor1 = { 2, 1, 74, 34, 0, 0 };
UBYTE select_country_code()
{
    struct cursor_pos pos;

    pos.x = 1; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &world_cursor1 );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( 8, 1, 2, 2, world_press );
            while( !(keycode.brk & J_EXIT) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( !(keycode.make & J_A) ) {
                    set_bkg_tiles( 8, 1, 2, 2, world_break );
                    nvm->country_code = country_code;
                    show_flag_page();
                    break;
                }
            }
        }
        if( cursor.x == 0 ) {
            return( 0 );
        }
    }
    return( 1 );
}

void show_world_frame()
{
    UBYTE i;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_data( 0x90, 32, c_world );
    set_sprite_data( 64, 3, s_world );
    set_bkg_tiles( 0, 0, 20, 4, world_head );
    for( i=4; i<17; i++ ) {
        set_bkg_tiles( 0, i, 20, 1, world_line );
    }
    set_bkg_tiles( 0, 17, 20, 1, world_tail );
    set_bkg_attribute( 1, 1, 2, 2, world_btn_a );
    set_bkg_attribute( 5, 1, 2, 2, world_btn_a );
    set_bkg_attribute( 8, 1, 2, 2, world_btn_a );
    set_sprite_tile( 5, 64 );
    set_sprite_tile( 6, 65 );
    set_sprite_tile( 7, 66 );
    move_sprite( 5, 20, 28 );
    move_sprite( 6, 52, 28 );
    move_sprite( 7, 76, 28 );
    set_sprite_prop( 5, 7 );
    set_sprite_prop( 6, 7 );
    set_sprite_prop( 7, 7 );

    set_bkg_tiles(  1,  6, 2, 2, world_city );
    set_bkg_tiles(  1,  8, 2, 2, world_timez );
    set_bkg_tiles( 12,  8, 2, 2, world_call );
    set_bkg_tiles(  1, 10, 2, 2, world_stime );
    set_bkg_tiles(  1, 12, 2, 2, world_pos );
    set_bkg_tiles(  1, 14, 2, 2, world_hld );
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_world( void *arg )
{
    UBYTE mode;

    show_world_frame();

    mode = 0;
    country_code = nvm->country_code;
    show_flag_page();

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        switch( mode ) {
          case 0:
            mode = select_world_control();
            break;
          case 1:
            mode = select_country_code();
            break;
        }
    }
}

/* EOF */