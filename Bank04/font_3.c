/*

 FONT_3.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 2 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 95

  Palette colors       : None.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char font_3[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x08,0x08,0x08,0x0C,0x0C,0x00,0x0C,0x00,
  0x36,0x36,0x12,0x00,0x00,0x00,0x00,0x00,
  0x24,0x7F,0x24,0x36,0x36,0x7F,0x36,0x00,
  0x08,0x7F,0x48,0x7F,0x0B,0x7F,0x08,0x00,
  0x71,0x52,0x74,0x08,0x17,0x25,0x47,0x00,
  0x7C,0x4C,0x45,0x79,0x65,0x62,0x7D,0x00,
  0x0C,0x0C,0x04,0x00,0x00,0x00,0x00,0x00,
  0x0C,0x10,0x10,0x18,0x18,0x18,0x0C,0x00,
  0x18,0x04,0x04,0x06,0x06,0x04,0x18,0x00,
  0x00,0x2A,0x1C,0x1C,0x1C,0x2A,0x00,0x00,
  0x00,0x08,0x08,0x3E,0x08,0x08,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x0C,0x0C,0x04,
  0x00,0x00,0x00,0x3E,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x0C,0x0C,0x00,
  0x00,0x02,0x04,0x08,0x10,0x20,0x00,0x00,
  0x7F,0x61,0x51,0x49,0x45,0x43,0x7F,0x00,
  0x08,0x08,0x08,0x0C,0x0C,0x0C,0x0C,0x00,
  0x7F,0x03,0x01,0x7F,0x60,0x60,0x7F,0x00,
  0x7F,0x01,0x01,0x1F,0x03,0x03,0x7F,0x00,
  0x7C,0x64,0x44,0x46,0x46,0x7F,0x06,0x00,
  0x7F,0x40,0x60,0x7F,0x03,0x03,0x7F,0x00,
  0x40,0x40,0x40,0x7F,0x63,0x63,0x7F,0x00,
  0x3E,0x02,0x02,0x06,0x06,0x06,0x06,0x00,
  0x7F,0x43,0x41,0x7F,0x61,0x61,0x7F,0x00,
  0x7F,0x61,0x41,0x7F,0x03,0x03,0x03,0x00,
  0x00,0x18,0x18,0x00,0x18,0x18,0x00,0x00,
  0x00,0x18,0x18,0x00,0x18,0x18,0x08,0x00,
  0x06,0x0C,0x18,0x30,0x18,0x0C,0x06,0x00,
  0x00,0x00,0x3E,0x00,0x3E,0x00,0x00,0x00,
  0x30,0x18,0x0C,0x06,0x0C,0x18,0x30,0x00,
  0x7F,0x41,0x01,0x0F,0x0C,0x00,0x0C,0x00,
  0x3E,0x41,0x5D,0x55,0x5F,0x40,0x3F,0x00,
  0x7F,0x41,0x41,0x7F,0x61,0x61,0x61,0x00,
  0x7E,0x42,0x42,0x7F,0x61,0x61,0x7F,0x00,
  0x7F,0x40,0x40,0x60,0x60,0x60,0x7F,0x00,
  0x7E,0x43,0x41,0x71,0x71,0x73,0x7E,0x00,
  0x7F,0x40,0x40,0x7F,0x60,0x60,0x7F,0x00,
  0x7F,0x40,0x40,0x7F,0x60,0x60,0x60,0x00,
  0x7F,0x40,0x40,0x67,0x61,0x61,0x7F,0x00,
  0x41,0x41,0x41,0x7F,0x61,0x61,0x61,0x00,
  0x08,0x08,0x08,0x0C,0x0C,0x0C,0x0C,0x00,
  0x04,0x04,0x04,0x06,0x06,0x46,0x7E,0x00,
  0x42,0x44,0x48,0x70,0x68,0x64,0x62,0x00,
  0x40,0x40,0x40,0x60,0x60,0x60,0x7F,0x00,
  0x7F,0x49,0x49,0x69,0x69,0x69,0x69,0x00,
  0x7F,0x41,0x41,0x61,0x61,0x61,0x61,0x00,
  0x7F,0x43,0x43,0x41,0x41,0x41,0x7F,0x00,
  0x7F,0x41,0x41,0x7F,0x60,0x60,0x60,0x00,
  0x7F,0x41,0x41,0x41,0x4F,0x4F,0x7F,0x00,
  0x7E,0x42,0x42,0x7F,0x61,0x61,0x61,0x00,
  0x7F,0x41,0x40,0x7F,0x03,0x43,0x7F,0x00,
  0x7F,0x08,0x08,0x0C,0x0C,0x0C,0x0C,0x00,
  0x41,0x41,0x41,0x61,0x61,0x61,0x7F,0x00,
  0x41,0x41,0x42,0x62,0x64,0x64,0x78,0x00,
  0x41,0x41,0x45,0x65,0x65,0x65,0x7F,0x00,
  0x71,0x11,0x11,0x7F,0x64,0x64,0x67,0x00,
  0x41,0x41,0x41,0x7F,0x0C,0x0C,0x0C,0x00,
  0x7F,0x41,0x01,0x7F,0x60,0x61,0x7F,0x00,
  0x1C,0x10,0x10,0x18,0x18,0x18,0x1C,0x00,
  0x00,0x20,0x10,0x08,0x04,0x02,0x00,0x00,
  0x1C,0x04,0x04,0x06,0x06,0x06,0x1E,0x00,
  0x08,0x14,0x22,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x3F,0x00,
  0x3E,0x41,0x5D,0x51,0x5D,0x41,0x3E,0x00,
  0x00,0x7C,0x04,0x7E,0x46,0x46,0x7E,0x00,
  0x40,0x40,0x40,0x7E,0x62,0x62,0x7E,0x00,
  0x00,0x00,0x7E,0x40,0x60,0x60,0x7E,0x00,
  0x04,0x04,0x04,0x7E,0x46,0x46,0x7E,0x00,
  0x00,0x00,0x7E,0x42,0x7E,0x60,0x7E,0x00,
  0x1C,0x10,0x10,0x7E,0x18,0x18,0x18,0x00,
  0x00,0x7C,0x44,0x44,0x7E,0x06,0x06,0x7E,
  0x20,0x20,0x20,0x3E,0x32,0x32,0x32,0x00,
  0x00,0x10,0x00,0x10,0x18,0x18,0x18,0x00,
  0x00,0x04,0x00,0x04,0x06,0x06,0x06,0x3E,
  0x20,0x20,0x22,0x34,0x38,0x34,0x32,0x00,
  0x18,0x08,0x08,0x0C,0x0C,0x0C,0x0C,0x00,
  0x00,0x00,0x7E,0x4A,0x6A,0x6A,0x6A,0x00,
  0x00,0x00,0x7E,0x42,0x62,0x62,0x62,0x00,
  0x00,0x00,0x7E,0x46,0x42,0x42,0x7E,0x00,
  0x00,0x00,0x7E,0x42,0x42,0x7E,0x60,0x60,
  0x00,0x00,0x7C,0x44,0x44,0x7E,0x06,0x06,
  0x00,0x20,0x20,0x3E,0x30,0x30,0x30,0x00,
  0x00,0x00,0x7E,0x40,0x7E,0x06,0x7E,0x00,
  0x10,0x10,0x7E,0x18,0x18,0x18,0x1E,0x00,
  0x00,0x00,0x42,0x42,0x62,0x62,0x7E,0x00,
  0x00,0x00,0x42,0x42,0x64,0x64,0x78,0x00,
  0x00,0x00,0x42,0x42,0x6A,0x6A,0x7E,0x00,
  0x00,0x00,0x36,0x16,0x1C,0x34,0x36,0x00,
  0x00,0x00,0x44,0x44,0x7E,0x06,0x06,0x7E,
  0x00,0x00,0x7E,0x02,0x7E,0x60,0x7E,0x00,
  0x0C,0x08,0x08,0x18,0x08,0x08,0x0C,0x00,
  0x10,0x10,0x10,0x18,0x18,0x18,0x18,0x00,
  0x18,0x08,0x08,0x0C,0x08,0x08,0x18,0x00,
  0x00,0x00,0x30,0x7F,0x06,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

/* End of FONT_3.C */
