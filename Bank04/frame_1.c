/*

 FRAME_1.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 10

  Palette colors       : None.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.1

*/

/* Start of tile array. */
unsigned char frame_1[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1F,
  0x00,0x1F,0x00,0x18,0x00,0x18,0x00,0x18,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,
  0x00,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xF8,
  0x00,0xF8,0x00,0x18,0x00,0x18,0x00,0x18,
  0x00,0x18,0x00,0x18,0x00,0x18,0x00,0x18,
  0x00,0x18,0x00,0x18,0x00,0x18,0x00,0x18,
  0x00,0x18,0x00,0x18,0x00,0x18,0x00,0x1F,
  0x00,0x1F,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x18,0x00,0x18,0x00,0x18,0x00,0xF8,
  0x00,0xF8,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x18,0x00,0x18,0x00,0x18,0x00,0x1F,
  0x00,0x1F,0x00,0x18,0x00,0x18,0x00,0x18,
  0x00,0x18,0x00,0x18,0x00,0x18,0x00,0xF8,
  0x00,0xF8,0x00,0x18,0x00,0x18,0x00,0x18,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,
  0x00,0xFF,0x00,0x18,0x00,0x18,0x00,0x18,
  0x00,0x18,0x00,0x18,0x00,0x18,0x00,0xFF,
  0x00,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x18,0x00,0x18,0x00,0x18,0x00,0xFF,
  0x00,0xFF,0x00,0x18,0x00,0x18,0x00,0x18
};

/* End of FRAME_1.C */
