/*

 CURSOR_4.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 16 x 16
  Tiles                : 0 to 7

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char cursor_4[] =
{
  0x04,0x04,0x06,0x06,0x05,0x07,0x04,0x07,
  0x04,0x07,0x04,0x07,0x04,0x07,0x04,0x07,
  0x04,0x07,0x05,0x07,0x06,0x06,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0x40,0xC0,0x20,0xE0,0x10,0xF0,0x08,0xF8,
  0x30,0xF0,0xA0,0xE0,0x90,0xF0,0x50,0x70,
  0x48,0x78,0x30,0x30,0x00,0x00,0x00,0x00,
  0x04,0x04,0x06,0x06,0x05,0x07,0x04,0x07,
  0x04,0x07,0x04,0x07,0x04,0x07,0x04,0x07,
  0x04,0x07,0x05,0x07,0x06,0x06,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0x40,0xC0,0x20,0xE0,0x10,0xF0,0x08,0xF8,
  0x30,0xF0,0xA0,0xE0,0x90,0xF0,0x50,0x70,
  0x48,0x78,0x30,0x30,0x00,0x00,0x00,0x00,
  0x04,0x04,0x06,0x06,0x05,0x07,0x04,0x07,
  0x04,0x07,0x04,0x07,0x04,0x07,0x04,0x07,
  0x04,0x07,0x05,0x07,0x06,0x06,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0x40,0xC0,0x20,0xE0,0x10,0xF0,0x08,0xF8,
  0x30,0xF0,0xA0,0xE0,0xB0,0xD0,0x70,0x50,
  0x78,0x48,0x30,0x30,0x00,0x00,0x00,0x00,
  0x04,0x04,0x06,0x06,0x05,0x07,0x04,0x07,
  0x04,0x07,0x04,0x07,0x04,0x07,0x04,0x07,
  0x04,0x07,0x07,0x05,0x06,0x06,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0x40,0xC0,0x20,0xE0,0x10,0xF0,0x38,0xC8,
  0xF0,0x30,0xE0,0xA0,0xF0,0x90,0x70,0x50,
  0x78,0x48,0x30,0x30,0x00,0x00,0x00,0x00,
  0x04,0x04,0x06,0x06,0x05,0x07,0x04,0x07,
  0x04,0x07,0x04,0x07,0x05,0x06,0x07,0x04,
  0x07,0x04,0x07,0x05,0x06,0x06,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0x40,0xC0,0x60,0xA0,0xF0,0x10,0xF8,0x08,
  0xF0,0x30,0xE0,0xA0,0xF0,0x90,0x70,0x50,
  0x78,0x48,0x30,0x30,0x00,0x00,0x00,0x00,
  0x04,0x04,0x06,0x06,0x05,0x07,0x04,0x07,
  0x05,0x06,0x07,0x04,0x07,0x04,0x07,0x04,
  0x07,0x04,0x07,0x05,0x06,0x06,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0xC0,0x40,0xE0,0x20,0xF0,0x10,0xF8,0x08,
  0xF0,0x30,0xE0,0xA0,0xF0,0x90,0x70,0x50,
  0x78,0x48,0x30,0x30,0x00,0x00,0x00,0x00,
  0x04,0x04,0x06,0x06,0x07,0x05,0x07,0x04,
  0x07,0x04,0x07,0x04,0x07,0x04,0x07,0x04,
  0x07,0x04,0x07,0x05,0x06,0x06,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0xC0,0x40,0xE0,0x20,0xF0,0x10,0xF8,0x08,
  0xF0,0x30,0xE0,0xA0,0xF0,0x90,0x70,0x50,
  0x78,0x48,0x30,0x30,0x00,0x00,0x00,0x00,
  0x04,0x04,0x06,0x06,0x07,0x05,0x07,0x04,
  0x07,0x04,0x07,0x04,0x07,0x04,0x07,0x04,
  0x07,0x04,0x07,0x05,0x06,0x06,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0xC0,0x40,0xE0,0x20,0xF0,0x10,0xF8,0x08,
  0xF0,0x30,0xE0,0xA0,0xF0,0x90,0x70,0x50,
  0x78,0x48,0x30,0x30,0x00,0x00,0x00,0x00
};

/* End of CURSOR_4.C */
