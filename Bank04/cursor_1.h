/*

 CURSOR_1.H

 Include File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 16 x 16
  Tiles                : 0 to 7

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/


/* Bank of tiles. */
#define cursor_1Bank 0

/* Super Gameboy palette 0 */
#define cursor_1SGBPal0c0 0
#define cursor_1SGBPal0c1 0
#define cursor_1SGBPal0c2 25368
#define cursor_1SGBPal0c3 0

/* Super Gameboy palette 1 */
#define cursor_1SGBPal1c0 4104
#define cursor_1SGBPal1c1 6
#define cursor_1SGBPal1c2 0
#define cursor_1SGBPal1c3 0

/* Super Gameboy palette 2 */
#define cursor_1SGBPal2c0 4104
#define cursor_1SGBPal2c1 6
#define cursor_1SGBPal2c2 0
#define cursor_1SGBPal2c3 0

/* Super Gameboy palette 3 */
#define cursor_1SGBPal3c0 4104
#define cursor_1SGBPal3c1 6
#define cursor_1SGBPal3c2 0
#define cursor_1SGBPal3c3 0

/* Gameboy Color palette 0 */
#define cursor_1CGBPal0c0 25344
#define cursor_1CGBPal0c1 32767
#define cursor_1CGBPal0c2 13087
#define cursor_1CGBPal0c3 0

/* Gameboy Color palette 1 */
#define cursor_1CGBPal1c0 6108
#define cursor_1CGBPal1c1 8935
#define cursor_1CGBPal1c2 6596
#define cursor_1CGBPal1c3 6368

/* Gameboy Color palette 2 */
#define cursor_1CGBPal2c0 6108
#define cursor_1CGBPal2c1 8935
#define cursor_1CGBPal2c2 6596
#define cursor_1CGBPal2c3 6368

/* Gameboy Color palette 3 */
#define cursor_1CGBPal3c0 6108
#define cursor_1CGBPal3c1 8935
#define cursor_1CGBPal3c2 6596
#define cursor_1CGBPal3c3 6368

/* Gameboy Color palette 4 */
#define cursor_1CGBPal4c0 6108
#define cursor_1CGBPal4c1 8935
#define cursor_1CGBPal4c2 6596
#define cursor_1CGBPal4c3 6368

/* Gameboy Color palette 5 */
#define cursor_1CGBPal5c0 6108
#define cursor_1CGBPal5c1 8935
#define cursor_1CGBPal5c2 6596
#define cursor_1CGBPal5c3 6368

/* Gameboy Color palette 6 */
#define cursor_1CGBPal6c0 6108
#define cursor_1CGBPal6c1 8935
#define cursor_1CGBPal6c2 6596
#define cursor_1CGBPal6c3 6368

/* Gameboy Color palette 7 */
#define cursor_1CGBPal7c0 6108
#define cursor_1CGBPal7c1 8935
#define cursor_1CGBPal7c2 6596
#define cursor_1CGBPal7c3 6368
/* Start of tile array. */
extern unsigned char cursor_1[];

/* End of CURSOR_1.H */
