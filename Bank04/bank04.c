/****************************************************************************
 *  bank04.c                                                                *
 *                                                                          *
 *    SYSTEMCALL FUNCTIONS (RESOURCE)                                       *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank04\cursor_1.h"
#include "bank04\cursor_2.h"
#include "bank04\cursor_3.h"
#include "bank04\cursor_4.h"
#include "bank04\cursor_5.h"

extern UBYTE pkey_code;  /* in fixed/fixed.c */


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank04\font_1.c"
#include "bank04\font_2.c"
#include "bank04\font_3.c"
#include "bank04\font_4.c"
#include "bank04\font_5.c"
#include "bank04\frame_1.c"
#include "bank04\frame_2.c"
#include "bank04\frame_3.c"
#include "bank04\frame_4.c"
#include "bank04\frame_5.c"
#include "bank04\button_1.c"
#include "bank04\button_2.c"
#include "bank04\button_3.c"
#include "bank04\button_4.c"
#include "bank04\button_5.c"
#include "bank04\cursor_1.c"
#include "bank04\cursor_2.c"
#include "bank04\cursor_3.c"
#include "bank04\cursor_4.c"
#include "bank04\cursor_5.c"

unsigned char *res_font[]  ={ font_1,  font_2,  font_3,  font_4,  font_5   };
unsigned char *res_frame[] ={ frame_1, frame_2, frame_3, frame_4, frame_5  };
unsigned char *res_button[]={ button_1,button_2,button_3,button_4,button_5 };
unsigned char *res_cursor[]={ cursor_1,cursor_2,cursor_3,cursor_4,cursor_5 };
UWORD cursor_1_pal[] =
    { 0, cursor_1CGBPal0c1, cursor_1CGBPal0c2, cursor_1CGBPal0c3 };
UWORD cursor_2_pal[] =
    { 0, cursor_2CGBPal0c1, cursor_2CGBPal0c2, cursor_2CGBPal0c3 };
UWORD cursor_3_pal[] =
    { 0, cursor_3CGBPal0c1, cursor_3CGBPal0c2, cursor_3CGBPal0c3 };
UWORD cursor_4_pal[] =
    { 0, cursor_4CGBPal0c1, cursor_4CGBPal0c2, cursor_4CGBPal0c3 };
UWORD cursor_5_pal[] =
    { 0, cursor_5CGBPal0c1, cursor_5CGBPal0c2, cursor_5CGBPal0c3 };
UWORD *cursor_palette[] =
    { cursor_1_pal,cursor_2_pal,cursor_3_pal,cursor_4_pal,cursor_5_pal };
UWORD clr_b_default[] = { CLR_WHITE, CLR_D_GLAY, CLR_L_GLAY, CLR_BLACK };
UWORD clr_f_default[] = { CLR_WHITE, CLR_WHITE,  CLR_L_GLAY, CLR_BLACK };

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
/*--------------------------------------------------------------------------*
 |  initialize default palette color                                        |
 *--------------------------------------------------------------------------*/
void pda_init_palette( void *arg )
{
    if( _cpu == CGB_TYPE ) {
        set_bkg_palette( 0, 1, clr_b_default );
    }
}

/*--------------------------------------------------------------------------*
 |  initialize system color                                                 |
 *--------------------------------------------------------------------------*/
void pda_init_color( UBYTE *arg )
{
    UWORD palette[4];

    if( _cpu == CGB_TYPE ) {

        /* attribute 0: frame */
        palette[0] = ~(nvm->back_color);
        palette[1] = CLR_WHITE;
        palette[2] = CLR_D_GLAY;
        palette[3] = CLR_BLACK;
        set_bkg_palette( 0, 1, palette );

        /* attribute 1: text */
        palette[1] = CLR_L_GLAY;
        palette[2] = CLR_D_GLAY;
        palette[3] = nvm->fore_color;
        set_bkg_palette( 1, 1, palette );

        /* attribute 2: button (break) */
        switch( nvm->button_type ) {
          case 1:
            palette[0] = CLR_WHITE;
            palette[1] = ~(nvm->back_color);
            palette[2] = CLR_D_GLAY;
            palette[3] = CLR_BLACK;
            break;
          case 2:
            palette[0] = ~(nvm->back_color);
            palette[1] = CLR_WHITE;
            palette[2] = ~(nvm->back_color);
            palette[3] = CLR_BLACK;
            break;
          default:
            palette[0] = CLR_L_GLAY;
            palette[1] = ~(nvm->back_color);
            palette[2] = CLR_WHITE;
            palette[3] = CLR_BLACK;
            break;
        }
        set_bkg_palette( 2, 1, palette );

        /* attribute 3: button (press) */
        switch( nvm->button_type ) {
          case 1:
            palette[0] = CLR_WHITE;
            palette[1] = CLR_L_GLAY;
            palette[2] = CLR_D_GLAY;
            palette[3] = CLR_BLACK;
            break;
          case 2:
            palette[0] = ~(nvm->back_color);
            palette[1] = CLR_BLACK;
            palette[2] = CLR_BLACK;
            palette[3] = nvm->fore_color;
            break;
          default:
            palette[0] = CLR_L_GLAY;
            palette[1] = ~(nvm->back_color);
            palette[2] = CLR_WHITE;
            palette[3] = CLR_BLACK;
            break;
        }
        set_bkg_palette( 3, 1, palette );
    }
}

/*--------------------------------------------------------------------------*
 |  set font type                                                           |
 *--------------------------------------------------------------------------*/
void pda_font_type( UBYTE *arg )
{
    unsigned char *p, buff[16];
    UBYTE  i, j;

    nvm->font_type = *arg;
    if( (nvm->font_type) > 4 ) {
        nvm->font_type = 0;
    }
    p = res_font[nvm->font_type];
    for( i=32; i<128; i++ ) {
        for( j=0; j<8; j++ ) {
            buff[j*2]   = *p;
            buff[j*2+1] = *p;
            p++;
        }
        set_bkg_data( i, 1, buff );
    }
}

/*--------------------------------------------------------------------------*
 |  set frame type                                                          |
 *--------------------------------------------------------------------------*/
void pda_frame_type( UBYTE *arg )
{
    nvm->frame_type = *arg;
    if( (nvm->frame_type) > 4 ) {
        nvm->frame_type = 0;
    }
    set_bkg_data( 1, 11, res_frame[nvm->frame_type] );
}

/*--------------------------------------------------------------------------*
 |  set button type                                                         |
 *--------------------------------------------------------------------------*/
void pda_button_type( UBYTE *arg )
{
    nvm->button_type = *arg;
    if( (nvm->button_type) > 4 ) {
        nvm->button_type = 0;
    }
    set_bkg_data( 12, 20, res_button[nvm->button_type] );
}

/*--------------------------------------------------------------------------*
 |  set cursor type                                                         |
 *--------------------------------------------------------------------------*/
void pda_cursor_type( UBYTE *arg )
{
    nvm->cursor_type = *arg;
    if( (nvm->cursor_type) > 4 ) {
        nvm->cursor_type = 0;
    }
    set_sprite_data( 1, 32, res_cursor[nvm->cursor_type] );
    if( _cpu == CGB_TYPE ) {
        set_sprite_palette( 0, 1, cursor_palette[nvm->cursor_type] );
    }
}

/*--------------------------------------------------------------------------*
 |  initialize system resource                                              |
 *--------------------------------------------------------------------------*/
void pda_init_screen( void *arg )
{
    pda_syscall( PDA_FONT_TYPE, (void *)&(nvm->font_type) );
    pda_syscall( PDA_FRAME_TYPE, (void *)&(nvm->frame_type) );
    pda_syscall( PDA_BUTTON_TYPE, (void *)&(nvm->button_type) );
    pda_syscall( PDA_CURSOR_TYPE, (void *)&(nvm->cursor_type) );

    pkey_code = 1; /* reset */
}

/*--------------------------------------------------------------------------*
 |  put 3x3 button                                                          |
 *--------------------------------------------------------------------------*/
void pda_put_3x3button( struct button *arg )
{
    unsigned char buff[9], buff_a[9];
    UBYTE i;

    for( i=0; i<9; i++ ) {
        buff[i]   = arg->no*9 + 128 + i;
        buff_a[i] = 2;
    }
    set_bkg_tiles( arg->x, arg->y, 3, 3, buff );
    if( _cpu == CGB_TYPE ) {
        *(UBYTE *)0xFF4F = 1;  /* select palette bank */
        set_bkg_tiles( arg->x, arg->y, 3, 3, buff_a );
        *(UBYTE *)0xFF4F = 1;  /* select palette bank */
    }
}

/*--------------------------------------------------------------------------*
 |  make 3x3 button                                                         |
 *--------------------------------------------------------------------------*/
void pdax_make_3x3button( struct button *arg )
{
    unsigned char buff[16];
    unsigned char *base;
    UBYTE i;

    base = (res_button[nvm->button_type])+ arg->on *128;

    /* (0,0) */
    for( i=0; i<8;  i++ )  buff[i] = *(base+i+64);
    for( i=8; i<16; i++ )  buff[i] = (*(base+i+64)&0xF0)|(*(arg->data+i-8)>>4);
    set_bkg_data( arg->no *9+128, 1, buff );
    /* (1,0) */
    for( i=0; i<8;  i++ )  buff[i] = *(base+i+80);
    for( i=8; i<16; i++ )  buff[i] = (*(arg->data+i-8)<<4)|(*(arg->data+i+24)>>4);
    set_bkg_data( arg->no *9+129, 1, buff );
    /* (2,0) */
    for( i=0; i<8;  i++ )  buff[i] = *(base+i+96);
    for( i=8; i<16; i++ )  buff[i] = (*(base+i+96)&0x0F)|(*(arg->data+i+24)<<4);
    set_bkg_data( arg->no *9+130, 1, buff );
    /* (0,1) */
    for( i=0; i<16; i++ )  buff[i] = (*(base+i+112)&0xF0)|(*(arg->data+i+8)>>4);
    set_bkg_data( arg->no *9+131, 1, buff );
    /* (1,1) */
    for( i=0; i<16; i++ )  buff[i] = (*(arg->data+i+8)<<4)|(*(arg->data+i+40)>>4);
    set_bkg_data( arg->no *9+132, 1, buff );
    /* (2,1) */
    for( i=0; i<16; i++ )  buff[i] = (*(base+i+128)&0x0F)|(*(arg->data+i+40)<<4);
    set_bkg_data( arg->no *9+133, 1, buff );
    /* (0,2) */
    for( i=0; i<8;  i++ )  buff[i] = (*(base+i+144)&0xF0)|(*(arg->data+i+24)>>4);
    for( i=8; i<16; i++ )  buff[i] = *(base+i+144);
    set_bkg_data( arg->no *9+134, 1, buff );
    /* (1,2) */
    for( i=0; i<8;  i++ )  buff[i] = (*(arg->data+i+24)<<4)|(*(arg->data+i+56)>>4);
    for( i=8; i<16; i++ )  buff[i] = *(base+i+160);
    set_bkg_data( arg->no *9+135, 1, buff );
    /* (2,2) */
    for( i=0; i<8;  i++ )  buff[i] = (*(base+i+176)&0x0F)|(*(arg->data+i+56)<<4);
    for( i=8; i<16; i++ )  buff[i] = *(base+i+176);
    set_bkg_data( arg->no *9+136, 1, buff );
}

/*--------------------------------------------------------------------------*
 |  make 2x2 button                                                         |
 *--------------------------------------------------------------------------*/
void pdax_make_2x2button( struct button *arg )
{
    unsigned char buff[16];
    unsigned char *base;
    UBYTE i;

    base = (res_button[nvm->button_type])+ arg->on *128;

    /* (0,0) */
    for( i=0; i<8;  i++ )  buff[i] = *(base+i+64);
    for( i=8; i<16; i++ )  buff[i] = (*(base+i+64)&0xF0)|(*(arg->data+i-8)>>4);
    set_bkg_data( arg->no *4+128, 1, buff );
    /* (1,0) */
    for( i=0; i<8;  i++ )  buff[i] = *(base+i+96);
    for( i=8; i<16; i++ )  buff[i] = (*(base+i+96)&0x0F)|(*(arg->data+i-8)<<4);
    set_bkg_data( arg->no *4+129, 1, buff );
    /* (0,1) */
    for( i=0; i<8;  i++ )  buff[i] = (*(base+i+144)&0xF0)|(*(arg->data+i+8)>>4);
    for( i=8; i<16; i++ )  buff[i] = *(base+i+144);
    set_bkg_data( arg->no *4+130, 1, buff );
    /* (1,1) */
    for( i=0; i<8;  i++ )  buff[i] = (*(base+i+176)&0x0F)|(*(arg->data+i+8)<<4);
    for( i=8; i<16; i++ )  buff[i] = *(base+i+176);
    set_bkg_data( arg->no *4+131, 1, buff );
}

/* EOF */