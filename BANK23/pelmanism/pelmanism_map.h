/*

 PELMANISM_MAP.H

 Map Include File.

 Info:
   Section       : 
   Bank          : 0
   Map size      : 20 x 18
   Tile set      : pelmanism_bkg.gbr
   Plane count   : 1 plane (8 bits)
   Plane order   : Tiles are continues
   Tile offset   : 0
   Split data    : No

 This file was generated by GBMB v1.8

*/

#define pelmanism_mapWidth 20
#define pelmanism_mapHeight 18
#define pelmanism_mapBank 0

extern unsigned char pelmanism_map[];

/* End of PELMANISM_MAP.H */
