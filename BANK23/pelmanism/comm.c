// Command Shell FrameWork for GBC
// 2001 TeamKNOx

#include "gb232.h"

#include "comm.h"

void comm_init()
{
    set_gb232( SPEED_9600 | DATA_8 | STOP_1 | PARITY_NONE );	//Mod
    init_gb232();
}


void sendStrings( char *strings )	
{
  while ( *strings ) {
	send_gb232( *strings );
	strings++;
  }
}
