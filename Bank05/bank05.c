/****************************************************************************
 *  bank05.c                                                                *
 *                                                                          *
 *    SYSTEMCALL FUNCTIONS (KEYBOARD)                                       *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank05\keypad.h"
#include "bank05\pencil.h"

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
extern UBYTE pkey_code;         /* in fixed/fixed.c */

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank05\keypad.c"
#include "bank05\keypad2.c"
#include "bank05\pencil.c"

unsigned char keypad_tile[] = {
    0x80,0x82,0x88,0x8A,0x90,0x92,0x98,0x9A,0xA0,0xA2,0xA8,0xAA,0xB0,0xB2,0xB8,0xBA,
    0x81,0x83,0x89,0x8B,0x91,0x93,0x99,0x9B,0xA1,0xA3,0xA9,0xAB,0xB1,0xB3,0xB9,0xBB,
    0x84,0x86,0x8C,0x8E,0x94,0x96,0x9C,0x9E,0xA4,0xA6,0xAC,0xAE,0xB4,0xB6,0xBC,0xBE,
    0x85,0x87,0x8D,0x8F,0x95,0x97,0x9D,0x9F,0xA5,0xA7,0xAD,0xAF,0xB5,0xB7,0xBD,0xBF,
    0xC0,0xC2,0xC8,0xCA,0xD0,0xD2,0xD8,0xDA,0xE0,0xE2,0xE8,0xEA,0xF0,0xF2,0xF8,0xFA,
    0xC1,0xC3,0xC9,0xCB,0xD1,0xD3,0xD9,0xDB,0xE1,0xE3,0xE9,0xEB,0xF1,0xF3,0xF9,0xFB,
    0xC4,0xC6,0xCC,0xCE,0xD4,0xD6,0xDC,0xDE,0xE4,0xE6,0xEC,0xEE,0xF4,0xF6,0xFC,0xFE,
    0xC5,0xC7,0xCD,0xCF,0xD5,0xD7,0xDD,0xDF,0xE5,0xE7,0xED,0xEF,0xF5,0xF7,0xFD,0xFF
};
unsigned char keypad_attribute[] = {
    0,7,7,7,7,7,7,7,7,7,7,7,7,7,7,0,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

unsigned char passwd_tile1[] = {
    0x01,0x50,0x61,0x73,0x73,0x77,0x6F,0x72,0x64,0x03
 };
unsigned char passwd_tile2[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
    0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char passwd_tile_a[] = { 1,1,1,1,1,1,1,1 };
unsigned char passwd_mask[] = { "*" };

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
/*==========================================================================*
 |  keypad                                                                  |
 *==========================================================================*/
struct cursor_info kp_c0 = { 13, 2, 39, 113, 8, 0 };
struct cursor_info kp_c1 = { 13, 3, 34, 121, 8, 0 };
struct cursor_info kp_c2 = { 13, 3, 37, 129, 8, 0 };
struct cursor_info kp_c3 = { 12, 3, 40, 137, 8, 0 };
struct cursor_info kp_c4 = {  1, 2, 82, 145, 0, 0 };
char kp_n0[13] = { "1234567890-_<" };
char kp_n1[13] = { "+QWERTYUIOP@+" };
char kp_n2[13] = { "*ASDFGHJKL;:+" };
char kp_n3[12] = { "!ZXCVBNM,./!" };
char kp_n4[1]  = { " " };
char kp_s0[13] = { "!`#$%&'()=~}<" };
char kp_s1[13] = { "+qwertyuiop{+" };
char kp_s2[13] = { "*asdfghjkl[]+" };
char kp_s3[12] = { "!zxcvbnm<>?!" };
char kp_s4[1]  = { " " };

UBYTE kp_x[] = { 13, 13, 13, 12, 1 };
struct cursor_info *kp_cursor[] = { &kp_c0, &kp_c1, &kp_c2, &kp_c3, &kp_c4 };
char *kp_normal[] = { kp_n0, kp_n1, kp_n2, kp_n3, kp_n4 };
char *kp_shift[]  = { kp_s0, kp_s1, kp_s2, kp_s3, kp_s4 };
char **kp_code[] = { kp_normal, kp_shift };
unsigned char *kp_data[] = { keypad, keypad2 };

/*-------- select virtual or real keyboard --------*/
/* mode flag
   bit 0:  '*'
   bit 1:  keypad position++
   bit 2:  not init cursor pos
*/
void pda_input_string( struct input_string *arg )
{
    UBYTE  i, x, y, len, line, new_line;
    unsigned char  *p;
    struct palette palette;
    struct cursor_pos save, pos;
    struct cursor_info cursor_info;

    /* keypad */
    set_bkg_data( 128, 128, kp_data[caps_lock] );
    set_sprite_data( 64, 1, pencil );
    if( (arg->mode) & 0x02 ) {
        set_bkg_tiles( 2, 12, 16, 6, keypad_tile );
        set_bkg_attribute( 2, 12, 16, 6, keypad_attribute );
    } else {
        set_bkg_tiles( 2, 11, 16, 7, keypad_tile );
        set_bkg_attribute( 2, 11, 16, 7, keypad_attribute );
    }
    palette.no = 7;
    palette.color0 = keypadCGBPal0c0;
    palette.color1 = keypadCGBPal0c1;
    palette.color2 = keypadCGBPal0c2;
    palette.color3 = keypadCGBPal0c3;
    pda_syscall( PDA_SET_PALETTE, &palette );
    palette.no = 15;
    palette.color1 = pencilCGBPal0c1;
    palette.color2 = pencilCGBPal0c2;
    palette.color3 = pencilCGBPal0c3;
    pda_syscall( PDA_SET_PALETTE, &palette );

    /* init pen */
    p = arg->data;
    for( i=0; i<(arg->max); i++ ) {
        if( i < (arg->def_x) ) {
            (arg->data)++;
        } else {
            *p = 0;
        }
        set_bkg_tiles( (arg->x)+i, arg->y, 1, 1, p );
        p++;
    }
    len = arg->def_x;
    set_sprite_tile( 5, 64 );
    move_sprite( 5, ((arg->x)+len)*8+8, (arg->y)*8+16 );
    set_sprite_prop( 5, 7 );

    /* cursor */
    save.x = cursor.x;  save.y = cursor.y;
    if( (arg->mode) & 0x04 ) {
        pos.x = keypad_pos.x;  new_line = pos.y = keypad_pos.y;
    } else {
        caps_lock = 0;  pos.x = 7;  new_line = pos.y = 2; /* J */
    }

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        line = new_line;
        cursor_info.max_x = kp_cursor[line]->max_x;
        cursor_info.max_y = kp_cursor[line]->max_y;
        cursor_info.add_x = kp_cursor[line]->add_x;
        cursor_info.add_y = kp_cursor[line]->add_y;
        cursor_info.off_x = kp_cursor[line]->off_x;
        if( (arg->mode) & 0x02 ) {
            cursor_info.off_y = kp_cursor[line]->off_y+8;
        } else {
            cursor_info.off_y = kp_cursor[line]->off_y;
        }
        pda_syscall( PDA_SET_CURSOR_MAP, &cursor_info );
        switch( line ) {
          case 0:
            if( pos.x >  0 )  pos.x--;
            if( pos.x > 12 )  pos.x=12;
            pos.y = 0;
            break;
          case 1:
            if( pos.x > 12 )  pos.x=12;
            pos.y = 1;
            break;
          case 2:
            if( pos.x > 12 )  pos.x=12;
            pos.y = 1;
            break;
          case 3:
            if( pos.x > 11 )  pos.x=11;
            pos.y = 1;
            break;
          case 4:
            pos.x = 0;  pos.y = 1;
            break;
        }
        pda_syscall( PDA_INIT_CURSOR, &pos );
        pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
        delay( 100 );

        while( !(keycode.brk & J_EXIT) && (line==new_line) ) {
            x = (arg->x) + len;
            y = arg->y;

#if 1  /* 2002/05/19 */
            /* real keyboard function */
            if( pkey_code ) {
                pda_syscall( PDA_GET_REAL_KEYBOARD, (void *)&pkey_code );
                switch( pkey_code ) {
                  case 0x00:  /* joypad */
                    pkey_code = 1;
                    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    break;
                  case 0x08:  /* BS */
                    if( len ) {
                        len--; (arg->data)--; x--;
                        *(arg->data) = 0;
                        set_bkg_tiles( x, y, 1, 1, arg->data );
                        move_sprite( 5, x*8+8, y*8+16 );
                    }
                    break;
                  case 0x0D:  /* CR */
                    keypad_pos.x = cursor.x;
                    keypad_pos.y = cursor.y;
                    cursor.x = save.x;
                    cursor.y = save.y;
                    move_sprite( 5, 0, 0 );
                    return;
                  default:
                    *(arg->data) = pkey_code;  // caps_lock...
                    if( (arg->mode) & 0x01 ) {
                        set_bkg_tiles( x, y, 1, 1, passwd_mask );
                    } else {
                        set_bkg_tiles( x, y, 1, 1, arg->data );
                    }
                    len++; (arg->data)++;  x++;
                    move_sprite( 5, x*8+8, y*8+16 );
                    break;
                }
            } else {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            }
#else
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
#endif

            if( keycode.brk & J_START ) {
                keycode.brk &= ~J_START;
                keypad_pos.x = cursor.x;
                keypad_pos.y = cursor.y;
                cursor.x = save.x;
                cursor.y = save.y;
                move_sprite( 5, 0, 0 );
                return;
            }
            pos.x = cursor.x;
            if( keycode.brk & J_SELECT ) {
                keycode.brk &= ~J_SELECT;
                caps_lock = 1- caps_lock;
                set_bkg_data( 128, 128, kp_data[caps_lock] );
            } else if( keycode.brk & J_B ) {
                keycode.brk &= ~J_B;
                if( len ) {
                    len--; (arg->data)--; x--;
                    *(arg->data) = 0;
                    set_bkg_tiles( x, y, 1, 1, arg->data );
                    move_sprite( 5, x*8+8, y*8+16 );
                }
            } else if( keycode.brk & J_A ) {
                keycode.brk &= ~J_A;
               if( (line==0) && (pos.x==12) ) { /* BS */
                    if( len ) {
                        len--; (arg->data)--; x--;
                        *(arg->data) = 0;
                        set_bkg_tiles( x, y, 1, 1, arg->data );
                        move_sprite( 5, x*8+8, y*8+16 );
                    }
                } else if( (line==2) && (pos.x==12) ) { /* CR */
                    keypad_pos.x = cursor.x;
                    keypad_pos.y = cursor.y;
                    cursor.x = save.x;
                    cursor.y = save.y;
                    move_sprite( 5, 0, 0 );
                    return;
                } else if( len < (arg->max) ) {
                    *(arg->data) = ((kp_code[caps_lock])[line])[pos.x];
                    if( (arg->mode) & 0x01 ) {
                        set_bkg_tiles( x, y, 1, 1, passwd_mask );
                    } else {
                        set_bkg_tiles( x, y, 1, 1, arg->data );
                    }
                    len++; (arg->data)++; x++;
                    move_sprite( 5, x*8+8, y*8+16 );
                }
            }
            switch( line ) {
              case 0:
                if( cursor.y == 1 ) {
                    new_line = 1;
                    pos.x++;
                }
                break;
              case 1:
                if( pos.x ==12 ) { /* CR */
                    new_line = 2;
                } else {
                    new_line = cursor.y;
                }
                break;
              case 2:
                if( (cursor.y==0) && (pos.x==12) ) {
                    new_line = 0;
                    pos.x++;
                } else {
                    new_line = cursor.y + 1;
                }
                break;
              case 3:
                new_line = cursor.y + 2;
                break;
              case 4:
                if( cursor.y == 0 ) {
                    new_line = 3;
                    pos.x = 5;
                }
                break;
            }
        }
    }
    keypad_pos.x = cursor.x;
    keypad_pos.y = cursor.y;
    cursor.x = save.x;
    cursor.y = save.y;
    move_sprite( 5, 0, 0 );
}

void pda_password( struct password *arg )
{
    UBYTE i;
    unsigned char *p;
    struct input_string input_string;

    set_bkg_tiles( 5, 5, 10, 1, passwd_tile1 );
    set_bkg_tiles( 5, 6, 10, 2, passwd_tile2 );
    set_bkg_attribute( 6, 6, 8, 1, passwd_tile_a );

    /* get password field */
    input_string.x = 6;
    input_string.y = 6;
    input_string.max = 8;
    input_string.def_x = 0;
    input_string.mode = arg->match;
    input_string.data = arg->password;
    pda_syscall( PDA_INPUT_STRING, &input_string );
    keycode.brk = 0;

    /* comp password */
    arg->match = 1; /* true */
    p = arg->password;
    for( i=0; i<8; i++ ) {
        if( nvm->password[i] != *p ) {
            arg->match = 0; /* misscompare */
        }
        p++;
    }
}

/* EOF */
