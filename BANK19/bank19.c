/****************************************************************************
 *  bank19.c                                                                *
 *                                                                          *
 *    GB-BAR  V2.11    2002/05/24 (Base: 2000/11 version)                   *
 *    GB-CAT  V2.11    2002/05/25 (Base: 2001/01 version)                   *
 *                                                                          *
 *    Copyright(C)  2000, 2001, 2002   TeamKNOx                             *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank19\bank19.h"
#include "bank19\i_bar.h"
#include "bank19\i_cat.h"

extern void gb_bar( void *arg );
extern void gb_cat( void *arg );
extern UBYTE ram[4][127];

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        2
struct function func_19[FUNC_NUM] = {
    { "GB-BAR  ", "2.11", gb_bar, i_bar, i_barCGBPal0c1, i_barCGBPal0c2 },
    { "GB-CAT  ", "2.11", gb_cat, i_cat, i_catCGBPal0c1, i_catCGBPal0c2 }
};
struct description desc_19 = { FUNC_NUM, (struct function *)func_19 };


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank19\i_bar.c"
#include "bank19\i_cat.c"

/* message */
unsigned char bar_msg_title[]  = { " IBM BarCode Reader " };
unsigned char bar_msg_frame[] = {
  0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,
  0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,
  0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char bar_break_button[] = {
  0x10,0x11,0x11,0x11,0x11,0x12,
  0x13,0x45,0x58,0x49,0x54,0x14,
  0x15,0x16,0x16,0x16,0x16,0x17
};
unsigned char bar_press_button[] = {
  0x18,0x19,0x19,0x19,0x19,0x1A,
  0x1B,0x45,0x58,0x49,0x54,0x1C,
  0x1D,0x1E,0x1E,0x1E,0x1E,0x1F
};
unsigned char bar_attrib2[] = { 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2 };

unsigned char bar_msg_spc[]    = { " " };
unsigned char bar_msg_jan13[]  = { "JAN13  " };
unsigned char bar_msg_jan8[]   = { "JAN8   " };
unsigned char bar_msg_code39[] = { "CODE39 " };
unsigned char bar_msg_nw7[]    = { "NW7    " };
unsigned char bar_msg_itf[]    = { "ITF    " };
unsigned char bar_msg_err[]    = { "ERROR !" };

unsigned char cat_msg_title[]  = { " :Cue:C.A.T. Reader " };
unsigned char cat_msg_clr[]    = { "                    " };
unsigned char cat_msg_spc[]    = { " " };
unsigned char cat_msg_base64[] = { "Base64" };
unsigned char cat_msg_num[]    = { "Serial No." };
unsigned char cat_msg_type[]   = { "Barcode Type" };
unsigned char cat_msg_data[]   = { "Barcode Data" };

/* base64 encode table */
UBYTE cat_base64_table[] = { 
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,  /* 0x00 */
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,  /* 0x10 */
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,  /* 0x20 */
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,  /* 0x30 */
  0x3C,0x3D,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,  /* 0x40 */
  0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,
  0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,  /* 0x50 */
  0x31,0x32,0x33,0xFF,0xFF,0xFF,0xFF,0xFF,
  0xFF,0x00,0x01,0x02,0x03,0x04,0x05,0x06,  /* 0x60 */
  0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,
  0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,  /* 0x70 */
  0x17,0x18,0x19,0xFF,0xFF,0xFF,0xFF,0xFF
};

/* 109 Japanese keyboard data */
UBYTE cat_key_table[] = {
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,  /* 0x00 */
  0x00,0x00,0x00,0x00,0x00,0x09,0x00,0x00,
  0x00,0x00,0x0F,0x00,0x00,0x71,0x31,0x00,  /* 0x10 */
  0x00,0x00,0x7A,0x73,0x61,0x77,0x32,0x00,
  0x00,0x63,0x78,0x64,0x65,0x34,0x33,0x00,  /* 0x20 */
  0x00,0x20,0x76,0x66,0x74,0x72,0x35,0x00,
  0x00,0x6E,0x62,0x68,0x67,0x79,0x36,0x00,  /* 0x30 */
  0x00,0x00,0x6D,0x6A,0x75,0x37,0x38,0x00,
  0x00,0x2C,0x6B,0x69,0x6F,0x30,0x39,0x00,  /* 0x40 */
  0x00,0x2E,0x2F,0x6C,0x3B,0x70,0x2D,0x00,
  0x00,0x5C,0x3A,0x00,0x40,0x5E,0x00,0x00,  /* 0x50 */
  0x00,0x0F,0x0D,0x5B,0x00,0x5D,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,  /* 0x60 */
  0x00,0x31,0x5C,0x34,0x37,0x00,0x00,0x00,
  0x30,0x2E,0x32,0x35,0x36,0x38,0x00,0x00,  /* 0x70 */
  0x00,0x2B,0x33,0x2D,0x2A,0x39,0x00,0x00
};

/* sample data */
extern UBYTE bar_cat_test_data1[] = {  /* J13 */
 ".C3nZC3nZC3nYCxfWC3b1CNnY.cxjW.D3PZCNb1C3fWE3zZDa."
};
extern UBYTE bar_cat_test_data2[] = {  /* E08 */
 ".C3nZC3nZC3nYCxfWC3b1CNnY.bNn7.C3n3Chj1Dhy."
};
//.C3nZC3nZEC3nYCNT dn 3CNnY.fHmc.C3DZCxPWCNGZwd nX.(NG)  /* UPA */
//.C3nZC3nZ C3nYCNT7DNf3CNnY.fHmc.C3DZCxPWCN zWDNnX.(OK)

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void bar_disp_value( UWORD d, UWORD e, UBYTE c, UBYTE x, UBYTE y )
{
	UWORD m;
	UBYTE i, n;
	unsigned char data[6];

	m = 1;
	if ( c > 1 ) {
		for ( i = 1; i < c; i++ ) {
			m = m * e;
		}
	}
	for ( i = 0; i < c; i++ ) {
		n = d / m; d = d % m; m = m / e;
		data[i] = '0' + n;		 /* '0' - '9' */
		if ( data[i] > '9' )  data[i] += 7;
	}
	set_bkg_tiles( x, y, c, 1, data );
}


/*--------------------------------------------------------------------------*
 |  read bar code                                                           |
 *--------------------------------------------------------------------------*/
void bar_ibm_read()
{
	UBYTE  len;
	unsigned char data[20];
  struct read_data read_data;

  /* wait bar code signal */
  len = 0;
  read_data.flag = BAR_READING;
  while( read_data.flag ) {
    pda_syscall( PDA_GET_KEYBOARD, &read_data );
    data[len] = read_data.data;
    len++; if( len >= 20 )  return;  /* overflow */
  }
  len--;  if( len < 4 )  return;  /* timeout */

  /* read out & display data footer data has been fixed as 0x83. */
  /* until encountered footer data.                              */
  if( data[1] ) {	 /* Data has been already read out ? */
    set_bkg_tiles( 1, 8, 18, 1, &bar_msg_frame[11] );
    set_bkg_tiles( 1, 8, len-3, 1, &data[2] );

#if 1
    /* display supplement data as header, bar code id, footer */
    bar_disp_value( data[0],     16, 2, 1, 10 );  /* header */
    bar_disp_value( data[1],     16, 2, 4, 10 );  /* bar code id */
    bar_disp_value( data[len-1], 16, 2, 7, 10 );  /* footer */
#endif
    switch ( data[1] ) {
      case BAR_JAN13:
        set_bkg_tiles( 1, 6, 7, 1, bar_msg_jan13  );
        break;
      case BAR_JAN8:
        set_bkg_tiles( 1, 6, 7, 1, bar_msg_jan8   );
        break;
      case BAR_CODE39:
        set_bkg_tiles( 1, 6, 7, 1, bar_msg_code39 );
        break;
      case BAR_ITF:
        set_bkg_tiles( 1, 6, 7, 1, bar_msg_itf    );
        break;
      case BAR_NW7:
        set_bkg_tiles( 1, 6, 7, 1, bar_msg_nw7    );
        break;
      default:
        set_bkg_tiles( 1, 6, 7, 1, bar_msg_err    );
        break;
    }
  }
}

/*--------------------------------------------------------------------------*
 |  loop                                                                    |
 *--------------------------------------------------------------------------*/
struct cursor_info bar_ibm_cursor = { 1, 1, 84, 132, 0, 0 };
void bar_ibm_loop()
{
  struct cursor_pos pos;

  pos.x = pos.y = 0;
  pda_syscall( PDA_SET_CURSOR_MAP, &bar_ibm_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
  delay( 100 );

  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.make & J_A ) {
      set_bkg_tiles( 7, 13,  6, 3, bar_press_button );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return;
      }
      keycode.brk |= J_EXIT;
      return;
    }
    bar_ibm_read();
  }
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void bar_ibm_init()
{
  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

  set_bkg_tiles( 0,  2, 20, 1, bar_msg_title );
  set_bkg_tiles( 0,  7, 20, 3, bar_msg_frame );
  set_bkg_tiles( 7, 13,  6, 3, bar_break_button );
  set_bkg_attribute( 7, 13, 6, 3, bar_attrib2 );
}

/*==========================================================================*
 |  gb_bar main                                                             |
 *==========================================================================*/
void gb_bar( void *arg )
{
  pda_syscall( PDA_DISABLE_INTERRUPT, (void *)0 );
  bar_ibm_init();

  while( !(keycode.brk & J_EXIT) ) {
    bar_ibm_loop();
  }
}


/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void cat_cry()
{
  NR50_REG = 0x77;
  NR51_REG = 0x11;
  NR52_REG = 0x8F;
  NR10_REG = 0x4F;
  NR11_REG = 0x00;
  NR12_REG = 0xF4;
  NR13_REG = 0xFF;
  NR14_REG = 0x87;
}

UBYTE cat_conv_ascii( struct cat_t *cat, UBYTE raw )
{
  UBYTE ascii;

  /* check for key off */
  if( raw & 0x80 )  return( 0xFF );

  /* convert to ascii code */
  ascii = cat_key_table[raw];

  /* check for shift key */
  if( ascii == 0x0F ) {
    cat->shift ^= 0x01;
    return( 0xFF );
  }

  /* convert to shift char */
  if( cat->shift == CAT_ON ) {
    if( (ascii>0x60) && (ascii<0x7B) ) {
      ascii = ascii - 0x20;
    } else if( (ascii>0x30) && (ascii<0x3C) ) {
      ascii = ascii - 0x10;
    } else if( (ascii>0x2B) && (ascii<0x30) ) {
      ascii = ascii + 0x10;
    } else if( (ascii>0x5A) && (ascii<0x5F) ) {
      ascii = 0x60;
    } else if( ascii == 0x40 ) {
			ascii = 0x60;
    } 
  }

  /* check for key up */
  if( cat->ascii_old == ascii ) {
    cat->ascii_old = ascii = 0xFF;
  } else {
    cat->ascii_old = ascii;
  }
  return( ascii );
}

void cat_encode( struct cat_t *cat )
{
  UBYTE base1, base2, base3, base4, d1, d2;

  /* base64 encode */
  base1 = cat_base64_table[cat->asc1];
  base2 = cat_base64_table[cat->asc2];
  base3 = cat_base64_table[cat->asc3];
  base4 = cat_base64_table[cat->asc4];

  /* 6bit to 8bit convert */
  d1 = ( base1 << 2 ) & 0xFC;
  d2 = ( base2 >> 4 ) & 0x03;
  cat->data1 = ( d1 | d2 );
  d1 = ( base2 << 4 ) & 0xF0;
  d2 = ( base3 >> 2 ) & 0x0F;
  cat->data2 = ( d1 | d2 );
  d1 = ( base3 << 6 ) & 0xC0;
  d2 = ( base4 ) & 0x3F;
  cat->data3 = ( d1 | d2 );

  /* decrypt by keycode 'C' */
  cat->data1 ^= 'C';
  cat->data2 ^= 'C';
  cat->data3 ^= 'C';
}

/*--------------------------------------------------------------------------*
 |  read cat bar code                                                       |
 *--------------------------------------------------------------------------*/
void bar_cat_read( struct cat_t *cat )
{
  UBYTE  i, j, k, d;
  UWORD  m, len;
  unsigned char ascii[127];
  unsigned char buff[20];
  struct read_data read_data;

  /* wait bar code signal */
  i = j = 0;
  read_data.flag = CAT_READING;
  while( read_data.flag ) {
    pda_syscall( PDA_GET_KEYBOARD, &read_data );
    ram[j][i] = read_data.data;
    i++;
    if( i > 126 ) {
      i = 0;
      j++;
    }
  }
  len = (UWORD)j*127;
  len = len + (UWORD)i;
  len--;

  /* clear mem */
  for( i=0; i<127; i++ )  ascii[i] = 0;

  if( len == 0 ) {  /* no data */
#if 0  // for debug
    for( i=0; i<sizeof(bar_cat_test_data2); i++ ) {
      ascii[i] = bar_cat_test_data2[i];
    }
#else
    return;
#endif
  } else {
    /* convert to ascii */
    cat->ascii_old = 0xFF;
    cat->shift = CAT_OFF;
    i = j = k = 0;
    m = 0;
    while( m < len ) {
      d = cat_conv_ascii( cat, ram[j][i] );
      if( (0x1F<d) && (d<0x80) ) {
        ascii[k] = d;
        k++;
        if( k > 126 )  m = len;  /* overflow */
      }
      i++;  m++;
      if( i > 126 ) {
        i = 0;
        j++;
      }
    }
  }
  set_bkg_tiles( 1, 3, 18, 3, ascii );

  len = (UWORD)k;
//  if( ascii[0] != '.' )  return;  /* error */ 
  k = 0;
  while( ascii[k] == '.' ) {
    k++;
    if( k > len )  return;  /* error */
  }

  /* get the serial number */
#if 1
  for( i=0; i<20; i++ )  buff[i] = 0;
  i = 0;
  while( ascii[k] != '.' ) {
    cat->asc1 = ascii[k];
    cat->asc2 = ascii[k+1];
    cat->asc3 = ascii[k+2];
    cat->asc4 = ascii[k+3];
    cat_encode( cat );
    buff[i]   = cat->data1;
    buff[i+1] = cat->data2;
    buff[i+2] = cat->data3;
    k = k + 4;  i = i + 3;
    if( k > len )  return;  /* error */
  }
  k++;
  set_bkg_tiles( 1,  8, 18, 1, buff );
#else
  while( ascii[k] != '.' ) {
    k++;
    if( k > len )  return;  /* error */
  }
  k++;
#endif

  /* get the barcode type */
  for( i=0; i<20; i++ )  buff[i] = CAT_CLEAR;
  i = 0;
  while( ascii[k] != '.' ) {
    cat->asc1 = ascii[k];
    cat->asc2 = ascii[k+1];
    cat->asc3 = ascii[k+2];
    cat->asc4 = ascii[k+3];
    cat_encode( cat );
    buff[i]   = cat->data1;
    buff[i+1] = cat->data2;
    buff[i+2] = cat->data3;
    k = k + 4;  i = i + 3;
    if( k > len )  return;  /* error */
  }
  k++;
  set_bkg_tiles( 1, 11, 18, 1, buff );

  /* get the barcode data */
  for( i=0; i<20; i++ )  buff[i] = CAT_CLEAR;
  i = 0;
  while( (ascii[k]!='.') && (ascii[k]) ) {
    cat->asc1 = ascii[k];
    cat->asc2 = ascii[k+1];
    cat->asc3 = ascii[k+2];
    cat->asc4 = ascii[k+3];
    cat_encode( cat );
    if( ascii[k+2] == '.' )  cat->data2 = cat->data3 = CAT_CLEAR;
    if( ascii[k+3] == '.' )  cat->data3 = CAT_CLEAR;
    buff[i]   = cat->data1;
    buff[i+1] = cat->data2;
    buff[i+2] = cat->data3;
    k = k + 4;  i = i + 3;
    if( k > len )  return;  /* error */
  }
  set_bkg_tiles( 1, 14, 18, 1, buff );
  cat_cry();
}

/*--------------------------------------------------------------------------*
 |  loop                                                                    |
 *--------------------------------------------------------------------------*/
struct cursor_info bar_cat_cursor = { 1, 1, 84, 148, 0, 0 };
void bar_cat_loop( struct cat_t *cat )
{
  struct cursor_pos pos;

  pos.x = pos.y = 0;
  pda_syscall( PDA_SET_CURSOR_MAP, &bar_cat_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
  delay( 100 );

  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.make & J_A ) {
      set_bkg_tiles( 7, 15,  6, 3, bar_press_button );
      while( keycode.make & J_A ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return;
      }
      keycode.brk |= J_EXIT;
      return;
    }
    bar_cat_read( cat );
  }
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void bar_cat_init()
{
  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

  set_bkg_tiles( 0,  0, 20, 1, cat_msg_title );
  set_bkg_tiles( 0,  2, 20, 2, bar_msg_frame );
  set_bkg_tiles( 1,  2,  6, 1, cat_msg_base64 );
  set_bkg_tiles( 0,  4, 20, 1, &bar_msg_frame[20] );
  set_bkg_tiles( 0,  5, 20, 2, &bar_msg_frame[20] );
  set_bkg_tiles( 0,  7, 20, 3, bar_msg_frame );
  set_bkg_tiles( 1,  7, 10, 1, cat_msg_num );
  set_bkg_tiles( 0, 10, 20, 3, bar_msg_frame );
  set_bkg_tiles( 1, 10, 12, 1, cat_msg_type );
  set_bkg_tiles( 0, 13, 20, 3, bar_msg_frame );
  set_bkg_tiles( 1, 13, 12, 1, cat_msg_data );
  set_bkg_tiles( 7, 15,  6, 3, bar_break_button );
  set_bkg_attribute( 7, 15, 6, 3, bar_attrib2 );

  cat_cry();
}

/*==========================================================================*
 |  gb_cat main                                                             |
 *==========================================================================*/
void gb_cat( void *arg )
{
  struct cat_t cat;

  pda_syscall( PDA_DISABLE_INTERRUPT, (void *)0 );
  bar_cat_init();

  while( !(keycode.brk & J_EXIT) ) {
    bar_cat_loop( &cat );
  }
}

/* EOF */