/****************************************************************************
 *  bank19.h                                                                *
 *                                                                          *
 *    Copyright(C)  2000, 2001   TeamKNOx                                   *
 ****************************************************************************/

//  This BarCode Reader gives Header, BarCode ID, and Footer as follows,
//
//  Header:            0x82
//
//  BarCode ID: JAN13  0x41
//              JAN8   0x42
//              ITF    0x49
//              CODE39 0x4D
//
//  Footer:            0x83

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define BAR_OFF           0
#define BAR_ON            1
#define BAR_FOREVER       1
#define BAR_READING       1
#define BAR_CLEAR         0
#define BAR_DATA_BUF_SIZE 20
#define BAR_HEADER_ID     0x82
#define BAR_FOOTER_ID     0x83
#define BAR_JAN13         0x41
#define BAR_JAN8          0x42
#define BAR_ITF           0x49
#define BAR_CODE39        0x4D
#define BAR_NW7           0x4E

#define CAT_OFF           0
#define CAT_ON            1
#define CAT_FOREVER       1
#define CAT_READING       1
#define CAT_CLEAR         0
#define CAT_CR            0x0D
#define CAT_LF            0x0A
#define CAT_SP            0x20

/*==========================================================================*
 |  work area                                                               |
 *==========================================================================*/
typedef struct cat_t {
  UBYTE sn[18];  /* serial number */
  UBYTE ascii_old;
  UBYTE shift;
  UBYTE data1;
  UBYTE data2;
  UBYTE data3;
  UBYTE asc1;
  UBYTE asc2;
  UBYTE asc3;
  UBYTE asc4;
} _cat_t;

/* EOF */