/****************************************************************************
 *  bank16.c                                                                *
 *                                                                          *
 *    GB-REMO  V2.00    2001/01/01                                          *
 *                                                                          *
 *    Copyright(C)  1999, 2000, 2001   TeamKNOx                             *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank16\i_remo.h"
#include "bank16\c_remo.h"

extern void gb_remo( void *arg );
extern UBYTE ram[2][127];

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define MODE_SELECT 0
#define MODE_PLAY   1
#define MODE_REC    2
#define FLAG_OFF    0
#define FLAG_ON     1

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank16\i_remo.c"
#include "bank16\c_remo.c"

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_16[FUNC_NUM] = {
    { "GB-REMO ", "2.00", gb_remo, i_remo, i_remoCGBPal0c1, i_remoCGBPal0c2 }
};
struct description desc_16 = { FUNC_NUM, (struct function *)func_16 };


/*==========================================================================*
 |  data                                                                    |
 *==========================================================================*/
/* maker code */
#define MAKER_SONY        0
#define MAKER_NEC         1
#define MAKER_MITSUBISHI  2
#define MAKER_YAMAHA      3
#define MAKER_OTHER       4
unsigned char maker_code[][8] = {
    "SONY    ",
    "NEC     ",
    "MITUBISI",
    "YAMAHA  ",
    "OTHER   "
};

/* BUTTON CODE */
#define KEY_NONE          0
#define KEY_POWER         1
#define KEY_PLAY          2
#define KEY_STOP          3
#define KEY_PAUSE         4
#define KEY_REWIND        5
#define KEY_SKIP          6
#define KEY_PLAYBACK      7
#define KEY_REC           8
#define KEY_INC           9
#define KEY_DEC           10
#define KEY_TV_VTR        11
#define KEY_TIMER         12
#define KEY_EJECT         13
#define KEY_0             14
#define KEY_1             15
#define KEY_2             16
#define KEY_3             17
#define KEY_4             18
#define KEY_5             19
#define KEY_6             20
#define KEY_7             21
#define KEY_8             22
#define KEY_9             23
#define KEY_VOL_INC       24
#define KEY_VOL_DEC       25



UBYTE sony_vtr_ch_inc[] = {
#if 0
    0x17,0x06,0x0C,0xF1,
    0x90,0x05,
    0x00,0x00
#else
    0x17,0x07,0x06,0x06,0x06,0x0C,0x06,0x06,0x0C,0x0C,0x06,0x0C,0x06, /* 1H */
    0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0xF1, /* 1L */
    0x17,0x06,0x06,0x06,0x06,0x0C,0x06,0x06,0x0C,0x0C,0x06,0x0C,0x06, /* 2H */
    0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0xF1  /* 2L */
#endif
};
UBYTE sony_vtr_ch_dec[] = {
#if 0
    0x17,0x06,0x0C,0xEC,
    0x91,0x05,
    0x00,0x00
#else
    0x17,0x0C,0x06,0x07,0x07,0x0B,0x07,0x06,0x0B,0x0C,0x06,0x0C,0x06, /* 1H */
    0x06,0x06,0x06,0x05,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0xEC, /* 1L */
    0x17,0x0B,0x07,0x06,0x06,0x0C,0x06,0x06,0x0C,0x0B,0x07,0x0B,0x07, /* 2H */
    0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0xEB  /* 2L */
#endif
};
UBYTE sony_vtr_power[] = {
    0x07,0x05,0x04,0x01,0x03,0x01,0x09,0x01,0x01,0x05,0x08,0x03,0x05, /* 1H */
    0x08,0x01,0x01,0x01,0x04,0x01,0x03,0x02,0x09,0x01,0x01,0x06,0x08, /* 2H */
    0x07,0x01,0x03,0x06,0x05,0x06,0x01,0x03,0x07,0x04,0x06,0x06,0x05,0x05,
    0x03,0x06,0x07,0x01,0x03,0x05,0x05,0x06,0x01,0x03,0x01,0x06,0x03,0x02,
    0x06,0x08,0x01,0x02,0x06,
    0x00 /* 1L & 2L */
};
UBYTE sony_vtr_rewind[] = {
    0x00
};
UBYTE sony_vtr_playback[] = {
    0x00
};
UBYTE sony_vtr_play[] = {
    0x00
};
UBYTE sony_vtr_skip[] = {
    0x00
};
UBYTE sony_vtr_pause[] = {
    0x00
};
UBYTE sony_vtr_stop[] = {
    0x00
};
UBYTE sony_vtr_rec[] = {
    0x00
};
UBYTE sony_vtr_tv[] = {
    0x00
};

/* remocon data tables */
typedef struct remocon_data {
    UBYTE key_type;
    UBYTE length;
    UBYTE *data;
} _remocon_data;

struct remocon_data remocon_data[] = {
    { KEY_NONE,      0, (UBYTE *)0 },
    { KEY_INC,      13, sony_vtr_ch_inc },
    { KEY_DEC,      13, sony_vtr_ch_dec },
    { KEY_POWER,    13, sony_vtr_power },
    { KEY_REWIND,   13, sony_vtr_rewind },
    { KEY_PLAYBACK, 13, sony_vtr_playback },
    { KEY_PLAY,     13, sony_vtr_play },
    { KEY_SKIP,     13, sony_vtr_skip },
    { KEY_PAUSE,    13, sony_vtr_pause },
    { KEY_STOP,     13, sony_vtr_stop },
    { KEY_REC,      13, sony_vtr_rec },
    { KEY_TV_VTR,   13, sony_vtr_tv },
    { KEY_TIMER,    13, sony_vtr_rec },
    { KEY_EJECT,    13, sony_vtr_rec },
    { KEY_0,        13, sony_vtr_rec },
    { KEY_1,        13, sony_vtr_rec },
    { KEY_2,        13, sony_vtr_rec },
    { KEY_3,        13, sony_vtr_rec },
    { KEY_4,        13, sony_vtr_rec },
    { KEY_5,        13, sony_vtr_rec },
    { KEY_6,        13, sony_vtr_rec },
    { KEY_7,        13, sony_vtr_rec },
    { KEY_8,        13, sony_vtr_rec },
    { KEY_9,        13, sony_vtr_rec },
    { KEY_VOL_INC,  13, sony_vtr_rec },
    { KEY_VOL_DEC,  13, sony_vtr_rec }
};


/* remocon form tables */
typedef struct remocon_form {
    unsigned char name[4];
    UBYTE code[18];
    unsigned char line[54];
} _remocon_form;

struct remocon_form remo_tv[] = {
    { "TV  ", 3, 0,15,16,17,14,24, 0,18,19,20, 1,25, 0,21,22,23, 2,
      "---            ---VOL            CH.                  " }
};
struct remocon_form remo_vtr[] = {
    { "VTR ", 3, 0, 0, 0, 0, 0, 4, 5, 6, 7, 0, 1, 8, 9,10,11, 0, 2,
      "------------------               CH.        TV/VTR    " }
};
struct remocon_form remo_amp[] = {
    { "AMP ", 3, 0, 0, 0, 0, 0,24, 0,15,16,17,18,25, 0,19,20,21,22,
      "------------------VOL     SELECTOR                    " }
};



/* remocon info tables */
typedef struct remocon_info {
    UBYTE         maker_code;
    unsigned char type_name[7];
    struct remocon_form  *form;
} _remocon_info;

struct remocon_info remo_tbl[] = {
    { MAKER_SONY,       "TV     ", remo_tv  },
    { MAKER_SONY,       "VTR1   ", remo_vtr },
    { MAKER_SONY,       "VTR2   ", remo_vtr },
    { MAKER_SONY,       "VTR3   ", remo_vtr },
    { MAKER_NEC,        "TV     ", remo_tv  },
    { MAKER_NEC,        "VTR    ", remo_vtr },
    { MAKER_MITSUBISHI, "TV     ", remo_tv  },
    { MAKER_YAMAHA,     "AV AMP ", remo_amp }
};

unsigned char remo_head1[] = {
    0x01,0x02,0x09,0x02,0x09,0x02,0x09,0x02,0x09,0x02,
    0x09,0x02,0x09,0x02,0x09,0x02,0x09,0x02,0x03,0x00,
    0x04,0x30,0x04,0x31,0x04,0x32,0x04,0x33,0x04,0x34,
    0x04,0x35,0x04,0x36,0x04,0x37,0x04,0x38,0x04,0x00
};
unsigned char remo_head2[] = {
    0x07,0x02,0x0A,0x02,0x0A,0x02,0x0A,0x02,0x0A,0x02,
    0x0A,0x02,0x0A,0x02,0x0A,0x02,0x0A,0x02,0x0A,0x03
};
unsigned char remo_line[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04
};
unsigned char remo_tail[] = {
    0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char remo_tab0[] = { 0x04,0x00,0x05 };
unsigned char remo_tab1[] = { 0x06,0x00,0x05 };

unsigned char remo_break1[] = { 0x10,0x11,0x12 };
unsigned char remo_break2[] = { 0x13,0x00,0x14 };
unsigned char remo_break3[] = { 0x15,0x16,0x17 };
unsigned char remo_press1[] = { 0x18,0x19,0x1A };
unsigned char remo_press2[] = { 0x1B,0x00,0x1C };
unsigned char remo_press3[] = { 0x1D,0x1E,0x1F };
unsigned char remo_space[]  = { 0,0,0,0,0,0,0,0,0 };
unsigned char remo_attrb0[] = { 2,2,2,2,2,2,2,2,2 };  /* enable */
unsigned char remo_attrb1[] = { 3,3,3,3,3,3,3,3,3 };  /* disable */


UBYTE remo_page_set[9] = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };


/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
#define HIGHLEVEL   0x10
#define LOWLEVEL    0x05
UBYTE remo_data_encode( UBYTE *from, UBYTE *to, UBYTE size )
{
    UBYTE  i, j, len, mask, work[8];

    /* make bit */
    len = 0;
    for( i=0; i<size; i++ ) {
        work[len] = work[len] << 1;
        if( *from >= HIGHLEVEL )  work[len] |= 0x01;
        from++;
        if( (i%8) == 7 )  len++;
    }
    if( i%8 )  len++;

    /* bit re-assign */
    for( j=0; j<len; j++ ) {
        mask = 0x01;
        for( i=0; i<8; i++ ) {
            *to = *to << 1;
            if( work[j] & mask )  *to |= 0x01;
            mask = mask << 1;
        }
        to++;
    }
    return( len );
}

UBYTE remo_data_decode( UBYTE *from, UBYTE *to, UBYTE size )
{
    UBYTE  i, j, len, mask;

    len = 0;
    for( j=0; j<size; j++ ) {
        mask = 0x01;
        for( i=0; i<8; i++ ) {
            if( *from & mask ) {
                *to = HIGHLEVEL;
            } else {
                *to = LOWLEVEL;
            }
            to++; len++;
            mask = mask << 1;
        }
    }
    return( len );
}

void remo_press_button( UBYTE x, UBYTE y )
{
    set_bkg_tiles( x,   y,   3, 1, remo_press1 );
    set_bkg_tiles( x,   y+1, 1, 1, remo_press2 );
    set_bkg_tiles( x+2, y+1, 1, 1, &remo_press2[2] );
    set_bkg_tiles( x,   y+2, 3, 1, remo_press3 );
}

void remo_break_button( UBYTE x, UBYTE y )
{
    set_bkg_tiles( x,   y,   3, 1, remo_break1 );
    set_bkg_tiles( x,   y+1, 1, 1, remo_break2 );
    set_bkg_tiles( x+2, y+1, 1, 1, &remo_break2[2] );
    set_bkg_tiles( x,   y+2, 3, 1, remo_break3 );
    set_bkg_attribute( x, y, 3, 3, remo_attrb0 );
}

void remo_space_button( UBYTE x, UBYTE y )
{
    set_bkg_tiles(     x, y, 3, 3, remo_space );
    set_bkg_attribute( x, y, 3, 3, remo_space );
}

void remo_data_send( UBYTE code )
{
    UBYTE i, len;
    UBYTE *data, *ram_hi, *ram_lo;

    len  = remocon_data[code].length;
    data = remocon_data[code].data;
    ram_hi = (UBYTE *)&ram[0][0];
    ram_lo = (UBYTE *)&ram[1][0];
    for( i=0; i<len; i++ ) {
        *ram_hi = *data;
        ram_hi++;  data++;
    }
    for( i=0; i<len; i++ ) {
        *ram_lo = *data;
        ram_lo++;  data++;
    }
    pda_syscall( PDA_PLAY_REMOCON, (void *)0 );
}

struct cursor_info remo3_cursor = { 3, 2, 22, 38, 24, 32 };
UBYTE remo_mode_rec( UBYTE page )
{
    struct cursor_pos pos;
    UBYTE  i, type, code;

    pos.x = 0;  pos.y = 1;
    pda_syscall( PDA_SET_CURSOR_MAP, &remo3_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( cursor.y > 0 ) {
        pos.x = cursor.x;
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( MODE_SELECT );
        if( cursor.y == 0 )         return( MODE_SELECT );
    }
    return( MODE_SELECT );
}

void remo_show_page( UBYTE page )
{
    UBYTE i, x, y, type;
    unsigned char code[1];

    set_bkg_tiles( 0, 2, 20, 1, remo_head2 );
    if( page )  set_bkg_tiles( page*2, 2, 3, 1, remo_tab1 );
    else        set_bkg_tiles( 0, 2, 3, 1, remo_tab0 );
    for( i=3; i<17; i++ ) {
        set_bkg_tiles( 0, i, 20, 1, remo_line );
    }

    if( page == 0 ) {
        for( i=0; i<8; i++ ) {
#if 0
            type = remo_page_set[i];
            code[0] = '1' + i;
            set_bkg_tiles( 1, i+3, 1, 1, code );
            set_bkg_tiles( 3, i+3, 8, 1,
                           &maker_code[remo_tbl[type].maker_code][0] );
            set_bkg_tiles( 12, i+3, 7, 1, remo_tbl[type].type_name );
#endif
        }
    } else {
        type = remo_page_set[page-1];

        /* setup button */
        set_bkg_tiles( 1,  3, 8, 1, &maker_code[remo_tbl[type].maker_code][0] );
        set_bkg_tiles( 10, 3, 7, 1, remo_tbl[type].type_name );
        for( y=0; y<3; y++ ) {
            for( x=0; x<6; x++ ) {
                i = remo_tbl[type].form->code[y*6+x];
                if( i ) {
                    remo_break_button( x*3+1, y*4+5 );
                    code[0] = remocon_data[i].key_type + 0x80;
                    set_bkg_tiles( x*3+2, y*4+6, 1, 1, code );
                } else {
                    remo_space_button( x*3+1, y*4+5 );
                }
            }
        }
        set_bkg_tiles( 1,  8, 18, 1, &remo_tbl[type].form->line[0] );
        set_bkg_tiles( 1, 12, 18, 1, &remo_tbl[type].form->line[18] );
        set_bkg_tiles( 1, 16, 18, 1, &remo_tbl[type].form->line[36] );
    }
}

struct cursor_info remo2_cursor = { 6, 5, 22, 6, 24, 32 };
UBYTE remo_mode_play( UBYTE page )
{
    struct cursor_pos pos;
    UBYTE  i, type, code;

    type = remo_page_set[page-1];
    pos.x = 0;  pos.y = 2;
    pda_syscall( PDA_SET_CURSOR_MAP, &remo2_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( cursor.y > 0 ) {
        pos.x = cursor.x;
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( MODE_SELECT );
        if( cursor.y == 0 )         return( MODE_SELECT );
        if( cursor.y == 1 ) {
            cursor.x = 0;
        } else {
            if( cursor.x > pos.x ) {
                for( i=0; i<6; i++ ) {
                    code = remo_tbl[type].form->code[(cursor.y-2)*6+cursor.x];
                    if( code ) break;
                    if( cursor.x == 5 )  cursor.x = 0;
                    else                 cursor.x++;
                }
            } else {
                for( i=0; i<6; i++ ) {
                    code = remo_tbl[type].form->code[(cursor.y-2)*6+cursor.x];
                    if( code ) break;
                    if( cursor.x == 0 )  cursor.x = 5;
                    else                 cursor.x--;
                }
            }
            if( keycode.make & J_A ) {
                code = remo_tbl[type].form->code[(cursor.y-2)*6+cursor.x];
                if( code ) {
                    remo_press_button( cursor.x*3+1, cursor.y*4-3 );
                    remo_data_send( code );
                    while( keycode.make & J_A ) {
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    }
                    remo_break_button( cursor.x*3+1, cursor.y*4-3 );
                }
            }
        }
    }
    return( MODE_SELECT );
}

struct cursor_info remo1_cursor = { 9, 2, 14, 30, 16, 0 };
UBYTE remo_mode_select( UBYTE *page )
{
    struct cursor_pos pos;
    UBYTE type, code;

    pos.x = *page;  pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &remo1_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( cursor.y == 0 ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 0 );
        if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            *page = cursor.x;
            remo_show_page( *page );
        }
    }
    if( cursor.x == 0 )  return( MODE_REC );
    return( MODE_PLAY );
}

void init_remo()
{
    UBYTE  i;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_data( 0x80, 128, c_remo );

    set_bkg_tiles( 0, 0, 20, 2, remo_head1 );
    set_bkg_tiles( 0, 2, 20, 1, remo_head2 );
    for( i=3; i<17; i++ ) {
        set_bkg_tiles( 0, i, 20, 1, remo_line );
    }
    set_bkg_tiles( 0, 17, 20, 1, remo_tail );
}

void gb_remo( void *arg )
{
    UBYTE i, j, mode, page;

    init_remo();

    mode = MODE_PLAY;
    page = 1;
    remo_show_page( page );

    while( !(keycode.brk&J_EXIT) ) {
        switch( mode ) {
          case MODE_SELECT:
            mode = remo_mode_select( &page );
            break;
          case MODE_PLAY:
            mode = remo_mode_play( page );
            break;
          case MODE_REC:
            mode = remo_mode_rec( page );
            break;
        }
    }
}

/* EOF */