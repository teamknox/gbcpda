/****************************************************************************
 *  bank22.c                                                                *
 *                                                                          *
 *    REMSCANs V2.10    2002/05/19 (Base: 2000/08 version)                  *
 *                                                                          *
 *    Copyright(C)  2000, 2001, 2002   TeamKNOx                             *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank22\i_scan.h"

extern void remscan( void *arg );

extern UBYTE ram[4][127];       /* for remocon driver */


/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_22[FUNC_NUM] = {
  { "RemScan!", "2.10", remscan, i_scan, i_scanCGBPal0c1, i_scanCGBPal0c2 }
};
struct description desc_22 = { FUNC_NUM, (struct function *)func_22 };

/*==========================================================================*
 |  definitions                                                             |
 *==========================================================================*/
#define RMS_OFF          0
#define RMS_ON           1

#define RMS_MODE_FROM    0
#define RMS_MODE_TO      1
#define RMS_MODE_STEP    2
#define RMS_MODE_PARAM   3
#define RMS_MODE_SELECT  4

#define RMS_TYPE_1       0
#define RMS_TYPE_2       1
#define RMS_TYPE_3       2

typedef struct rms_t {
  UBYTE type;
  UBYTE input[14];
  UBYTE code[4];
  UBYTE flag;
} _rms_t;

#define RMS_FROM_CODE    0
#define RMS_TO_CODE      3
#define RMS_STEP         6
#define RMS_REPEAT       7
#define RMS_LEADER_H     8
#define RMS_LEADER_L     9
#define RMS_TRAILER_H    10
#define RMS_TRAILER_L    11
#define RMS_DATA_0       12
#define RMS_DATA_1       13


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank22\i_scan.c"

unsigned char rms_form_0[] = { "Remocon Scan!" };
unsigned char rms_form_1[] = { 1,2,2,2,2,2,2,2,2,2,2,2,3,
                               4,0,0,0,0,0,0,0,0,0,0,0,4,
                               5,2,2,2,2,2,2,2,2,2,2,2,6 };
unsigned char rms_form_2[] = { 1,2,2,3, 4,0,0,4, 5,2,2,6 };
unsigned char rms_form_3[] = { 1,2,2,2,2,2,2,2,2,3,
                               4,0,0,0,0,0,0,0,0,4,
                               5,2,2,2,2,2,2,2,2,6 };
unsigned char rms_form_4[] = { 0 };
unsigned char rms_form_5[] = { "from:" };
unsigned char rms_form_6[] = { "to  :" };
unsigned char rms_form_7[] = { "--" };
unsigned char rms_form_8[] = { "step:  ,repeat:" };
unsigned char rms_form_9[] = { "LH LL TH TL D0 D1" };
unsigned char rms_cursor[] = { 0x11, 0x11 };
unsigned char rms_clear[]  = { 0x20, 0x20 };
unsigned char rms_attrb[]  = { 1, 1 };
unsigned char rms_form_type[][8] = { "Type-N ", "Type-S1", "Type-S2" };

UBYTE rms_type[] = {  3,  1,  3 };
UBYTE rms_pos[]  = {  7, 12,  7 };
UBYTE rms_loop[] = { 32, 11, 19 };


/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void rms_save_nvm( struct rms_t *rms )
{
  UBYTE  i;

  for( i=0; i<14; i++ ) {
    nvm->rem_code[i] = rms->input[i];
  }
}

void rms_load_nvm( struct rms_t *rms )
{
  UBYTE  i;

  for( i=0; i<14; i++ ) {
    rms->input[i] = nvm->rem_code[i];
  }
}

void rms_set_onkyo_mc( struct rms_t *rms )
{ /* type 1 */
  rms->input[RMS_FROM_CODE+0] = 0xD2;
  rms->input[RMS_FROM_CODE+1] = 0x6D;
  rms->input[RMS_FROM_CODE+2] = 0x00;
  rms->input[RMS_TO_CODE  +0] = 0xD2;
  rms->input[RMS_TO_CODE  +1] = 0x6D;
  rms->input[RMS_TO_CODE  +2] = 0xFF;
  rms->input[RMS_STEP]        = 1;
  rms->input[RMS_REPEAT]      = 5;
  rms->input[RMS_LEADER_H]    = 0x55;
  rms->input[RMS_LEADER_L]    = 0x2A;
  rms->input[RMS_TRAILER_H]   = 0x05;
  rms->input[RMS_TRAILER_L]   = 0xFF;
  rms->input[RMS_DATA_0]      = 0x05;
  rms->input[RMS_DATA_1]      = 0x11;
}

void rms_set_sony_tv( struct rms_t *rms )
{ /* type 2 */
  rms->input[RMS_FROM_CODE+0] = 0x90;
  rms->input[RMS_FROM_CODE+1] = 0;
  rms->input[RMS_FROM_CODE+2] = 0;
  rms->input[RMS_TO_CODE  +0] = 0xAF;
  rms->input[RMS_TO_CODE  +1] = 0;
  rms->input[RMS_TO_CODE  +2] = 0;
  rms->input[RMS_STEP]        = 1;
  rms->input[RMS_REPEAT]      = 5;
  rms->input[RMS_LEADER_H]    = 0x17;
  rms->input[RMS_LEADER_L]    = 0x06;
  rms->input[RMS_TRAILER_H]   = 0x06;
  rms->input[RMS_TRAILER_L]   = 0xEB;
  rms->input[RMS_DATA_0]      = 0x06;
  rms->input[RMS_DATA_1]      = 0x0C;
}

void rms_set_sony_skyper( struct rms_t *rms )
{ /* type 3 */
  rms->input[RMS_FROM_CODE+0] = 0x00;
  rms->input[RMS_FROM_CODE+1] = 0x1D;
  rms->input[RMS_FROM_CODE+2] = 0x07;
  rms->input[RMS_TO_CODE  +0] = 0xFF;
  rms->input[RMS_TO_CODE  +1] = rms->input[RMS_FROM_CODE+1];
  rms->input[RMS_TO_CODE  +2] = rms->input[RMS_FROM_CODE+2];
  rms->input[RMS_STEP]        = 1;
  rms->input[RMS_REPEAT]      = 5;
  rms->input[RMS_LEADER_H]    = 0x19;
  rms->input[RMS_LEADER_L]    = 0x06;
  rms->input[RMS_TRAILER_H]   = 0x07;
  rms->input[RMS_TRAILER_L]   = 0x7E;
  rms->input[RMS_DATA_0]      = 0x06;
  rms->input[RMS_DATA_1]      = 0x0D;
}

void rms_disp_value( UWORD d, UWORD e, UBYTE c, UBYTE x, UBYTE y )
{
	UWORD  m;
	UBYTE  i, n;
	unsigned char data[6];

	m = 1;
	if ( c > 1 ) {
		for( i=1; i<c; i++ ) {
			m = m * e;
		}
	}
	for( i=0; i<c; i++ ) {
		n = d / m; d = d % m; m = m / e;
		data[i] = '0' + n;	  /* '0' - '9' */
		if( data[i] > '9' )	 data[i] += 7;
	}
	set_bkg_tiles( x, y, c, 1, data );
}

void rms_disp_code( struct rms_t *rms )
{
  UBYTE  i;
	
  switch( rms->type ) {

    case RMS_TYPE_1:
      for( i=0; i<4; i++ ) {
        rms_disp_value( rms->code[i], 16, 2, i*3+4, 5 );
      }
      break;

    case RMS_TYPE_2:
      rms_disp_value( rms->code[0], 16, 2, 9, 5 );
      break;

    case RMS_TYPE_3:
      for( i=0; i<3; i++ ) {
        rms_disp_value( rms->code[i], 16, 2, i*3+6, 5 );
      }
      break;
  }
}

void rms_set_code( struct rms_t *rms )
{
  UBYTE  i;
	
  for( i=0; i<rms_type[rms->type]; i++ ) {
    rms->code[i] = rms->input[RMS_FROM_CODE+i];
  }
  switch( rms->type ) {

    case RMS_TYPE_1:
      rms->code[3] = ~rms->code[2];
      break;

    case RMS_TYPE_2:
      rms->code[1] = 0;
      break;
  }
  rms_disp_code( rms );
  rms->flag = RMS_OFF;

  rms_save_nvm( rms );
}

void rms_disp_data( struct rms_t *rms )
{
  UBYTE  i, x;

  for( i=0; i<rms_type[rms->type]; i++ ) {
    x = i*3 + rms_pos[rms->type];
    rms_disp_value( rms->input[RMS_FROM_CODE+i], 16, 2, x,  8 );
    rms_disp_value( rms->input[RMS_TO_CODE+i],   16, 2, x, 10 );
  }
  rms_disp_value( rms->input[RMS_STEP],   16, 2,  6, 12 );
  rms_disp_value( rms->input[RMS_REPEAT], 16, 2, 16, 12 );
  for( i=0; i<6; i++ ) {
    rms_disp_value( rms->input[RMS_LEADER_H+i], 16, 2, i*3+1, 15 );
  }
  rms_set_code( rms );
}

void rms_encode( struct rms_t *rms )
{
  UBYTE  i, n, m, bit, w_code;
	
  n = m = 0;
  ram[0][n] = rms->input[RMS_LEADER_H];
  ram[1][n] = rms->input[RMS_LEADER_L];
  n++;
  bit = 8;
  w_code = rms->code[0];

  for( i=0; i<rms_loop[rms->type]; i++ ) {
    ram[0][n] = rms->input[RMS_TRAILER_H];
    ram[1][n] = rms->input[RMS_TRAILER_H];
    switch( rms->type ) {

      case RMS_TYPE_1:
        if( w_code & 0x01 ) {
          ram[1][n] = rms->input[RMS_DATA_1];
        } else {
          ram[1][n] = rms->input[RMS_DATA_0];
        }
        break;

      case RMS_TYPE_2:
      case RMS_TYPE_3:
        if( w_code & 0x01 ) {
          ram[0][n] = rms->input[RMS_DATA_1];
        } else {
          ram[0][n] = rms->input[RMS_DATA_0];
        }
        break;
    }
    w_code = w_code >> 1;
    bit--;
    if( !bit ) {
      m++;
      w_code = rms->code[m];
      bit = 8;
    }
    n++;
  }
  ram[0][n] = rms->input[RMS_TRAILER_H];
  ram[1][n] = rms->input[RMS_TRAILER_L];
  n++;
  ram[0][n] = 0;
  ram[1][n] = 0;
  if( rms->type == RMS_TYPE_1 ) {
    ram[2][0] = rms->input[RMS_LEADER_H];
    ram[3][0] = rms->input[RMS_LEADER_L];
    ram[2][1] = rms->input[RMS_TRAILER_H];
    ram[3][1] = rms->input[RMS_TRAILER_L];
    ram[2][2] = 0;
    ram[3][2] = 0;
  }
}

UBYTE rms_inc_code( struct rms_t *rms )
{
  UBYTE  i;

  switch( rms->type ) {

    case RMS_TYPE_1:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[2]++;
        if( rms->code[2] == 0 ) {
          rms->code[1]++;
          if( rms->code[1] == 0 )  rms->code[0]++;
        }
      }
      rms->code[3] = ~rms->code[2];
      break;

    case RMS_TYPE_2:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[0]++;
      }
      break;

    case RMS_TYPE_3:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[0]++;
      }
      break;
  }
  rms_disp_code( rms );
  return( RMS_ON );
}

UBYTE rms_dec_code( struct rms_t *rms )
{
  UBYTE  i;

  switch( rms->type ) {

    case RMS_TYPE_1:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[2]--;
        if( rms->code[2] == 0xFF ) {
          rms->code[1]--;
          if( rms->code[1] == 0xFF )  rms->code[0]--;
        }
      }
      rms->code[3] = ~rms->code[2];
      break;

    case RMS_TYPE_2:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[0]--;
      }
      break;

    case RMS_TYPE_3:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[0]--;
      }
      break;
  }
  rms_disp_code( rms );
  return( RMS_ON );
}

UBYTE rms_auto_inc_code( struct rms_t *rms )
{
  UBYTE  i;

  switch( rms->type ) {

    case RMS_TYPE_1:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[2]++;
        if( rms->code[2] == 0 ) {
          rms->code[1]++;
          if( rms->code[1] == 0 )  rms->code[0]++;
        }
        if( (rms->code[0]==(rms->input[RMS_TO_CODE]))
         && (rms->code[1]==(rms->input[RMS_TO_CODE+1]))
         && (rms->code[2]==(rms->input[RMS_TO_CODE+2])) )  return( RMS_OFF );
      }
      rms->code[3] = ~rms->code[2];
      break;

    case RMS_TYPE_2:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[0]++;
        if( rms->code[0] == (rms->input[RMS_TO_CODE]) )  return( RMS_OFF );
      }
      break;

    case RMS_TYPE_3:
      for( i=0; i<(rms->input[RMS_STEP]); i++ ) {
        rms->code[0]++;
        if( rms->code[0] == (rms->input[RMS_TO_CODE]) )  return( RMS_OFF );
      }
      break;
  }
  rms_disp_code( rms );
  return( RMS_ON );
}

void rms_play_remocon( struct rms_t *rms )
{
  UBYTE  flag;

  flag = 0;
  pda_syscall( PDA_PLAY_REMOCON, &flag );
  if( rms->type == RMS_TYPE_1 )  flag = 1;

  while( keycode.code=joypad() ) {
    pda_syscall( PDA_PLAY_REMOCON, &flag );
    cursor.sp = 0;
    pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
    pda_syscall( PDA_SCAN_PAD, (void *)0 );
    if( keycode.brk & J_EXIT )  return;
  }
}

void rms_play_remocon2( struct rms_t *rms )
{
  UBYTE  i, flag;

  flag = 0;
  pda_syscall( PDA_PLAY_REMOCON, &flag );
  if( rms->type == RMS_TYPE_1 )  flag = 1;

  for( i=0; i<(rms->input[RMS_REPEAT]); i++ ) {
    pda_syscall( PDA_PLAY_REMOCON, &flag );
  }
}

/*--------------------------------------------------------------------------*
 |  move cursor                                                             |
 *--------------------------------------------------------------------------*/
void rms_move_cursor()
{
  keycode.code = joypad();
  if( keycode.flag ) {
    if( !(keycode.code&(J_UP|J_DOWN|J_LEFT|J_RIGHT)) ) {
      keycode.flag = 0;
    }
  } else {
    if( keycode.code & J_LEFT ) {
      if( cursor.x > 0 ) {
        cursor.x--;
      }
    } else if( keycode.code & J_RIGHT ) {
      if( cursor.x < (cursor.info.max_x-1) ) {
        cursor.x++;
      }
    } else {
      pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
      pda_syscall( PDA_SCAN_PAD, (void *)0 );
      return;
    }
    keycode.flag = 1;
    keycode.brk = 0;
  }
  pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
  pda_syscall( PDA_SCAN_PAD, (void *)0 );
}

UBYTE rms_function( struct rms_t *rms, UBYTE index )
{
  UBYTE  flag;

  if( keycode.code & J_DOWN ) {
    if( rms->input[index] == 0x00 )  rms->input[index] = 0xFF;
    else                             rms->input[index]--;
    while( keycode.code & J_DOWN ) {
      keycode.code = joypad();
      pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
      pda_syscall( PDA_SCAN_PAD, (void *)0 );
      if( keycode.brk & J_EXIT )  return( 0 );
    }
    rms->flag = RMS_ON;
    return( 1 );
  } else if( keycode.code & J_UP ) {
    if( rms->input[index] == 0xFF )  rms->input[index] = 0x00;
    else                             rms->input[index]++;
    while( keycode.code & J_UP ) {
      keycode.code = joypad();
      pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
      pda_syscall( PDA_SCAN_PAD, (void *)0 );
      if( keycode.brk & J_EXIT )  return( 0 );
    }
    rms->flag = RMS_ON;
    return( 1 );
  } else if( keycode.code & J_A ) {
    if( rms->flag == RMS_ON ) {
      rms_set_code( rms );
    } else {
      rms_inc_code( rms );
    }
    rms_encode( rms );
    rms_play_remocon( rms );
  } else if( keycode.code & J_B ) {
    if( rms->flag == RMS_ON ) {
      rms_set_code( rms );
    } else {
      rms_dec_code( rms );
    }
    rms_encode( rms );
    rms_play_remocon( rms );
  } else if( keycode.brk & J_START ) {
    keycode.brk &= ~J_START;
    rms_set_code( rms );
    flag = RMS_ON;
    while( flag ) {
      rms_encode( rms );
      rms_play_remocon2( rms );
      flag = rms_auto_inc_code( rms );
      keycode.code = joypad();
      cursor.sp = 0;
      pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
      pda_syscall( PDA_SCAN_PAD, (void *)0 );
      if( keycode.brk & J_EXIT )  return( 0 );
      if( keycode.code )  flag = RMS_OFF;
    }
    rms_disp_code( rms );
    rms_encode( rms );
    rms_play_remocon2( rms );
  } else if( keycode.brk & J_SHIFT_A ) {
    keycode.brk &= ~J_SHIFT_A;
    switch( rms->type ) {

      case RMS_TYPE_1:
        rms_set_onkyo_mc( rms );
        break;

      case RMS_TYPE_2:
        rms_set_sony_tv( rms );
        break;

      case RMS_TYPE_3:
        rms_set_sony_skyper( rms );
        break;
    }
    rms_disp_data( rms );
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  from                                                                    |
 *--------------------------------------------------------------------------*/
struct cursor_info rms1_from_cursor = { 5, 1,  42, 86, 24, 0 };
struct cursor_info rms2_from_cursor = { 3, 1, 106, 86,  0, 0 };
struct cursor_info *rms_from_cursor[] = { &rms1_from_cursor,
                                          &rms2_from_cursor,
                                          &rms1_from_cursor };
UBYTE rms_from( struct rms_t *rms, struct cursor_pos *pos )
{
  UBYTE  index;

  pda_syscall( PDA_SET_CURSOR_MAP, rms_from_cursor[rms->type] );
  pda_syscall( PDA_INIT_CURSOR, pos );
  pos->x = 1;

  while( cursor.x > 0 ) {
    rms_move_cursor();
    if( keycode.brk & J_EXIT )  return( 0 );
    if( cursor.x > (rms_from_cursor[rms->type]->max_x)-2 ) {
      return( RMS_MODE_TO );
    }
    index = cursor.x - 1;
    if( rms_function(rms,index) ) {
      rms_disp_value( rms->input[index], 16, 2,
                      cursor.x*3+rms_pos[rms->type]-3, 8 );
      if( (rms->type==RMS_TYPE_3) && (cursor.x!=0) ) {
        rms->input[index+3] = rms->input[index];
        rms_disp_value( rms->input[index], 16, 2,
                        cursor.x*3+rms_pos[rms->type]-3, 10 );
      }
    }
  }
  return( RMS_MODE_SELECT );
}

/*--------------------------------------------------------------------------*
 |  to                                                                      |
 *--------------------------------------------------------------------------*/
struct cursor_info rms1_to_cursor = { 5, 1,  42, 102, 24, 0 };
struct cursor_info rms2_to_cursor = { 3, 1, 106, 102,  0, 0 };
struct cursor_info rms3_to_cursor = { 3, 1,  66, 102,  0, 0 };
struct cursor_info *rms_to_cursor[] = { &rms1_to_cursor,
                                        &rms2_to_cursor,
                                        &rms3_to_cursor };
UBYTE rms_to( struct rms_t *rms, struct cursor_pos *pos )
{
  UBYTE  index;

  pda_syscall( PDA_SET_CURSOR_MAP, rms_to_cursor[rms->type] );
  pda_syscall( PDA_INIT_CURSOR, pos );
  pos->x = 1;

  while( cursor.x > 0 ) {
    rms_move_cursor();
    if( keycode.brk & J_EXIT )  return( 0 );
    if( cursor.x > (rms_to_cursor[rms->type]->max_x)-2 ) {
      return( RMS_MODE_STEP );
    }
    index = cursor.x + 2;
    if( rms_function(rms,index) ) {
      rms_disp_value( rms->input[index], 16, 2,
                      cursor.x*3+rms_pos[rms->type]-3, 10 );
    }
  }
  pos->x = (rms_from_cursor[rms->type]->max_x)-2;
  return( RMS_MODE_FROM );
}

/*--------------------------------------------------------------------------*
 |  step                                                                    |
 *--------------------------------------------------------------------------*/
struct cursor_info rms_step_cursor = { 4, 1, (UBYTE)-22, 118, 80, 0 };
UBYTE rms_step( struct rms_t *rms, struct cursor_pos *pos )
{
  UBYTE  index;

  pda_syscall( PDA_SET_CURSOR_MAP, &rms_step_cursor );
  pda_syscall( PDA_INIT_CURSOR, pos );
  pos->x = 1;

  while( cursor.x > 0 ) {
    rms_move_cursor();
    if( keycode.brk & J_EXIT )  return( 0 );
    if( cursor.x > 2 )  return( RMS_MODE_PARAM );
    index = cursor.x + 5;
    if( rms_function(rms,index) ) {
      rms_disp_value( rms->input[index], 16, 2, cursor.x*10-4, 12 );
    }
  }
  pos->x = (rms_to_cursor[rms->type]->max_x)-2;
  return( RMS_MODE_TO );
}

/*--------------------------------------------------------------------------*
 |  param                                                                   |
 *--------------------------------------------------------------------------*/
struct cursor_info rms_param_cursor = { 7, 1, (UBYTE)-6, 142, 24, 0 };
UBYTE rms_param( struct rms_t *rms, struct cursor_pos *pos )
{
  UBYTE  index;

  pda_syscall( PDA_SET_CURSOR_MAP, &rms_param_cursor );
  pda_syscall( PDA_INIT_CURSOR, pos );

  while( cursor.x > 0 ) {
    rms_move_cursor();
    if( keycode.brk & J_EXIT )  return( 0 );
    index = cursor.x + 7;
    if( rms_function(rms,index) ) {
      rms_disp_value( rms->input[index], 16, 2, cursor.x*3-2, 15 );
    }
  }
  pos->x = 2;
  return( RMS_MODE_STEP );
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void rms_init( struct rms_t *rms )
{
  UBYTE  i;

  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

  set_bkg_tiles(  3,  1, 13, 1, rms_form_0 );
  set_bkg_tiles(  6,  2,  7, 1, &rms_form_type[rms->type][0] );

  switch( rms->type ) {

    case RMS_TYPE_1:
      set_bkg_tiles(  3,  4, 13, 3, rms_form_1 );
      set_bkg_tiles(  1,  8,  5, 1, rms_form_5 );
      set_bkg_tiles(  1, 10,  5, 1, rms_form_6 );
      set_bkg_tiles( 16,  8,  2, 1, rms_form_7 );
      set_bkg_tiles( 16, 10,  2, 1, rms_form_7 );
      set_bkg_attribute( 13,  5, 2, 1, rms_attrb );
      break;

    case RMS_TYPE_2:
      set_bkg_tiles(  8,  4,  4, 3, rms_form_2 );
      set_bkg_tiles(  6,  8,  5, 1, rms_form_5 );
      set_bkg_tiles(  6, 10,  5, 1, rms_form_6 );
      break;

    case RMS_TYPE_3:
      set_bkg_tiles(  5,  4, 10, 3, rms_form_3 );
      set_bkg_tiles(  1,  8,  5, 1, rms_form_5 );
      set_bkg_tiles(  1, 10,  5, 1, rms_form_6 );
      set_bkg_attribute( 10, 10, 2, 1, rms_attrb );
      set_bkg_attribute( 13, 10, 2, 1, rms_attrb );
      break;
  }
  set_bkg_tiles(  1, 12, 15, 1, rms_form_8 );
  set_bkg_tiles(  1, 14, 17, 1, rms_form_9 );

  rms_disp_data( rms );
}

/*--------------------------------------------------------------------------*
 |  select mode                                                             |
 *--------------------------------------------------------------------------*/
struct cursor_info rms_mode_cursor = { 2, 1, 78, 38, 0, 0 };
UBYTE rms_mode_select( struct rms_t *rms )
{
  struct cursor_pos pos;

  pos.x = pos.y = 0;
  pda_syscall( PDA_SET_CURSOR_MAP, &rms_mode_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );

  while( cursor.x == 0 ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_EXIT )  return( 0 );
    if( keycode.brk & J_A ) {
      keycode.brk &= ~J_A;
      rms->type++;
      if( rms->type > RMS_TYPE_3 )  rms->type = RMS_TYPE_1;
      rms_init( rms );
      pda_syscall( PDA_INIT_CURSOR, &pos );
    }
  }
  return( RMS_MODE_FROM );
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void remscan( void *arg )
{
  struct rms_t rms;
  struct cursor_pos pos;
  UBYTE  mode;

  pda_syscall( PDA_DISABLE_INTERRUPT, (void *)0 );
  rms.type = RMS_TYPE_1;
  mode = RMS_MODE_FROM;
  pos.x = 1;  pos.y = 0;
  rms_load_nvm( &rms );
  rms_init( &rms );

  while( !(keycode.brk & J_EXIT) ) {
    switch( mode ) {

      case RMS_MODE_FROM:
        mode = rms_from( &rms, &pos );
        break;

      case RMS_MODE_TO:
        mode = rms_to( &rms, &pos );
        break;

      case RMS_MODE_STEP:
        mode = rms_step( &rms, &pos );
        break;

      case RMS_MODE_PARAM:
        mode = rms_param( &rms, &pos );
        break;

      case RMS_MODE_SELECT:
        mode = rms_mode_select( &rms );
        break;
    }
  }
}

/* EOF */