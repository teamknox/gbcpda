/****************************************************************************
 *  bank01.c                                                                *
 *                                                                          *
 *    DESKTOP / ABOUT                                                       *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "fixed\fixed.h"
#include "bank01\i_left.h"
#include "bank01\i_right.h"
#include "bank01\i_config.h"
#include "bank01\i_about.h"

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank01\i_left.c"
#include "bank01\i_right.c"
#include "bank01\i_config.c"
#include "bank01\i_about.c"

#include "bank01\c_about.c"

unsigned char icon_4x4[16] = {
    0x00, 0x02, 0x08, 0x0A,
    0x01, 0x03, 0x09, 0x0B,
    0x04, 0x06, 0x0C, 0x0E,
    0x05, 0x07, 0x0D, 0x0F
};

unsigned char desktop_attribute[] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,1,1,1,1,0,0,2,2,2,2,0,0,3,3,3,3,0,0,
    0,0,1,1,1,1,0,0,2,2,2,2,0,0,3,3,3,3,0,0,
    0,0,1,1,1,1,0,0,2,2,2,2,0,0,3,3,3,3,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,4,4,4,4,0,0,5,5,5,5,0,0,6,6,6,6,0,0,
    0,0,4,4,4,4,0,0,5,5,5,5,0,0,6,6,6,6,0,0,
    0,0,4,4,4,4,0,0,5,5,5,5,0,0,6,6,6,6,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,7,7,7,7,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,7,7,7,7,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,7,7,7,7,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

unsigned char desktop_character[] = {
    0x80,0x82,0x88,0x8A,0x00,0x00,0x90,0x92,0x98,0x9A,0x00,0x00,0xA0,0xA2,0xA8,0xAA,
    0x81,0x83,0x89,0x8B,0x00,0x00,0x91,0x93,0x99,0x9B,0x00,0x00,0xA1,0xA3,0xA9,0xAB,
    0x84,0x86,0x8C,0x8E,0x00,0x00,0x94,0x96,0x9C,0x9E,0x00,0x00,0xA4,0xA6,0xAC,0xAE,
    0x85,0x87,0x8D,0x8F,0x00,0x00,0x95,0x97,0x9D,0x9F,0x00,0x00,0xA5,0xA7,0xAD,0xAF,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0xB0,0xB2,0xB8,0xBA,0x00,0x00,0xC0,0xC2,0xC8,0xCA,0x00,0x00,0xD0,0xD2,0xD8,0xDA,
    0xB1,0xB3,0xB9,0xBB,0x00,0x00,0xC1,0xC3,0xC9,0xCB,0x00,0x00,0xD1,0xD3,0xD9,0xDB,
    0xB4,0xB6,0xBC,0xBE,0x00,0x00,0xC4,0xC6,0xCC,0xCE,0x00,0x00,0xD4,0xD6,0xDC,0xDE,
    0xB5,0xB7,0xBD,0xBF,0x00,0x00,0xC5,0xC7,0xCD,0xCF,0x00,0x00,0xD5,0xD7,0xDD,0xDF,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0xE0,0xE2,0xE8,0xEA,0x00,0x00,0xF0,0xF2,0xF8,0xFA,
    0x00,0x00,0x00,0x00,0x00,0x00,0xE1,0xE3,0xE9,0xEB,0x00,0x00,0xF1,0xF3,0xF9,0xFB,
    0x00,0x00,0x00,0x00,0x00,0x00,0xE4,0xE6,0xEC,0xEE,0x00,0x00,0xF4,0xF6,0xFC,0xFE,
    0x00,0x00,0x00,0x00,0x00,0x00,0xE5,0xE7,0xED,0xEF,0x00,0x00,0xF5,0xF7,0xFD,0xFF
};

struct cursor_info desktop_cursor = { 3, 3, 32, 64, 48, 40 };

/*==========================================================================*
 |  prototypes                                                              |
 *==========================================================================*/
extern void desktop_menu( void *arg );
extern void pda_about( void *arg );
extern void configuration( void *arg );

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/

/*--------------------------------------------------------------------------*
 |  show_desktop_page()                                                     |
 *--------------------------------------------------------------------------*/
void show_desktop_page()
{
    UBYTE i;
    struct palette palette;

    /* setup palette & attribute */
    palette.no = 0;
    palette.color0 = ~nvm->back_color;
    palette.color1 = i_aboutCGBPal0c1;
    palette.color2 = i_aboutCGBPal0c2;
    palette.color3 = CLR_BLACK;
    pda_syscall( PDA_SET_PALETTE, &palette );
    palette.no = 15;
    pda_syscall( PDA_SET_PALETTE, &palette );

    /* setup apps icon data */
    pda_syscall( PDA_SETUP_APPS_ICON, (void *)0 );
    if( desktop_page ) {
        set_sprite_data( 64, 16, i_left );
        palette.no = 14;
        palette.color1 = i_leftCGBPal0c1;
        palette.color2 = i_leftCGBPal0c2;
        pda_syscall( PDA_SET_PALETTE, &palette );
    } else {
        set_sprite_data( 64, 16, i_config );
        palette.no = 14;
        palette.color1 = i_configCGBPal0c1;
        palette.color2 = i_configCGBPal0c2;
        pda_syscall( PDA_SET_PALETTE, &palette );
    }
    if( !apps_num || (desktop_page==(apps_num-1)/7) ) {
        set_bkg_data( 0xF0, 16, i_about );
    } else {
        set_bkg_data( 0xF0, 16, i_right );
    }

    /* show desktop menu */
    set_bkg_attribute( 0, 0, 20, 18, desktop_attribute );
    set_bkg_tiles( 2, 3, 16, 14, desktop_character );
    for( i=0; i<16; i++ ) {
        if( i < 12 ) {
            set_sprite_prop( i+5, 6 );
        } else {
            set_sprite_prop( i+5, 7 );
        }
        set_sprite_tile( i+5, icon_4x4[i]+64 );
        move_sprite( i+5, (i%4)*8+24, (i/4)*8+120 );
    }
}

/*--------------------------------------------------------------------------*
 |  execute_apps()                                                          |
 *--------------------------------------------------------------------------*/
void execute_apps( UBYTE num )
{
    UBYTE no;

    if( num < apps_num ) {
        no = nvm->apps[num];
        pda_longjmp( dispatch[no].bank, dispatch[no].entry, (void *)0 );
    }
}

/*==========================================================================*
 |  desktop menu                                                            |
 *==========================================================================*/
void desktop_menu( void *arg )
{
    UBYTE num;
    struct cursor_pos desktop_pos;

    /* initialize cursor position */
    desktop_pos.x = 1;
    desktop_pos.y = 1;

    /* endless loop */
    while( 1 ) {

        /* show desktop page */
        set_bkg_tiles( 4, 1, 11, 1, (unsigned char *)PDA_TITLE );
        show_desktop_page();

        /* setup cursor */
        pda_syscall( PDA_SET_CURSOR_MAP, &desktop_cursor );
        pda_syscall( PDA_INIT_CURSOR, &desktop_pos );

        /* select apps icon */
        while( !(keycode.brk & J_A) ) {
            pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        }
        keycode.brk = 0;

        /* save cursor position */
        desktop_pos.x = cursor.x;
        desktop_pos.y = cursor.y;

        /* check cursor position */
        if( cursor.y == 2 ) {
            switch( cursor.x ) {
              case 0:
                if( desktop_page ) {
                    desktop_page--;
                    if( !desktop_page ) {
                        desktop_pos.x = 2;
                        desktop_pos.y = 2;
                    }
                } else {
                    pda_longjmp( 2, configuration, (void *)0 );
                }
                break;
              case 1:
                execute_apps( desktop_page*7+6 );
                break;
              case 2:
                if( desktop_page == (apps_num-1)/7 ) {
                    pda_longjmp( 1, pda_about, (void *)0 );
                } else {
                    desktop_page++;
                    if( desktop_page == (apps_num-1)/7 ) {
                        desktop_pos.x = 0;
                        desktop_pos.y = 2;
                    }
                }
                break;
            }
        } else {
            execute_apps( desktop_page*7+cursor.y*3+cursor.x );
        }
        pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
        pda_syscall( PDA_INIT_SCREEN, (void *)0 );
    }
}

/*==========================================================================*
 |  about                                                                   |
 *==========================================================================*/
unsigned char about_head[] = {
    0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03
};
unsigned char about_line[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04
};
unsigned char about_tail[] = {
    0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char msg_about0[] = { "by TeamKNOx" };
unsigned char msg_about1[] = { "Build" };
unsigned char about_break[] = { 0x10,0x11,0x11,0x12,
                                0x13,0x80,0x81,0x14,
                                0x15,0x16,0x16,0x17 };
unsigned char about_press[] = { 0x18,0x19,0x19,0x1A,
                                0x1B,0x82,0x83,0x1C,
                                0x1D,0x1E,0x1E,0x1F };
unsigned char about_button_a[] = { 2,2,2,2,2,2,2,2,2,2,2,2 };
struct cursor_info about_cursor = { 1, 1, 82, 144, 0, 0 };

void pda_about( void *arg )
{
    UBYTE i;
    unsigned char tmp[11];
    struct cursor_pos pos;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_data( 0x80, 4, c_about );
    set_bkg_tiles( 0, 0, 20, 1, about_head );
    for( i=0; i<16; i++ ) {
        set_bkg_tiles( 0, i+1, 20, 1, about_line );
    }
    set_bkg_tiles( 0, 17, 20, 1, about_tail );

    set_bkg_tiles(  4,  2, 11, 1, (unsigned char *)PDA_TITLE );
    set_bkg_tiles(  4,  4, 11, 1, msg_about0 );
    set_bkg_tiles(  7,  7,  5, 1, msg_about1 );
    sprintf( tmp, "%lu/  /  ", BUILD_YEAR );
    tmp[5] = '0'+(BUILD_MONTH/10);
    tmp[6] = '0'+(BUILD_MONTH%10);
    tmp[8] = '0'+(BUILD_DAY/10);
    tmp[9] = '0'+(BUILD_DAY%10);
    set_bkg_tiles(  5,  9, 10, 1, tmp );
    set_bkg_tiles(  8, 14, 4, 3, about_break );
    set_bkg_attribute( 8, 14, 4, 3, about_button_a );

    pos.x = pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &about_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( 8, 14, 4, 3, about_press );
            while( !(keycode.brk & J_EXIT) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( !(keycode.make & J_A) ) {
                    set_bkg_tiles( 8, 14, 4, 3, about_break );
                    return;
                }
            }
        }
    }
}

/* EOF */