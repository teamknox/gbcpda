/****************************************************************************
 *  bank09.c                                                                *
 *                                                                          *
 *    GB-DATE  V2.11    2000/08/21                                          *
 *                                                                          *
 *    Copyright(C)  1998, 1999, 2000   TeamKNOx                             *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank09\i_date.h"
#include "bank09\c_date.h"
#include "bank09\s_date.h"

extern void gb_date( void *arg );

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank09\i_date.c"
#include "bank09\c_date.c"
#include "bank09\s_date.c"

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_09[FUNC_NUM] = {
    { "GB-DATE ", "2.11", gb_date, i_date, i_dateCGBPal0c1, i_dateCGBPal0c2 }
};
struct description desc_09 = { FUNC_NUM, func_09 };

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define WEEKS           7
#define MONTHS          12
#define LEAP_MONTH      2
#define LEAP_DAY        29
#define LEAP_CYC        4
#define START_YEAR      1980
#define END_YEAR        2099
#define START_DAY       2     /* means Tue */
#define NORMAL_YEAR_LEN 365
#define LEAP_YEAR_LEN   366

/* mode definition of main */
#define MODE_DEC_AREA   0
#define MODE_INC_AREA   1
#define MODE_TODAY_AREA 2
#define MODE_MAIN_AREA  3
#define MODE_FLAG_AREA  4

/* mode definition of option */
#define MODE_OPT_RET    0
#define MODE_OPT_DAY    1
#define MODE_OPT_WEEK   2

/* mode definition of schedule */
#define MODE_SCH_CTRL   0
#define MODE_SCH_TIME   1

#define DB_DATE_TAG     0x7E


/*==========================================================================*
 |  data                                                                    |
 *==========================================================================*/
unsigned char date_head[] = {
    0x00,0x00,0x00,0x00,0x10,0x12,0x10,0x12,0x00,0x00,
    0x00,0x00,0x10,0x12,0x10,0x12,0x10,0x12,0x10,0x12,
    0x00,0x00,0x00,0x00,0x15,0x17,0x15,0x17,0x00,0x00,
    0x00,0x00,0x15,0x17,0x15,0x17,0x15,0x17,0x15,0x17,
    0x02,0x02,0x09,0x02,0x02,0x09,0x02,0x02,0x09,0x02,
    0x02,0x09,0x02,0x02,0x09,0x02,0x02,0x09,0x02,0x02
};
unsigned char date_line[] = {
    0x00,0x00,0x04,0x00,0x00,0x04,0x00,0x00,0x04,0x00,
    0x00,0x04,0x00,0x00,0x04,0x00,0x00,0x04,0x00,0x00,
    0x02,0x02,0x0B,0x02,0x02,0x0B,0x02,0x02,0x0B,0x02,
    0x02,0x0B,0x02,0x02,0x0B,0x02,0x02,0x0B,0x02,0x02
};
unsigned char date_tail[] = {
    0x00,0x00,0x04,0x00,0x00,0x04,0x00,0x00,0x04,0x00,
    0x00,0x04,0x00,0x00,0x04,0x00,0x00,0x04,0x00,0x00,
    0x02,0x02,0x0A,0x02,0x02,0x0A,0x02,0x02,0x0A,0x02,
    0x02,0x0A,0x02,0x02,0x0A,0x02,0x02,0x0A,0x02,0x02
};
unsigned char date_head_a[] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,2,2,2,2,0,0,0,0,2,2,2,2,2,2,2,2,
    0,0,0,0,2,2,2,2,0,0,0,0,2,2,2,2,2,2,2,2
};
unsigned char date_opt_head[] = {
    0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03
};
unsigned char date_opt_line[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04
};
unsigned char date_opt_tail[] = {
    0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06
};
unsigned char date_sch_head[] = {
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x30,0x30,0x30,0x30,0x2E,0x30,0x30,0x2E,0x30,0x30,
    0x04,0x00,0x10,0x12,0x10,0x12,0x10,0x12,0x10,0x12,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x04,0x00,0x15,0x17,0x15,0x17,0x15,0x17,0x15,0x17,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x0A,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02
};
unsigned char date_sch_line[] = {
    0x00,0x00,0x99,0x9A,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x9B,0x9C,0x9C,0x9C,0x9C,0x9C,0x9C,
    0x9C,0x9C,0x9C,0x9C,0x9C,0x9C,0x9C,0x9C,0x9C,0x00
};
unsigned char date_sch_tail[] = {
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02
};

unsigned char date_week[] = { 0x53, 0x90,  /* Sunday    */
                              0x4D, 0x91,  /* Monday    */
                              0x54, 0x92,  /* Tuesday   */
                              0x57, 0x93,  /* Wednesday */
                              0x54, 0x94,  /* Thursday  */
                              0x46, 0x95,  /* Friday    */
                              0x53, 0x96   /* Saturday  */
};
unsigned char date_space[]     = { "     " };
unsigned char date_normal_a[]  = { 0,0,0,0,0,0,0,0,0,0 };
unsigned char date_holiday_a[] = { 5,5,5,5,5,5,5,5,5,5 };
unsigned char date_satday_a[]  = { 6,6,6,6,6,6,6,6,6,6 };
unsigned char date_srash[]     = { "/" };

unsigned char date_msg_t[] = { "    OPTION    " };
unsigned char date_msg0[] = { "First Day of Week " };
unsigned char date_msg1[] = { "First Week of Year" };
unsigned char date_msg2[] = { "Starts on Jan.1" };
unsigned char date_msg3[] = { "1st 4-day week " };
unsigned char date_msg4[] = { "1st full week  " };
unsigned char date_rb_0[] = { 0x97 };  /* radio button off */
unsigned char date_rb_1[] = { 0x98 };  /* radio button on  */
unsigned char date_0_a[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char date_1_a[] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
unsigned char date_break[] = { 0x10,0x12,0x15,0x17 };
unsigned char date_press[] = { 0x18,0x1A,0x1D,0x1F };
unsigned char date_break_a[] = { 2,2,2,2 };

unsigned char date_msg_s[] = { "SCHEDULE" };
unsigned char date_msg_today[] = { "Today" };

unsigned char es_head0[] = { "DATA ENTRY" };
unsigned char es_head1[] = {
    0x00,0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x45,0x76,
    0x65,0x6E,0x74,0x02,0x02,0x02,0x02,0x02,0x03,0x00
};
unsigned char es_head2[] = {
    0x00,0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x00,
    0x00,0x05,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
    0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x06,0x00
};

struct palette date_palette[] = {
     5, c_dateCGBPal1c0, c_dateCGBPal1c1, c_dateCGBPal1c2, c_dateCGBPal1c3, 
     6, c_dateCGBPal2c0, c_dateCGBPal2c1, c_dateCGBPal2c2, c_dateCGBPal2c3, 
    13, c_dateCGBPal4c0, c_dateCGBPal4c1, c_dateCGBPal4c2, c_dateCGBPal4c3,
    14, c_dateCGBPal5c0, c_dateCGBPal5c1, c_dateCGBPal5c2, c_dateCGBPal5c3
};

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
/*--------------------------------------------------------------------------*
 |  subroutines                                                             |
 *--------------------------------------------------------------------------*/
void setup_today_box()
{
    UBYTE i;

    for( i=0; i<12; i++ ) {
        set_sprite_prop( 28+i, 6 );
        set_sprite_tile( 28+i, 72+i );
        move_sprite( 28+i, 0, 0 );
    }
}

void hide_today_box()
{
    UBYTE i;

    for( i=0; i<12; i++ ) {
        move_sprite( 28+i, 0, 0 );
    }
}

void show_today_box( UBYTE x, UBYTE y )
{
    move_sprite( 28, x,    y    );
    move_sprite( 29, x+8,  y    );
    move_sprite( 30, x+16, y    );
    move_sprite( 31, x+24, y    );
    move_sprite( 32, x,    y+8  );
    move_sprite( 33, x+8,  y+8  );
    move_sprite( 34, x+16, y+8  );
    move_sprite( 35, x+24, y+8  );
    move_sprite( 36, x,    y+16 );
    move_sprite( 37, x+8,  y+16 );
    move_sprite( 38, x+16, y+16 );
    move_sprite( 39, x+24, y+16 );
}

UBYTE first_day( UWORD year, UBYTE month )
{
    UBYTE m;
    UWORD y, total_days;
    struct date date;

    total_days = 0;
    for( y=START_YEAR; y<year; y++ ) {
        if( y % LEAP_CYC ) {
            total_days = (total_days+365) % WEEKS;
        } else if( (y%100) || !(y%400) ) {
            total_days = (total_days+366) % WEEKS;
        } else {
            total_days = (total_days+365) % WEEKS;
        }
    }

    date.year = year;
    for( m=0; m<(month-1); m++ ) {
        date.month = m+1;
        pda_syscall( PDA_GET_DAY_LENGTH, &date );
        total_days += date.day;
    }
    return( (total_days+START_DAY+7-((nvm->week_day)&0x07))%WEEKS );
}

void update_date( struct date *temp )
{
    char work[8];
    UWORD id;
    UBYTE len, today, flag;
    UBYTE i, j, x, y, pos_x, pos_y, sun_x, sat_x;
    UBYTE the_day, day_len;
    struct date date;
    struct national_flag n_flag;
    struct search_schedule tag;

    hide_today_box();

    /* year */
    sprintf( work, "%lu", temp->year );
    set_bkg_tiles( 8, 1, 4, 1, (unsigned char *)work );

    /* month */
    if( (temp->month) < 10 )  work[0] = ' ';
    else                      work[0] = '1';
    work[1] = '0' + (temp->month)%10;
    set_bkg_tiles( 9, 2, 2, 1, (unsigned char *)work );

    /* flag */
    n_flag.x  = 0;
    n_flag.y  = 1;
    n_flag.no = 3; /* bkg */
    pda_syscall( PDA_COUNTRY_FLAG, &n_flag );

    /* setup week */
    j = (nvm->week_day) & 0x07;
    for( i=0; i<7; i++ ) {
        set_bkg_tiles( i*3, 4, 2, 1, &date_week[j*2] );
        if( j == 0 ) {
            sun_x = i * 3;
            set_bkg_attribute( sun_x, 4, 2, 1, date_holiday_a );
        } else if( j == 6 ) {
            sat_x = i * 3;
            set_bkg_attribute( sat_x, 4, 2, 1, date_satday_a );
        } else {
            set_bkg_attribute( i*3, 4, 2, 1, date_normal_a );
        }
        j++; if( j > 6 )  j = 0;
    }

    /* calc */
    if( (temp->year==nvm->date.year) && (temp->month==nvm->date.month) ) {
        today = 1;
    } else {
        today = 0;
    }
    date.year  = temp->year;
    date.month = temp->month;
    pda_syscall( PDA_GET_DAY_LENGTH, &date );
    day_len = date.day;
    the_day = first_day( temp->year, temp->month );
    y = 0;	

    for( j=0; j<6; j++ ) {
        for( i=0; i<7; i++ ) {
            set_bkg_tiles( i*3, j*2+6, 2, 1, date_space );
        }
    }

    tag.table = &id;
    tag.date.year  = temp->year;
    tag.date.month = temp->month;
    tag.hour       = 0xFF;

    /* to make calender main part */
    for( i=0; i<day_len; i++ ) {

        /* calculation for the pos_y */
        if( !((i+the_day)%WEEKS) && (i!=0) ) {
            y++;
        }

        /* schedule flag */
        tag.max = 1;
        tag.date.day = i+1;
        pda_syscall( PDA_SEARCH_SCHEDULE_TAG, &tag );
        if( tag.max ) {
            flag = 0x70;
        } else {
            flag = 0x00;
        }

        if( (i+1)/10 == 0 )  work[0] = ' ';
        else                 work[0] = '0' + (i+1)/10 + flag;
        work[1] = '0' + (i+1)%10 + flag;
        pos_x = ((i+the_day)%WEEKS)*3;
        pos_y = y * 2 + 6;
        if( today && ((i+1)==nvm->date.day) ) {
            show_today_box( pos_x*8, pos_y*8+8 );
        }
        set_bkg_tiles( pos_x, pos_y, 2, 1, (unsigned char *)work );
        date.day = i+1;
        pda_syscall( PDA_CHECK_HOLIDAYS, &date );
        if( (date.day) || (pos_x==sun_x) ) {
            set_bkg_attribute( pos_x, pos_y, 2, 1, date_holiday_a );
        } else if( pos_x == sat_x ) {
            set_bkg_attribute( pos_x, pos_y, 2, 1, date_satday_a );
        } else {
            set_bkg_attribute( pos_x, pos_y, 2, 1, date_normal_a );
        }
    }
}

UBYTE calendar_week( struct date *temp )
{
    UWORD total_days;
    UBYTE m, cw, the_day;
    struct date date;

    total_days = (UWORD)temp->day;
    date.year = temp->year;
    for( m=0; m<((temp->month)-1); m++ ) {
        date.month = m+1;
        pda_syscall( PDA_GET_DAY_LENGTH, &date );
        total_days += date.day;
    }
    the_day = (7-first_day(temp->year,1)) % 7;

    switch( ((nvm->week_day)/16)&0x03 ) {
      case 0: /* starts on Jan.1 */
        cw = (UBYTE)((total_days+6-the_day) / WEEKS);
        if( the_day )  cw++;
        break;
      case 1: /* first 4 days in week */
        cw = (UBYTE)((total_days+6-the_day) / WEEKS);
        if( the_day > 3 )  cw++;
        break;
      case 2: /* first full(7 days) week */
        cw = (UBYTE)((total_days+6-the_day) / WEEKS);
        break;
    }
    if( cw == 0 )  cw = 0; /* or 52 or 53 ??? */
    return( cw );
}

void show_calendar_week( struct date *temp )
{
    UBYTE  cw;
    struct cursor_pos pos;

    if( (nvm->week_day) & 0x80 ) {  /* show cw */
        cw = calendar_week( temp );  /* calendar week */
        pos.x = cursor.x * cursor.info.add_x + cursor.info.off_x;
        pos.y = cursor.y * cursor.info.add_y + cursor.info.off_y + 17;
        if( cursor.y == 6 ) {
            pos.x += 16;
            pos.y -= 16;
        }
        set_sprite_tile( 26, (cw/10)+0x60 );
        set_sprite_tile( 27, (cw%10)+0x70 );
        move_sprite( 26, pos.x,   pos.y );
        move_sprite( 27, pos.x+8, pos.y );
    } else {
        move_sprite( 26, 0, 0 );
        move_sprite( 27, 0, 0 );
    }
}

void draw_date_frame()
{
    UBYTE i;

    set_bkg_tiles( 0,  1, 20, 3, date_head );
    for( i=0; i<6; i++ ) {
        set_bkg_tiles( 0, i*2+4, 20, 2, date_line );
    }
    set_bkg_tiles( 0, 16, 20, 2, date_tail );
    set_bkg_attribute( 0, 0, 20, 3, date_head_a );

    /* setup button */
    for( i=0; i<6; i++ ) {
        set_sprite_tile( i+5, i+64 );
        set_sprite_prop( i+5, 7 );
    }
    set_sprite_prop( 9, 6 );
    move_sprite(  5,  44, 28 );
    move_sprite(  6,  60, 28 );
    move_sprite(  7, 108, 28 );
    move_sprite(  8, 124, 28 );
    move_sprite(  9, 140, 28 );
    move_sprite( 10, 156, 28 );

    /* setup today's frame */
    setup_today_box();
}

/*--------------------------------------------------------------------------*
 |  day schedule                                                            |
 *--------------------------------------------------------------------------*/
void show_schedule_time( struct date *temp, UBYTE hour )
{
    UBYTE  i;
    UWORD  id;
    unsigned char work[18];
    struct search_schedule tag;
    struct phone_number name;

    if( hour > 10 )  hour = 10;
    for( i=0; i<7; i++ ) {
        if( i == 6 ) {
            set_bkg_tiles( 0, 4+i*2, 20, 1, date_sch_line );
        } else {
            set_bkg_tiles( 0, 4+i*2, 20, 2, date_sch_line );
        }
        work[0] = '0'+(hour+i*2)/10;
        work[1] = '0'+(hour+i*2)%10;
        set_bkg_tiles( 0, 4+i*2, 2, 1, work );

        /* get schedule tag */
        tag.table = &id;
        tag.max = 1;
        tag.date.year  = temp->year;
        tag.date.month = temp->month;
        tag.date.day   = temp->day;
        tag.hour       = hour + i*2;
        pda_syscall( PDA_SEARCH_SCHEDULE_TAG, &tag );
        if( tag.max ) {
            name.no  = id;
            name.str = work;
            pda_syscall( PDA_GET_PHONE_NAME, &name );
            set_bkg_tiles( 4, 4+i*2, 16, 1, &work[1] );
            set_bkg_attribute( 4, 4+i*2, 16, 1, date_1_a );
        }
    }
}

void show_schedule_date( struct date *temp, UBYTE hour )
{
    UBYTE  i;
    char   work[10];
    struct date date;

    /* year/month/day */
    date.year  = temp->year;
    date.month = temp->month;
    date.day   = temp->day;
    sprintf( work, "%lu", date.year );
    work[4] = '/';
    work[5] = '0' + (date.month)/10;
    work[6] = '0' + (date.month)%10;
    work[7] = '/';
    work[8] = '0' + (date.day)/10;
    work[9] = '0' + (date.day)%10;
    set_bkg_tiles( 0, 1, 10, 1, (unsigned char *)work );
    i = first_day( date.year, date.month );
    i = (i+(date.day)+((nvm->week_day)&0x07)+6) % WEEKS;
    set_bkg_tiles( 7, 2, 2, 1, &date_week[i*2] );
    pda_syscall( PDA_CHECK_HOLIDAYS, &date );
    if( (date.day) || (i==0) ) {
        set_bkg_attribute( 0, 1, 10, 1, date_holiday_a );
        set_bkg_attribute( 7, 2, 2, 1, date_holiday_a );
    } else if( i == 6 ) {
        set_bkg_attribute( 0, 1, 10, 1, date_satday_a );
        set_bkg_attribute( 7, 2, 2, 1, date_satday_a );
    } else {
        set_bkg_attribute( 0, 1, 10, 1, date_normal_a );
        set_bkg_attribute( 7, 2, 2, 1, date_normal_a );
    }
    if( (temp->year==nvm->date.year) && (temp->month==nvm->date.month)
        &&(temp->day==nvm->date.day) ) {
        set_bkg_tiles( 0, 2, 5, 1, date_msg_today );
        set_bkg_attribute( 0, 2, 5, 1, date_1_a );
    } else {
        set_bkg_tiles( 0, 2, 5, 1, date_space );
    }

    /* time table */
    show_schedule_time( temp, hour );
}

void show_schedule_page( struct date *temp )
{
    UBYTE i;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

    set_bkg_tiles( 0, 0, 20, 4, date_sch_head );
    set_bkg_tiles( 0, 17, 20, 1, date_sch_tail );
    set_bkg_attribute( 12, 1, 2, 2, date_break_a );
    set_bkg_attribute( 14, 1, 2, 2, date_break_a );
    set_bkg_attribute( 16, 1, 2, 2, date_break_a );
    set_bkg_attribute( 18, 1, 2, 2, date_break_a );
    set_sprite_prop( 5, 7 );
    set_sprite_prop( 6, 5 );
    set_sprite_prop( 7, 7 );
    set_sprite_prop( 8, 7 );
    set_sprite_tile( 5, 65 );
    set_sprite_tile( 6, 71 );
    set_sprite_tile( 7, 66 );
    set_sprite_tile( 8, 70 );
    move_sprite( 5, 108, 28 );
    move_sprite( 6, 124, 28 );
    move_sprite( 7, 140, 28 );
    move_sprite( 8, 156, 28 );
}

void edit_schedule( struct date *temp, UBYTE hour )
{
    UBYTE  i;
    UWORD  id;
    unsigned char work[16];
    struct db_file data;
    struct input_string input_string;
    struct phone_number name;
    struct search_schedule tag;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_tiles( 5,  1, 10, 1, es_head0 );
    set_bkg_tiles( 0,  3, 20, 1, es_head1 );
    set_bkg_tiles( 0,  4, 20, 2, es_head2 );
    set_bkg_attribute( 2, 4, 16, 1, date_1_a );

    /* get schedule tag */
    tag.table = &id;
    tag.max = 1;
    tag.date.year  = temp->year;
    tag.date.month = temp->month;
    tag.date.day   = temp->day;
    tag.hour       = hour;
    pda_syscall( PDA_SEARCH_SCHEDULE_TAG, &tag );
    if( tag.max ) {
        name.no  = id;
        name.str = data.name;
        pda_syscall( PDA_GET_PHONE_NAME, &name );
        set_bkg_tiles( 2, 4, 16, 1, &data.name[1] );
        set_bkg_attribute( 2, 4, 16, 1, date_1_a );
        pda_syscall( PDA_REMOVE_RAM_DATA, &id );
    } else {
        for( i=1; i<18; i++ ) {
            data.name[i]  = 0;
        }
    }
    data.name[0] = DB_DATE_TAG;

    /* time stamp */
#if 1
    sprintf( work, "%lu", temp->year );
    work[4] = '/';
    work[5] = '0' + (temp->month)/10;
    work[6] = '0' + (temp->month)%10;
    work[7] = '/';
    work[8] = '0' + (temp->day)/10;
    work[9] = '0' + (temp->day)%10;
    work[10] = ' ';
    work[11] = '0' + (hour)/10;
    work[12] = '0' + (hour)%10;
    work[13] = ':';
    work[14] = '0';
    work[15] = '0';
    set_bkg_tiles( 2, 6, 16, 1, (unsigned char *)work );
#endif

    /* get name field */
    input_string.x = 2;
    input_string.y = 4;
    input_string.max = 16;
    input_string.mode = 0;
    input_string.data = &data.name[1];
    input_string.def_x = 0;
    for( i=1; i<17; i++ ) {
        if( data.name[i] ) {
            input_string.def_x = i;
        }
    }
    pda_syscall( PDA_INPUT_STRING, &input_string );
    if( (data.name[1]==0)||(keycode.brk&J_EXIT) )  return;

    data.phone[0] = (temp->year)%256;
    data.phone[1] = (temp->year)/256;
    data.phone[2] = temp->month;
    data.phone[3] = temp->day;
    data.phone[4] = hour;

    /* set ram data base */
    pda_syscall( PDA_SET_RAM_DATA, &data );
}

struct cursor_info schedule_cursor2 = { 2, 9,  42, 34,  0, 16 };
void schedule_time( struct date *temp, UBYTE *hour )
{ 
    UWORD  id;
    struct cursor_pos pos;
    struct search_schedule tag;

    pos.x = 0;
    pos.y = 4;
    pda_syscall( PDA_SET_CURSOR_MAP, &schedule_cursor2 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    while( !(keycode.brk&J_EXIT) ) {
        pos.y = cursor.y;
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( cursor.y < 1 ) {
            pda_syscall( PDA_INIT_CURSOR, &pos );
            if( *hour > 0 ) {
                *hour -= 2;
                show_schedule_time( temp, *hour );
            }
        } else if( cursor.y > 7 ) {
            pda_syscall( PDA_INIT_CURSOR, &pos );
            if( *hour < 10 ) {
                *hour += 2;
                show_schedule_time( temp, *hour );
            }
        } else if( cursor.x > 0 ) {
            return;
        }
        if( keycode.make & J_A ) {
            while( keycode.make & J_A ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT ) return;
            }
            pos.x = 0;
            pos.y = cursor.y;
            edit_schedule( temp, (*hour)+(cursor.y-1)*2 );
            /* re-load resources */
            set_bkg_data( 0x90, 26, c_date );
            show_schedule_page( temp );
            show_schedule_date( temp, *hour );
            pda_syscall( PDA_SET_CURSOR_MAP, &schedule_cursor2 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
        } else if( keycode.make & J_B ) {
            while( keycode.make & J_B ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT ) return;
            }
            /* get schedule tag */
            tag.table = &id;
            tag.max   = 1;
            tag.date.year  = temp->year;
            tag.date.month = temp->month;
            tag.date.day   = temp->day;
            tag.hour       = (*hour)+(cursor.y-1)*2;
            pda_syscall( PDA_SEARCH_SCHEDULE_TAG, &tag );
            if( tag.max ) {
                pda_syscall( PDA_REMOVE_RAM_DATA, &id );
                show_schedule_time( temp, *hour );
            }
        }
    }
}

struct cursor_info schedule_cursor1 = { 4, 1, 106, 34, 16,  0 };
void schedule_main( struct date *temp )
{
    UBYTE  day, day_max, hour;
    struct cursor_pos pos;
    struct date date;

    date.year      = temp->year;
    date.month     = temp->month;
    day = date.day = temp->day;
    show_schedule_page( &date );
    pda_syscall( PDA_GET_DAY_LENGTH, &date );
    day_max = date.day;
    date.day = day;

    /* year/month/day, time table */
    hour = 8;
    show_schedule_date( temp, hour );
 
    pos.x = 3;
    pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &schedule_cursor1 );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( cursor.x*2+12, 1, 2, 2, date_press );
            while( keycode.make & J_A ) {
                if( keycode.brk & J_EXIT ) return;
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
            }
            switch( cursor.x ) {
              case 0: /* dec */
                if( day > 1 ) {
                    day--;
                } else if( date.month > 1 ) {
                    date.month--;
                    pda_syscall( PDA_GET_DAY_LENGTH, &date );
                    day = day_max = date.day;
                } else if( date.year > START_YEAR ) {
                    date.year--;
                    date.month = 12;
                    pda_syscall( PDA_GET_DAY_LENGTH, &date );
                    day = day_max = date.day;
                }
                date.day = day;
                show_schedule_date( &date, hour );
                break;
              case 1: /* edit */
                schedule_time( &date, &hour );
                set_sprite_data( 64, 1, s_date ); // broken by keyboard
                pos.x = 1;
                pos.y = 0;
                pda_syscall( PDA_SET_CURSOR_MAP, &schedule_cursor1 );
                pda_syscall( PDA_INIT_CURSOR, &pos );
                break;
              case 2: /* inc */
                if( day < day_max ) {
                    day++;
                } else if( date.month < 12 ) {
                    date.month++;
                    pda_syscall( PDA_GET_DAY_LENGTH, &date );
                    day_max = date.day;
                    day = 1;
                } else if( date.year < END_YEAR ) {
                    date.year++;
                    date.month = 1;
                    pda_syscall( PDA_GET_DAY_LENGTH, &date );
                    day_max = date.day;
                    day = 1;
                }
                date.day = day;
                show_schedule_date( &date, hour );
                break;
              case 3: /* return */
                return;
            }
            set_bkg_tiles( cursor.x*2+12, 1, 2, 2, date_break );
        }
    }
}

/*--------------------------------------------------------------------------*
 |  date options setting                                                    |
 *--------------------------------------------------------------------------*/
void date_opt_page()
{
    UBYTE i, x, y;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

    set_bkg_tiles( 3, 1, 14, 1, date_msg_t );
    set_bkg_tiles( 18, 1, 2, 2, date_break );
    set_bkg_attribute( 18, 1, 2, 2, date_break_a );
    set_sprite_tile( 5, 70 );
    set_sprite_prop( 5, 7 );
    move_sprite( 5, 156, 28 );

    /* first day of week */
    set_bkg_tiles( 0, 3, 20, 1, date_opt_head );
    set_bkg_tiles( 0, 4, 20, 1, date_opt_line );
    set_bkg_tiles( 0, 5, 20, 1, date_opt_line );
    set_bkg_tiles( 0, 6, 20, 1, date_opt_tail );
    set_bkg_tiles( 1, 3, 17, 1, date_msg0 );
    for( i=0; i<7; i++ ) {
        x = (i%4)*4 + 2;
        y = (i/4) + 4;
        set_bkg_tiles( x, y, 1, 1, date_rb_0 );
        set_bkg_tiles( x+1, y, 2, 1, &date_week[i*2] );
    }
    x = (((nvm->week_day)&0x07)%4)*4 + 2;
    y = (((nvm->week_day)&0x07)/4) + 4;
    set_bkg_tiles( x, y, 1, 1, date_rb_1 );
    set_bkg_attribute( x+1, y, 2, 1, date_1_a );

    /* first week of year */
    set_bkg_tiles( 0,  7, 20, 1, date_opt_head );
    set_bkg_tiles( 0,  8, 20, 1, date_opt_line );
    set_bkg_tiles( 0,  9, 20, 1, date_opt_line );
    set_bkg_tiles( 0, 10, 20, 1, date_opt_line );
    set_bkg_tiles( 0, 11, 20, 1, date_opt_tail );
    set_bkg_tiles( 1,  7, 18, 1, date_msg1 );
    set_bkg_tiles( 2,  8, 1,  1, date_rb_0 );
    set_bkg_tiles( 3,  8, 15, 1, date_msg2 );
    set_bkg_tiles( 2,  9, 1,  1, date_rb_0 );
    set_bkg_tiles( 3,  9, 15, 1, date_msg3 );
    set_bkg_tiles( 2, 10, 1,  1, date_rb_0 );
    set_bkg_tiles( 3, 10, 15, 1, date_msg4 );
    y = (((nvm->week_day)/16)&0x03) + 8;
    set_bkg_tiles( 2, y, 1, 1, date_rb_1 );
    set_bkg_attribute( 3, y, 15, 1, date_1_a );
}

struct cursor_info date_cursor6 = { 1, 2, 154, 34,  0, 0 };
struct cursor_info date_cursor7 = { 4, 4,  22, 44, 32, 8 };
struct cursor_info date_cursor8 = { 1, 4,  22, 76,  0, 8 };
void date_options()
{
    UBYTE x, y, mode;
    struct cursor_pos pos;

    date_opt_page();

    mode = MODE_OPT_RET;
    while( !(keycode.brk & J_EXIT) ) {
        switch( mode ) {
          case MODE_OPT_RET:
            pos.x = pos.y = 0;
            pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor6 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( cursor.y < 1 ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT ) return;
                if( keycode.make & J_A ) {
                    set_bkg_tiles( 18, 1, 2, 2, date_press );
                    while( keycode.make & J_A ) {
                        if( keycode.brk & J_EXIT ) return;
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    }
                    set_bkg_tiles( 18, 1, 2, 2, date_break );
                    return;
                }
            }
            mode = MODE_OPT_DAY;
            break;
          case MODE_OPT_DAY: /* first day of week */
            pos.x = ((nvm->week_day)&0x07)%4;
            pos.y = ((nvm->week_day)&0x07)/4 + 1;
            pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor7 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( (cursor.y>0) && (cursor.y<3) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT ) return;
                if( (cursor.x==3) && (cursor.y==2) ) {
                    if( pos.x == 3 ) {
                        cursor.y = 3;
                    } else {
                        cursor.x = pos.x;  cursor.y = pos.y;
                        pda_syscall( PDA_INIT_CURSOR, &pos );
                    }
                }
                if( keycode.make & J_A ) {
                    x = (((nvm->week_day)&0x07)%4)*4 + 2;
                    y = (((nvm->week_day)&0x07)/4) + 4;
                    set_bkg_tiles( x, y, 1, 1, date_rb_0 );
                    set_bkg_attribute( x+1, y, 2, 1, date_0_a );
                    nvm->week_day &= 0xF0;
                    nvm->week_day |= (((cursor.y-1)*4+cursor.x)&0x07);
                    x = cursor.x * 4 + 2;
                    y = cursor.y + 3;
                    set_bkg_tiles( x, y, 1, 1, date_rb_1 );
                    set_bkg_attribute( x+1, y, 2, 1, date_1_a );
                    while( keycode.make & J_A ) {
                        if( keycode.brk & J_EXIT ) return;
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    }
                }
                pos.x = cursor.x;  pos.y = cursor.y;
            }
            if( cursor.y==0 ) {
                mode = MODE_OPT_RET;
            } else {
                mode = MODE_OPT_WEEK;
            }
            break;
          case MODE_OPT_WEEK: /* first week of year */
            pos.x = 0;
            pos.y = (((nvm->week_day)/16)&0x03) + 1;
            pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor8 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            while( cursor.y > 0 ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT ) return;
                if( keycode.make & J_A ) {
                    y = (((nvm->week_day)/16)&0x03) + 8;
                    set_bkg_tiles( 2, y, 1, 1, date_rb_0 );
                    set_bkg_attribute( 3, y, 15, 1, date_0_a );
                    nvm->week_day &= 0xCF;
                    nvm->week_day |= (((cursor.y-1)&0x03)*16);
                    y = cursor.y + 7;
                    set_bkg_tiles( 2, y, 1, 1, date_rb_1 );
                    set_bkg_attribute( 3, y, 15, 1, date_1_a );
                    while( keycode.make & J_A ) {
                        if( keycode.brk & J_EXIT ) return;
                        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    }
                }
                pos.x = cursor.x;  pos.y = cursor.y;
            }
            if( cursor.y == 0 ) {
                mode = MODE_OPT_DAY;
            }
            break;
        }
    }
}

/*--------------------------------------------------------------------------*
 |  dec area                                                                |
 *--------------------------------------------------------------------------*/
struct cursor_info date_cursor0 = { 4, 2, 26, 34, 16,  0 };
UBYTE dec_date( struct date *temp )
{
    struct cursor_pos pos;

    pos.x = 2; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor0 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( cursor.x*2+2, 1, 2, 2, date_press );
            while( !(keycode.brk & J_EXIT) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( !(keycode.make & J_A) ) {
                    set_bkg_tiles( cursor.x*2+2, 1, 2, 2, date_break );
                    switch( cursor.x ) {
                      case 1: /* year */
                        if( temp->year > START_YEAR ) {
                            temp->year--;
                        }
                        break;
                      case 2: /* month */
                        if( temp->month > 1 ) {
                            temp->month--;
                        } else if( temp->year > START_YEAR ) {
                            temp->month = 12;
                            temp->year--;
                        }
                        break;
                    }
                    update_date( temp );
                    break;
                }
            }
        }
        if( cursor.x == 0 ) {
            return( MODE_FLAG_AREA );
        } else if( cursor.x == 3 ) {
            return( MODE_INC_AREA );
        } else if( cursor.y ) {
            return( MODE_MAIN_AREA );
        }
    }
    return( MODE_DEC_AREA );
}

/*--------------------------------------------------------------------------*
 |  inc area                                                                |
 *--------------------------------------------------------------------------*/
struct cursor_info date_cursor1 = { 4, 2, 90, 34, 16,  0 };
UBYTE inc_date( struct date *temp )
{
    struct cursor_pos pos;

    pos.x = 1; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor1 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( cursor.x*2+10, 1, 2, 2, date_press );
            while( !(keycode.brk & J_EXIT) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( !(keycode.make & J_A) ) {
                    set_bkg_tiles( cursor.x*2+10, 1, 2, 2, date_break );
                    switch( cursor.x ) {
                      case 1: /* month */
                        if( temp->month < 12 ) {
                            temp->month++;
                        } else if( temp->year < END_YEAR ) {
                            temp->month = 1;
                            temp->year++;
                        }
                        break;
                      case 2: /* year */
                        if( temp->year < END_YEAR ) {
                            temp->year++;
                        }
                        break;
                    }
                    update_date( temp );
                    break;
                }
            }
        }
        if( cursor.x == 0 ) {
            return( MODE_DEC_AREA );
        } else if( cursor.x == 3 ) {
            return( MODE_TODAY_AREA );
        } else if( cursor.y == 1 ) {
            return( MODE_MAIN_AREA );
        }
    }
    return( MODE_INC_AREA );
}

/*--------------------------------------------------------------------------*
 |  today area                                                              |
 *--------------------------------------------------------------------------*/
struct cursor_info date_cursor2 = { 3, 2, 122, 34, 16,  0 };
UBYTE today_date( struct date *temp )
{
    struct cursor_pos pos;

    pos.x = 1; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor2 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( cursor.x*2+14, 1, 2, 2, date_press );
            while( !(keycode.brk & J_EXIT) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( !(keycode.make & J_A) ) {
                    set_bkg_tiles( cursor.x*2+14, 1, 2, 2, date_break );
                    switch( cursor.x ) {
                      case 1: /* today */
                        temp->year  = nvm->date.year;
                        temp->month = nvm->date.month;
                        temp->day   = nvm->date.day;
                        update_date( temp );
                        return( MODE_MAIN_AREA );
                      case 2: /* options */
                        pos.x = cursor.x;  /* save */
                        pos.y = cursor.y;
                        date_options();
                        if( keycode.brk & J_EXIT )  return( MODE_TODAY_AREA );
                        pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
                        draw_date_frame();  /* redraw */
                        update_date( temp );
                        pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor2 );
                        pda_syscall( PDA_INIT_CURSOR, &pos );
                        break;
                    }
                    break;
                }
            }
        }
        if( cursor.x == 0 ) {
            return( MODE_INC_AREA );
        } else if( cursor.y == 1 ) {
            return( MODE_MAIN_AREA );
        }
    }
    return( MODE_TODAY_AREA );
}

/*--------------------------------------------------------------------------*
 |  main area                                                               |
 *--------------------------------------------------------------------------*/
struct cursor_info date_cursor3 = { 7, 7, 10, 52, 24, 16 };
UBYTE main_date( struct date *temp )
{
    UBYTE  start, length, day;
    struct cursor_pos pos;

    start = first_day( temp->year, temp->month );
    pda_syscall( PDA_GET_DAY_LENGTH, temp );
    length = temp->day;
    if( (temp->year==nvm->date.year) && (temp->month==nvm->date.month) ) {
        pos.x = (start + nvm->date.day-1)%7;
        pos.y = (start + nvm->date.day-1)/7+1;
        temp->day = nvm->date.day;
    } else {
        pos.x = start;
        pos.y = 1;
        temp->day = 1;
    }
    pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor3 );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    show_calendar_week( temp );  /* calendar week */

    while( !(keycode.brk & J_EXIT) ) {
        pos.x = cursor.x;  pos.y = cursor.y;  /* save pos */
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        day = cursor.x + (cursor.y-1)*7;
        temp->day = 1 + day - start;
        if( (start>day) || (day>=(start+length)) ) {  /* out of range */
            if( cursor.y < pos.y ) {
                move_sprite( 26, 0, 0 );
                move_sprite( 27, 0, 0 );
                return( MODE_TODAY_AREA );
            } else {
                cursor.x = pos.x;  cursor.y = pos.y;
                pda_syscall( PDA_INIT_CURSOR, &pos );  /* restore pos */
            }
        } else if( (pos.x!=cursor.x) || (pos.y!=cursor.y) ) {
            show_calendar_week( temp );  /* calendar week */
        }
        if( keycode.brk & J_SHIFT_A ) {
            keycode.brk &= ~J_SHIFT_A;
            nvm->date.year  = temp->year;
            nvm->date.month = temp->month;
            nvm->date.day   = (day - start)+1;
            pos.x = (cursor.x * 3)*8;
            pos.y = (cursor.y * 2 + 4)*8+8;
            show_today_box( pos.x, pos.y );
        } else if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            pos.x = cursor.x;  pos.y = cursor.y;  /* save pos */
            schedule_main( temp );  /* to do list */
            if( keycode.brk & J_EXIT )  return( MODE_MAIN_AREA );
            pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
            draw_date_frame();  /* redraw */
            update_date( temp );
            pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor3 );
            pda_syscall( PDA_INIT_CURSOR, &pos );
            show_calendar_week( temp );  /* calendar week */
        } else if( keycode.brk & J_B ) {
            keycode.brk &= ~J_B;
            if( (nvm->week_day) & 0x80 ) {  /* show cw */
                nvm->week_day &= ~0x80;  /* clear */
            } else {
                nvm->week_day |= 0x80;  /* set */
            }
            show_calendar_week( temp );  /* calendar week */
        }
    }
    return( MODE_MAIN_AREA );
}

/*--------------------------------------------------------------------------*
 |  national flags area                                                     |
 *--------------------------------------------------------------------------*/
struct cursor_info date_cursor4 = { 3, 2, 6, 36, 16,  0 };
UBYTE flag_date( struct date *temp )
{
    struct cursor_pos pos;

    pos.x = 1; pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &date_cursor4 );
    pda_syscall( PDA_INIT_CURSOR, &pos );

    /* loop */
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.make & J_A ) {
            while( !(keycode.brk & J_EXIT) ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( !(keycode.make & J_A) ) {
                    switch( cursor.x ) {
                      case 0: /* dec */
                        if( country_code ) {
                            country_code--;
                        } else {
                            country_code = 15;
                        }
                        break;
                      case 1: /* inc */
                        if( country_code < 15 ) {
                            country_code++;
                        } else {
                            country_code = 0;
                        }
                        break;
                    }
                    update_date( temp );
                    break;
                }
            }
        }
        if( cursor.x == 2 ) {
            return( MODE_DEC_AREA );
        } else if( cursor.y == 1 ) {
            return( MODE_MAIN_AREA );
        }
    }
    return( MODE_FLAG_AREA );
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_date( void *arg )
{
    UBYTE i, mode;
    struct palette palette;
    struct date temp;

    /* get RTC and update year, month, day */
    pda_syscall( PDA_GET_TIME, &time );
    pda_syscall( PDA_UPDATE_DATE, (void *)0 );
    pda_syscall( PDA_SET_TIME, &time );

    temp.year  = nvm->date.year;
    temp.month = nvm->date.month;
    temp.day   = nvm->date.day;
    country_code = nvm->country_code;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

    if( _cpu==CGB_TYPE ) {
        palette.color0 = ~nvm->back_color;
        for( i=0; i<4; i++ ) {
            palette.no = date_palette[i].no;
            palette.color1 = date_palette[i].color1;
            palette.color2 = date_palette[i].color2;
            palette.color3 = date_palette[i].color3;
            pda_syscall( PDA_SET_PALETTE, &palette );
        }
    }

    /* load resources */
    set_bkg_data( 0x90, 26, c_date );
    set_sprite_data( 64, 64, s_date );

    /* initialize the background */
    draw_date_frame();
    update_date( &temp );

    mode = MODE_TODAY_AREA;
    while( !(keycode.brk & J_EXIT) ) {
        switch( mode ) {
          case MODE_DEC_AREA:
            mode = dec_date( &temp );
            break;
          case MODE_INC_AREA:
            mode = inc_date( &temp );
            break;
          case MODE_TODAY_AREA:
            mode = today_date( &temp );
            break;
          case MODE_MAIN_AREA:
            mode = main_date( &temp );
            break;
          case MODE_FLAG_AREA:
            mode = flag_date( &temp );
            break;
        }
    }
}

/* EOF */