/****************************************************************************
 *  pda.h                                                                   *
 *                                                                          *
 *    GB-PDA SYSTEM DEFINITION                                              *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#define PDA_TITLE	"GB-PDA V4.5"
#define BUILD_ID	0x0008
#define BUILD_YEAR      2002
#define BUILD_MONTH     5
#define BUILD_DAY       19

#define MONO            1  /* 1: only CGB */

/*==========================================================================*
 |  structure                                                               |
 *==========================================================================*/
typedef struct cursor_info {
    UBYTE max_x;
    UBYTE max_y;
    UBYTE off_x;
    UBYTE off_y;
    UBYTE add_x;
    UBYTE add_y;
} _cursor_info;

typedef struct cursor_pos {
    UBYTE x;
    UBYTE y;
} _cursor_pos;

typedef struct cursor {
    UBYTE x;
    UBYTE y;
    UBYTE ani;
    UBYTE sp;
    struct cursor_info info;
} _cursor;

typedef struct keycode {
    UBYTE code;
    UBYTE flag;
    UBYTE make;
    UBYTE brk;
} _keycode;

typedef struct time {
    UBYTE flag;
    UBYTE sec;
    UBYTE min;
    UBYTE hor;
    UBYTE day_l;
    UBYTE day_h;
} _time;

typedef struct date {
    UWORD year;
    UBYTE month;
    UBYTE day;
} _date;

typedef struct clock {
    UBYTE map;
    UBYTE left_x;
    UBYTE left_y;
    UBYTE mid_x;
    UBYTE mid_y;
    UBYTE right_x;
    UBYTE right_y;
} _clock;

typedef struct palette {
    UBYTE no;
    UWORD color0;
    UWORD color1;
    UWORD color2;
    UWORD color3;
} _palette;

typedef struct button {
    UBYTE no;
    UBYTE on;
    UBYTE x;
    UBYTE y;
    unsigned char *data;
} button;

typedef struct db_file {
    unsigned char name[18];
    unsigned char phone[18];
} _db_file;

typedef struct search_phone {
    unsigned char tag;
    UWORD *table;
    UBYTE max;
} _search_phone;

typedef struct search_name {
    unsigned char *name;
    UWORD no;
} _search_name;

typedef struct phone_number {
    UBYTE mode;
    UWORD no;
    unsigned char *str;
} _phone_number;

typedef struct national_flag {
    UBYTE no;
    UBYTE x;
    UBYTE y;
} _national_flag;

typedef struct input_string {
    UBYTE x;
    UBYTE y;
    UBYTE max;
    UBYTE def_x;
    UBYTE mode;
    unsigned char *data;
} _input_string;

typedef struct password {
    UBYTE match;
    unsigned char *password;
} _password;

typedef struct summertime {
    UBYTE  start_month;  /* begin date */
    UBYTE  start_day;    /* begin date */
    UBYTE  end_month;    /* begin date */
    UBYTE  end_day;      /* begin date */
} _summertime;

typedef struct country_data {
    unsigned char name[14];
    unsigned char name3[3];
    unsigned char city[14];
    struct date   *holidays;
    BYTE          timezone;     /* 30min/div */
    struct summertime summertime;
    UWORD         location;     /* for GB-CLOCK */
    unsigned char phone[3];     /* international call number */
} _country_data;

typedef struct search_schedule {
    struct date date;
    UBYTE       hour;
    UWORD       *table;
    UBYTE       max;
} _search_schedule;

typedef struct read_data {
    UBYTE       flag; /* 0:non data, 1:read, 2:error */
    UBYTE       data;
} _read_data;

typedef struct memo_data {
    UBYTE         page;
    UBYTE         row;
    unsigned char *buff; /* 18 byte */
} _memo_data;

/*==========================================================================*
 |  pda api function                                                        |
 *==========================================================================*/
extern void pda_syscall( UBYTE func, void *arg );
#define PDA_NO_OPRATION         0
#define PDA_WAIT                1
#define PDA_INIT_NVM            2
#define PDA_CLEAR_SCREEN        3
#define PDA_FONT_TYPE           4
#define PDA_FRAME_TYPE          5
#define PDA_BUTTON_TYPE         6
#define PDA_CURSOR_TYPE         7
#define PDA_INIT_SCREEN         8
#define PDA_SET_CURSOR_MAP      9
#define PDA_INIT_CURSOR         10
#define PDA_SHOW_CURSOR         11
#define PDA_MOVE_CURSOR         12
#define PDA_SCAN_PAD            13
#define PDA_INIT_PALETTE        14
#define PDA_SET_TIME            15
#define PDA_GET_TIME            16
#define PDA_SET_PALETTE         17
#define PDA_GET_DAY_LENGTH      18
#define PDA_UPDATE_DATE         19
#define PDA_INIT_DIAL           20
#define PDA_DIALTONE            21
#define PDA_MAKE_3X3BUTTON      22
#define PDA_SEARCH_PHONE_TAG    23
#define PDA_SORT_PHONE_NAME     24
#define PDA_SHOW_PHONE_LINE     25
#define PDA_MAKE_DIAL_NUMBER    26
#define PDA_COUNTRY_FLAG        27
#define PDA_INPUT_STRING        28
#define PDA_PROBE_DATA_BASE     29
#define PDA_SET_RAM_DATA        30
#define PDA_REMOVE_RAM_DATA     31
#define PDA_PASSWORD            32
#define PDA_PROBE_ROM_BANKS     33
#define PDA_GET_PHONE_NAME      34
#define PDA_REVIVAL_ROM_DATA    35
#define PDA_GET_PHONE_NUMBER    36
#define PDA_REC_REMOCON         37
#define PDA_PLAY_REMOCON        38
#define PDA_INIT_COLOR          39
#define PDA_PUT_3X3BUTTON       40
#define PDA_COUNTRY_NAME        41
#define PDA_COUNTRY_NAME3       42
#define PDA_COUNTRY_CITY        43
#define PDA_COUNTRY_TIMEZONE    44
#define PDA_COUNTRY_PHONE       45
#define PDA_COUNTRY_SUMMERTIME  46
#define PDA_CHECK_HOLIDAYS      47
#define PDA_MAKE_2X2BUTTON      48
#define PDA_SEARCH_SCHEDULE_TAG 49
#define PDA_GET_KEYBOARD        50
#define PDA_GET_REAL_KEYBOARD   51
#define PDA_INIT_IRDA           52
#define PDA_SEND_IRDA           53
#define PDA_RECV_IRDA           54
#define PDA_GET_ROM_MEMO_DATA   55
#define PDA_DISABLE_INTERRUPT   56
#define PDA_ENABLE_INTERRUPT    57
#define PDA_PDC_ON_HOOK         58
#define PDA_PDC_DIALING         59
#define PDA_SEARCH_PHONE_NAME   60
#define PDA_SETUP_APPS_ICON     61

extern void pda_longjmp( UBYTE bank, void (*entry)(), void *arg );
extern void set_bkg_attribute( UBYTE x, UBYTE y, UBYTE sx, UBYTE sy, unsigned char *p );

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/
#define MAX_BANKS	32
#define MAX_APPS	28
#define	J_SHIFT_A	0x08U
#define	J_SHIFT_B	0x04U
#define	J_EXIT		0x01U

#define CLR_RGB(r,g,b)  ((((UWORD)b)<<10)|(((UWORD)g)<<5)|((UWORD)r))
#define CLR_WHITE	32767
#define CLR_L_GLAY	25368
#define CLR_D_GLAY	15855
#define CLR_BLACK	0

/*==========================================================================*
 |  backup ram data (bank00)                                                |
 *==========================================================================*/
typedef struct backup_ram0 {
    UWORD build_id;
    UBYTE font_type;
    UBYTE frame_type;
    UBYTE button_type;
    UBYTE cursor_type;
    UWORD fore_color;
    UWORD back_color;
    struct clock clock;
    struct date date;
    UBYTE week_day;
    unsigned char password[8];
    UBYTE apps[MAX_APPS];
    UWORD dscan_hi_score;
    UWORD gunpey_hi_score;
    UWORD draw_num;
    UBYTE tag_mode;
    UBYTE tag_page;
    UBYTE country_code;
    UBYTE rem_code[14];
    struct db_file data_base[1];
} _backup_ram0;
extern UBYTE nvm0_all[8][32][32];

/*==========================================================================*
 |  system work area (non backup)                                           |
 *==========================================================================*/
extern UBYTE rom_bank;
extern UBYTE desktop_page;
extern UBYTE apps_num;
extern struct cursor cursor;
extern struct keycode keycode;
extern struct time time;
extern UBYTE max_rom_file, max_ram_file;
extern UBYTE max_rom_memo_page;
extern UBYTE caps_lock;
extern struct cursor_pos keypad_pos;
extern UBYTE country_code;
extern UBYTE *nvm_phone_flag;
extern UBYTE *nvm_memo_flag;
extern UBYTE *nvm_memo_data;
extern UBYTE *nvm_draw_data;
extern struct backup_ram0 *nvm;

/*--------------------------------------------------------------------------*
 |  bank information                                                        |
 *--------------------------------------------------------------------------*/
typedef struct function {
    unsigned char name[8];
    unsigned char version[4];
    void (*entry)();
    unsigned char *icon;
    UWORD color1;
    UWORD color2;
} _function;

typedef struct description {
    UBYTE func_num;
    struct function *func_pos;
} _description;


/* EOF */