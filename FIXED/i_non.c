/*

 I_NON.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 32 x 32
  Tiles                : 0 to 0

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v1.6

*/

/* Start of tile array. */
unsigned char i_non[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,
  0x02,0x02,0x04,0x04,0x08,0x08,0x10,0x10,
  0x10,0x10,0x20,0x20,0x20,0x20,0x40,0x40,
  0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
  0x00,0x00,0x1F,0x1F,0x60,0x60,0x80,0x80,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
  0x40,0x40,0x20,0x20,0x20,0x20,0x10,0x10,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0x00,0x00,0xF8,0xF8,0x06,0x06,0x01,0x01,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0x40,0x40,0x20,0x20,0x10,0x10,0x08,0x08,
  0x08,0x08,0x04,0x04,0x04,0x04,0x02,0x02,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,0x00,
  0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
  0x02,0x02,0x04,0x04,0x04,0x04,0x08,0x08,
  0x08,0x08,0x10,0x10,0x20,0x20,0x40,0x40,
  0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00
};

/* End of I_NON.C */
