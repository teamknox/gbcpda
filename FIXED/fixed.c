/****************************************************************************
 *  fixed.c                                                                 *
 *                                                                          *
 *    Main Module of GB-PDA (GameBoy Personal Digital Assistant)            *
 *                                                                          *
 *      Version 4.5    2002/05/19                                           *
 *                                                                          *
 *    Copyright(C)  1998 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "fixed\fixed.h"

extern void desktop_menu( void *arg );

#define DOUBLE_SPEED    0  /* 0:non  1:x2 speed */


/*==========================================================================*
 |  system work area (non backup)                                           |
 *==========================================================================*/
UBYTE rom_bank;
UBYTE desktop_page;
UBYTE apps_num;
UBYTE vbl_cnt;
struct time time;
struct cursor cursor;
struct keycode keycode;
struct dispatch dispatch[MAX_APPS];
UBYTE max_rom_file, max_ram_file;
UBYTE max_rom_memo_page;
UBYTE caps_lock;
struct cursor_pos keypad_pos;
UBYTE country_code;
UBYTE *nvm_phone_flag;
UBYTE *nvm_memo_flag;
UBYTE *nvm_memo_data;
UBYTE *nvm_draw_data;
struct backup_ram0 *nvm;
UBYTE pkey_flag;
UBYTE pkey_code;
UBYTE irda_speed;
UBYTE irda_parity;


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
struct description *description[24] = {
    &desc_08, &desc_09, &desc_10, &desc_11,
    &desc_12, &desc_13, &desc_14, &desc_15,
    &desc_16, &desc_17, &desc_18, &desc_19,
    &desc_20, &desc_21, &desc_22, &desc_23,
    &desc_24, &desc_25, &desc_26, &desc_27,
    &desc_28, &desc_29, &desc_30, &desc_31
};
#include "fixed\i_non.h"
#include "fixed\i_non.c"

unsigned char msg_nonCGB1[] = { "GB-PDA is designed" };
unsigned char msg_nonCGB2[] = { "  for only GBC !! " };
unsigned char msg_clearnvm1[] = { "Clear backup RAM" };
unsigned char msg_clearnvm2[] = { "  Reboot again  " };

/*==========================================================================*
 |  APIs table                                                              |
 *==========================================================================*/
struct pda_api api_tbl[] = {
    { 3, pda_nop },
    { 3, pda_wait },
    { 3, pda_init_nvm },
    { 3, pda_clear_screen },
    { 4, pda_font_type },
    { 4, pda_frame_type },
    { 4, pda_button_type },
    { 4, pda_cursor_type },
    { 4, pda_init_screen },
    { 0, pda_set_cursor_map },
    { 0, pda_init_cursor },
    { 3, pda_show_cursor },
    { 3, pda_move_cursor },
    { 3, pda_scan_pad },
    { 4, pda_init_palette },
    { 3, pda_set_time },
    { 3, pda_get_time },
    { 3, pda_set_palette },
    { 3, pda_get_day_length },
    { 3, pda_update_date },
    { 3, pda_init_dial },
    { 3, pda_dialtone },
    { 0, pda_make_3x3button },
    { 7, pda_search_phone_tag },
    { 7, pda_sort_phone_name },
    { 7, pda_show_phone_line },
    { 7, pda_make_dial_number },
    { 6, pda_country_flag },
    { 5, pda_input_string },
    { 7, pda_probe_data_base },
    { 7, pda_set_ram_data },
    { 7, pda_remove_ram_data },
    { 5, pda_password },
    { 0, pda_probe_rom_banks },
    { 7, pda_get_phone_name },
    { 7, pda_revival_rom_data },
    { 7, pda_get_phone_number },
    { 3, pda_rec_remocon },
    { 3, pda_play_remocon },
    { 4, pda_init_color },
    { 4, pda_put_3x3button },
    { 6, pda_country_name },
    { 6, pda_country_name3 },
    { 6, pda_country_city },
    { 6, pda_country_timezone },
    { 6, pda_country_phone },
    { 6, pda_country_summertime },
    { 6, pda_check_holidays },
    { 0, pda_make_2x2button },
    { 7, pda_search_schedule_tag },
    { 3, pda_get_keyboard },
    { 3, pda_get_real_keyboard },
    { 3, pda_init_irda },
    { 3, pda_send_irda },
    { 3, pda_recv_irda },
    { 7, pda_get_rom_memo_data },
    { 3, pda_disable_interrupt },
    { 3, pda_enable_interrupt },
    { 3, pda_pdc_on_hook },
    { 3, pda_pdc_dialing },
    { 7, pda_search_phone_name },
    { 0, pda_setup_apps_icon }
};

/*==========================================================================*
 |  PDA system call APIs                                                    |
 *==========================================================================*/
void pda_syscall( UBYTE func, void *arg )
{
    pda_longjmp( api_tbl[func].bank, api_tbl[func].func, arg );
}

/*--------------------------------------------------------------------------*
 |  pda_longjmp()                                                           |
 *--------------------------------------------------------------------------*/
void pda_longjmp( UBYTE bank, void (*entry)(), void *arg )
{
    UBYTE save_rom_bank;

    /* save current bank number */
    save_rom_bank = rom_bank;

    /* change new bank number */
    if( bank ) {
        rom_bank = bank;
        SWITCH_ROM_MBC1( rom_bank );
    }

    /* call functions */
    (entry)( arg );

    /* restore bank number */
    rom_bank = save_rom_bank;
    SWITCH_ROM_MBC1( rom_bank );
}

/*--------------------------------------------------------------------------*
 |  pda_probe_rom_banks()                                                   |
 *--------------------------------------------------------------------------*/
void pda_probe_rom_banks( UBYTE *arg )
{
    UBYTE i, j, k, num;
    struct function *func;

    num = 0;
    for( i=8; i<MAX_BANKS; i++ ) {
        SWITCH_ROM_MBC1( i );
        func = (description[i-8]->func_pos);
        for( j=0; j<(description[i-8]->func_num); j++ ) {
            dispatch[num].bank   = i;
            dispatch[num].entry  = func->entry;
            dispatch[num].icon   = func->icon;
            dispatch[num].color1 = func->color1;
            dispatch[num].color2 = func->color2;
            for( k=0; k<8; k++ ) {
                dispatch[num].name[k] = func->name[k];
            }
            for( k=0; k<4; k++ ) {
                dispatch[num].version[k] = func->version[k];
            }
            func++; num++;
        }
    }
    *arg = num;
}

/*--------------------------------------------------------------------------*
 |  pda_set_apps_icon()                                                     |
 *--------------------------------------------------------------------------*/
void pda_setup_apps_icon( void *arg )
{
    UBYTE i, j, num;
    struct palette palette;

    palette.color0 = ~nvm->back_color;
    palette.color3 = CLR_BLACK;
    num = desktop_page * 7;
    for( i=0; i<7; i++ ) {
        if( (num>=apps_num) || (dispatch[nvm->apps[num]].bank<8) ) {
            set_bkg_data( i*16+0x80, 16, i_non );
            palette.no = i+1;
            palette.color1 = i_nonCGBPal0c1;
            palette.color2 = i_nonCGBPal0c2;
            pda_syscall( PDA_SET_PALETTE, &palette );
        } else {
            SWITCH_ROM_MBC1( dispatch[nvm->apps[num]].bank );
            set_bkg_data( i*16+0x80, 16, dispatch[nvm->apps[num]].icon );
            palette.no = i+1;
            palette.color1 = dispatch[nvm->apps[num]].color1;
            palette.color2 = dispatch[nvm->apps[num]].color2;
            pda_syscall( PDA_SET_PALETTE, &palette );
            num++;
        }
    }
}

/*--------------------------------------------------------------------------*
 |  pda_set_cursor_map()                                                    |
 *--------------------------------------------------------------------------*/
void pda_set_cursor_map( struct cursor_info *arg )
{
    cursor.info.max_x = arg->max_x;
    cursor.info.max_y = arg->max_y;
    cursor.info.off_x = arg->off_x;
    cursor.info.off_y = arg->off_y;
    cursor.info.add_x = arg->add_x;
    cursor.info.add_y = arg->add_y;
}

/*--------------------------------------------------------------------------*
 |  pda_init_cursor()                                                       |
 *--------------------------------------------------------------------------*/
void pda_init_cursor( struct cursor_pos *arg )
{
    cursor.x     = arg->x;
    cursor.y     = arg->y;
    cursor.ani   = 7;
    cursor.sp    = 0;
    keycode.code = 0;
    keycode.flag = 1;
    keycode.make = 0;
    keycode.brk  = 0;
}

/*--------------------------------------------------------------------------*
 |  pda_make_3x3button()                                                    |
 *--------------------------------------------------------------------------*/
void pda_make_3x3button( struct button *arg )
{
    unsigned char data[64];
    UBYTE i;

    for( i=0; i<64; i++ ) {
        data[i] = *(arg->data);
        arg->data++;
    }
    arg->data = data;
    pda_longjmp( 4, pdax_make_3x3button, arg );
}

/*--------------------------------------------------------------------------*
 |  pda_make_2x2button()                                                    |
 *--------------------------------------------------------------------------*/
void pda_make_2x2button( struct button *arg )
{
    unsigned char data[16];
    UBYTE i;

    for( i=0; i<16; i++ ) {
        data[i] = *(arg->data);
        arg->data++;
    }
    arg->data = data;
    pda_longjmp( 4, pdax_make_2x2button, arg );
}

/*==========================================================================*
 |  additional functions for GBDK                                           |
 *==========================================================================*/
void set_bkg_attribute( UBYTE x, UBYTE y, UBYTE sx, UBYTE sy, unsigned char *p )
{
    if( _cpu == CGB_TYPE ) {
        VBK_REG = 1;
        set_bkg_tiles( x, y, sx, sy, p );
        VBK_REG = 0;
    }
}

/*==========================================================================*
 |  IRQ Handlers                                                            |
 *==========================================================================*/
void dummy_hdr()
{
}

void vbl()
{
#if 0  /* before ver4.3 */
    vbl_cnt++;
    if( vbl_cnt > 60 ) {  /* 60 Hz */
        vbl_cnt -= 60;
        time.sec++;
        if( time.sec > 59 ) {
            time.sec -= 60;
            time.min++;
            if( time.min > 59 ) {
                time.min -= 60;
                time.hor++;
                if( time.hor > 23 ) {
                    time.hor -= 24;
                    time.day_l++;
                }
            }
        }
    }
#endif
}

void tim()
{
    vbl_cnt++;
    if( vbl_cnt > 32 ) {  /* 32 Hz */
        vbl_cnt -= 32;
        time.sec++;
        if( time.sec > 59 ) {
            time.sec -= 60;
            time.min++;
            if( time.min > 59 ) {
                time.min -= 60;
                time.hor++;
                if( time.hor > 23 ) {
                    time.hor -= 24;
                    time.day_l++;
                }
            }
        }
    }
}

void sio()
{
#if 0
    unsigned char pkey;

    receive_byte();
    pkey = _io_in;

    switch( pkey ) {
      case 0xF0:
        pkey_flag |= PKF_BREAK;
        break;
      case 0xE0:
        pkey_flag |= PKF_EXTEND;
        break;
      default:
        if( pkey_flag & PKF_BREAK ) {
        } else if( pkey_flag & PKF_EXTEND ) {
            switch( pkey ) {
              case 0x75:
                keycode.code |= J_UP;
                break;
              case 0x72:
                keycode.code |= J_DOWN;
                break;
              case 0x68:
                keycode.code |= J_LEFT;
                break;
              case 0x74:
                keycode.code |= J_RIGHT;
                break;
            }
        } else {
            switch( pkey ) {
              case 0x22:
                keycode.code |= J_A;
                break;
              case 0x1A:
                keycode.code |= J_B;
                break;
              case 0x1B:
                keycode.code |= J_START;
                break;
              case 0x1C:
                keycode.code |= J_SELECT;
                break;
              default:
//              key_buff[lkey_w] = key_table[pkey];
                break;
            }
        }
        pkey_flag &= ~(PKF_BREAK | PKF_EXTEND);        
    }
#endif
}


/*==========================================================================*
 |  initialize GB-PDA system environment                                    |
 *==========================================================================*/
void init_system_env()
{
    /* disable interrupt */
    disable_interrupts();

    /* get RTC and update year, month, day */
    time.flag = 1;
    pda_syscall( PDA_GET_TIME, &time );
    pda_syscall( PDA_UPDATE_DATE, (void *)0 );
    pda_syscall( PDA_SET_TIME, &time );
    if( (time.sec==0) && (time.min==0) && (time.hor==0) ) {
        time.flag = 0;
    }

    /* add interrupt handler */
#if 0
    vbl_cnt = 0;
//  add_SIO( sio );  /* for keyboard */
    add_TIM( tim );  /* for clock    */

    /* Handle TIM interrupt (32 counts/sec) */
    TMA_REG = 0x00U;  /* Set TMA to divide clock by 0x100 */
    TAC_REG = 0x04U;  /* Set clock to 4096 Hertz */

    set_interrupts( VBL_IFLAG | TIM_IFLAG );
    enable_interrupts();
#else
    pda_syscall( PDA_ENABLE_INTERRUPT, (void *)0 );
#endif

    /* restore backup data */
    pda_syscall( PDA_INIT_SCREEN, (void *)0 );

    /* probe phone data base */
    pda_syscall( PDA_PROBE_DATA_BASE, (void *)0 );
}

/*==========================================================================*
 |  display copyright messages                                              |
 *==========================================================================*/
void copyright()
{
    struct password pw;
    unsigned char tmp[8];

    /* clear screen */
    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    set_bkg_tiles( 4, 1, 11, 1, (unsigned char *)PDA_TITLE );
    DISPLAY_ON;

    /* check cpu mode */
    if( _cpu != CGB_TYPE ) {
        set_bkg_tiles( 1, 7, 18, 1, msg_nonCGB1 );
        set_bkg_tiles( 1, 9, 18, 1, msg_nonCGB2 );
        while( 1 ); /* endless loop */
#if DOUBLE_SPEED
    } else if( !(KEY1_REG & 0x80) ) {
        disable_interrupts();
        KEY1_REG = 0x01;  /* double speed */
        enable_interrupts();
#endif /* DOUBLE SPEED */
    }

    /* check password */
    if( nvm->password[0] ) {
        pw.match = 0; /* false */
        pw.password = tmp;
        while( !(pw.match) ) {
            pw.match = 1; /* mask mode */
            pda_syscall( PDA_PASSWORD, &pw );
        }
        pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
    }
}

void set_nvm_pointer( UBYTE *p )
{
    /* setup backup ram pointer */
    p += (UWORD)2;    /* dummy */
    nvm_phone_flag = p;
    p += (UWORD)64;   /* 512bit */
    nvm_memo_flag  = p;
    p += (UWORD)4;    /* 16+16bit(page) */
    nvm_memo_data  = p;
    p += (UWORD)1800; /* 180x10page */
    nvm_draw_data  = p;
    p += (UWORD)768;  /* 256x3 */ 
    nvm = (struct backup_ram0 *)p;
}

/*==========================================================================*
 |  entry point of GB-PDA                                                   |
 *==========================================================================*/
void main()
{
    /* initialize bank number */
    ENABLE_RAM_MBC1;
    SWITCH_RAM_MBC1( 0 );
    rom_bank = 0;

    /* setup backup ram pointer */
    set_nvm_pointer( &nvm0_all[0][0][0] );

    /* check backup ram */
    if( (joypad()&J_START) || ((nvm->build_id)!=BUILD_ID) ) {

        init_system_env();
        pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
        set_bkg_tiles( 4, 1, 11, 1, (unsigned char *)PDA_TITLE );
        DISPLAY_ON;

        /* initialize nvm data to system default value */
        pda_syscall( PDA_INIT_NVM, (void *)0 );

        set_bkg_tiles( 2, 7, 16, 1, msg_clearnvm1 );
        set_bkg_tiles( 2, 9, 16, 1, msg_clearnvm2 );
        while( 1 );
    }

    /* initiazie system environment */
    init_system_env();

    /* display copyright messages */
    copyright();

    /* probe apps */
    pda_syscall( PDA_PROBE_ROM_BANKS, &apps_num );
    desktop_page = 0;

    /* jump desktop */
    while( 1 ) {
        pda_longjmp( 1, desktop_menu, (void *)0 );
    }
}

/* EOF */