/****************************************************************************
 *  fixed.h                                                                 *
 ****************************************************************************/

/*==========================================================================*
 |  definition                                                              |
 *==========================================================================*/

/*==========================================================================*
 |  system work area (non backup)                                           |
 *==========================================================================*/
extern struct dispatch dispatch[MAX_APPS];

/*==========================================================================*
 |  structure                                                               |
 *==========================================================================*/
typedef struct pda_api {
    UBYTE bank;
    void (*func)();
} _pda_api;

typedef struct dispatch {
    UBYTE bank;
    void (*entry)();
    unsigned char *icon;
    UWORD color1;
    UWORD color2;
    unsigned char name[8];
    unsigned char version[4];
} _dispatch;

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/

/*==========================================================================*
 |  pda api function                                                        |
 *==========================================================================*/
extern void pda_nop( void *arg );
extern void pda_wait( void *arg );
extern void pda_init_nvm( void *arg );
extern void pda_clear_screen( void *arg );
extern void pda_font_type( void *arg );
extern void pda_frame_type( void *arg );
extern void pda_button_type( void *arg );
extern void pda_cursor_type( void *arg );
extern void pda_init_screen( void *arg );
extern void pda_set_cursor_map( struct cursor_info *arg );
extern void pda_init_cursor( struct cursor_pos *arg );
extern void pda_show_cursor( void *arg );
extern void pda_move_cursor( void *arg );
extern void pda_scan_pad( void *arg );
extern void pda_init_palette( void *arg );
extern void pda_set_time( struct time *arg );
extern void pda_get_time( struct time *arg );
extern void pda_set_palette( struct palette *arg );
extern void pda_get_day_length( struct date *arg );
extern void pda_update_date( void *arg );
extern void pda_init_dial( void *arg );
extern void pda_dialtone( unsigned char *arg );
extern void pda_make_3x3button( struct button *arg );
extern void pdax_make_3x3button( struct button *arg );
extern void pda_search_phone_tag( struct search_phone *arg );
extern void pda_sort_phone_name( struct search_phone *arg );
extern void pda_show_phone_line( struct search_phone *arg );
extern void pda_make_dial_number( struct phone_number *arg );
extern void pda_country_flag( struct national_flag *arg );
extern void pda_input_string( struct input_string *arg );
extern void pda_probe_data_base( void *arg );
extern void pda_set_ram_data( struct db_file *arg );
extern void pda_remove_ram_data( UWORD *arg );
extern void pda_password( struct password *arg );
extern void pda_probe_rom_banks( UBYTE *arg );
extern void pda_get_phone_name( struct phone_number *arg );
extern void pda_revival_rom_data( UWORD *arg );
extern void pda_get_phone_number( struct phone_number *arg );
extern void pda_rec_remocon( void *arg );
extern void driver_rec_remcon();
extern void pda_play_remocon( void *arg );
extern void driver_play_remcon00();
extern void driver_play_remcon33();
extern void driver_play_remcon35();
extern void driver_play_remcon38();
extern void driver_play_remcon40();
extern void driver_play_remcon44();
extern void driver_play_remcon48();
extern void driver_play_remcon52();
extern void pda_init_color( void *arg );
extern void pda_put_3x3button( void *arg );
extern void pda_country_name( struct national_flag *arg );
extern void pda_country_name3( struct national_flag *arg );
extern void pda_country_city( struct national_flag *arg );
extern void pda_country_timezone( BYTE *arg );
extern void pda_country_phone( unsigned char *arg );
extern void pda_country_summertime( struct summer_time *arg );
extern void pda_check_holidays( struct date *arg );
extern void pda_make_2x2button( struct button *arg );
extern void pdax_make_2x2button( struct button *arg );
extern void pda_search_schedule_tag( struct search_schedule *arg );
extern void pda_get_keyboard( struct read_data *arg );
extern void pda_get_real_keyboard( unsigned char *arg );
extern void driver_get_keyboard();
extern void pda_init_irda( void *arg );
extern void pda_send_irda( unsigned char *arg );
extern void driver_irda_snd( unsigned char arg );
extern void pda_recv_irda( struct read_data *arg );
extern unsigned char driver_irda_rcv();
extern void pda_get_rom_memo_data( struct memo_data *arg );
extern void pda_disable_interrupt( void *arg );
extern void pda_enable_interrupt( void *arg );
extern void pda_pdc_on_hook( void *arg );
extern void pda_pdc_dialing( unsigned char *arg );
extern void pda_search_phone_name( struct search_name *arg );
extern void pda_setup_apps_icon( void *arg );

/*==========================================================================*
 |  APPs rom information                                                    |
 *==========================================================================*/
extern struct description desc_08,   desc_09,   desc_10,   desc_11;
extern struct description desc_12,   desc_13,   desc_14,   desc_15;
extern struct description desc_16,   desc_17,   desc_18,   desc_19;
extern struct description desc_20,   desc_21,   desc_22,   desc_23;
extern struct description desc_24,   desc_25,   desc_26,   desc_27;
extern struct description desc_28,   desc_29,   desc_30,   desc_31;
extern struct function    func_08[], func_09[], func_10[], func_11[];
extern struct function    func_12[], func_13[], func_14[], func_15[];
extern struct function    func_16[], func_17[], func_18[], func_19[];
extern struct function    func_20[], func_21[], func_22[], func_23[];
extern struct function    func_24[], func_25[], func_26[], func_27[];
extern struct function    func_28[], func_29[], func_30[], func_31[];

/* EOF */