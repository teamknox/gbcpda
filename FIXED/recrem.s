;*************************************************
;*                                               *
;*         IR DRIVER for Color Game Boy          *
;*            [ Recording Remocon ]              *
;*                                               *
;*                   2000/08                     *
;*            TeamKNOx Kiyoshi Izumi             *
;*                                               *
;*-----------------------------------------------*
;* -Sampling Rate                                *
;*  :0.1ms (4.194304MHz*0.1ms = 419.4clock)      *
;* -Carrier Frequency                            *
;*  :from 28.5kHz to 57kHz                       *
;* -Receive Check Limit Time                     *
;*  :5sec (4.194304MHz*5sec = 20,971,520clock)   *
;* -Outoput                                      *
;*  :rem[4][MaxLength]                           *
;*************************************************

  MinTrailer	= 100           ; 10ms = 0.1ms*100
  PriTimer	= 7               ; 5sec(about)
          ; 2,901,050*7count+12
          ; = 20,307,362clock
  SecTimer	= 37              ; 100ms = 419,430clock
          ; 11,332*37+12
          ; = 419,296clock
  MaxLength = 127
 
  .area   _BSS
_ram::
	.DS     4 * MaxLength

	.area	_CODE

_driver_rec_remcon::
  DI

  LDH  A, (#0x56)             ; Read Enable
  OR   #0b11000000
  LDH	 (#0x56), A

first_set:
  CALL receive_check
  LD   A, D
  CP   #<0
  JR   Z, error_end

1$:
  LD   B, #<2                 ; 1st & 2nd set sampling

  LD   DE, #_ram              ; high
  LD   HL, #_ram + MaxLength  ; low
  LD   C, #MaxLength - 1
  PUSH BC

high:
  LD   A, #<1
  LD   (DE), A
high_count:
  CALL ir_sampling                            ; 24 + 132
  CP   #0x00                                  ;  8
  JR   Z, low           ; High Count End      ;  8
  LD   A, (DE)                                ;  8
  INC  A                                      ;  4
  LD   (DE), A                                ;  8
  LD   B, #<10                                ;  8
  CALL wait                                   ; 24 + 16*10 + 12
  NOP                                         ;  4
  NOP                                         ;  4
  NOP                                         ;  4
  JR   high_count                             ; 12
                     ; ----------------------------------
                                ; total clock  420(0.1ms)
low:
  LD   (HL), #<1
low_count:
  CALL ir_sampling                            ; 24 + 132
  CP   #0x01                                  ;  8
  JR   Z, trailer_check     ; Low Count End   ;  8
  LD   A, (HL)                                ;  8
  INC  A                                      ;  4
  JR   Z, set_end    ; Counter OverFlow pass  ;  8
  LD   (HL), A                                ;  8
  LD   B, #<10                                ;  8
  CALL wait                                   ; 24 + 16*10 + 12
  NOP                                         ;  4
  NOP                                         ;  4
  JR   low_count                              ;  8
				             ; ----------------------------------
                                ; total clock  420(0.1ms)
trailer_check:
  LD   A, (HL)
  CP   #<MinTrailer         ; Trailer Check
  JR   NC, set_end
  POP  BC
  DEC  C
  JR   Z, error_end         ; Data Length OverFlow
  PUSH BC
  INC  HL                   ; high Address Increment
  INC  DE                   ; low Address Increment
  JP   high
set_end:
  INC  DE
  INC  HL
  XOR  A
  LD   (DE), A              ; NULL Data
  LD   (HL), A
  POP  BC
  DEC  B
  JR   Z, all_end           ; 2nd set Ended

second_set:
  LD   DE, #_ram + MaxLength * 2      ; high
  LD   HL, #_ram + MaxLength * 3      ; low
  LD   C, #MaxLength
  PUSH BC

  LD   C, #SecTimer
  CALL rcv_chk1
  LD   A, C
  CP   #<0
  JP   NZ, high
  POP  BC
  LD   A, #<0
  LD   (#_ram + MaxLength * 2), A
  LD   (#_ram + MaxLength * 3), A
  JP   all_end

error_end:
  LD   A, #<0
  LD   (#_ram), A
  LD   (#_ram + MaxLength), A
  LD   (#_ram + MaxLength * 2), A
  LD   (#_ram + MaxLength * 3), A

all_end:
  LDH  A, (#0x56)             ; Read Disable
  AND  #0b00111111
  LDH  (#0x56), A
  EI
  RET

;=================================================
; ir_sampling (Sampling IR Pulse)
;	input     : non
;	output    : A- Pulse signal(0 or 1)
;	destroyed : A, C
;-------------------------------------------------
; Sampling Pitch for 28.5kHz to 57kHz Carrier Cut
;	:4.194304MHz/38kHz/2*75% = 41.4clock
;=================================================
ir_sampling:
	LDH  A, (#0x56)                             ;	12
  CPL                                         ;  4
  LD   C, A                                   ;  4
  NOP                                         ;  4
  NOP                                         ;  4
  LDH  A, (#0x56)                             ; 12
  LDH  A, (#0x56)                             ; 12
  CPL                                         ;  4
  OR   C                                      ;  4
  LD   C, A                                   ;  4
  NOP                                         ;  4
  LDH  A, (#0x56)                             ; 12
  LDH  A, (#0x56)                             ; 12
  CPL                                         ;  4
  OR   C                                      ;  4
  SRL  A                                      ;  8
  AND  #0x01                                  ;  8
  RET                                         ; 16
                              ; --------------------------
		                            ; total clock   40  40  52

;=================================================
; receive_check
;	input     : non
;	output    : D
;	destroyed : A, BC
;=================================================
receive_check:
  LD   D, #PriTimer           ; Timer Set
1$:
  LD   C, #0xFF               ;  8
  CALL rcv_chk1               ; 24 + 11344*256 + 12
  LD   A, C                   ;  4
  CP   #<0                    ;  8
  RET  NZ                     ;  8
  DEC  D                      ;  4
  JR   NZ, 1$                 ; 12 - 4
  RET           ; Time Over   ; 16
  			   ; -------------------------
           ; total clock   2904144  12

rcv_chk1:
  LD   B, #0xFF               ;  8
  CALL rcv_chk2               ; 24 + 44*256 + 12
  LD   A, B                   ;  4
  CP   #<0                    ;  8
  RET  NZ                     ;  8
  DEC  C                      ;  4
  JR   NZ, rcv_chk1           ; 12 - 4
  RET                         ; 16
				  ; --------------------------
				  ; total clock      11344  12

rcv_chk2:
  LDH  A, (#0x56)             ; 12
  AND  #0x02  ;Receive Check  ;  8
  RET  Z                      ;  8 
  DEC  B                      ;  4
  JR   NZ, rcv_chk2           ; 12 - 4
  RET                         ; 16
          ; --------------------------
          ; total clock         44  12

;=================================================
; wait
;	input		: B
;	output		: non
;	destroyed	: B
;=================================================
wait:
  DEC  B                      ;  4
  JR   NZ, wait               ; 12 - 4
  RET                         ; 16
          ; --------------------------
          ; total clock         16  12

; End of recrem.s
