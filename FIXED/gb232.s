;**********************************
; Serial Driver for Game Boy Color
; Sep08/2001
; TeamKNOx
;**********************************
;
;[Pin Assign. of GBC Serial Port]
;pin1 5V
;pin2 Sout = TxD
;pin3 nc
;pin4 P14  = RxD
;pin5 nc
;pin6 GND  = GND
;

	.area	_BSS
_gRcvData::
	.DS 127
.param_gb232:	;speed | data | stop | parity
	.DS 1
.param2_gb232:	;adjust speed
	.DS 1

	.area	_CODE
_set_gb232::
	LDA	HL,2(SP)	;skip return address
	LD	A,(HL+)
	LD	(.param_gb232),A
	LD	A,(HL)
	LD	(.param2_gb232),A
	RET

;------------------------------------------------------------------------------
_init_gb232::
	LD	B,#<127		;clear buffer
	LD	HL,#_gRcvData
	XOR	A
1$:	LD	(HL+),A
	DEC	B
	JR	NZ,1$

	LD	A,#0xFF		;send stop bit
	LDH	(#0x01),A
	LD	A,#0x83
	LDH	(#0x02),A
	RET

;------------------------------------------------------------------------------
_receive_gb232::
	DI
	PUSH	BC
	LDA	HL,4(SP)	;skip return address and registers
	LD	D,(HL)		; D = data length
	INC	HL
	LD	E,(HL)		; E = time-out counter
	PUSH	DE		;put data length & time-out counter
	LD	HL,#_gRcvData	; HL = store address
	PUSH	HL		;put store address

receive_start_bit:
	LDH	A,(#0x56)					;12
	BIT	4,A						; 8
	JR	NZ,wait_for_start_bit				;12
	POP	HL		;get store address
	LD	(HL),#0x00	;clear data
	POP	DE
	LD	E,#0x02		;overrun error!
	POP	BC
	EI
	RET

wait_for_start_bit:
	CALL	receive_check			;28(return)	;24+28(call)
	LD	A,E				; 4
	CP	#<0				; 8
	JR	NZ,receive_data_bit		;12
	POP	HL		;get store address
	LD	(HL),#0x00	;clear data
	POP	DE
	LD	E,#0x01		;time-out error!
	POP	BC
	EI
	RET

receive_data_bit:
	LD	D,#0x00		;clear data	; 8
	LD	L,#0x00	;clear parity counter	; 8
	LD	E,#<8	;set bit counter	; 8
	LD	A,(.param_gb232)		;16
	BIT	3,A				; 8
	JR	Z,1$				;12  8
	DEC	E		;data bit 7	;    4
1$:	CALL	delay_15	;*1.5 delay	;24+delay_15

2$:	PUSH	BC		;NOP*4		;16		;16
	POP	BC		;NOP*3		;12		;12
	NOP					; 4		; 4
	LDH	A,(#0x56)			;12		;12
						;=180+delay_15
	SWAP	A					; 8	; 8	; 8
	LD	B,A					; 4	; 4	; 4
	RRA			;put pin 4 into carry	; 4	; 4	; 4
	RR	D		;store data bit		; 8	; 8	; 8
	LD	A,B					; 4	; 4	; 4
	XOR	L		;count parity 		; 4	; 4	; 4
	LD	L,A					; 4	; 4	; 4
	CALL	delay					;24+dly	;24+dly	;24+dly
	DEC	E		;count bit		; 4	; 4	; 4
	JR	NZ,2$					; 8	;12	; 8
								;=120+delay
receive_parity_bit:
	LD	A,(.param_gb232)			;16		;16
	BIT	1,A					; 8		; 8
	JR	Z,receive_stop_bit2			; 8		;12

	LD	H,A					; 4
	LDH	A,(#0x56)				;12
							;=120+delay
	SWAP	A				; 4
	XOR	H				; 4
	XOR	L				; 4
	AND	#0x01				; 8
	JR	Z,receive_stop_bit		;12
	POP	HL		;get store address
	LD	(HL),#0x00	;clear data
	POP	DE
	LD	E,#0x04		;parity error!
	POP	BC
	EI
	RET

receive_stop_bit:
	CALL	delay				;24+delay
	PUSH	BC		;NOP*4		;16
	POP	BC		;NOP*3		;12
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
receive_stop_bit2:
	LDH	A,(#0x56)			;12			;12
						;=120+delay		;=120+delay
	BIT	4,A						; 8
	JR	NZ,data_store					;12
	POP	HL		;get store address
	LD	(HL),#0x00	;clear data
	POP	DE
	LD	E,#0x03		;framing error!
	POP	BC
	EI
	RET

data_store:
	LD	A,(.param_gb232)				;16
	BIT	3,A						; 8
	JR	Z,1$						;12  8
	SRL	D						;    8
1$:	POP	HL		;get store address		;12
	LD	(HL),D		;store data			; 8
	INC	HL		;next store address		; 8
	POP	DE	;get data length & time-out counter	;12
	DEC	D		;count data length		; 4
	PUSH	DE		;put data length		;16
	PUSH	HL		;put store address		;16
	JP	NZ,receive_start_bit				;16
								;=232(include receive_start)
	POP	HL
	POP	DE
	LD	E,#0x00		;received
	POP	BC
	EI
	RET

;------------------------------------------------------------------------------
receive_check:
				;time-out time = E * 0.1sec
				;E = time-out counter
				;0.1sec (4.194304MHz*0.1sec = 419430clock)
				;->(44*232+20)*41+20=419368clock
	LD	B,#<232							; 8
	LD	C,#<41							; 8
1$:
	LDH	A,(#0x56)			;12			;12
	BIT	4,A				; 8		; 8	;=28(call)
	RET	Z		;received	; 8		;20
	DEC	B				; 4		;=28(return)
	JR	NZ,1$				;12	-4
	LD	B,#<232				;=44	 8
	DEC	C				;	 4
	JR	NZ,1$				;	12	-4
	LD	C,#<41				;	=20	 8
	DEC	E				;		 4
	JR	NZ,1$				;		12
	RET			;time-out!	;		=20

;------------------------------------------------------------------------------
_send_gb232::
	DI
	PUSH	BC
	LDA	HL,4(SP)	;skip return address and registers
	LD	D,(HL)		; D = send data

	LD	B,D
	LD	E,#<8		;set data counter
	LD	A,(.param_gb232)
	BIT	3,A
	JR	Z,1$
	DEC	E		;data bit 7
1$:	LD	C,E
	LD	L,#<0		;clear parity counter
2$:	LD	A,B
	RRCA
	LD	B,A
	XOR	L		;parity counter
	LD	L,A
	DEC	E
	JR	NZ,2$

	LD	A,(.param_gb232)
	LD	H,A
	XOR	L
	AND	#0x01
	LD	L,A		;set parity
	LD	E,C		;set data counter
send_start_bit:	
	LD	A,#0x00
	LDH	(#0x01),A
	LD	A,#0x83
	LDH	(#0x02),A
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
send_data_bit:
	CALL	delay				;24+dly	;24+dly
	NOP					; 4	; 4
	NOP					; 4	; 4
	NOP					; 4	; 4
	NOP					; 4	; 4
	NOP					; 4	; 4
	NOP					; 4	; 4
	XOR	A		;A = 0x00	; 4	; 4
	RR	D				; 8	; 8
	JR	NC,1$				;12  8	;12  8
	CPL			;A = 0xFF	;    4	;    4
1$:	LDH	(#0x01),A			;12	;12
	LD	A,#0x83				; 8	; 8
	LDH	(#0x02),A			;12	;12
	DEC	E		;count bit	; 4	;=120+delay	; 4	; 4
	JR	NZ,send_data_bit		;12			; 8	; 8
						;=120+delay
send_parity_bit:
	CALL	delay							;24+dly	;24+dly
	NOP								; 4	; 4
	NOP								; 4	; 4
	NOP								; 4	; 4
	BIT	1,H							; 8	; 8
	JR	Z,send_stop_bit						; 8	;12

	XOR	A		;A = 0x00				; 4
	BIT	0,L							; 8
	JR	Z,1$							;12  8
	CPL			;A = 0xFF				;    4
1$:	LDH	(#0x01),A						;12
	LD	A,#0x83							; 8
	LDH	(#0x02),A						;12
									;=120+delay
	CALL	delay						;24+dly
	PUSH	BC		;NOP*4				;16
	POP	BC		;NOP*3				;12
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
send_stop_bit:
	NOP							; 4		; 4
	NOP							; 4		; 4
	NOP							; 4		; 4
	LD	A,#0xFF						; 8		; 8
	LDH	(#0x01),A					;12		;12
	LD	A,#0x83						; 8		; 8
	LDH	(#0x02),A					;12		;12
								;=120+delay	;=120+(delay)
	CALL	delay
	POP	BC
	EI
	RET

;------------------------------------------------------------------------------
delay:
	LD	A,(.param2_gb232)			;16
	CP	#0x02		;adjust ratio = +3%	; 8
	JP	Z,delay_p3				;16	;12
	CP	#0x01		;adjust ratio = 0%	; 8
	JP	Z,delay_0				;16	;12
	CP	#0x00		;adjust ratio = -3%	; 8
	JP	Z,delay_m3				;16	;12
	RET

delay_p3:
	LD	A,(.param_gb232)		;16
	AND	#0xF0				; 8
	CP	#0x70		;14400bps	; 8
	JR	NZ,1$				; 8	;12
	LD	BC,#1				;12
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	JP	delay_loop			;16
1$:
	CP	#0x60		;9600bps		; 8
	JR	NZ,2$					; 8	;12
	LD	BC,#5					;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	JP	delay_loop				;16
2$:
	CP	#0x50		;4800bps			; 8
	JR	NZ,3$						; 8	;12
	LD	BC,#20						;12
	NOP							; 4
	NOP							; 4
	JP	delay_loop					;16
3$:
	CP	#0x40		;2400bps				; 8
	JR	NZ,4$							; 8	;12
	LD	BC,#49							;12
	NOP								; 4
	NOP								; 4
	NOP								; 4
	NOP								; 4
	NOP								; 4
	NOP								; 4
	JP	delay_loop						;16
4$:
	CP	#0x30		;1200bps					; 8
	JR	NZ,5$						;12		; 8
	LD	BC,#109								;12
	NOP									; 4
	NOP									; 4
	NOP									; 4
	NOP									; 4
	NOP									; 4
	JP	delay_loop							;16
5$:
	CP	#0x20		;600bps				; 8
	JR	NZ,6$					;12	; 8
	LD	BC,#230						;12
	JP	delay_loop					;16
6$:
	CP	#0x10		;300bps			; 8
	JR	NZ,7$				;12	; 8
	LD	BC,#471					;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	JP	delay_loop				;16
7$:
	CP	#0x00		;110bps		; 8
	JR	NZ,8$			;12	; 8
	LD	BC,#1307			;12
	NOP					; 4
	NOP					; 4
	NOP					; 4
	JP	delay_loop			;16
8$:
	LD	BC,#0xFFFF	;speed param. error
	RET

delay_0:
	LD	A,(.param_gb232)		;16
	AND	#0xF0				; 8
	CP	#0x70		;14400bps	; 8
	JR	NZ,1$				; 8	;12
	LD	BC,#1				;12
	NOP					; 4
	JP	delay_loop			;16
1$:
	CP	#0x60		;9600bps		; 8
	JR	NZ,2$					; 8	;12
	LD	BC,#5					;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	JP	delay_loop				;16
2$:
	CP	#0x50		;4800bps			; 8
	JR	NZ,3$						; 8	;12
	LD	BC,#20						;12
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
	JP	delay_loop					;16
3$:
	CP	#0x40		;2400bps				; 8
	JR	NZ,4$							; 8	;12
	LD	BC,#51							;12
	JP	delay_loop						;16
4$:
	CP	#0x30		;1200bps					; 8
	JR	NZ,5$						;12		; 8
	LD	BC,#112								;12
	NOP									; 4
	NOP									; 4
	NOP									; 4
	NOP									; 4
	NOP									; 4
	JP	delay_loop							;16
5$:
	CP	#0x20		;600bps				; 8
	JR	NZ,6$					;12	; 8
	LD	BC,#236						;12
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
	JP	delay_loop					;16
6$:
	CP	#0x10		;300bps			; 8
	JR	NZ,7$				;12	; 8
	LD	BC,#485					;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	NOP						; 4
	JP	delay_loop				;16
7$:
	CP	#0x00		;110bps		; 8
	JR	NZ,8$			;12	; 8
	LD	BC,#1347			;12
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	JP	delay_loop			;16
8$:
	LD	BC,#0xFFFF	;speed param. error
	RET
	
delay_m3:
	LD	A,(.param_gb232)		;16
	AND	#0xF0				; 8
	CP	#0x70		;14400bps	; 8
	JR	NZ,1$				; 8	;12
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	RET					;16
1$:
	CP	#0x60		;9600bps		; 8
	JR	NZ,2$					; 8	;12
	LD	BC,#5					;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	JP	delay_loop				;16
2$:
	CP	#0x50		;4800bps			; 8
	JR	NZ,3$						; 8	;12
	LD	BC,#20						;12
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
	JP	delay_loop					;16
3$:
	CP	#0x40		;2400bps				; 8
	JR	NZ,4$							; 8	;12
	LD	BC,#52							;12
	NOP								; 4
	JP	delay_loop						;16
4$:
	CP	#0x30		;1200bps					; 8
	JR	NZ,5$						;12		; 8
	LD	BC,#115								;12
	NOP									; 4
	NOP									; 4
	NOP									; 4
	NOP									; 4
	NOP									; 4
	JP	delay_loop							;16
5$:
	CP	#0x20		;600bps				; 8
	JR	NZ,6$					;12	; 8
	LD	BC,#243						;12
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
	JP	delay_loop					;16
6$:
	CP	#0x10		;300bps			; 8
	JR	NZ,7$				;12	; 8
	LD	BC,#500					;12
	JP	delay_loop				;16
7$:
	CP	#0x00		;110bps		; 8
	JR	NZ,8$			;12	; 8
	LD	BC,#1387			;12
	NOP					; 4
	NOP					; 4
	NOP					; 4
	NOP					; 4
	JP	delay_loop			;16
8$:
	LD	BC,#0xFFFF	;speed param. error
	RET
	
delay_15:
	LD	A,(.param_gb232)		;16
	AND	#0xF0				; 8
	CP	#0x70		;14400bps	; 8
	JR	NZ,1$				; 8	;12
	LD	BC,#6				;12
	NOP					; 4
	NOP					; 4
	JP	delay_loop			;16
1$:
	CP	#0x60		;9600bps		; 8
	JR	NZ,2$					; 8	;12
	LD	BC,#13					;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	JP	delay_loop				;16
2$:
	CP	#0x50		;4800bps			; 8
	JR	NZ,3$						; 8	;12
	LD	BC,#36						;12
	NOP							; 4
	JP	delay_loop					;16
3$:
	CP	#0x40		;2400bps				; 8
	JR	NZ,4$							; 8	;12
	LD	BC,#82							;12
	NOP								; 4
	JP	delay_loop						;16
4$:
	CP	#0x30		;1200bps					; 8
	JR	NZ,5$						;12		; 8
	LD	BC,#175								;12
	NOP									; 4
	JP	delay_loop							;16
5$:
	CP	#0x20		;600bps				; 8
	JR	NZ,6$					;12	; 8
	LD	BC,#361						;12
	NOP							; 4
	NOP							; 4
	NOP							; 4
	NOP							; 4
	JP	delay_loop					;16
6$:
	CP	#0x10		;300bps			; 8
	JR	NZ,7$				;12	; 8
	LD	BC,#735					;12
	NOP						; 4
	NOP						; 4
	NOP						; 4
	JP	delay_loop				;16
7$:
	CP	#0x00		;110bps		; 8
	JR	NZ,8$			;12	; 8
	LD	BC,#2028			;12
	NOP					; 4
	NOP					; 4
	NOP					; 4
	JP	delay_loop			;16
8$:
	LD	BC,#0xFFFF	;speed param. error
	RET

delay_loop:
	DEC	BC			; 8
	LD	A,B			; 4
	OR	C			; 4
	JR	NZ,delay_loop		;12	-4
	RET				;	16
					;=28*BC+12
;[clock calc.]
;		    	(0%)	(+3%)	(-3%)	(*1.5)
;4194304Hz/14400bps=	  291	  283	  300	  437 clock
;4194304Hz/ 9600bps=	  437	  424	  450	  655 clock
;4194304Hz/ 4800bps=	  874	  848	  900	 1311 clock
;4194304Hz/ 2400bps=	 1748	 1695	 1800	 2621 clock
;4194304Hz/ 1200bps=	 3495	 3390	 3600	 5243 clock
;4194304Hz/  600bps=	 6991	 6781	 7200	10486 clock
;4194304Hz/  300bps=	13981	13562	14400	20972 clock
;4194304Hz/  110bps=	38130	36986	39274	57195 clock

;End of gb232.s