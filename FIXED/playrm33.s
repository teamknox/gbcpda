;*************************************************
;*                                               *
;*         IR DRIVER for Color Game Boy          *
;*              [ Play Remocon 33]               *
;*                                               *
;*                   1999/08                     *
;*            TeamKNOx Kiyoshi Izumi             *
;*                                               *
;*-----------------------------------------------*
;* -Carrier Frequency : 33kHz                    *
;* -Input : Set No.                              *
;*************************************************

	.globl	_ram

	MaxLength	=	127

_driver_play_remcon33::
	DI

	LDA	HL,2(SP)	;Skip return address
	LD	A,(HL)		;Set No.
	CP	#<1
	JR	Z,second
first:
	LD	DE,#_ram
	LD	HL,#_ram + MaxLength
	LD	C,#MaxLength
	JR	high
second:
	LD	DE,#_ram + MaxLength * 2
	LD	HL,#_ram + MaxLength * 3
	LD	C,#MaxLength

high:
	LD	A,(DE)		;High Pulse
	CP	#0x00
	JR	Z,exit
	LD	C,A
	LD	A,#0x01
	CALL	trans_pulse33
	INC	DE

low:
	LD	C,(HL)		;Low Pulse
	LD	A,#0x00
	CALL	trans_pulse33
	INC	HL
	JR	high

exit:
	EI
	RET

;=================================================
; trans_pulse33 (Transmitting Pulse)
;	input		:A - Pulse signal(0 or 1)
;			 C - Pulse Length(*0.1ms)
;	destroyed	:BC
;-------------------------------------------------
;	carrier frequency:33KHz
;		(4.194304MHz/33KHz=127.1clock)
;=================================================
trans_pulse33:
1$:	LD	B,#<3		;0.1ms(420clock) = 64*2*3+36

				;high	low
2$:	LDH	(#0x56),A	;	12
	NOP			; 4
	NOP			; 4
	NOP			; 4
	NOP			; 4
	NOP			; 4
	NOP			; 4
	NOP			; 4
	NOP			; 4
	PUSH	AF		;16
	XOR	A		; 4
	LDH	(#0x56),A	;12
	POP	AF		;	12
	NOP			;	 4
	NOP			;	 4
	NOP			;	 4
	NOP			;	 4
	NOP			;	 4
	NOP			;	 4
	DEC	B		;	 4
	JR	NZ,2$		;	12
	NOP			;		 4
	NOP			;		 4
	NOP			;		 4
	NOP			;		 4
	NOP			;		 4
	NOP			;		 4
	NOP			;		 4
	NOP			;		 4
	NOP			;		 4
	DEC	C
	JR	NZ,1$
	RET
		;----------------------------------
		;total clock:	 64	64	36

;End of playrm33.s
