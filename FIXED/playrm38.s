;*************************************************
;*                                               *
;*         IR DRIVER for Color Game Boy          *
;*              [ Play Remocon 38]               *
;*                                               *
;*                   2000/08                     *
;*            TeamKNOx Kiyoshi Izumi             *
;*                                               *
;*-----------------------------------------------*
;* -Carrier Frequency : 38kHz                    *
;* -Input : Set No.                              *
;*************************************************

  .globl    _ram

  MaxLength = 127

_driver_play_remcon38::
  DI

  LDA HL,2(SP)          ; Skip return address
  LD  A,(HL)            ; Set No.
  CP  #<1
  JR  Z,second
first:
  LD  DE,#_ram
  LD  HL,#_ram + MaxLength
  LD  C,#MaxLength
  JR  high
second:
  LD  DE,#_ram + MaxLength * 2
  LD  HL,#_ram + MaxLength * 3
  LD  C,#MaxLength

high:                   ; high low
  LD  A,(DE)            ; 8
  CP  #0x00             ; 8
  JR  Z,exit            ; 8
  LD  C,A               ; 4
  LD  A,#0x01           ; 8
  CALL  trans_pulse38   ; 420*C 24+20+28
  INC DE                ; 4

low:
  LD  C,(HL)            ; 8
  DEC C                 ; 4
  XOR A                 ; 4
  CALL  trans_pulse38   ; 24+20+420*(C-1)+28
  LD  B,#<2	;adjust     ; 8
  CALL  wait            ; 24+16*B+12
  INC HL                ; 4
  JR  high              ; 12

  ;--------------------------
  ; total clock   420*C  420*(C-1)+260+16*B
exit:
  EI
  RET

;=================================================
; trans_pulse38 (Transmitting Pulse)
; input     :A - Pulse signal(0 or 1)
;            C - Pulse Length(*0.1ms)
; destroyed :BC
;-------------------------------------------------
; carrier frequency:38KHz
;     (4.194304MHz/38KHz=110.4clock)
;=================================================
trans_pulse38:
  PUSH  DE              ; 16
  LD  D,A               ; 4
        ; 0.1ms(420clock) = 8+56*2*3+16+44+16
        ; high low
1$:
  LD  B,#<3             ; 8
2$:
  LD  A,D               ; 4
  LDH	(#0x56),A         ; 12
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  XOR A                 ; 4
  LDH (#0x56),A         ; 12
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  DEC B                 ; 4
  JR  NZ,2$             ; 12(8)

  NOP                   ; (4)
  LD  A,D               ; 4
  LDH (#0x56),A         ; 12
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  NOP                   ; 4
  XOR A                 ; 4
  LDH (#0x56),A         ; 12
  DEC C                 ; 4
  JR  NZ,1$             ; 12
  POP DE                ; 12
  RET                   ; 16

;=================================================
; wait
; input     :B
; output    :non
; destroyed :B
;=================================================
wait:
  DEC B                 ; 4
  JR  NZ,wait           ; 12-4
  RET                   ; 16
  ;--------------------------
  ; total clock           16 12

; End of playrm38.s
