// gb232.h
// Sep08/2001
// TeamKNOx

extern UBYTE gRcvData[127];	 /* in fixed/gb232.s */

void	set_gb232( UBYTE comm_parameter, UBYTE adjust_speed );
//usage sample
//set_gb232( SPEED_9600 | DATA_8 | STOP_1 | PARITY_NONE, ADJUST_0P )
#define SPEED_110   0x00U	// 0000 ****
#define SPEED_300   0x10U	// 0001 ****
#define SPEED_600   0x20U	// 0010 ****
#define SPEED_1200  0x30U	// 0011 ****
#define SPEED_2400  0x40U	// 0100 ****
#define SPEED_4800  0x50U	// 0101 ****
#define SPEED_9600  0x60U	// 0110 ****
#define SPEED_14400 0x70U	// 0111 ****
#define DATA_8      0x00U	// **** 0***
#define DATA_7      0x08U	// **** 1***
#define STOP_1      0x00U	// **** *0**
#define STOP_2      0x04U	// **** *1**
#define PARITY_NONE 0x00U	// **** **00
#define PARITY_ODD  0x03U	// **** **11
#define PARITY_EVEN 0x02U	// **** **10
#define ADJUST_M3P  0x00U	// adjust -3%
#define ADJUST_0P   0x01U	// adjust 0%
#define ADJUST_P3P  0x02U	// adjust +3%

void	init_gb232();

UWORD
	receive_gb232( UBYTE byte_length,
	               UBYTE timeout_count );
void
	send_gb232( UBYTE send_data );

