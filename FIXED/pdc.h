/* PDC dialing functions */

/* PDC command */
#define PDC_SERVICE_START       0x1A
#define PDC_SYSTEM_REQ          0x1B
#define PDC_IDENTFY_REQ         0x1C
#define PDC_IDENTFY_MODE        0x1E

#define PDC_VOX_OFF             0x6A
#define PDC_VOX_ON              0x6B

#define PDC_SUSPEND             0x82
#define PDC_WRITE_MEMORY        0x83
#define PDC_DIAL_END            0x86
#define PDC_READ_MEMORY         0x87
#define PDC_DIAL_LOCK           0x8A
#define PDC_HOOKING             0x8B
#define PDC_REQUEST_NUMBER      0x8E

#define PDC_DIAL_D              0x90
#define PDC_DIAL_1              0x91
#define PDC_DIAL_2              0x92
#define PDC_DIAL_3              0x93
#define PDC_DIAL_4              0x94
#define PDC_DIAL_5              0x95
#define PDC_DIAL_6              0x96
#define PDC_DIAL_7              0x97
#define PDC_DIAL_8              0x98
#define PDC_DIAL_9              0x99
#define PDC_DIAL_0              0x9A
#define PDC_DIAL_AS             0x9B
#define PDC_DIAL_SH             0x9C
#define PDC_DIAL_A              0x9D
#define PDC_DIAL_B              0x9E
#define PDC_DIAL_C              0x9F

#define PDC_ON_HOOK             0xA4
#define PDC_OFF_HOOK            0xA5
#define PDC_ON_CRADLE           0xA6
#define PDC_OFF_CRADLE          0xA7

#define PDC_READ_ID             0xB1
#define PDC_SECCODE_REQ         0xB5
#define PDC_SECCODE_CHG         0xB6

#define PDC_MUTE_OFF            0xC0
#define PDC_MUTE_ON             0xC1
#define PDC_SIGHLENT_OFF        0xC2
#define PDC_SIGHLENT_ON         0xC3

#define PDC_FORWORD             0xE0
#define PDC_RECORD              0xE2
#define PDC_A_FORWORD           0xE3
#define PDC_SWITCH_3            0xE4
#define PDC_MIXING_3            0xE5
#define PDC_ABORT_SUSPENDING    0xE6
#define PDC_FAX_MODEM           0xE8
#define PDC_DISABLE             0xE9
#define PDC_DIAL_OK             0xEB
#define PDC_ROMMING_MODE        0xEC
#define PDC_ROMMING_INFO        0xED

/* PDC status */
#define PDC_S_SYSTEM_REQ        0x1B
#define PDC_S_IDENTFY_MODE      0x1E
#define PDC_S_LEVEL_CMD         0xC0
#define PDC_S_LEVEL_MASK        0x0F
#define PDC_S_RESET             0xA3

