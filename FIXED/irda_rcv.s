;*************************************************
;*                                               *
;*         IR DRIVER for Game Boy Color          *
;*          [ IrDA SIR 1.0 Receiver ]            *
;*                                               *
;*                   2000/09                     *
;*               K.I. @ TeamKNOx                 *
;* --------------------------------------------- *
;*   - set -                                     *
;*         speed  ; 0: 2400bps, 1: 9600bps       *
;*         parity ; 0: non, 1: odd, 2: even      *
;*   - input -                                   *
;*         none                                  *
;*   - output -                                  *
;*         return value ; 0: not received        *
;*                      ; 1: received            *
;*                      ; 2: parity error        *
;*         irda_buff    ; received data(1 byte)  *
;*************************************************
;clock calc.
;   4.194304MHz /  2400bps = 1747.6clock
;   4.194304MHz /  9600bps =  436.9clock

        .globl _irda_speed          ; 0:2400bps, 1:9600bps
        .globl _irda_parity         ; 0:non, 1:odd, 2:even

        .area   _BSS
_irda_buff::
        .ds     1

        .area   _CODE
_driver_irda_rcv::
        di
        ld   a, #0b11000000
        ldh  (#0x56), a             ; read enable

        ld   a, (#_irda_speed)
        ld   h, a
        ld   a, (#_irda_parity)
        ld   l, a

;*** Start Bit ***
rcv_start:
        call rcv_check                               ; 24 + 28
        ld   a, b                                    ;  4
        cp   #<0                                     ;  8
        jr   z, rcv_non     ; not received           ;  8

        call delay                                   ; 24 + delay
        call delay2                                  ; 24 + 68
        nop                                          ;  4

;*** Data Bit ***
        ld   d, #<0         ; data clear             ;  8
        ld   e, #<0         ; parity counter clear   ;  8
        ld   c, #<8         ; 8 bit counter          ;  8
                                     ; ===== total:   252 + delay-36 =====
rcv_data:
        call rcv_bit                                 ; 24 + 160
        ld   b, a                                    ;  4
        or   d              ; data | rcv_bit         ;  4
        rrca                ; data>>1                ;  4
        ld   d, a                                    ;  4
        ld   a, b                                    ;  4
        xor  e                                       ;  4
        ld   e, a                                    ;  4
        call delay                                   ; 24 + delay
        dec  c                                       ;  4
        jr   nz, rcv_data                            ; 12
                                     ; ===== total:   252 + delay =====

;*** Parity Bit ***
        ld   a, l                                    ;  4
        cp   #<0                                     ;  8
        jr   z, rcv_stop                             ;  8

        call rcv_bit                                 ; 24 + 160
        xor  e                                       ;  4
        xor  l                                       ;  4
        and  #0x01                                   ;  8
        jp   nz, rcv_err                             ;  8

        call delay                                   ; 24 + delay
                                     ; ===== total:   252 + delay =====

;*** Stop Bit ***
rcv_stop:
;       ldh  a, (#0x56)
;       and  #0x02
;       jr   z, rcv_stop

        ld   b, #0xFF                ; added timeout function
1$:     ldh  a, (#0x56)              ;
        and  #0x02                   ;
        jr   nz, 2$                  ;
        dec  b                       ;
        jr   nz, 1$                  ;
        jr   rcv_non                 ;
2$:                                  ;

        ld   a, d
        ld   (_irda_buff), a         ; data store
        ld   e, #<1                  ; received
        jr   rcv_end

rcv_non:
        ld   e, #<0                  ; not received
        jr   rcv_end

rcv_err:
        ld   e, #<2                  ; parity error

rcv_end:
        xor  a
        ldh  (#0x56), a              ; read disable
        ei
        ret

;=================================================
; rcv_check
;       input           : non
;       output          : b
;       destroyed       : a, b
;=================================================
rcv_check:
        ld   b, #0xFF
1$:     ldh  a, (#0x56)
        and  #0x02                                   ;  8
        ret  z                                       ; 20
                                     ; ===== total:    28 =====
        dec  b
        jr   nz, 1$
        ret
        
;=================================================
; rcv_bit
;       input           : non
;       output          : a
;       destroyed       : b
;=================================================
rcv_bit:
        ldh  a, (#0x56)               ; 12
        ld   b, a                     ;  4
        ldh  a, (#0x56)               ; 12
        and  b                        ;  4
        ld   b, a                     ;  4
        ldh  a, (#0x56)               ; 12
        and  b                        ;  4
        ld   b, a                     ;  4
        ldh  a, (#0x56)               ; 12
        and  b                        ;  4
        ld   b, a                     ;  4
        ldh  a, (#0x56)               ; 12
        and  b                        ;  4
        ld   b, a                     ;  4
        ldh  a, (#0x56)               ; 12
        and  b                        ;  4
        ld   b, a                     ;  4
        ldh  a, (#0x56)               ; 12
        and  b                        ;  4
        rrca                          ;  4
        and  #0x01                    ;  8
        ret                           ; 16
                      ; ===== total:   160 =====

;=================================================
; delay
;       input           : h
;       output          : non
;       destroyed       : a, b
;=================================================
delay:
        ld   a, h                     ;  4
        rrca                          ;  4
        jr   c, d96                   ; 12(8)
        jr   d24                      ; 12
d96:
        ld   b, #<9                   ;  8
1$:
        dec  b                        ;  4
        jr   nz, 1$                   ; 12(8)

        ret                           ; 16
                      ; ===== total:    16*9 + 40 = 184 =====

d24:
        ld   b, #<90                  ;  8
1$:
        dec  b                        ;  4
        jr   nz, 1$                   ; 12(8)

        nop                           ;  4
        nop                           ;  4
        ret                           ; 16
                      ; ===== total:    16*90 + 56 = 1576 =====
;=================================================
; delay2
;       input           : b
;       output          : non
;       destroyed       : b
;=================================================
delay2:
        ld   b, #<3                   ;  8
1$:
        dec  b                        ;  4
        jr   nz, 1$                   ; 12(8)

        ret                           ; 16
                      ; ===== total:    16*3 + 20 = 68 =====

