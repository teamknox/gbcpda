;*************************************************
;*                                               *
;*         IR DRIVER for Color Game Boy          *
;*              [ Play Remocon 00]               *
;*                                               *
;*                   1999/08                     *
;*            TeamKNOx Kiyoshi Izumi             *
;*                                               *
;*-----------------------------------------------*
;* -Carrier Frequency : non                      *
;* -Input : Set No.                              *
;*************************************************

	.globl	_ram

	MaxLength	=	127

_driver_play_remcon00::
	DI

	LDA	HL,2(SP)	;Skip return address
	LD	A,(HL)		;Set No.
	CP	#<1
	JR	Z,second
first:
	LD	DE,#_ram
	LD	HL,#_ram + MaxLength
	LD	C,#MaxLength
	JR	high
second:
	LD	DE,#_ram + MaxLength * 2
	LD	HL,#_ram + MaxLength * 3
	LD	C,#MaxLength

high:
	LD	A,(DE)		;High Pulse
	CP	#0x00
	JR	Z,exit
	LD	C,A
	LD	A,#0x01
	CALL	trans_pulse00
	INC	DE

low:
	LD	C,(HL)		;Low Pulse
	LD	A,#0x00
	CALL	trans_pulse00
	INC	HL
	JR	high

exit:
	EI
	RET

;=================================================
; trans_pulse00 (Transmitting Pulse)
;	input		:A - Pulse signal(0 or 1)
;			 C - Pulse Length(*0.1ms)
;	destroyed	:BC
;-------------------------------------------------
;	carrier frequency:non
;=================================================
trans_pulse00:
1$:	LD	B,#<15		;0.1ms(420clock) = 15 

2$:	LDH	(#0x56),A	;12
	DEC	B		; 4
	JR	NZ,2$		;12
	DEC	C
	JR	NZ,1$
	RET
		;--------------------
		;total clock:	 28

;End of playrm00.s
