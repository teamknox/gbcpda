/****************************************************************************
 *  bank27.c                                                                *
 *                                                                          *
 *    GB-TERM  V2.00      2002/05/19                                        *
 *                                                                          *
 *    Copyright(C)  2002   TeamKNOx                                         *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "fixed\gb232.h"
#include "bank27\i_term.h"

extern void gb_term();

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_27[FUNC_NUM] = {
    { "GB-TERM ", "2.00", gb_term, i_term, i_termCGBPal0c1, i_termCGBPal0c2 }
};
struct description desc_27 = { FUNC_NUM, (struct function *)func_27 };

/*==========================================================================*
 |  definitions                                                             |
 *==========================================================================*/
#define TERM_MODE_MENU          0
#define TERM_MODE_TERM          1
#define TERM_MODE_SETTING       2
#define TERM_MODE_USAGE         3

#define TERM_MODE_SEND          4
#define TERM_MODE_RECEIVE       5
#define TERM_MODE_SEND_RECEIVE  6
#define TERM_MODE_INPUT         7
#define TERM_MODE_ENTRY         8
#define TERM_MODE_MACRO         9

typedef struct term_t {
  UBYTE  connect;
  UBYTE  pos;
  UBYTE  port;
  UBYTE  baurate;
  UBYTE  parity;
  UBYTE  databit;
  UBYTE  stopbit;
  UBYTE  adjust;
  UBYTE  x;
  UBYTE  y;
  unsigned char *buff;
} _term_t;

/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank27\i_term.c"
#include "bank27\c_term.c"
#include "bank27\n_term.c"

unsigned char tm_head[]  = { 1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3 };
unsigned char tm_line[]  = { 4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4 };
unsigned char tm_space[] = { 7,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,8 };
unsigned char tm_tail[]  = { 5,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,6 };

unsigned char tm_line_a[]  = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char tm_attrib[]  = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };


unsigned char tm_title[]  = { "G B - T E R M " };
unsigned char tm_credit[] = { "`2001 TeamKNOx" };

unsigned char tm_menu[][16]    = { "  - M E N U -  ",
                                   "    Term       ",
                                   "    Setting    ",
                                   "    Usage      " };
unsigned char tm_usage[][16]   = { " - U S A G E - ",
                                   "     A: Insert ",
                                   "     B: Delete ",
                                   " START: Send   ",
                                   "SELECT: Receive" };
unsigned char tm_setting[][16] = { "  - SETTING -  ",
                                   "Port           ",
                                   "Baudrate       ",
                                   "  Adjust       ",
                                   "Data bit       ",
                                   "Parity         ",
                                   "Stop bit       " };
unsigned char tm_return[]      = { "Return" };

unsigned char tm_port[][4]    = { "COM","Ir " };
unsigned char tm_baurate[][6] = { "110  ","300  ","600  ","1200 ",
                                  "2400 ","4800 ","9600 ","14400" };
unsigned char tm_databit[][2] = { "8","7" };
unsigned char tm_parity[][5]  = { "none","odd ","even" };
unsigned char tm_stopbit[][2] = { "1","2" };
unsigned char tm_adjust[][4]  = { "-3%","0% ","+3%" };

unsigned char tm_cursor[] = { "<>" };

/*==========================================================================*
 |  functions                                                               |
 |                                                                          |
 |  Terminal Emulator (cterm.c)  Jim Kyle, (C) 1987 Microsoft               |
 *==========================================================================*/

#if 0  /* microsoft */
UBYTE kbd_wait()  /* wait for input */
{
  UBYTE  c;

  while( (c=kb_file()) == -1 );
  return( c );
}

UBYTE kb_file()  /* input from kb or file */
{
  UBYTE  c;

  if( in_file ) {  /* using script */
    c = wants_to_abort();  /* use first as flag */
    if( waitchr && !c ) {
      c = -1;  /* then for char */
    } else if( c || (c=getc(in_file))==EOF || c==26 ) {
      fclose( in_file );
      cputs( "\r\nScript File Closed\r\n" );
      in_file = NULL;
      waitchr = 0;
      c = -1;
    } else if( c == '\n' ) {  /* ignore LFs in file */
      c = -1;
    }
    if( c == '\\' ) {  /* process esc sequence */
      c = esc();
    }
    if( vflag && (c!=-1) ) {  /* verify file char */
      putchx( '{' );
      putchx( c );
      putchx( '}' );
    }
  } else {  /* using console */
    c = read_keyboard();  /* if not using file */
  }
  return( c );
}

UBYTE esc()  /* script translator */
{
  UBYTE  c;

  c = getc( in_file );  /* control chars in file */
  switch( c ) {
    case 'E':
      c = ESC;
      break;
    case 'N':
      c = '\n';
      break;
    case 'R':
      c = '\r';
      break;
    case 'T':
      c = '\t';
      break;
    case '^':
      c = getc( in_file ) & 0x1F;
      break;
  }
  return( c );
}
#endif /* microsoft */


void term_dump_log( struct term_t *tm, UBYTE n )
{
  UBYTE  x, y;
  unsigned char tmp[2];
  unsigned char *p;

  x = tm->x;
  y = tm->y;
  p = tm->buff;

  while( n ) {
    tmp[0] = (((*p)>>4)&0x0F) | 0xE0;
    tmp[1] = ((*p)&0x0F) | 0xF0;
    set_bkg_tiles( x*2+2, y+3, 2, 1, tmp );
    if( x < 7 ) {
      x++;
    } else {
      x = 0;
      if( y < 9 )  y++;
      else         y=0;
    }
    p++;
    n--;
  }
  set_bkg_tiles( x*2+2, y+3, 2, 1, tm_cursor );
  tm->x = x;
  tm->y = y;
}

/*--------------------------------------------------------------------------*
 |  receive                                                                 |
 *--------------------------------------------------------------------------*/
UBYTE term_receive_mode( struct term_t *tm )
{
  UBYTE  i, j, k;
  unsigned char tmp[64];

  /* init screen */
  tm->x = tm->y = 0;
  set_bkg_tiles( (tm->x)*2+2, (tm->y)+3, 2, 1, tm_cursor );
  i = 0;  j = 0;  k = 0;
  tm->buff = tmp;

  /* receiveing loop */
  while( !k ) {
    if( !receive_gb232(1,1) ) {
      tmp[i] = gRcvData[0];
      i++;  j = 0;
      if( i > 64 ) {  /* buffer full */
        term_dump_log( tm, i );
        i = 0;
      }
    } else if( i ) {
      if( j > 4 ) {
        term_dump_log( tm, i );
        i = 0;
      } else {
        j++;
      }
    }
    k = joypad();
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  term                                                                    |
 *--------------------------------------------------------------------------*/
UBYTE term_main( struct term_t *tm )
{
  UBYTE  i;

  /* frame */
  for( i=0; i<12; i++ ) {
    set_bkg_tiles( 0, i+3, 20, 1, tm_line );
    set_bkg_attribute( 0, i+3, 20, 1, tm_line_a );
  }

  /* init rs232 setting */
  i = tm->baurate<<4 | tm->databit<<3 | tm->stopbit<<2 | tm->parity;
  set_gb232( i, tm->adjust );
  init_gb232();

  term_receive_mode( tm );

  return( TERM_MODE_MENU );
}

/*--------------------------------------------------------------------------*
 |  setting                                                                 |
 *--------------------------------------------------------------------------*/
struct cursor_info term_setting_cursor = { 3, 7, 102, 68, 0, 8 };
UBYTE term_setting( struct term_t *tm )
{
  UBYTE  i;
  struct cursor_pos pos;

  /* frame */
  for( i=0; i<12; i++ ) {
    set_bkg_tiles( 0, i+3, 20, 1, tm_line );
    set_bkg_attribute( 0, i+3, 20, 1, tm_line_a );
  }

  /* setting */
  set_bkg_tiles( 2,  4, 15, 1, tm_setting[0] );
  for( i=1; i<7; i++ ) {
    set_bkg_tiles( 2,  5+i, 15, 1, tm_setting[i] );
  }
  set_bkg_tiles( 12, 12, 6, 1, tm_return );

  /* settings */
  set_bkg_tiles( 12,  6, 3, 1, tm_port[tm->port] );
  set_bkg_tiles( 12,  7, 5, 1, tm_baurate[tm->baurate] );
  set_bkg_tiles( 12,  8, 3, 1, tm_adjust[tm->adjust] );
  set_bkg_tiles( 12,  9, 1, 1, tm_databit[tm->databit] );
  set_bkg_tiles( 12, 10, 4, 1, tm_parity[tm->parity] );
  set_bkg_tiles( 12, 11, 1, 1, tm_stopbit[tm->stopbit] );
  set_bkg_attribute( 12,  6, 5, 4, tm_attrib );
  set_bkg_attribute( 12, 10, 5, 2, tm_attrib );

  pos.x = 1;
  pos.y = 0;
  pda_syscall( PDA_SET_CURSOR_MAP, &term_setting_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( cursor.x == 0 ) {
      keycode.brk |= J_B;
    } else if( cursor.x == 2 ) {
      if( cursor.y < 6 )  keycode.brk |= J_A;
    }
    cursor.x = 1;

    if( keycode.brk & J_A ) {
      keycode.brk &= ~J_A;
      switch( cursor.y ) {
        case 0:
          if( tm->port < 1 )     tm->port++;
          else                   tm->port = 0;
          break;
        case 1:
          if( tm->baurate < 7 )  tm->baurate++;
          else                   tm->baurate = 0;
          break;
        case 2:
          if( tm->adjust < 2 )   tm->adjust++;
          else                   tm->adjust = 0;
          break;
        case 3:
          if( tm->databit < 1 )  tm->databit++;
          else                   tm->databit = 0;
          break;
        case 4:
          if( tm->parity < 2 )   tm->parity++;
          else                   tm->parity = 0;
          break;
        case 5:
          if( tm->stopbit < 1 )  tm->stopbit++;
          else                   tm->stopbit = 0;
          break;
        case 6:
          return( TERM_MODE_MENU );
      }
      set_bkg_tiles( 12,  6, 3, 1, tm_port[tm->port] );
      set_bkg_tiles( 12,  7, 5, 1, tm_baurate[tm->baurate] );
      set_bkg_tiles( 12,  8, 3, 1, tm_adjust[tm->adjust] );
      set_bkg_tiles( 12,  9, 1, 1, tm_databit[tm->databit] );
      set_bkg_tiles( 12, 10, 4, 1, tm_parity[tm->parity] );
      set_bkg_tiles( 12, 11, 1, 1, tm_stopbit[tm->stopbit] );
    } else if( keycode.brk & J_B ) {
      keycode.brk &= ~J_B;
      switch( cursor.y ) {
        case 0:
          if( tm->port > 0 )     tm->port--;
          else                   tm->port = 1;
          break;
        case 1:
          if( tm->baurate > 0 )  tm->baurate--;
          else                   tm->baurate = 7;
          break;
        case 2:
          if( tm->adjust > 0 )   tm->adjust--;
          else                   tm->adjust = 2;
          break;
        case 3:
          if( tm->databit > 0 )  tm->databit--;
          else                   tm->databit = 1;
          break;
        case 4:
          if( tm->parity > 0 )   tm->parity--;
          else                   tm->parity = 2;
          break;
        case 5:
          if( tm->stopbit > 0 )  tm->stopbit--;
          else                   tm->stopbit = 1;
          break;
      }
      set_bkg_tiles( 12,  6, 3, 1, tm_port[tm->port] );
      set_bkg_tiles( 12,  7, 5, 1, tm_baurate[tm->baurate] );
      set_bkg_tiles( 12,  8, 3, 1, tm_adjust[tm->adjust] );
      set_bkg_tiles( 12,  9, 1, 1, tm_databit[tm->databit] );
      set_bkg_tiles( 12, 10, 4, 1, tm_parity[tm->parity] );
      set_bkg_tiles( 12, 11, 1, 1, tm_stopbit[tm->stopbit] );
    }
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  usage                                                                   |
 *--------------------------------------------------------------------------*/
struct cursor_info term_usage_cursor = { 1, 1, 80, 132, 0, 0 };
UBYTE term_usage( struct term_t *tm )
{
  UBYTE  i;
  struct cursor_pos pos;

  /* frame */
  for( i=0; i<12; i++ ) {
    set_bkg_tiles( 0, i+3, 20, 1, tm_line );
    set_bkg_attribute( 0, i+3, 20, 1, tm_line_a );
  }

  /* usage */
  for( i=0; i<5; i++ ) {
    set_bkg_tiles( 2,  4+i*2, 15, 1, tm_usage[i] );
  }
  set_bkg_tiles( 7, 14, 6, 1, tm_return );

  pos.x = pos.y = 0;
  pda_syscall( PDA_SET_CURSOR_MAP, &term_usage_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_A ) {
      return( TERM_MODE_MENU );
    }
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  menu                                                                    |
 *--------------------------------------------------------------------------*/
struct cursor_info term_menu_cursor = { 1, 3, 60, 68, 0, 16 };
UBYTE term_menu( struct term_t *tm )
{
  UBYTE  i;
  struct cursor_pos pos;

  /* frame */
  for( i=0; i<12; i++ ) {
    set_bkg_tiles( 0, i+3, 20, 1, tm_line );
    set_bkg_attribute( 0, i+3, 20, 1, tm_line_a );
  }

  /* menu */
  for( i=0; i<4; i++ ) {
    set_bkg_tiles( 2, 4+i*2, 15, 1, tm_menu[i] );
  }

  /* settings */
  set_bkg_tiles(  4, 14, 3, 1, tm_port[tm->port] );
  set_bkg_tiles(  8, 14, 5, 1, tm_baurate[tm->baurate] );
  set_bkg_tiles( 13, 14, 1, 1, tm_parity[tm->parity] );
  set_bkg_tiles( 14, 14, 1, 1, tm_databit[tm->databit] );
  set_bkg_tiles( 15, 14, 1, 1, tm_stopbit[tm->stopbit] );
  set_bkg_tiles( 17, 14, 1, 1, tm_adjust[tm->adjust] );

  /* loop */
  pos.x = 0;
  pos.y = tm->pos;
  pda_syscall( PDA_SET_CURSOR_MAP, &term_menu_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.brk & J_A ) {
      tm->pos = cursor.y;
      return( cursor.y+TERM_MODE_TERM );
    }
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void term_init( struct term_t *tm )
{
  UBYTE  i;

  /* setup bkg data */
  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );
  set_bkg_data( 0x80, 16, c_term );
  set_bkg_data( 0xE0, 32, n_term );

  set_bkg_tiles( 0, 0, 20, 1, tm_head );
  set_bkg_tiles( 0, 1, 20, 1, tm_line );
  set_bkg_tiles( 0, 2, 20, 1, tm_space );
  for( i=0; i<12; i++ ) {
    set_bkg_tiles( 0, i+3, 20, 1, tm_line );
  }
  set_bkg_tiles( 0, 15, 20, 1, tm_space );
  set_bkg_tiles( 0, 16, 20, 1, tm_line );
  set_bkg_tiles( 0, 17, 20, 1, tm_tail );

  set_bkg_tiles( 3,  1, 14, 1, tm_title );
  set_bkg_tiles( 3, 16, 14, 1, tm_credit );

  /* init settings */
  tm->pos     = 0;
  tm->port    = 0;  // com
#if 0  // for debug
  tm->baurate = 6;  // 9600
  tm->parity  = 0;  // none
  tm->databit = 0;  // 8
  tm->stopbit = 0;  // 1
  tm->adjust  = 1;  // 0%
#else
  tm->baurate = 2;  // 600
  tm->parity  = 2;  // even
  tm->databit = 0;  // 8
  tm->stopbit = 0;  // 1
  tm->adjust  = 1;  // 0%
#endif
  tm->x       = 0;
  tm->y       = 0;
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void gb_term()
{
  UBYTE  mode;
  struct term_t tm;

  term_init( &tm );

  mode = TERM_MODE_MENU;
  while( !(keycode.brk & J_EXIT) ) {
    switch( mode ) {
      case TERM_MODE_MENU:
        mode = term_menu( &tm );
        break;
      case TERM_MODE_TERM:
        mode = term_main( &tm );
        break;
      case TERM_MODE_SETTING:
        mode = term_setting( &tm );
        break;
      case TERM_MODE_USAGE:
        mode = term_usage( &tm );
        break;
    }
  }
}

/* EOF */
