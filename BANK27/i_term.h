/*

 I_TERM.H

 Include File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 32 x 32
  Tiles                : 0 to 0

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/


/* Bank of tiles. */
#define i_termBank 0

/* Super Gameboy palette 0 */
#define i_termSGBPal0c0 0
#define i_termSGBPal0c1 17408
#define i_termSGBPal0c2 6108
#define i_termSGBPal0c3 8935

/* Super Gameboy palette 1 */
#define i_termSGBPal1c0 6596
#define i_termSGBPal1c1 6368
#define i_termSGBPal1c2 6108
#define i_termSGBPal1c3 8935

/* Super Gameboy palette 2 */
#define i_termSGBPal2c0 6596
#define i_termSGBPal2c1 6368
#define i_termSGBPal2c2 6108
#define i_termSGBPal2c3 8935

/* Super Gameboy palette 3 */
#define i_termSGBPal3c0 6596
#define i_termSGBPal3c1 6368
#define i_termSGBPal3c2 6108
#define i_termSGBPal3c3 8935

/* Gameboy Color palette 0 */
#define i_termCGBPal0c0 25344
#define i_termCGBPal0c1 32767
#define i_termCGBPal0c2 24815
#define i_termCGBPal0c3 0

/* Gameboy Color palette 1 */
#define i_termCGBPal1c0 6108
#define i_termCGBPal1c1 8935
#define i_termCGBPal1c2 6596
#define i_termCGBPal1c3 6368

/* Gameboy Color palette 2 */
#define i_termCGBPal2c0 6108
#define i_termCGBPal2c1 8935
#define i_termCGBPal2c2 6596
#define i_termCGBPal2c3 6368

/* Gameboy Color palette 3 */
#define i_termCGBPal3c0 6108
#define i_termCGBPal3c1 8935
#define i_termCGBPal3c2 6596
#define i_termCGBPal3c3 6368

/* Gameboy Color palette 4 */
#define i_termCGBPal4c0 6108
#define i_termCGBPal4c1 8935
#define i_termCGBPal4c2 6596
#define i_termCGBPal4c3 6368

/* Gameboy Color palette 5 */
#define i_termCGBPal5c0 6108
#define i_termCGBPal5c1 8935
#define i_termCGBPal5c2 6596
#define i_termCGBPal5c3 6368

/* Gameboy Color palette 6 */
#define i_termCGBPal6c0 6108
#define i_termCGBPal6c1 8935
#define i_termCGBPal6c2 6596
#define i_termCGBPal6c3 6368

/* Gameboy Color palette 7 */
#define i_termCGBPal7c0 6108
#define i_termCGBPal7c1 8935
#define i_termCGBPal7c2 6596
#define i_termCGBPal7c3 6368
/* Start of tile array. */
extern unsigned char i_term[];

/* End of I_TERM.H */
