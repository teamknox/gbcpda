/*

 I_MEMO.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 32 x 32
  Tiles                : 0 to 0

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v1.6

*/

/* Start of tile array. */
unsigned char i_memo[] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,
  0x03,0x03,0x07,0x07,0x0F,0x0F,0x1F,0x1F,
  0x1F,0x1E,0x3F,0x3D,0x3E,0x3B,0x7C,0x77,
  0x7E,0x77,0x7D,0x75,0x7C,0x74,0x7C,0x74,
  0x00,0x00,0x1F,0x1F,0x7F,0x7F,0xFF,0xF7,
  0xFF,0xEB,0xFF,0xDD,0xFF,0xB6,0xFF,0x7B,
  0xFF,0xFF,0x00,0xFF,0x00,0xFF,0x00,0xFF,
  0x08,0xFF,0x14,0xF7,0xA2,0xE3,0x41,0x41,
  0x7C,0x74,0x7C,0x74,0x7C,0x74,0x7C,0x74,
  0x7C,0x74,0x3C,0x34,0x3C,0x34,0x1C,0x14,
  0xFF,0x00,0xFF,0x7D,0xFF,0x55,0xFF,0x55,
  0xFF,0x75,0xFF,0x75,0xFF,0x75,0xFF,0x75,
  0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,
  0x41,0x41,0x41,0x41,0x41,0x41,0x41,0x41,
  0xFF,0x00,0xFF,0xF7,0xFF,0x05,0xFF,0x05,
  0xFF,0xF7,0xFF,0x87,0xFF,0x87,0xFF,0xF7,
  0x00,0x00,0xF8,0xF8,0xFE,0xFE,0xFF,0xFF,
  0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x7F,
  0xFF,0xBF,0x7F,0xDF,0x3F,0xEF,0x1F,0xF7,
  0x3F,0xF6,0x5F,0xD5,0x9F,0x92,0x1F,0x17,
  0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,
  0xC0,0xC0,0xE0,0xE0,0xF0,0xF0,0xF8,0xF8,
  0xF8,0xF8,0xFC,0xFC,0xFC,0xFC,0xFE,0xFE,
  0xFE,0x3E,0xFE,0xDE,0xFE,0x2E,0xFE,0xF6,
  0x1F,0x14,0x1F,0x14,0x1F,0x14,0x1F,0x14,
  0x1F,0x14,0x1F,0x14,0x1F,0x14,0x1F,0x14,
  0xFF,0x00,0xFF,0xDF,0xFF,0x53,0xFF,0x51,
  0xFF,0x51,0xFF,0x51,0xFF,0x51,0xFF,0x5F,
  0xFE,0x16,0xFE,0x16,0xFE,0x16,0xFE,0x16,
  0xFE,0x16,0xFC,0x14,0xFC,0x14,0xF8,0x18,
  0xF8,0x78,0xF0,0x70,0xE0,0x60,0xC0,0x40,
  0x80,0x00,0x80,0x00,0x80,0x00,0x80,0x00
};

/* End of I_MEMO.C */
