/****************************************************************************
 *  bank13.c                                                                *
 *                                                                          *
 *    GB-MEMO   V2.13    2002/05/19                                         *
 *                                                                          *
 *    Copyright(C)  1999 - 2002   TeamKNOx                                  *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank13\i_memo.h"
#include "bank13\s_memo.h"

extern void gb_memo( void *arg );

#define MAX_MEMO_PAGE   10
#define MAX_MEMO_COL    18
#define MAX_MEMO_ROW    10


/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank13\i_memo.c"
#include "bank13\c_memo.c"
#include "bank13\b_memo.c"
#include "bank13\s_memo.c"

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_13[FUNC_NUM] = {
  { "GB-MEMO ", "2.13", gb_memo, i_memo, i_memoCGBPal0c1, i_memoCGBPal0c2 }
};
struct description desc_13 = { FUNC_NUM, (struct function *)func_13 };

/*==========================================================================*
 |  data                                                                    |
 *==========================================================================*/
unsigned char mm_head[] = {
    0x01,0x02,0x09,0x02,0x02,0x09,0x02,0x02,0x09,0x02,
    0x02,0x09,0x02,0x02,0x09,0x02,0x02,0x09,0x02,0x03,
    0x04,0x00,0x0A,0x00,0x00,0x0A,0x00,0x00,0x0A,0x00,
    0x00,0x0A,0x00,0x00,0x0A,0x00,0x00,0x0A,0x00,0x07
};
unsigned char mm_line[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07
};
unsigned char mm_tail[] = {
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x2D,0x00,0x30,
    0x30,0x00,0x2D,0x00,0x00,0x00,0x00,0x00,0x00,0x07,
    0x05,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,
    0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x06
};
unsigned char mm_btn[] =  {
    0x00,0x00,0x00,0x80,0x81,0x82,0x89,0x8A,0x8B,0x92,
    0x93,0x94,0x9B,0x9C,0x9D,0xA4,0xA5,0xA6,0x00,0x00,
    0x00,0x00,0x00,0x83,0x84,0x85,0x8C,0x8D,0x8E,0x95,
    0x96,0x97,0x9E,0x9F,0xA0,0xA7,0xA8,0xA9,0x00,0x00,
    0x00,0x00,0x00,0x86,0x87,0x88,0x8F,0x90,0x91,0x98,
    0x99,0x9A,0xA1,0xA2,0xA3,0xAA,0xAB,0xAC,0x00,0x00
};
unsigned char mm_find[] = {
    0x01,0x02,0x02,0x02,0x46,0x69,0x6E,0x64,0x02,0x02,0x02,0x03,
    0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07,
    0x05,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x06
};
unsigned char mm_find_a[] = {
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};
unsigned char mm_find_btn[] = { 0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC };
unsigned char mm_find_btn_a[] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

unsigned char mm_yes_no[] = {
    0x01,0x02,0x02,0x44,0x65,0x6C,0x65,0x74,0x65,0x3F,0x02,0x02,0x02,0x03,
    0x04,0x00,0x10,0x11,0x11,0x12,0x00,0x00,0x10,0x11,0x11,0x12,0x00,0x07,
    0x04,0x00,0x15,0x16,0x16,0x17,0x00,0x00,0x15,0x16,0x16,0x17,0x00,0x07,
    0x05,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x06
};
unsigned char mm_yes_no_a[] = {
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x02,0x02,0x02,0x02,0x00,0x00,0x02,0x02,0x02,0x02,0x00,0x00,
    0x00,0x00,0x02,0x02,0x02,0x02,0x00,0x00,0x02,0x02,0x02,0x02,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};
unsigned char mm_yn_press[] = { 0x18,0x19,0x19,0x1A,0x1D,0x1E,0x1E,0x1F };
unsigned char mm_fore_a[] = { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };

unsigned char mm_space[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char mm_line_a[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
unsigned char mm_btn_a[]  = {
    0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,
    0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,
    0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0
};

unsigned char mm_rom_data[] = { "[ROM]" };
unsigned char mm_ram_data[] = { "[RAM]" };

struct palette memo_palette[] = {
    14, s_memoCGBPal0c0, s_memoCGBPal0c1, s_memoCGBPal0c2, s_memoCGBPal0c3
};

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void clear_memo_page( UWORD page )
{
    UWORD i;

    for( i=0; i<180; i++ ) {
        *(nvm_memo_data+(page*180+i)) = 0;
    }
}

struct cursor_info memo_yn_cursor = {  2,  1, 60, 80, 48, 0 };
UBYTE check_memo_yes_no()
{
    struct cursor_pos pos;
    UBYTE i;

    pos.x = 1;  pos.y = 0;
    set_bkg_tiles( 3, 6, 14, 4, mm_yes_no );
    set_bkg_attribute( 3, 6, 14, 4, mm_yes_no_a );
    for( i=0; i<5; i++ ) {
        set_sprite_tile( i+23, i+76 );
        if( i < 3 ) { /* yes */
            move_sprite( i+23, i*8+52, 76 );
        } else {
            move_sprite( i+23, i*9+78, 76 );
        }
    }
    pda_syscall( PDA_SET_CURSOR_MAP, &memo_yn_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 0 );
        if( keycode.make & J_A ) {
            set_bkg_tiles( cursor.x*6+5, 7, 4, 2, mm_yn_press );
            while( keycode.make & J_A ) {
                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                if( keycode.brk & J_EXIT )  return( 0 );
            }
            return( 1 - cursor.x );
        }
    }
    return( 0 );
}

void show_memo_line( unsigned char *p, UWORD page )
{
    UBYTE y;

    p += (page*180);
    for( y=0; y<MAX_MEMO_ROW; y++ ) {
        set_bkg_tiles( 1, y+2, MAX_MEMO_COL, 1, p );
        set_bkg_attribute( 0, y+2, 20, 1, mm_line_a );
        p += MAX_MEMO_COL;
    }
}

void show_rom_memo_line( UBYTE page )
{
    UBYTE y;
    unsigned char tmp_buff[18];
    struct memo_data data;

    data.page = page;
    data.buff = tmp_buff;
    for( y=0; y<MAX_MEMO_ROW; y++ ) {
        data.row = y;
        pda_syscall( PDA_GET_ROM_MEMO_DATA, &data );
        set_bkg_tiles( 1, y+2, MAX_MEMO_COL, 1, tmp_buff );
        set_bkg_attribute( 0, y+2, 20, 1, mm_line_a );
    }
}

void show_memo_page( UBYTE page )
{
    unsigned char tmp_buff[2];

    if( page < max_rom_memo_page ) {
        /* rom data */
        show_rom_memo_line( page );
        set_bkg_tiles( 14, 13, 5, 1, mm_rom_data );
    } else if( page < (10+max_rom_memo_page) ) {
        /* ram data */
        show_memo_line((unsigned char *)nvm_memo_data,page-max_rom_memo_page);
        set_bkg_tiles( 14, 13, 5, 1, mm_ram_data );
    }
    tmp_buff[0] = 0x30 + ((page+1)/MAX_MEMO_PAGE);
    tmp_buff[1] = 0x30 + ((page+1)%MAX_MEMO_PAGE);
    set_bkg_tiles( 9, 13, 2, 1, tmp_buff );
}

void show_memo_tag( UBYTE page )
{
    set_sprite_prop( 16, 6 );
    set_sprite_prop( 17, 6 );
    set_sprite_prop( 18, 6 );
    set_sprite_prop( 19, 6 );
    set_sprite_prop( 20, 6 );
    set_sprite_prop( 21, 6 );

    if( nvm->tag_mode ) {
        if( (nvm->tag_page) == page ) {
            set_sprite_tile( 16, 0+72 );  move_sprite( 16, 15, 108 );
            set_sprite_tile( 17, 1+72 );  move_sprite( 17, 15, 116 );
            set_sprite_tile( 18, 1+72 );  move_sprite( 18, 15, 124 );
            set_sprite_tile( 19, 1+72 );  move_sprite( 19, 15, 132 );
            set_sprite_tile( 20, 2+72 );  move_sprite( 20, 15, 140 );
            set_sprite_tile( 21, 3+72 );  move_sprite( 21, 15, 148 );
        } else {
            move_sprite( 16, 0, 0 );
            move_sprite( 17, 0, 0 );
            move_sprite( 18, 0, 0 );
            set_sprite_tile( 19, 1+72 );  move_sprite( 19, 15, 132 );
            set_sprite_tile( 20, 2+72 );  move_sprite( 20, 15, 140 );
            set_sprite_tile( 21, 3+72 );  move_sprite( 21, 15, 148 );
        }
    } else {
        set_sprite_tile( 16, 0+72 );  move_sprite( 16, 15, 140 );
        set_sprite_tile( 17, 1+72 );  move_sprite( 17, 15, 148 );
        set_sprite_tile( 18, 1+72 );  move_sprite( 18, 15, 156 );
        move_sprite( 19, 0, 0 );
        move_sprite( 20, 0, 0 );
        move_sprite( 21, 0, 0 );
    }
}

UBYTE comp_memo_strings( UWORD p, unsigned char *b, UBYTE n )
{
    UBYTE i;
    unsigned char *a;

    a = (unsigned char *)(nvm_memo_data+p);
    for( i=0; i<n; i++ ) {
        if( (*a++) != (*b++) )  return( 0 );
    }
    return( 1 );
}

UBYTE comp_rom_memo_strings( UWORD p, unsigned char *b, UBYTE n )
{
    UBYTE i;
    unsigned char *a;

    a = (unsigned char *)p;
    for( i=0; i<n; i++ ) {
        if( (*a++) != (*b++) )  return( 0 );
    }
    return( 1 );
}

UBYTE find_memo_strings( UWORD page, unsigned char *find_strings )
{
    UBYTE  flag;
    UWORD  i, j, k, n, p;
    unsigned char *q, tmp_buff[18];
    struct memo_data data;

    flag = 0;
    q = find_strings;
    for( n=0; n<10; n++ ) {
        if( (*q++) == 0 )  break;
    }
    for( i=0; i<(max_rom_memo_page+MAX_MEMO_PAGE); i++ ) {
        for( j=0; j<MAX_MEMO_ROW; j++ ) {
            if( page < max_rom_memo_page ) {
                /* rom data */
                data.page = page;
                data.buff = tmp_buff;
                data.row = j;
                pda_syscall( PDA_GET_ROM_MEMO_DATA, &data );
                p = (UWORD)tmp_buff;
                for( k=0; k<=(MAX_MEMO_COL-n); k++ ) { 
                    if( comp_rom_memo_strings(p+k,find_strings,n) ) {
                        if( !flag ) { /* found */
                            flag = 1;
                            show_memo_page( page );
                        }
                        set_bkg_attribute( k+1, j+2, n, 1, mm_fore_a );
                    }
                }
            } else {
                /* ram data */
                p = ((page-max_rom_memo_page)*MAX_MEMO_ROW+j)*MAX_MEMO_COL;
                for( k=0; k<=(MAX_MEMO_COL-n); k++ ) { 
                    if( comp_memo_strings(p+k,find_strings,n) ) {
                        if( !flag ) { /* found */
                            flag = 1;
                            show_memo_page( page );
                        }
                        set_bkg_attribute( k+1, j+2, n, 1, mm_fore_a );
                    }
                }
            }
        }
        if( flag )  return( page );
        page++;
        if( page > (max_rom_memo_page+MAX_MEMO_PAGE-1) )  page = 0;
    }
    return( max_rom_memo_page+MAX_MEMO_PAGE );
}

UBYTE dec_next_memo_page( UBYTE page )
{
    UBYTE  i, p, s;

    for( i=1; i<=page; i++ ) {
        s = page - i;
        if( s < max_rom_memo_page ) {
            p = s + 10;
        } else {
            p = s - max_rom_memo_page;
        }
        if( !(*(nvm_memo_flag+p/8)&(1<<(p%8))) )  return( s );
    }
    for( i=0; i<(max_rom_memo_page+10-page); i++ ) {
        s = max_rom_memo_page + 9 - i;
        if( s < max_rom_memo_page ) {
            p = s + 10;
        } else {
            p = s - max_rom_memo_page;
        }
        if( !(*(nvm_memo_flag+p/8)&(1<<(p%8))) )  return( s );
    }
    return( max_rom_memo_page );
}

UBYTE inc_next_memo_page( UBYTE page )
{
    UBYTE  i, p;

    for( i=page+1; i<max_rom_memo_page+10; i++ ) {
        if( i < max_rom_memo_page ) {
            p = i + 10;
        } else {
            p = i - max_rom_memo_page;
        }
        if( !(*(nvm_memo_flag+p/8)&(1<<(p%8))) )  return( i );
    }
    for( i=0; i<=page; i++ ) {
        if( i < max_rom_memo_page ) {
            p = i + 10;
        } else {
            p = i - max_rom_memo_page;
        }
        if( !(*(nvm_memo_flag+p/8)&(1<<(p%8))) )  return( i );
    }
    return( max_rom_memo_page );
}

void init_memo()
{
    UBYTE  i;
    UWORD  j;
    struct button btn;
    struct palette palette;

    pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

    /* setup bkg data */
    set_bkg_data( 1, 10, c_memo );
    set_sprite_data( 72, 9, s_memo );
    palette.no = memo_palette[0].no;
    palette.color1 = memo_palette[0].color1;
    palette.color2 = memo_palette[0].color2;
    palette.color3 = memo_palette[0].color3;
    pda_syscall( PDA_SET_PALETTE, &palette );

    set_bkg_tiles( 0, 0, 20, 2, mm_head );
    for( i=0; i<11; i++ ) {
        set_bkg_tiles( 0, i+2, 20, 1, mm_line );
        set_bkg_attribute( 0, i+2, 20, 1, mm_line_a );
    }
    set_bkg_tiles( 0, 13, 20, 2, mm_tail );

    for( i=0, j=0; i<5; i++ ) {
        btn.no = i;
        btn.on = 0; // off
        btn.data = b_memo + j;
        pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
        j += 128;
    }
    set_bkg_tiles( 0, 15, 20, 3, mm_btn );
    set_bkg_attribute( 0, 15, 20, 3, mm_btn_a );
}

struct cursor_info memo1_cursor = {  6,  1, 13, 144, 24, 0 };
struct cursor_info memo2_cursor = {  4,  1, 69, 144, 24, 0 };
struct cursor_info memo3_cursor = { 18, 12, 14,  36,  8, 8 };

void gb_memo( void *arg )
{
    UBYTE  i, x, y, page, save, line, flag;
    UWORD  p;
    unsigned char find_strings[10];
    struct cursor_pos pos;
    struct button btn;
    struct input_string input_string;

    init_memo();
    if( nvm->tag_mode )  page = nvm->tag_page;
    else                 page = inc_next_memo_page( max_rom_memo_page+9 );
    show_memo_page( page );
    show_memo_tag( page );
    for( i=0; i<18; i++ )  find_strings[i]=0;

    pos.x = 2;  pos.y = 0;
    pda_syscall( PDA_SET_CURSOR_MAP, &memo1_cursor );
    pda_syscall( PDA_INIT_CURSOR, &pos );
    while( !(keycode.brk & J_EXIT) ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return;
        if( page < max_rom_memo_page ) {  /* skip */
            if( cursor.x == 3 ) {
                cursor.x = 5;
            } else if( cursor.x == 4 ) {
                cursor.x = 2;
            }
        }
        pos.x = cursor.x;
        if( keycode.make & J_A ) {
            if( pos.x ) {
                btn.no = pos.x-1;
                btn.on = 1; // on
                btn.data = b_memo+btn.no*128+64;
                pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
                while( keycode.make & J_A ) {
                    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                    if( keycode.brk & J_EXIT )  return;
                }
                switch( pos.x ) {
                  case 1: /* page-- */
                    page = dec_next_memo_page( page );
                    show_memo_page( page );
                    show_memo_tag( page );
                    break;
                  case 2: /* page++ (max<99?) */
                    page = inc_next_memo_page( page );
                    show_memo_page( page );
                    show_memo_tag( page );
                    break;
                  case 3: /* edit */
                    if( page < max_rom_memo_page )  break;  /* rom data */
                    y = 2;
                    caps_lock = 0;  keypad_pos.x = 7;  keypad_pos.y = 2;
                    while( !(keycode.brk & J_EXIT) ) {
                        p = page - max_rom_memo_page;
                        p = (p*MAX_MEMO_ROW+(y-2))*MAX_MEMO_COL;
                        line = 0;
                        for( x=0; x<MAX_MEMO_COL; x++ ) {
                            if( *(nvm_memo_data+(p+x)) ) {
                                line = x+1;
                            }
                        }
                        input_string.x = 1;
                        input_string.y = y;
                        input_string.max = MAX_MEMO_COL;
                        input_string.def_x = line;
                        input_string.mode = 0x06;
                        input_string.data = (unsigned char *)nvm_memo_data+p;
                        pda_syscall( PDA_INPUT_STRING, &input_string );
                        y++; if( y > (MAX_MEMO_ROW+1) )  break;
                        if( keycode.brk & J_SHIFT_A )  break;
                    }
                    pda_syscall( PDA_SET_CURSOR_MAP, &memo1_cursor );
                    pda_syscall( PDA_INIT_CURSOR, &pos );
                    init_memo();
                    show_memo_page( page );
                    show_memo_tag( page );
                    break;
                  case 4: /* clear */
                    if( page < max_rom_memo_page ) {
                        /* rom data*/
                    } else {
                        /* ram data*/
                        if( check_memo_yes_no() ) {
                            clear_memo_page( page-max_rom_memo_page );
                        }
                        pda_syscall( PDA_SET_CURSOR_MAP, &memo1_cursor );
                        pda_syscall( PDA_INIT_CURSOR, &pos );
                        for( i=0; i<5; i++ ) {
                            move_sprite( i+23, 0, 0 );
                        }
                        show_memo_page( page );
                    }
                    break;
                  case 5: /* find */
                    set_bkg_tiles( 4, 5, 12, 3, mm_find );
                    set_bkg_attribute( 4, 5, 12, 3, mm_find_a );
                    caps_lock = 0;  keypad_pos.x = 7;  keypad_pos.y = 2;
                    input_string.x = 5;
                    input_string.y = 6;
                    input_string.max = 10;
                    input_string.mode = 0;
                    input_string.data = find_strings;
                    input_string.def_x = 0;
                    for( i=1; i<8; i++ ) {
                        if( find_strings[i] ) {
                            input_string.def_x = i+1;
                            keypad_pos.x = 12;
                        }
                    }
                    pda_syscall( PDA_INPUT_STRING, &input_string );
                    init_memo();
                    show_memo_page( page );
                    show_memo_tag( page );
                    pda_syscall( PDA_SET_CURSOR_MAP, &memo1_cursor );
                    pda_syscall( PDA_INIT_CURSOR, &pos );
                    pda_syscall( PDA_SHOW_CURSOR, (void *)0 );
                    if( !((find_strings[1]==0)||(keycode.brk&J_EXIT)) ) {
                        /* find */
                        save = find_memo_strings( page, find_strings );
                        if( save < (max_rom_memo_page+MAX_MEMO_PAGE) ) {
                            page = save;
                            show_memo_tag( page );
                            save = page + 1;
                            if( save > (max_rom_memo_page+MAX_MEMO_PAGE-1) ) {
                                save = 0;
                            }
                            set_bkg_tiles( 3, 15, 12, 3, mm_find );
                            set_bkg_tiles( 4, 16, 10, 1, find_strings );
                            set_bkg_attribute( 3, 15, 12, 3, mm_find_btn_a );
                            while( (cursor.x==5) && !(keycode.brk&J_B) ) {
                                pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
                                if( keycode.brk & J_EXIT )  return;
                                if( keycode.make & J_A ) {
                                    btn.on = 1; // on
                                    btn.data = b_memo+btn.no*128+64;
                                    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
                                    while( keycode.make & J_A ) {
                                        pda_syscall(PDA_MOVE_CURSOR,(void *)0);
                                        if( keycode.brk & J_EXIT )  return;
                                    }
                                    /* find */
                                    page=find_memo_strings(save,find_strings);
                                    show_memo_tag( page );
                                    save = page + 1;
                                    if( save > 
                                      (max_rom_memo_page+MAX_MEMO_PAGE-1) ) {
                                        save = 0;
                                    }
                                    btn.on = 0; // off
                                    btn.data = b_memo+btn.no*128;
                                    pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
                                }
                            }
                            set_bkg_tiles( 0, 15, 20, 3, mm_btn );
                            set_bkg_attribute( 0, 15, 20, 3, mm_btn_a );
                        }
                    }
                    break;
                }
                btn.on = 0; // off
                btn.data = b_memo+btn.no*128;
                pda_syscall( PDA_MAKE_3X3BUTTON, &btn );
            }
        }
        if( keycode.brk & J_A ) {
            keycode.brk &= ~J_A;
            if( pos.x == 0 ) {
                if( nvm->tag_mode ) {
                    if( (nvm->tag_page) == page ) {
                        nvm->tag_mode = 0;
                    } else {
                        page = nvm->tag_page;
                        show_memo_page( page );
                    }
                } else {
                    nvm->tag_mode = 1;
                    nvm->tag_page = page;
                }
                show_memo_tag( page );
            }
        }
    }
}

/* EOF */