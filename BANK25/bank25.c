/****************************************************************************
 *  bank25.c                                                                *
 *                                                                          *
 *    Mine Sweeper  V2.00    2002/05/19                                     *
 *                                                                          *
 *    Copyright(C)  2002   TeamKNOx                                         *
 ****************************************************************************/

#include <gb.h>
#include "pda.h"
#include "bank25\i_mine.h"
#include "bank25\c_mine.h"
#include "bank25\s_mine.h"

extern void mine_sweeper();

/*==========================================================================*
 |  descripters                                                             |
 *==========================================================================*/
#define FUNC_NUM        1
struct function func_25[FUNC_NUM] = {
  { "SWEEPER ","2.00",mine_sweeper,i_mine,i_mineCGBPal0c1,i_mineCGBPal0c2 }
};
struct description desc_25 = { FUNC_NUM, (struct function *)func_25 };

/*==========================================================================*
 |  definitions                                                             |
 *==========================================================================*/
#define MINE_MODE_DEMO    0
#define MINE_MODE_PLAY    1
#define MINE_MODE_REPLAY  2

/* game level */
#define MINE_LV_EASY      0
#define MINE_LV_NORMAL    1
#define MINE_LV_HARD      2
#define MINE_LV_ORIGINAL  3

/* attributes */
#define MINE_A_CHECK      0x01
#define MINE_A_NOTICE     0x02
#define MINE_A_MASK       0x03
#define MINE_A_OPENED     0x40
#define MINE_A_BLOCK      0x50
#define MINE_A_BANG       0x80
#define MINE_A_CLR_MASK   0x41

/* screen */
#define MINE_MAX_X        18
#define MINE_MAX_Y        15
typedef struct map_t {
  UBYTE map[1][MINE_MAX_X+2];
} _map_t;

typedef struct mine_t {
  UBYTE  level;
  UWORD  mine_num;
  UWORD  rest_num;
  UWORD  pane_num;
  struct cursor_pos size;
  struct cursor_pos pos;
  struct map_t *p;
} _mine_t;



/*==========================================================================*
 |  resources                                                               |
 *==========================================================================*/
#include "bank25\i_mine.c"
#include "bank25\c_mine.c"
#include "bank25\s_mine.c"

unsigned char mine_head[] = {
    0x91,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,
    0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x93
};
unsigned char mine_line[] = {
    0x94,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
    0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x94
};
unsigned char mine_tail[] = {
    0x95,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,
    0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x96
};
unsigned char mine_line_a[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
UBYTE mine_sprite_tile[] = {
    0x0,0x2,0x8,0xA,0x1,0x3,0x9,0xB,0x4,0x6,0xC,0xE,0x5,0x7,0xD,0xF
};
unsigned char mine_title[]   = "MINE SWEEPER";
unsigned char mine_level[]   = "GAME LEVEL";
unsigned char mine_easy[]    = "  EASY  ";
unsigned char mine_normal[]  = " NORMAL ";
unsigned char mine_hard[]    = "  HARD  ";
unsigned char mine_original[]= "ORIGINAL";
unsigned char mine_credit[]  = "`TeamKNOX 2002";
unsigned char mine_blue_a[]  = { 4,4,4,4,4,4,4,4,4,4,4,4,4,4 };
unsigned char mine_green_a[] = { 5,5,5,5,5,5,5,5,5,5,5,5,5,5 };
unsigned char mine_red_a[]   = { 6,6,6,6,6,6,6,6,6,6,6,6,6,6 };

UWORD mine_palette[] = {
    c_mineCGBPal0c0, c_mineCGBPal0c1, c_mineCGBPal0c2, c_mineCGBPal0c3,
    c_mineCGBPal1c0, c_mineCGBPal1c1, c_mineCGBPal1c2, c_mineCGBPal1c3,
    c_mineCGBPal2c0, c_mineCGBPal2c1, c_mineCGBPal2c2, c_mineCGBPal2c3,
    c_mineCGBPal3c0, c_mineCGBPal3c1, c_mineCGBPal3c2, c_mineCGBPal3c3,
    c_mineCGBPal4c0, c_mineCGBPal4c1, c_mineCGBPal4c2, c_mineCGBPal4c3,
    c_mineCGBPal5c0, c_mineCGBPal5c1, c_mineCGBPal5c2, c_mineCGBPal5c3,
    c_mineCGBPal6c0, c_mineCGBPal6c1, c_mineCGBPal6c2, c_mineCGBPal6c3,
    c_mineCGBPal7c0, c_mineCGBPal7c1, c_mineCGBPal7c2, c_mineCGBPal7c3
};
UWORD mine_palette_s[] = {
    s_mineCGBPal0c1, s_mineCGBPal0c2, s_mineCGBPal0c3,
    s_mineCGBPal1c1, s_mineCGBPal1c2, s_mineCGBPal1c3,
    s_mineCGBPal2c1, s_mineCGBPal2c2, s_mineCGBPal2c3,
    s_mineCGBPal3c1, s_mineCGBPal3c2, s_mineCGBPal3c3,
    s_mineCGBPal4c1, s_mineCGBPal4c2, s_mineCGBPal4c3,
    s_mineCGBPal5c1, s_mineCGBPal5c2, s_mineCGBPal5c3
};

/*==========================================================================*
 |  functions                                                               |
 *==========================================================================*/
void mine_put_tile( UBYTE x, UBYTE y, UBYTE a )
{
  unsigned char buff[1];

  if( x < 1 )           return;
  if( x > MINE_MAX_X )  return;
  if( y < 1 )           return;
  if( y > MINE_MAX_Y )  return;

  if( a == MINE_A_BLOCK )  a = 16;
  buff[0] = 0x80+a;  y++;
  set_bkg_tiles( x, y, 1, 1, buff );
  set_bkg_attribute( x, y, 1, 1, &c_mineCGB[a] );
}

UBYTE mine_get_map( struct mine_t *m, UBYTE x, UBYTE y )
{
  return( (m->p->map[y][x]) );
}

void mine_put_map( struct mine_t *m, UBYTE x, UBYTE y, UBYTE a )
{
  (m->p->map[y][x]) = a;
}

void mine_num( UBYTE x, UBYTE y, UWORD v )
{
  UBYTE  x100, x10, x1;
  unsigned char buff[3];

  x100 = (v/100)%10;
  x10  = (v/10)%10;
  x1   = v%10;

  if( x100 ) {
    buff[0] = 0x30 + x100;
    buff[1] = 0x30 + x10;
  } else {
    buff[0] = 0x20;
    if( x10 )  buff[1] = 0x30 + x10;
    else       buff[1] = 0x20;    
  }
  buff[2] = 0x30 + x1;
  set_bkg_tiles( x, y, 3, 1, buff );
}

void mine_hex( UBYTE x, UBYTE y, UBYTE v )
{
  UBYTE  x10, x1;
  unsigned char buff[2];

  x10  = (v/16)%16;
  x1   = v%16;

  buff[0] = 0x30 + x10;  if( x10 > 9 )  buff[0] += 7;
  buff[1] = 0x30 +  x1;  if(  x1 > 9 )  buff[1] += 7;
  set_bkg_tiles( x, y, 2, 1, buff );
}

void mine_clear_game( struct mine_t *m )
{
  UBYTE  i, a, x, y;

  for( i=0; i<4; i++ ) {
    for( y=1; y<=MINE_MAX_Y; y++ ) {
      for( x=1; x<=MINE_MAX_X; x++ ) {
        a = m->p->map[y][x];
        if( a == 0x81 ) {
          mine_put_tile( x, y, i+24 );
        }
      }
    }
  }
  mine_num( 17, 0, 0 );
}

void mine_info( struct mine_t *m )
{
  mine_num( 0, 0, (m->pane_num) );
  mine_num( 17, 0, (m->rest_num) );
}

UBYTE mine_check_map( struct mine_t *m, UBYTE x, UBYTE y )
{
  UBYTE  a, i, j;

  if( (m->p->map[y][x]) & MINE_A_BANG ) {
    return( 3 );
  }
  a = 6;
  for( j=0; j<3; j++ ) {
    for( i=0; i<3; i++ ) {
      if( (m->p->map[y+j-1][x+i-1]) & MINE_A_BANG ) {
        a++;
      }
    }
  }
  return( a );
}

void mine_all_bang( struct mine_t *m, UBYTE sx, UBYTE sy )
{
  UBYTE  i, a, c, x, y;
  unsigned char *p;

  delay( 100 );
  sx = sx*8 - 4;
  sy = sy*8 + 12;
  for( i=0; i<16; i++ ) {
    set_sprite_tile( i+6, mine_sprite_tile[i]+112 );
    move_sprite( i+6, (i%4)*8+sx, (i/4)*8+sy );
  }
  p = s_mine;
  for( x=0; x<8; x++ ) {
    c = s_mineCGB[x];
    set_sprite_data( 112, 16, p );
    for( i=0; i<16; i++ ) {
      set_sprite_prop( i+6, c+2 );
    }
    delay( 80 );
    p += 256;
  }
  for( i=0; i<16; i++ )  move_sprite( i+6, 0, 0 );
  delay( 100 );

  for( y=1; y<=MINE_MAX_Y; y++ ) {
    for( x=1; x<=MINE_MAX_X; x++ ) {
      a = m->p->map[y][x];
      if( !(a & MINE_A_OPENED) ) {
        if( a & MINE_A_BANG ) {
          if( (a & MINE_A_MASK) != MINE_A_CHECK ) {
            mine_put_tile( x, y, 5 );
          }
        } else if( (a&MINE_A_MASK) == MINE_A_CHECK ) {
          mine_put_tile( x, y, 4 );
        }
      }
    }
  }
}

void mine_redraw( struct mine_t *m )
{
  UBYTE  a, x, y;

  for( y=1; y<=MINE_MAX_Y; y++ ) {
    for( x=1; x<=MINE_MAX_X; x++ ) {
      a = m->p->map[y][x];
      if( a & MINE_A_OPENED ) {
        mine_put_tile( x, y, mine_check_map(m,x,y) );
      } else {
        mine_put_tile( x, y, (a & MINE_A_MASK) );
      }
    }
  }
}

WORD mine_check_right( struct mine_t *m, WORD x, WORD y )
{
  while( x < MINE_MAX_X ) {
    x++;
    if( mine_check_map(m,x,y) != 6 ) {
      x--;
      return( x );
    }
  }
  return( x );
}

WORD mine_check_left( struct mine_t *m, WORD x, WORD y )
{
  while( x > 1 ) {
    x--;
    if( mine_check_map(m,x,y) != 6 ) {
      x++;
      return( x );
    }
  }
  return( x );
}

void mine_safety_zone( struct mine_t *m, UBYTE x, UBYTE y )
{ /* seed fill */
  UBYTE  b, c, d, f, s, e;
  WORD   i, lx, rx, uy, dy;
  struct cursor_pos buff[120];

  s = 0;
  e = 1;
  buff[s].x = x;
  buff[s].y = y;

  while( s != e ) {
    lx = (WORD)buff[s].x;
    rx = (WORD)buff[s].x;
    uy = (WORD)buff[s].y;
    dy = (WORD)buff[s].y;
    s++;  if( s == 120 )  s = 0;
    if( !(mine_get_map(m,lx,uy) & MINE_A_OPENED) ) {
      rx = mine_check_right( m, rx, uy );
      lx = mine_check_left( m, lx, uy );
      for( i=lx; i<=rx; i++ ) {
        b = mine_get_map( m, i, uy );
        if( !(b & MINE_A_CLR_MASK) ) {
          b |= MINE_A_OPENED;
          (m->pane_num)--;
          mine_num( 0, 0, (m->pane_num) );
          mine_put_map( m, i, uy, b );
          mine_put_tile( i, uy, mine_check_map(m,i,uy) );
        }
      }
      if( lx > 1 ) {
        lx--;
        b = mine_get_map( m, lx, uy );
        if( !(b & MINE_A_CLR_MASK) ) {
          b |= MINE_A_OPENED;
          (m->pane_num)--;
          mine_num( 0, 0, (m->pane_num) );
          mine_put_map( m, lx, uy, b );
          mine_put_tile( lx, uy, mine_check_map(m,lx,uy) );
        }
      }
      if( rx < MINE_MAX_X ) {
        rx++;
        b = mine_get_map( m, rx, uy );
        if( !(b & MINE_A_CLR_MASK) ) {
          b |= MINE_A_OPENED;
          (m->pane_num)--;
          mine_num( 0, 0, (m->pane_num) );
          mine_put_map( m, rx, uy, b );
          mine_put_tile( rx, uy, mine_check_map(m,rx,uy) );
        }
      }

      if( uy > 1 ) {
        uy--;
        f = 0;
        for( i=lx; i<=rx; i++ ) {
          d = mine_check_map(m,i,uy);
          if( f ) {
            if( d != 6 ) {
              f = 0;
              buff[e].x = (UBYTE)(i - 1);
              buff[e].y = (UBYTE)uy;
              e++;  if( e == 120 )  e = 0;
            }
          } else if( d == 6 ) {
            f = 1;
          } else {
            b = mine_get_map( m, i, uy );
            if( !(b & MINE_A_CLR_MASK) ) {
              b |= MINE_A_OPENED;
              (m->pane_num)--;
              mine_num( 0, 0, (m->pane_num) );
              mine_put_map( m, i, uy, b );
              mine_put_tile( i, uy, mine_check_map(m,i,uy) );
            }
          }
        }
        if( f == 1 ) {
          buff[e].x = (UBYTE)rx;
          buff[e].y = (UBYTE)uy;
          e++;  if( e == 120 )  e = 0;
        }
      }

      if( dy < MINE_MAX_Y ) {
        dy++;
        f = 0;
        for( i=lx; i<=rx; i++ ) {
          d = mine_check_map(m,i,dy);
          if( f ) {
            if( d != 6 ) {
              f = 0;
              buff[e].x = (UBYTE)(i - 1);
              buff[e].y = (UBYTE)dy;
              e++;  if( e == 120 )  e = 0;
            }
          } else if( d == 6 ) {
            f = 1;
          } else {
            b = mine_get_map( m, i, dy );
            if( !(b & MINE_A_CLR_MASK) ) {
              b |= MINE_A_OPENED;
              (m->pane_num)--;
              mine_num( 0, 0, (m->pane_num) );
              mine_put_map( m, i, dy, b );
              mine_put_tile( i, dy, mine_check_map(m,i,dy) );
            }
          }
        }
        if( f == 1 ) {
          buff[e].x = (UBYTE)rx;
          buff[e].y = (UBYTE)dy;
          e++;  if( e == 120 )  e = 0;
        }
      }
    }
  }
}

void mine_scan_map( struct mine_t *m )
{
  UBYTE  a, i, j, x, y;

  for( y=1; y<=MINE_MAX_Y; y++ ) {
    for( x=1; x<=MINE_MAX_X; x++ ) {
      i = mine_check_map( m, x, y );
      if( i == 6 )  mine_safety_zone( m, x, y );
    }
  }
}

void mine_init_map_data( struct mine_t *m )
{
  UBYTE  a, i, j, x, y;
  UWORD  palette[4];

  m->pane_num = (UWORD)(m->size.x) * (m->size.y);
  m->rest_num = 0;
  switch( (m->level) ) {
    case MINE_LV_EASY:
      m->mine_num = (m->pane_num) / 10;
      break;
    case MINE_LV_NORMAL:
      m->mine_num = (m->pane_num) / 7;
      break;
    case MINE_LV_HARD:
    default:
      m->mine_num = (m->pane_num) / 4;
      break;
  }

  for( y=0; y<(MINE_MAX_Y+2); y++ ) {
    for( x=0; x<(MINE_MAX_X+2); x++ ) {
      m->p->map[y][x] = 0x00;
    }
  }
  for( y=1; y<(MINE_MAX_Y+1); y++ ) {
    for( x=1; x<(MINE_MAX_X+1); x++ ) {
      if( ((m->level)==MINE_LV_ORIGINAL) && ((rand()%10)==0) ) {
        m->p->map[y][x] = MINE_A_BLOCK;
        mine_put_tile( x, y, MINE_A_BLOCK );
        m->pane_num--;
      } else {
        mine_put_tile( x, y, 0 );
      }
    }
  }

  for( i=0; i<(m->mine_num); i++ ) {
    x = rand() % (m->size.x) + 1;
    y = rand() % (m->size.y) + 1;
    if( (m->p->map[y][x]) == 0x00 ) {
      m->p->map[y][x] = (UBYTE)MINE_A_BANG;
      m->rest_num++;
      mine_num( 17, 0, (m->rest_num) );
    } else {
      i--;
    }
  }
  m->pane_num -= m->rest_num;
  mine_num( 0, 0, (m->pane_num) );
  for( i=4; i<7 ; i++ ) {
    palette[0] = mine_palette[i*4];
    palette[1] = mine_palette[i*4+1];
    palette[2] = mine_palette[i*4+2];
    palette[3] = mine_palette[i*4+3];
    set_bkg_palette( i, 1, palette );
  }
  if( (m->level) == MINE_LV_ORIGINAL ) {
    mine_scan_map( m );  // hint
  }
}

/*--------------------------------------------------------------------------*
 |  main loop                                                               |
 *--------------------------------------------------------------------------*/
struct cursor_info mine_cursor = { 18, 15, 14, 36, 8, 8 };
UBYTE mine_play( struct mine_t *m )
{
  UBYTE  a, b, x, y;
  struct cursor_pos pos;

  mine_init_map_data( m );

  /* loop */
  pos.x = MINE_MAX_X / 2;
  pos.y = MINE_MAX_Y / 2;
  pda_syscall( PDA_SET_CURSOR_MAP, &mine_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    x = cursor.x + 1;
    y = cursor.y + 1;
    a = mine_get_map( m, x, y );
    if( keycode.make & J_START ) {
      mine_hex( 0, 17, a );  // hint
      while( keycode.make & J_START ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
        if( keycode.brk & J_EXIT )  return( 0 );
      }
      set_bkg_tiles( 0, 17, 20, 1, mine_tail );
//    mine_redraw( m );
    }
    if( keycode.brk & J_A ) {
      keycode.brk &= ~J_A;
      if( !(a & (MINE_A_MASK|MINE_A_OPENED)) ) {
        b = mine_check_map( m, x, y );
        if( b == 6 ) {
          mine_safety_zone( m, x, y );
        } else {
          a |= MINE_A_OPENED;
          mine_put_map( m, x, y, a );
          mine_put_tile( x, y, b );
          if( b == 3 ) {  // bang!!
            mine_all_bang( m, x, y );
            return( MINE_MODE_REPLAY );
          }
          (m->pane_num)--;
          mine_num( 0, 0, (m->pane_num) );
        }
        if( !(m->pane_num) ) {  // clear!!
          mine_clear_game( m );
          return( MINE_MODE_REPLAY );
        }
      }
    }
    if( keycode.brk & (J_B|J_SELECT) ) {
      keycode.brk &= ~(J_B|J_SELECT);
      if( !(a & MINE_A_OPENED) ) {
        b = a & 0xFC;
        a &= MINE_A_MASK;
        a++;  if( a > 2 )  a = 0;
        if( a == MINE_A_CHECK ) {
          if( m->rest_num )  (m->rest_num)--;
          else               a = MINE_A_NOTICE;
        } else if( a == MINE_A_NOTICE ) {
          (m->rest_num)++;
        }
        mine_num( 17, 0, (m->rest_num) );
        mine_put_tile( x, y, a );
        b |= a;
        mine_put_map( m, x, y, b );
      }
    }
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  replay                                                                  |
 *--------------------------------------------------------------------------*/
UBYTE mine_replay( struct mine_t *m )
{
  fixed seed;

  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( keycode.make & J_START ) {
      /* init rand */
      seed.b.l = DIV_REG;
      while( keycode.make & J_START ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
      }
      seed.b.h = DIV_REG;
      initarand( seed.w );
      return( MINE_MODE_PLAY );
    }
    if( keycode.brk & J_SELECT ) {
      return( MINE_MODE_DEMO );
    }
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  demo                                                                    |
 *--------------------------------------------------------------------------*/
struct cursor_info mine_demo_cursor = { 1, 4, 82, 68, 0, 16 };
UBYTE mine_demo( struct mine_t *m )
{
  UBYTE  i;
  fixed  seed;
  UWORD  palette[4];
  struct cursor_pos pos;

  for( i=4; i<7 ; i++ ) {
    palette[0] = mine_palette[18];
    palette[3] = mine_palette[i*4];
    set_bkg_palette( i, 1, palette );
  }
  set_bkg_tiles( 0, 1, 20, 1, mine_head );
  for( i=0; i<15; i++ ) {
    set_bkg_tiles( 0, i+2, 20, 1, mine_line );
    set_bkg_attribute( 0, i+2, 20, 1, mine_line_a );
  }
  set_bkg_tiles( 0, 17, 20, 1, mine_tail );
  set_bkg_tiles(     5,  4, 10, 1, mine_level );
  set_bkg_attribute( 5,  4, 10, 1, mine_green_a );
  set_bkg_tiles(     6,  6,  8, 1, mine_easy );
  set_bkg_attribute( 6,  6,  8, 1, mine_blue_a );
  set_bkg_tiles(     6,  8,  8, 1, mine_normal );
  set_bkg_attribute( 6,  8,  8, 1, mine_blue_a );
  set_bkg_tiles(     6, 10,  8, 1, mine_hard );
  set_bkg_attribute( 6, 10,  8, 1, mine_blue_a );
  set_bkg_tiles(     6, 12,  8, 1, mine_original );
  set_bkg_attribute( 6, 12,  8, 1, mine_blue_a );
  set_bkg_tiles(     3, 15, 14, 1, mine_credit );
  set_bkg_attribute( 3, 15, 14, 1, mine_green_a );
  pos.x = 0;
  pos.y = (m->level);
  set_bkg_attribute( 6, 6+(pos.y)*2, 8, 1, mine_red_a );

  pda_syscall( PDA_SET_CURSOR_MAP, &mine_demo_cursor );
  pda_syscall( PDA_INIT_CURSOR, &pos );
  while( !(keycode.brk & J_EXIT) ) {
    pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
    if( pos.y != cursor.y ) {
      set_bkg_attribute( 6, 6+(pos.y)*2, 8, 1, mine_blue_a );
      set_bkg_attribute( 6, 6+(cursor.y)*2, 8, 1, mine_red_a );
      pos.y = cursor.y;
    }
    if( keycode.make & J_START ) {
      /* init rand */
      seed.b.l = DIV_REG;
      while( keycode.make & J_START ) {
        pda_syscall( PDA_MOVE_CURSOR, (void *)0 );
      }
      seed.b.h = DIV_REG;
      initarand( seed.w );
      m->level = cursor.y;
      return( MINE_MODE_PLAY );
    }
  }
  return( 0 );
}

/*--------------------------------------------------------------------------*
 |  initialize                                                              |
 *--------------------------------------------------------------------------*/
void mine_init( struct mine_t *m )
{
  UBYTE  i;
  UWORD  palette[4];

  pda_syscall( PDA_CLEAR_SCREEN, (void *)0 );

  /* setup bkg data */
  set_bkg_data( 128, 48, c_mine );
  for( i=0; i<8 ; i++ ) {
    if( i )  palette[0] = mine_palette[i*4];
    else     palette[0] = ~nvm->back_color;
    palette[1] = mine_palette[i*4+1];
    palette[2] = mine_palette[i*4+2];
    palette[3] = mine_palette[i*4+3];
    set_bkg_palette( i, 1, palette );
  }
  for( i=0; i<6 ; i++ ) {
    palette[1] = mine_palette_s[i*3];
    palette[2] = mine_palette_s[i*3+1];
    palette[3] = mine_palette_s[i*3+2];
    set_sprite_palette( i+2, 1, palette );
  }
  for( i=0; i<16; i++ ) {
    move_sprite( i+6, 0, 0 );
    set_sprite_prop( i+6, 3 );
  }

  /* draw sheet */
  set_bkg_tiles( 4, 0, 12, 1, mine_title );

  m->size.x = MINE_MAX_X;
  m->size.y = MINE_MAX_Y;
  m->level  = MINE_LV_NORMAL;
}

/*==========================================================================*
 |  main                                                                    |
 *==========================================================================*/
void mine_sweeper()
{
  UBYTE  mode;
  struct mine_t m;
  UBYTE  map[MINE_MAX_Y+2][MINE_MAX_X+2];

  /* make map */
  m.p = (struct map_t *)map;
  mine_init( &m );

  mode = MINE_MODE_DEMO;
  while( !(keycode.brk & J_EXIT) ) {
    switch( mode ) {
      case MINE_MODE_DEMO:
        mode = mine_demo( &m );
        break;
      case MINE_MODE_PLAY:
        mode = mine_play( &m );
        break;
      case MINE_MODE_REPLAY:
        mode = mine_replay( &m );
        break;
    }
  }
}

/* EOF */
