/*

 C_MINE.H

 Include File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 47

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : 1 Byte per entry.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/


/* Bank of tiles. */
#define c_mineBank 0

/* Super Gameboy palette 0 */
#define c_mineSGBPal0c0 6076
#define c_mineSGBPal0c1 8935
#define c_mineSGBPal0c2 6596
#define c_mineSGBPal0c3 5344

/* Super Gameboy palette 1 */
#define c_mineSGBPal1c0 6076
#define c_mineSGBPal1c1 8935
#define c_mineSGBPal1c2 6596
#define c_mineSGBPal1c3 5344

/* Super Gameboy palette 2 */
#define c_mineSGBPal2c0 6076
#define c_mineSGBPal2c1 8935
#define c_mineSGBPal2c2 6596
#define c_mineSGBPal2c3 5344

/* Super Gameboy palette 3 */
#define c_mineSGBPal3c0 6076
#define c_mineSGBPal3c1 8935
#define c_mineSGBPal3c2 6596
#define c_mineSGBPal3c3 5344

/* Gameboy Color palette 0 */
#define c_mineCGBPal0c0 25344
#define c_mineCGBPal0c1 32767
#define c_mineCGBPal0c2 24311
#define c_mineCGBPal0c3 0

/* Gameboy Color palette 1 */
#define c_mineCGBPal1c0 11647
#define c_mineCGBPal1c1 32767
#define c_mineCGBPal1c2 24311
#define c_mineCGBPal1c3 0

/* Gameboy Color palette 2 */
#define c_mineCGBPal2c0 32107
#define c_mineCGBPal2c1 32767
#define c_mineCGBPal2c2 24311
#define c_mineCGBPal2c3 0

/* Gameboy Color palette 3 */
#define c_mineCGBPal3c0 11947
#define c_mineCGBPal3c1 32767
#define c_mineCGBPal3c2 24311
#define c_mineCGBPal3c3 0

/* Gameboy Color palette 4 */
#define c_mineCGBPal4c0 31744
#define c_mineCGBPal4c1 15
#define c_mineCGBPal4c2 20083
#define c_mineCGBPal4c3 0

/* Gameboy Color palette 5 */
#define c_mineCGBPal5c0 480
#define c_mineCGBPal5c1 15840
#define c_mineCGBPal5c2 20083
#define c_mineCGBPal5c3 0

/* Gameboy Color palette 6 */
#define c_mineCGBPal6c0 31
#define c_mineCGBPal6c1 15375
#define c_mineCGBPal6c2 20083
#define c_mineCGBPal6c3 0

/* Gameboy Color palette 7 */
#define c_mineCGBPal7c0 15360
#define c_mineCGBPal7c1 495
#define c_mineCGBPal7c2 20083
#define c_mineCGBPal7c3 0
/* CGBpalette entries. */
extern unsigned char c_mineCGB[];
/* Start of tile array. */
extern unsigned char c_mine[];

/* End of C_MINE.H */
